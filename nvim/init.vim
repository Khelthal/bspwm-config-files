call plug#begin('~/.vim/plugged')
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'airblade/vim-rooter'
Plug 'tpope/vim-commentary'
Plug 'mhinz/vim-signify'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'sonph/onehalf', { 'rtp': 'vim' }
Plug 'arcticicestudio/nord-vim'
Plug 'preservim/nerdtree'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'jiangmiao/auto-pairs'
Plug 'ryanoasis/vim-devicons'
Plug 'bagrat/vim-buffet'
Plug 'mhinz/vim-startify'
Plug 'uiiaoo/java-syntax.vim'
Plug 'https://github.com/zah/nim.vim.git'
Plug 'habamax/vim-godot'
Plug 'https://github.com/bfrg/vim-cpp-modern.git'
Plug 'udalov/kotlin-vim'
Plug 'mhinz/vim-signify'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'junegunn/gv.vim'
call plug#end()

" Config
set completeopt=menuone,noinsert,noselect
set number
set nowrap
set tabstop=2
set shiftwidth=2
"set expandtab
set encoding=UTF-8
set t_Co=256
set t_ut=
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif
colorscheme nord
"hi! EndOfBuffer ctermbg=bg ctermfg=bg guibg=bg guifg=bg
set hlsearch!
nnoremap <F3> :set hlsearch!<CR>
inoremap <C-s> <Esc>:w<CR>a
inoremap <C-Up> <Esc>ddko
nnoremap <C-s> :w<CR>
nnoremap <C-e> :bd<CR>
nnoremap <C-q> :wq<CR>
nnoremap <C-Left> <C-W><Left>
nnoremap <C-Right> <C-W><Right>
nnoremap <C-Up> <C-W><Up>
nnoremap <C-Down> <C-W><Down>

" NERDTree
nnoremap <tab> :NERDTreeToggle<CR>

" Buffet
function! g:BuffetSetCustomColors()
  hi! BuffetCurrentBuffer ctermbg=255 ctermfg=255 guibg=255 guifg=255
  hi! BuffetBuffer ctermbg=NONE guibg=#1F2228 guifg=#909090
  hi! BuffetActiveBuffer ctermbg=NONE guibg=255 guifg=255
  hi! BuffetModCurrentBuffer ctermbg=255 ctermfg=255 guibg=255 guifg=255
  hi! BuffetModeActiveBuffer ctermbg=255 ctermfg=255 guibg=255 guifg=255
  hi! BuffetModBuffer ctermbg=255 ctermfg=255 guibg=255 guifg=255
  hi! BuffetTrunc ctermbg=255 ctermfg=255 guibg=255 guifg=255
  hi! BuffetTab ctermbg=255 ctermfg=255 guibg=255 guifg=bg
endfunction
let g:buffet_separator = ''
let g:buffet_show_index = 0
let g:buffet_max_plug = 10
let g:buffet_modified_icon = ' '
noremap <S-Right> :bn<CR>
noremap <S-Left> :bp<CR>

" Ejecutar
nnoremap <F5> :call ExecuteFile("")<CR>
nnoremap <F6> :call ExecuteFile("input")<CR>

function ExecuteFile(input)
  let filename = expand("%:t:r")
  let fileexte = expand("%:t:e")
  let filepath = expand("%:p:h")

  let comando = ""

  if fileexte == "py"
    let comando = ":term "."python ".filepath."/".filename.".".fileexte
  elseif fileexte == "java"
    let proyecto = 0
    for part in split(filepath, "/")
      if part == "src"
        let proyecto = 1
        break
      endif
    endfor
    if proyecto
      let ruta = "/"
      let paquete = ""
      let encontrado = 0
      for part in split(filepath, "/")
        if part == "src"
          let encontrado = 1
          continue
        endif
        if encontrado
          let paquete = paquete.part."/"
        else
          let ruta = ruta.part."/"
        endif
      endfor
      let comando = ":term javac -d ".ruta."out/production/ed_2021_1/ --source-path ".ruta."src"." ".ruta."src/".paquete.filename.".".fileexte." && java -cp ".ruta."out/production/ed_2021_1/ ".paquete.filename
    else
      let comando = ":term "."javac ".filepath."/".filename.".".fileexte." && java -cp ".filepath."/ ".filename
    endif
  elseif fileexte == "cs"
    let comando = ":term "."dotnet run --project ".filepath
  elseif fileexte == "cp"
    let comando = ":term g++ ".filepath."/".filename.".".fileexte." -o ".filepath."/".filename.".out"." && ".filepath."/".filename.".out"
  elseif fileexte == "cpp"
    let comando = ":term g++ ".filepath."/".filename.".".fileexte." -o ".filepath."/".filename.".out"." && ".filepath."/".filename.".out"
  elseif fileexte == "cc"
    let comando = ":term g++ ".filepath."/".filename.".".fileexte." -o ".filepath."/".filename.".out"." && ".filepath."/".filename.".out"
  elseif fileexte == "nim"
    let comando = ":term nim compile --run ".filepath."/".filename.".".fileexte
  endif
  if a:input != "" 
    let comando = comando." < ~/Documentos/competitiva/input.txt"
  endif
  exec comando
endfunction

" Startify
let g:startify_lists = [
          \ { 'type': 'files',     'header': ['   Files']            },
          \ { 'type': 'dir',       'header': ['   Current Directory '. getcwd()] },
          \ { 'type': 'sessions',  'header': ['   Sessions']       },
          \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
          \ ]

let g:startify_bookmarks = [
            \ { 'c': '~/.config/bspwm/bspwmrc' },
            \ { 'i': '~/.config/nvim/init.vim' },
            \ { 'r': '~/Documentos/competitiva/input.txt' },
            \ ]

let g:startify_custom_header = [
			\' _______                        .__           ',
			\' \      \    ____   ____ ___  __|__|  _____   ',
			\' /   |   \ _/ __ \ /  _ \\  \/ /|  | /     \  ',
			\'/    |    \\  ___/(  <_> )\   / |  ||  Y Y  \ ',
			\'\____|__  / \___  >\____/  \_/  |__||__|_|  / ',
			\'        \/      \/                        \/  ',
			\]

" Coc
" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
inoremap <expr> <CR> complete_info().selected != -1 ?
            \ &filetype == "gdscript" ? (coc#expandable() ?  "\<C-y>" : "\<C-y><Esc>a") : "\<C-y>"
            \ : "\<C-g>u\<CR>"

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

" Signify
let g:signify_sign_add               = '+'
let g:signify_sign_delete            = '_'
let g:signify_sign_delete_first_line = '‾'
let g:signify_sign_change            = '~'

let g:signify_sign_show_count = 0
let g:signify_sign_show_text = 1

" Fzf
nnoremap <C-f> :Files <CR>
let g:fzf_layout = {'up':'~90%', 'window': { 'width': 0.8, 'height': 0.8,'yoffset':0.5,'xoffset': 0.5, 'highlight': 'Todo', 'border': 'sharp' } }

let $FZF_DEFAULT_OPTS = '--layout=reverse --info=inline'

" Customize fzf colors to match your color scheme
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }
