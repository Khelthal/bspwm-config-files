from typing import Union

class Nodo:
    def __init__(self, data : object) -> None:
        self._data : object = data
        self._hijos : list[Nodo] = []

    def data(self) -> object:
        return self._data

    def agregar_hijo(self, data : object) -> None:
        nodo = Nodo(data)
        self._hijos.append(nodo)

    def buscar_hijo(self, data : object) -> Union['Nodo', None]:
        for hijo in self._hijos:
            if hijo.data() == data:
                return hijo
        return None
    
    def imprimir(self, tabs : int):
        print(tabs*"   ", end="")
        print(self)
        for hijo in self._hijos:
            hijo.imprimir(tabs+1)

    def escribir(self, tabs : int):
        with open("arbol_contenido.csv", "a") as arbol:
            arbol.write(tabs*"," + str(self) + "\n")
        for hijo in self._hijos:
            hijo.escribir(tabs+1)

    def __str__(self) -> str:
        return str(self._data)
