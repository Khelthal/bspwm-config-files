from typing import Union
from nodo import Nodo

class ArbolContenido():
    def __init__(self) -> None:
        self.raiz : Nodo = Nodo("/")

    def agregar_url(self, url : list[str]) -> None:
        padre : Union[Nodo, None] = self.raiz
        while len(url) != 0:
            carpeta : str = url.pop(0)
            hijo = padre.buscar_hijo(carpeta)
            if hijo == None:
                padre.agregar_hijo(carpeta)
                hijo = padre.buscar_hijo(carpeta)
            padre = hijo

    def imprimir(self) -> None:
        self.raiz.imprimir(0)

    def escribir(self) -> None:
        with open("arbol_contenido.csv", "w") as arbol:
            arbol.write("")
        self.raiz.escribir(0)
