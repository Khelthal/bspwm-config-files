from arbol_contenido import ArbolContenido
from html.parser import HTMLParser
import urllib.request

global ligas
global url
ligas : list[str] = []

class LinkScrape(HTMLParser):
    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            for attr in attrs:
                if attr[0] == 'href':
                    link = attr[1]
                    if link[0] == "/":
                        ligas.append(link)


if __name__ == '__main__':
    url = input('Enter URL > ')
    request_object = urllib.request.Request(url)
    page_object = urllib.request.urlopen(request_object)
    link_parser = LinkScrape()
    link_parser.feed(page_object.read().decode('utf-8'))

    arbol_contenido : ArbolContenido = ArbolContenido()
    ligas.sort()

    for liga in ligas:
        lista : list[str] = liga.split("/")
        if lista[0] == "":
            lista.pop(0)
        if lista[-1] == "":
            lista.pop(-1)
        arbol_contenido.agregar_url(lista)
    arbol_contenido.escribir()
