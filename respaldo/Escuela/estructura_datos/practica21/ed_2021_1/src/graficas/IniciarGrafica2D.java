package graficas;

import calculos.correlacion.Correlacion;
import edlineal.ListaLigada;
import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;
import entradasalida.archivos.ArchivoTexto;

/**
 * Esta clase se encarga de iniciar una Grafica2D.
 */
public class IniciarGrafica2D {
  public static void main(String[] args) {
    SalidaEstandar.consola("Ingrese la ruta del archivo con los valores de X\n:");
    String fileX = EntradaConsola.consolaCadena();
    SalidaEstandar.consola("Ingrese la ruta del archivo con los valores de Y\n:");
    String fileY = EntradaConsola.consolaCadena();
    ListaLigada listaX = ArchivoTexto.leerNumerosListaLigada(fileX);
    ListaLigada listaY = ArchivoTexto.leerNumerosListaLigada(fileY);
    double coeficientePearsonMuestral = Correlacion.calcularCoeficientePearsonMuestral(listaX, listaY);
    double coeficientePearsonPoblacional = Correlacion.calcularCoeficientePearsonPoblacional(listaX, listaY);

    SalidaEstandar.consola("Ingrese el titulo de la grafica\n:");
    String titulo = EntradaConsola.consolaCadena();

    SalidaEstandar.consola("Ingrese el titulo del eje X de la grafica\n:");
    String ejeX = EntradaConsola.consolaCadena();

    SalidaEstandar.consola("Ingrese el titulo del eje Y de la grafica\n:");
    String ejeY = EntradaConsola.consolaCadena();

    SalidaEstandar.consola("Ingrese el nombre de la relacion entre el eje X y el eje Y\n:");
    String key = EntradaConsola.consolaCadena();

    Grafica2D grafica2D = new Grafica2D(listaX, listaY);
    grafica2D.init(key, titulo, ejeX, ejeY);
  }
}
