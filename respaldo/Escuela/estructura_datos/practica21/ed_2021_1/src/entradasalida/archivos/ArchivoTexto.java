package entradasalida.archivos;

import edlineal.Arreglo;
import edlineal.ListaDobleLigada;
import edlineal.ListaLigada;

import java.io.*;

/**
 * Esta clase se encarga de leer archivos de texto.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class ArchivoTexto {

    /**
     * Lee numeros de un archivo de texto y los almacena en un Arreglo.
     * @param archivo Es el nombre del archivo de texto.
     * @param tamanioArchivo Es el numero de lineas del archivo de texto.
     * @return Regresa un arreglo con los numeros del archivo de texto.
     */
    public static Arreglo leerNumeros(String archivo, int tamanioArchivo){
        FileReader input=null;
        int registro=0;

        Arreglo datos=new Arreglo(tamanioArchivo);
        try {
            String cadena=null;
            input = new FileReader(archivo);
            BufferedReader buffer = new BufferedReader(input);
            while((cadena = buffer.readLine())!=null) {
                datos.agregar(Double.parseDouble(cadena));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try{
                input.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        return datos;
    }

    /**
     * Lee un archivo de texto y guarda cada linea en un Arreglo.
     * @param archivo Es el nombre del archivo de texto.
     * @param tamanioArchivo Es el numero de lineas del archivo de texto.
     * @return Regresa un arreglo con las lineas del archivo de texto.
     */
    public static Arreglo leer(String archivo, int tamanioArchivo){
        FileReader input=null;
        int registro=0;

        Arreglo datos=new Arreglo(tamanioArchivo);
        try {
            String cadena=null;
            input = new FileReader(archivo);
            BufferedReader buffer = new BufferedReader(input);
            while((cadena = buffer.readLine())!=null) {
                datos.agregar(cadena);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try{
                input.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        return datos;
    }

    /**
     * Lee un archivo de texto y guarda cada linea en una ListaDobleLigada.
     * @param archivo Es el nombre del archivo de texto.
     * @return Regresa un arreglo con las lineas del archivo de texto.
     */
    public static ListaDobleLigada leerNumerosListaDobleLigada(String archivo){
        FileReader input=null;

        ListaDobleLigada datos=new ListaDobleLigada();
        try {
            String cadena=null;
            input = new FileReader(archivo);
            BufferedReader buffer = new BufferedReader(input);
            while((cadena = buffer.readLine())!=null) {
                datos.agregar(Double.parseDouble(cadena));
            }
            buffer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try{
                input.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        return datos;
    }

    /**
     * Lee un archivo de texto y guarda cada linea en una ListaLigada.
     * @param archivo Es el nombre del archivo de texto.
     * @return Regresa un arreglo con las lineas del archivo de texto.
     */
    public static ListaLigada leerNumerosListaLigada(String archivo){
        FileReader input=null;

        ListaLigada datos=new ListaLigada();
        try {
            String cadena=null;
            input = new FileReader(archivo);
            BufferedReader buffer = new BufferedReader(input);
            while((cadena = buffer.readLine())!=null) {
                datos.agregar(Double.parseDouble(cadena));
            }
            buffer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try{
                input.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        return datos;
    }
}
