package edlineal.catalogo;

public class Nodo {
    protected Object info;
    protected Nodo ligaDer;

    public Nodo(Object info){
        this.info=info;
        ligaDer=null;
    }

    public Nodo(Object info, Nodo ligaDer){
        this.info=info;
        this.ligaDer=ligaDer;
    }

    public Object getInfo() {
        return info;
    }

    public Nodo getLigaDer() {
        return ligaDer;
    }

    public void setInfo(Object info) {
        this.info = info;
    }

    public void setLigaDer(Nodo ligaDer) {
        this.ligaDer = ligaDer;
    }

    @Override
    public String toString(){
        return info.toString();
    }
}
