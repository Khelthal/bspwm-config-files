package edlineal;

import comparadores.ComparadorObject;
import edlineal.catalogo.NodoHash;
import ednolineal.Matriz2D;
import entradasalida.SalidaEstandar;

/**
 * Esta clase almacena elementos de forma dinamica.
 */
public class ListaLigadaHash {
  protected NodoHash primero;
  protected NodoHash ultimo;
  protected NodoHash iterador;

  /**
   * Crea una nueva instancia de ListaLigadaHash.
   */
  public ListaLigadaHash() {
    primero = null;
    ultimo = null;
  }
  
  /**
   * Inserta una pareja clave : valor al final de la lista.
   * @param clave Es la llave del elemento.
   * @param valor Es el elemento agregado.
   * @return Regresa <b>true</b> si se pudo agregar el elemento, <b>false</b> en caso contrario.
   */
  public boolean insertar(Object clave, Object valor) {
    if (vacia()) {
      NodoHash nuevoNodo = new NodoHash(clave, valor);
      if (nuevoNodo == null) {
        return false;
      }
      primero = nuevoNodo;
      ultimo = nuevoNodo;
    } else {
      if (buscar(clave) != null) {
        return false;
      }
      NodoHash nuevoNodo = new NodoHash(clave, valor);
      ultimo.setLigaDer(nuevoNodo);
      ultimo = nuevoNodo;
    }
    return true;
  }

  /**
   * Elimina un elemento que coincida con la clave pasada como argumento.
   * @param clave Es la clave del elemento a eliminar.
   * @return Regresa el valor del elemento si lo encontro, null en caso contrario.
   */
  public Object eliminar(Object clave) {
    Object elementoBorrado = null;
    if (vacia() == false) {
      NodoHash anterior = primero;
      NodoHash encontrado = primero;
      while (encontrado != null && ComparadorObject.compare(encontrado.getClave(), clave) != 0) {
        anterior = encontrado;
        encontrado = encontrado.getLigaDer();
      }
      if (encontrado == null) {
        return null;
      } else {
        elementoBorrado = encontrado.getValor();
        if (primero == ultimo) {
          primero = null;
          ultimo = null;
        } else if (primero == encontrado) {
          primero = primero.getLigaDer();
        } else if (ultimo == encontrado) {
          anterior.setLigaDer(null);
          ultimo = anterior;
        } else {
          NodoHash siguiente = encontrado.getLigaDer();
          anterior.setLigaDer(siguiente);
        }
        return elementoBorrado;
      }
    } else {
      return null;
    }
  }
  
  /**
   * Elimina un elemento que coincida con el valor pasado como argumento.
   * @param valor Es el valor a eliminar.
   * @return Regresa el valor del elemento si lo encontro, null en caso contrario.
   */
  public Object eliminarValor(Object valor) {
    Object elementoBorrado = null;
    if (vacia() == false) {
      NodoHash anterior = primero;
      NodoHash encontrado = primero;
      while (encontrado != null && ComparadorObject.compare(encontrado.getValor(), valor) != 0) {
        anterior = encontrado;
        encontrado = encontrado.getLigaDer();
      }
      if (encontrado == null) {
        return null;
      } else {
        elementoBorrado = encontrado.getValor();
        if (primero == ultimo) {
          primero = null;
          ultimo = null;
        } else if (primero == encontrado) {
          primero = primero.getLigaDer();
        } else if (ultimo == encontrado) {
          anterior.setLigaDer(null);
          ultimo = anterior;
        } else {
          NodoHash siguiente = encontrado.getLigaDer();
          anterior.setLigaDer(siguiente);
        }
        return elementoBorrado;
      }
    } else {
      return null;
    }
  }
  
  /**
   * Permite buscar un elemento por su clave.
   * @param clave Es la clave buscada.
   * @return Regresa el valor contenido en la clave encontrada, null en caso contrario.
   */
  public Object buscar(Object clave) {
    NodoHash recorrer = primero;

    while (recorrer != null && ComparadorObject.compare(recorrer.getClave(), clave) != 0) {
      recorrer = recorrer.getLigaDer();
    }

    if (recorrer == null) {
      return null;
    } else {
      return recorrer.getValor();
    }
  }

  /**
   * Permite buscar un elemento por su valor.
   * @param valor Es el valor del elemento buscado.
   * @return Regresa el valor si pudo encontrarlo, null en caso contrario.
   */
  public Object buscarValor(Object valor) {
    NodoHash recorrer = primero;

    while (recorrer != null && ComparadorObject.compare(recorrer.getValor(), valor) != 0) {
      recorrer = recorrer.getLigaDer();
    }

    if (recorrer == null) {
      return null;
    } else {
      return recorrer.getValor();
    }
  }

  /**
   * Reemplaza el valor de la clave solicitada por un nuevo valor.
   * @param clave Es la clave de la cual se cambiara el elemento.
   * @param valorNuevo Es el nuevo valor.
   * @return Regresa <b>true</b> si se pudo substituir el valor, <b>false</b> en caso contrario.
   */
  public boolean substituir(Object clave, Object valorNuevo) {
    NodoHash recorrer = primero;

    while (recorrer != null && ComparadorObject.compare(recorrer.getClave(), clave) != 0) {
      recorrer = recorrer.getLigaDer();
    }

    if (recorrer != null) {
      recorrer.setValor(valorNuevo);
      return true;
    }
    return false;
  }

  /**
   * Cambia un valor que conicida con el valor pasado como argumento por un nuevo valor.
   * @param valor Es el valor a reemplazar.
   * @param valorNuevo Es el nuevo valor.
   * @return Regresa <b>true</b> si se pudo substituir el valor, <b>false</b> en caso contrario.
   */
  public boolean substituirValor(Object valor, Object valorNuevo) {
    NodoHash recorrer = primero;

    while (recorrer != null && ComparadorObject.compare(recorrer.getValor(), valor) != 0) {
      recorrer = recorrer.getLigaDer();
    }

    if (recorrer != null) {
      recorrer.setValor(valorNuevo);
      return true;
    }
    return false;
  }

  /**
   * Imprime las parejas clave : valor de la lista.
   * */
  public void imprimir() {
    NodoHash recorrer = primero;

    while (recorrer != null) {
      SalidaEstandar.consola(recorrer.getClave() + " : " + recorrer.getValor() + " -> ");
      recorrer = recorrer.getLigaDer();
    }
    SalidaEstandar.consola("null");
  }

  /**
   * Imprime las claves de la lista.
   * */
  public void imprimirClaves() {
    NodoHash recorrer = primero;

    while (recorrer != null) {
      SalidaEstandar.consola(recorrer.getClave() + " -> ");
      recorrer = recorrer.getLigaDer();
    }
    SalidaEstandar.consola("null");
  }

  /**
   * Imprime los valores de la lista.
   * */
  public void imprimirValores() {
    NodoHash recorrer = primero;

    while (recorrer != null) {
      SalidaEstandar.consola(recorrer.getValor() + " -> ");
      recorrer = recorrer.getLigaDer();
    }
    SalidaEstandar.consola("null");
  }

  /**
   * Obtiene una lista ligada que contiene dos arreglos, uno que coniene las claves de la lista actual
   * y otro que contiene los valores de la lista actual
   * @return Regresa una lista ligada con los arreglos de claves y valores.
   */
  public ListaLigada aArreglos() {
    int size = longitud();
    Arreglo claves = new Arreglo(size);
    Arreglo valores = new Arreglo(size);
    ListaLigada arreglos = new ListaLigada();
    arreglos.agregar(claves);
    arreglos.agregar(valores);

    NodoHash recorrer = primero;

    while (recorrer != null) {
      claves.agregar(recorrer.getClave());
      valores.agregar(recorrer.getValor());
      recorrer = recorrer.getLigaDer();
    }

    return arreglos;
  }

  /**
   * Obtiene una lista ligada que contiene dos listas ligadas, una que coniene las claves de la lista actual
   * y otra que contiene los valores de la lista actual
   * @return Regresa una lista ligada con las listas de claves y valores.
   */
  public ListaLigada aListas() {
    ListaLigada claves = new ListaLigada();
    ListaLigada valores = new ListaLigada();
    ListaLigada arreglos = new ListaLigada();
    arreglos.agregar(claves);
    arreglos.agregar(valores);

    NodoHash recorrer = primero;

    while (recorrer != null) {
      claves.agregar(recorrer.getClave());
      valores.agregar(recorrer.getValor());
      recorrer = recorrer.getLigaDer();
    }

    return arreglos;
  }

  /**
   * Obtiene una matriz 2D con las parejas clave : valor de la lista en cada renglon.
   * @return Regresa la matriz 2D obtenida.
   */
  public Matriz2D aMatriz() {
    Matriz2D diccionario = new Matriz2D(longitud(), 2);
    int i = 0;

    NodoHash recorrer = primero;
    while (recorrer != null) {
      diccionario.cambiarInfo(i, 0, recorrer.getClave());
      diccionario.cambiarInfo(i, 1, recorrer.getValor());
      recorrer = recorrer.getLigaDer();
      i++;
    }
    
    return diccionario;
  }

  /**
   * Elimina todos los elementos de la lista.
   * */
  public void vaciar() {
    primero = null;
    ultimo = null;
  }

  /**
   * Obtiene la clave pasada como argumento.
   * @param clave Es la clave buscada.
   * @return Regresa la clave si la encontro, null en caso contrario.
   */
  public Object obtener(Object clave) {
    NodoHash recorrer = primero;

    while (recorrer != null && ComparadorObject.compare(recorrer.getClave(), clave) != 0) {
      recorrer = recorrer.getLigaDer();
    }

    if (recorrer == null) {
      return null;
    } else {
      return recorrer.getClave();
    }
  }

  /**
   * Determina si la lista esta vacia.
   * @return Regresa <b>true</b> si la lista esta vacia, <b>false</b> en caso contrario.
   */
  public boolean vacia() {
    return primero == null;
  }

  /**
   * Agrega los elementos de la ListaLigadaHash pasada como argumento a la lista actual.
   * @param lista2 Es la lista de la cual se agregaran elementos.
   * @return Regresa <b>true</b> si se pudieron agregar todos los elementos de la lista, <b>false</b> en caso contrario.
   */
  public boolean agregarTodos(ListaLigadaHash lista2) {
    return agregarMatriz(lista2.aMatriz());
  }

  /**
   * Obtiene la cantidad de elementos en la lista.
   * @return Regresa la cantidad de elementos en la lista.
   */
  public int longitud() {
    int contador = 0;
    NodoHash recorrer = primero;

    while (recorrer != null) {
      contador++;
      recorrer = recorrer.getLigaDer();
    }

    return contador;
  }

  /**
   * Recibe 2 arreglos, uno con claves y otro con valores, y agrega parejas de clave : valor a la lista actual.
   * @param arregloClaves Es el arreglo que contiene las claves.
   * @param arregloValores Es el arreglo que contiene los valores.
   * @return Regresa <b>true</b> si todos los elementos de los arreglo pudieron ser agregados, <b>false</b> en caso contrario.
   */
  public boolean agregarArreglos(Arreglo arregloClaves, Arreglo arregloValores) {
    if (arregloClaves.numElementos() != arregloValores.numElementos()) {
      return false;
    }
    boolean todos = true;

    for (int i = 0; i < arregloClaves.numElementos(); i++) {
      if (!insertar(arregloClaves.obtener(i), arregloValores.obtener(i))) {
        todos = false;
      }
    }

    return todos;
  }

  /**
   * Recibe 2 listas, una con claves y otra con valores, y agrega parejas de clave : valor a la lista actual.
   * @param listaClaves Es la lista que contiene las claves.
   * @param listaValores Es la lista que contiene los valores.
   * @return Regresa <b>true</b> si todos los elementos de las listas pudieron ser agregados, <b>false</b> en caso contrario.
   */
  public boolean agregarListas(ListaLigada listaClaves, ListaLigada listaValores) {
    if (listaClaves.vacia() || listaValores.vacia()) {
      return false;
    }
    boolean todos = true;
    int i = 0;
    Object clave;
    Object valor;
    while ((clave = listaClaves.obtener(i)) != null && (valor = listaValores.obtener(i)) != null) {
      if (!insertar(clave, valor)) {
        todos = false;
      }
      i++;
    }
    return todos;
  }

  /**
   * Agrega las parejas clave : valor contenidas en la matriz a la lista.
   * @param matriz Es la matriz que contiene las parejas clave : valor.
   * @return Regresa <b>true</b> si todos los elementos de la matriz pudieron ser agregados, <b>false</b> en caso contrario.
   */
  public boolean agregarMatriz(Matriz2D matriz) {
    if (matriz.columnas() != 2 || matriz.renglones() == 0) {
      return false;
    }
    boolean todos = true;

    for (int i = 0; i < matriz.renglones(); i++) {
      if (!insertar(matriz.obtenerInfo(i, 0), matriz.obtenerInfo(i, 1))) {
        todos = false;
      }
    }

    return todos;
  }
}
