package edlineal;

/**
 * Esta clase maneja pilas y distintas operaciones con pilas.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class PilaArreglo implements Lote {
    protected Arreglo pila;

    /**
     * Crea una nueva pila con el tamanio indicado.
     * @param capacidad Es el tamanio de la pila.
     */
    public PilaArreglo(int capacidad){
        pila=new Arreglo(capacidad);
    }

    @Override
    public boolean vacio(){
        return pila.vacia();
    }

    @Override
    public boolean lleno(){
        return pila.lleno();
    }

    @Override
    public boolean poner(Object info){
        int retorno=pila.agregar(info);
        if(retorno==-1){
            return false;
        }else{
            return true;
        }
    }

    @Override
    public Object quitar(){
        return pila.eliminar();
    }

    @Override
    public void imprimir(){
        pila.imprimir();
    }

    @Override
    public Object verLimite(){
        if (pila.vacia()) {
            return null;
        }
        else {
            return pila.obtener(pila.numElementos()-1);
        }
    }
}
