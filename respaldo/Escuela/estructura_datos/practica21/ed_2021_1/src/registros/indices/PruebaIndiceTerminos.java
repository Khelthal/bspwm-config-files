package registros.indices;

/**
 * Esta clase sirve para iniciar MenuIndiceTerminos.
 */
public class PruebaIndiceTerminos {
    public static void main(String[] args) {
        MenuIndiceTerminos menuIndiceTerminos = new MenuIndiceTerminos();
        menuIndiceTerminos.iniciar();
    }
}
