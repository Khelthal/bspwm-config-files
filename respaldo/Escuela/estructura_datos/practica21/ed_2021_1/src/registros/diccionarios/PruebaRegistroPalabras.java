package registros.diccionarios;

/**
 * Esta clase se encarga de iniciar MenuRegistroPalabras.
 */
public class PruebaRegistroPalabras {
    public static void main(String[] args) {
        MenuRegistroPalabras menuRegistroPalabras = new MenuRegistroPalabras();
        menuRegistroPalabras.iniciar();
    }
}
