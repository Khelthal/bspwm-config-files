package registros.pintores;

/**
 * Esta clase se encarga de almacenar informacion de un pintor.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class Pintor {
    protected String nombre;
    protected int edad;
    protected String direccion;

    /**
     * Crea un nuevo pintor con el nombre. edad y direccion seleccionados.
     * @param nombre Es el nombre del pintor.
     * @param edad Es la edad del pintor.
     * @param direccion Es la direccion del pintor.
     */
    public Pintor(String nombre, int edad, String direccion) {
        this.nombre = nombre;
        this.edad = edad;
        this.direccion = direccion;
    }

    /**
     * Obtiene el valor del nombre del pintor.
     * @return Regresa el nombre del pintor.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Obtiene la edad del pintor.
     * @return Regresa la edad del pintor.
     */
    public int getEdad() {
        return edad;
    }

    /**
     * Obtiene la direccion del pintor.
     * @return Regresa la direccion del pintor.
     */
    public String getDireccion() {
        return direccion;
    }

    @Override
    public String toString() {
        return nombre;
    }

}
