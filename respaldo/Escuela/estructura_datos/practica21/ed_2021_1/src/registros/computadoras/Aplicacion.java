package registros.computadoras;

/**
 * Esta clase guarda los datos de una aplicacion.
 */
public class Aplicacion {
  protected String nombre;
  protected String urlLogo;
  protected String autores;
  protected String version;
  protected float ramMin;

  /**
   * Crea una nueva instancia de Aplicacion.
   * @param nombre Es el nombre de la aplicacion.
   * @param urlLogo Es la URL del logo de la aplicacion.
   * @param autores Son los creadores de la aplicacion.
   * @param version Es la version actual de la aplicacion.
   * @param ramMin Es la ram minima que requiere la aplicacion.
   */
  public Aplicacion(String nombre, String urlLogo, String autores, String version, float ramMin) {    
    this.nombre = nombre;
    this.urlLogo = urlLogo;
    this.autores = autores;
    this.version = version;
    this.ramMin = ramMin;
  }

  @Override
  public String toString() {
    return nombre;
  }

  /**
   * Obtiene la ram minima para usar la aplicacion.
   * @return Regresa la ram minima para usar la aplicacion.
   */
  public float getRamMin() {
    return ramMin;
  }
}
