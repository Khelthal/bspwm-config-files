package registros.computadoras;

import utilerias.fecha.Fecha;

/**
 * Esta clase se encarga de iniciar MenuRegistroCentroComputo.
 */
public class PruebaRegistroCentroComputo {
  public static void main(String[] args) {
    CentroComputo c1 = new CentroComputo("Centro del norte", 12);
    CentroComputo c2 = new CentroComputo("Centro del sur", 15);
    CentroComputo c3 = new CentroComputo("Centro del este", 9);

    Aplicacion a1 = new Aplicacion("Firefox", "https://upload.wikimedia.org/wikipedia/commons/d/d2/Firefox_Logo%2C_2017.png", "Blake Ross", "86.0.1", 2);
    Aplicacion a2 = new Aplicacion("Godot", "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/Godot_icon.svg/768px-Godot_icon.svg.png", "Open Source", "3.2.3", 2);
    Aplicacion a3 = new Aplicacion("Neovim", "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn.dribbble.com%2Fusers%2F2008%2Fscreenshots%2F1442436%2Fmark-dribbble.png&f=1&nofb=1", "Open Source", "0.4.4", 1);
    Aplicacion a4 = new Aplicacion("World Of Warcraft", "http://www.pngmart.com/files/10/Warcraft-WOW-Logo-PNG-Photos.png", "Blizzard Entertainment", "9.0.5", 20);
    Aplicacion a5 = new Aplicacion("Dark Souls", "http://img2.wikia.nocookie.net/__cb20131120052851/darksouls/images/archive/d/dd/20131120054736!Dark_Souls_Logo.png", "Bandai Namco", "3", 16);

    Usuario u1 = new Usuario("Elias Emiliano", 20);
    Usuario u2 = new Usuario("Leonardo Francisco", 23);
    Usuario u3 = new Usuario("Ernesto", 28);
    Usuario u4 = new Usuario("Omar", 30);

    Computadora pc1 = new Computadora(c1, 1, 512, "Intel core i5 860", "Gigabyte");
    Computadora pc2 = new Computadora(c1, 8, 2048, "AMD Ryzen 5 3600g", "Toshiba");
    Computadora pc3 = new Computadora(c3, 16, 1024, "Intel core i5 9600K", "HP");

    pc1.agregarAplicacion(a1);
    pc1.agregarAplicacion(a2);

    pc2.agregarAplicacion(a3);
    pc2.agregarAplicacion(a4);

    UsoComputadora uu1 = new UsoComputadora(u1, new Fecha(12, 3, 2021, 19, 38), new Fecha(12, 3, 2021, 21, 38));
    uu1.registarUsoAplicacion(a1);
    uu1.registarUsoAplicacion(a2);
    uu1.registarUsoAplicacion(a4);

    pc1.agregarRegistroUso(uu1);

    UsoComputadora uu2 = new UsoComputadora(u3, new Fecha(13, 3, 2021, 12, 10), new Fecha(13, 3, 2021, 16, 10));
    uu2.registarUsoAplicacion(a4);
    uu2.registarUsoAplicacion(a3);

    pc2.agregarRegistroUso(uu2);

    RegistroCentroComputo registroCentroComputo = new RegistroCentroComputo();
    registroCentroComputo.agregarCentroComputo(c1);
    registroCentroComputo.agregarCentroComputo(c2);
    registroCentroComputo.agregarCentroComputo(c3);

    registroCentroComputo.agregarAplicacion(a1);
    registroCentroComputo.agregarAplicacion(a2);
    registroCentroComputo.agregarAplicacion(a3);
    registroCentroComputo.agregarAplicacion(a4);
    registroCentroComputo.agregarAplicacion(a5);

    registroCentroComputo.agregarUsuario(u1);
    registroCentroComputo.agregarUsuario(u2);
    registroCentroComputo.agregarUsuario(u3);
    registroCentroComputo.agregarUsuario(u4);

    registroCentroComputo.agregarComputadora(pc1);
    registroCentroComputo.agregarComputadora(pc2);
    registroCentroComputo.agregarComputadora(pc3);

    MenuRegistroCentroComputo menuRegistroCentroComputo = new MenuRegistroCentroComputo(registroCentroComputo);
    menuRegistroCentroComputo.iniciar();
  }
}
