package calculos.correlacion;

import edlineal.ListaDobleLigada;
import edlineal.ListaLigada;
import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;
import entradasalida.archivos.ArchivoTexto;

/**
 * Esta clase se encarga de probar la correlacion de Pearson con ListaLigada.
 */
public class PruebaCorrelacionPearsonLL {

  public static void main(String[] args) {
    SalidaEstandar.consola("Ingrese la ruta del archivo con los valores de X\n:");
    String fileX = EntradaConsola.consolaCadena();
    SalidaEstandar.consola("Ingrese la ruta del archivo con los valores de Y\n:");
    String fileY = EntradaConsola.consolaCadena();
    ListaLigada listaX = ArchivoTexto.leerNumerosListaLigada(fileX);
    ListaLigada listaY = ArchivoTexto.leerNumerosListaLigada(fileY);
    double coeficientePearsonMuestral = Correlacion.calcularCoeficientePearsonMuestral(listaX, listaY);
    double coeficientePearsonPoblacional = Correlacion.calcularCoeficientePearsonPoblacional(listaX, listaY);

    SalidaEstandar.consola("Coeficiente de Pearson muestral: " + coeficientePearsonMuestral + "\n");
    SalidaEstandar.consola("Coeficiente de Pearson poblacional: " + coeficientePearsonPoblacional + "\n");

    SalidaEstandar.consola("\n");

    SalidaEstandar.consola("Interpretacion Coeficiente de Pearson muestral\n");
    if (coeficientePearsonMuestral == -1) {
      SalidaEstandar.consola("Correlacion negativa perfecta\n");
    }
    else if (coeficientePearsonMuestral > -1 && coeficientePearsonMuestral < 0) {
      SalidaEstandar.consola("Correlacion negativa\n");
    }
    else if (coeficientePearsonMuestral == 0) {
      SalidaEstandar.consola("Ninguna correlacion\n");
    }
    else if (coeficientePearsonMuestral > 0 && coeficientePearsonMuestral < 1) {
      SalidaEstandar.consola("Correlacion positiva\n");
    }
    else if (coeficientePearsonMuestral == 1) {
      SalidaEstandar.consola("Correlacion positiva perfecta\n");
    }

    SalidaEstandar.consola("\n");

    SalidaEstandar.consola("Interpretacion Coeficiente de Pearson poblacional\n");
    if (coeficientePearsonPoblacional == -1) {
      SalidaEstandar.consola("Correlacion negativa perfecta\n");
    }
    else if (coeficientePearsonPoblacional > -1 && coeficientePearsonPoblacional < 0) {
      SalidaEstandar.consola("Correlacion negativa\n");
    }
    else if (coeficientePearsonPoblacional == 0) {
      SalidaEstandar.consola("Ninguna correlacion\n");
    }
    else if (coeficientePearsonPoblacional > 0 && coeficientePearsonPoblacional < 1) {
      SalidaEstandar.consola("Correlacion positiva\n");
    }
    else if (coeficientePearsonPoblacional == 1) {
      SalidaEstandar.consola("Correlacion positiva perfecta\n");
    }
  }
}
