package interfaces;

import edlineal.Arreglo;
import edlineal.ListaLigada;
import edlineal.ListaLigadaHash;
import ednolineal.Matriz2D;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de hacer pruebas con la clase ListaLigadaHash
 */
public class PruebaListaLigadaHash {

  public static void main(String[] args) {
    ListaLigadaHash lista = new ListaLigadaHash();
    lista.insertar('a', 97);
    lista.insertar('c', 99);
    lista.insertar('e', 101);
    lista.insertar('f', 102);
    SalidaEstandar.consola("Imprimiendo lista en parejas clave : valor\n");
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Imprimiendo las claves de la lista\n");
    lista.imprimirClaves();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Imprimiendo los valores de la lista\n");
    lista.imprimirValores();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Convirtiendo lista a matriz\n");
    Matriz2D listaMatriz = lista.aMatriz();
    listaMatriz.imprimirR();
    SalidaEstandar.consola("Convirtiendo lista a arreglos\n");
    ListaLigada contenedor = lista.aArreglos();
    Arreglo claves = (Arreglo) contenedor.obtener(0);
    Arreglo valores = (Arreglo) contenedor.obtener(1);
    SalidaEstandar.consola("Claves\n");
    claves.imprimirOI();
    SalidaEstandar.consola("Valores\n");
    valores.imprimirOI();
    SalidaEstandar.consola("Convirtiendo lista a listas\n");
    contenedor = lista.aListas();
    ListaLigada listaClaves = (ListaLigada) contenedor.obtener(0);
    ListaLigada listaValores = (ListaLigada) contenedor.obtener(1);
    SalidaEstandar.consola("Claves\n");
    listaClaves.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Valores\n");
    listaValores.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Insertando pareja 'b' : 97 : " + lista.insertar('b', 97) + "\n");
    SalidaEstandar.consola("Insertando pareja 'a' : 200 : " + lista.insertar('a', 200) + "\n");
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Buscando el valor de la llave 'c': " + lista.buscar('c') + "\n");
    SalidaEstandar.consola("Buscando el valor 200: " + lista.buscarValor(200) + "\n");
    SalidaEstandar.consola("\n");

    SalidaEstandar.consola("Vaciando lista\n");
    lista.vaciar();
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Agregando matriz a la lista\n");
    lista.agregarMatriz(listaMatriz);
    lista.imprimir();
    SalidaEstandar.consola("\n");

    SalidaEstandar.consola("Vaciando lista\n");
    lista.vaciar();
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Agregando arreglos a la lista\n");
    lista.agregarArreglos(claves, valores);
    lista.imprimir();
    SalidaEstandar.consola("\n");

    SalidaEstandar.consola("Vaciando lista\n");
    lista.vaciar();
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Agregando listas a la lista\n");
    lista.agregarListas(listaClaves, listaValores);
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Creando nueva lista\n");
    ListaLigadaHash nuevaLista = new ListaLigadaHash();
    SalidaEstandar.consola("Agregando lista original a la nueva lista\n");
    nuevaLista.agregarTodos(lista);
    nuevaLista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Eliminando llave 'e' de la nueva lista" + "\n");
    nuevaLista.eliminar('e');
    nuevaLista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Eliminando campo con el valor 99 de la nueva lista" + "\n");
    nuevaLista.eliminarValor(99);
    nuevaLista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Cambiando el elemento de la llave 'a' por el valor -25\n");
    nuevaLista.substituir('a', -25);
    nuevaLista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Cambiando el valor 102 por el valor 3.14\n");
    nuevaLista.substituirValor(102, 3.14);
    nuevaLista.imprimir();
    SalidaEstandar.consola("\n");
  }
}
