package interfaces;

import edlineal.Monticulo;
import edlineal.TipoOrden;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de hacer pruebas con la clase monticulo.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class PruebaMonticulo {
    public static void main(String[] args) {
        Monticulo monticulo = new Monticulo(6, TipoOrden.CRECIENTE);
        SalidaEstandar.consola("Insertando\n");
        monticulo.agregar(20);
        SalidaEstandar.consola("Insertando\n");
        monticulo.agregar(18);
        SalidaEstandar.consola("Insertando\n");
        monticulo.agregar(9);
        SalidaEstandar.consola("Insertando\n");
        monticulo.agregar(8);
        SalidaEstandar.consola("Insertando\n");
        monticulo.agregar(10);
        SalidaEstandar.consola("Insertando\n");
        monticulo.agregar(12);
        SalidaEstandar.consola("\nImprimiendo\n");
        monticulo.imprimirOI();
        while (!monticulo.vacio()) {
            monticulo.eliminar();
            SalidaEstandar.consola("\nBorrando\n");
            monticulo.imprimirOI();
        }
    }
}
