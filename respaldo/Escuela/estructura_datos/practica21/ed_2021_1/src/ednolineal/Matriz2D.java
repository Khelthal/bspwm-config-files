package ednolineal;

import comparadores.ComparadorObject;
import edlineal.Arreglo;
import entradasalida.SalidaEstandar;

/**
 * Esta clase maneja arreglos de 2 dimensiones y distintas operaciones con arreglos de 2 dimensiones.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class Matriz2D {
    protected int renglones;
    protected int columnas;
    protected Object datos[][];

    /**
     * Crea una nueva instancia Matriz2D con el numero de renglones y columnas indicadas.
     * @param reng Es el numero de renglones.
     * @param colum Es el numero de columnas.
     */
    public Matriz2D(int reng, int colum) {
        renglones = reng;
        columnas = colum;
        datos = new Object[renglones][columnas];
    }

    /**
     * Crea una nueva instancia Matriz2D con el numero de renglones y columnas indicadas e inicializa los valores con info.
     * @param reng Es el numero de renglones.
     * @param colum Es el numero de columnas.
     * @param info Es el dato que inicializara las posiciones de la matriz.
     */
    public Matriz2D(int reng, int colum, Object info) {
        renglones = reng;
        columnas = colum;
        datos = new Object[renglones][columnas];
        rellenar(info);
    }

    /**
     * Obtiene el numero de renglones de la matriz.
     * @return Regresa el numero de renglones de la matriz.
     */
    public int renglones() {
        return renglones;
    }

    /**
     * Obtiene el numero de columnas de la matriz.
     * @return Regresa el numero de columnas de la matriz.
     */
    public int columnas() {
        return columnas;
    }

    /**
     * Cambia el elemento guardado en la posicion indicada por un nuevo objeto.
     * @param renglon Es el renglon donde se cambiara el dato.
     * @param columna Es la columna donde se cambiara el dato.
     * @param info Es el nuevo dato.
     * @return Regresa <b>true</b> si se pudo hacer el cambio, <b>false</b> en caso contrario.
     */
    public boolean cambiarInfo(int renglon, int columna, Object info){
        if(validarDimension(renglon, renglones) && validarDimension(columna, columnas)){
            datos[renglon][columna]=info;
            return true;
        }else{
            return false;
        }
    }

    /**
     * Determina si la matriz esta vacia.
     * @return Regresa <b>true</b> si todos los elementos de la matriz son <b>null</b>, <b>false</b> en caso contrario.
     */
    public boolean vacia() {
        for (int renglon = 0; renglon < renglones; renglon++) {
            for (int columna = 0; columna < columnas; columna++) {
                if (datos[renglon][columna] != null) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Obtiene el elemento en la posicion indicada.
     * @param renglon Es el renglon donde se encuentra el elemento.
     * @param columna Es la columna donde se encuentra el elemento.
     * @return Regresa el objeto si pudo encontrarlo, null en caso contrario.
     */
    public Object obtenerInfo(int renglon, int columna){
        if(validarDimension(renglon, renglones) && validarDimension(columna, columnas)) {
            return datos[renglon][columna];
        }else{
            return null;
        }
    }


    /**
     * Determina si un valor esta en dentro de un rango valido.
     * @param dimension Es el valor a evaluar.
     * @param limiteDimension Es el limite del rango.
     * @return Regresa <b>true</b> si el valor se encuentra dentro del rango, <b>false</b> en caso contrario.
     */
    private boolean validarDimension(int dimension, int limiteDimension){
        return (dimension>=0 && dimension<limiteDimension);
    }

    /**
     * Rellena la matriz con el objeto indicado.
     * @param info Es el objeto con el que se rellenara la matriz.
     * @return Regresa <b>true</b> si el objeto es diferente de <b>null</b> y relleno al menos un espacio, <b>false</b> en caso contrario.
     */
    public boolean rellenar(Object info){
        if (info == null) {
            return false;
        }
        boolean cambio = false;
        for(int renglon=0;renglon<renglones;renglon++){
            for(int columna=0; columna<columnas;columna++){
                if (datos[renglon][columna] == null) {
                    datos[renglon][columna] = info;
                    cambio = true;
                }
            }
        }
        return cambio;
    }

    /**
     * Imprime la matriz renglon por renglon
     */
    public void imprimirR(){
        for(int renglon=0;renglon<renglones;renglon++) {
           for(int columna=0;columna<columnas;columna++){
                SalidaEstandar.consola(datos[renglon][columna] + " ");
            }
            SalidaEstandar.consola("\n");
        }
    }

    /**
     * Imprime la matriz columna por columna
     */
    public void imprimirC() {
        for (int columna = 0; columna < columnas; columna++) {
            for (int renglon = 0; renglon < renglones; renglon++) {
                SalidaEstandar.consola(datos[renglon][columna] + " ");
            }
            SalidaEstandar.consola("\n");
        }
    }

    /**
     * Convierte la matriz a su forma transpuesta.
     */
    public void aplicarTranspuesta() {
        Object[][] ndatos = datos;
        datos = new Object[columnas][renglones];
        int temp = columnas;
        columnas = renglones;
        renglones = temp;

        for (int renglon = 0; renglon < renglones; renglon++) {
            for (int columna = 0; columna < columnas; columna++) {
                datos[renglon][columna] = ndatos[columna][renglon];
            }
        }
    }

    /**
     * Crea una copia de la matriz.
     * @return Regresa la copia de la matriz.
     */
    public Matriz2D clonar() {
        Matriz2D copia = new Matriz2D(renglones, columnas);
        for (int renglon = 0; renglon < renglones; renglon++) {
            for (int columna = 0; columna < columnas; columna++) {
                copia.cambiarInfo(renglon, columna, datos[renglon][columna]);
            }
        }
        return copia;
    }

    /**
     * Compara la matriz con otra matriz para determinar si son iguales.
     * @param matriz2 Es la matriz a comparar.
     * @return Regresa <b>true</b> si las matrices son iguales, <b>false</b> en caso contrario.
     */
    public boolean esIdentica(Matriz2D matriz2) {
        if (matriz2 == null) {
            return false;
        }
        if (renglones != matriz2.renglones() || columnas != matriz2.columnas()) {
            return false;
        }

        for(int renglon=0;renglon<renglones;renglon++) {
            for(int columna=0;columna<columnas;columna++){
                if (ComparadorObject.compare(datos[renglon][columna], matriz2.obtenerInfo(renglon, columna)) != 0) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Convierte a la matriz vacia en un vector columna inicializado con el valor del objeto seleccionado.
     * @param numRenglones Es el numero de renglones del vector columna.
     * @param info Es el objeto con el que se rellenara el vector columna.
     * @return Regresa <b>true</b> si se pudo crear el vector columna, <b>false</b> en caso contrario.
     */
    public boolean vectorColumna(int numRenglones, Object info) {
        if (!vacia() || info == null) {
            return false;
        }
        if (numRenglones < 1) {
            return false;
        }
        Matriz2D columna = new Matriz2D(numRenglones, 1);
        columna.rellenar(info);
        redefinirMatriz(columna);
        return true;
    }

    /**
     * Convierte a la matriz vacia en un vector renglon inicializado con el valor del objeto seleccionado.
     * @param numColumnas Es el numero de columnas del vector renglon.
     * @param info Es el objeto con el que se rellenara el vector renglon.
     * @return Regresa <b>true</b> si se pudo crear el vector renglon, <b>false</b> en caso contrario.
     */
    public boolean vectorRenglon(int numColumnas, Object info) {
        if (!vacia() || info == null) {
            return false;
        }
        if (numColumnas < 1) {
            return false;
        }
        Matriz2D renglon = new Matriz2D(1, numColumnas);
        renglon.rellenar(info);
        redefinirMatriz(renglon);
        return true;
    }

    /**
     * Cambia el tamanio de la matriz.
     * @param numRenglones Es el nuevo numero de renglones de la matriz.
     * @param numColumnas Es el nuevo numero de columnas de la matriz.
     */
    public void redimensionar(int numRenglones, int numColumnas) {
        Matriz2D original = clonar();
        datos = new Object[numRenglones][numColumnas];
        renglones = numRenglones;
        columnas = numColumnas;
        int iterarRenglones = (numRenglones < original.renglones())?numRenglones:original.renglones();
        int iterarColumnas = (numColumnas < original.columnas())?numColumnas:original.columnas();
        for (int renglon = 0; renglon < iterarRenglones; renglon++) {
            for (int columna = 0; columna < iterarColumnas; columna++) {
                datos[renglon][columna] = original.obtenerInfo(renglon, columna);
            }
        }
    }

    /**
     * Convierte a la matriz en una copia de la matriz indicada
     * @param matriz2 Es la matriz a copiar.
     * @return Regresa <b>true</b> si se pudo hacer el cambio, <b>false</b> en caso contrario.
     */
    public boolean redefinirMatriz(Matriz2D matriz2) {
        if (matriz2 == null) {
            return false;
        }
        redimensionar(matriz2.renglones(), matriz2.columnas());
        for(int renglon=0;renglon<renglones;renglon++) {
            for(int columna=0;columna<columnas;columna++){
                datos[renglon][columna] = matriz2.obtenerInfo(renglon, columna);
            }
        }

        return true;
    }

    /**
     * Agrega un renglon al final de la matriz.
     * @param arreglo Es el arreglo que contiene los valores del nuevo renglon.
     * @return Regresa <b>true</b> si se pudo agregar el renglon, <b>false</b> en caso contrario.
     */
    public boolean agregarRenglon(Arreglo arreglo) {
        if (arreglo == null) {
            return false;
        }
        if (arreglo.longitud() != columnas) {
            return false;
        }

        redimensionar(renglones+1, columnas);

        for (int columna = 0; columna < arreglo.numElementos(); columna++) {
            datos[renglones-1][columna] = arreglo.obtener(columna);
        }

        return true;
    }

    /**
     * Agrega una columna al final de la matriz.
     * @param arreglo Es el arreglo que contiene los valores de la nueva columna.
     * @return Regresa <b>true</b> si se pudo agregar la columna, <b>false</b> en caso contrario.
     */
    public boolean agregarColumna(Arreglo arreglo) {
        if (arreglo == null) {
            return false;
        }
        if (arreglo.longitud() != renglones) {
            return false;
        }

        redimensionar(renglones, columnas+1);
        for (int renglon = 0; renglon < arreglo.numElementos(); renglon++) {
            datos[renglon][columnas-1] = arreglo.obtener(renglon);
        }
        return false;
    }

    /**
     * Recibe una Matriz2D y la agrega como columnas a la derecha.
     * @param matriz2 Es la matriz a agregar como columnas.
     * @return Regresa <b>true</b> si se pudo agregar la matriz, <b>false</b> en caso contrario.
     */
    public boolean agregarMatrizColumna(Matriz2D matriz2) {
        if (matriz2 == null) {
            return false;
        }
        if (renglones != matriz2.renglones()) {
            return false;
        }
        int inicio = columnas;
        redimensionar(renglones, columnas + matriz2.columnas());
        for (int renglon = 0; renglon < renglones; renglon++) {
            for (int columna = 0; columna < matriz2.columnas(); columna++) {
                datos[renglon][columna+inicio] = matriz2.obtenerInfo(renglon, columna);
            }
        }
        return true;
    }

    /**
     * Recibe una Matriz2D y la agrega como renglones abajo.
     * @param matriz2 Es la matriz a agregar como renglones.
     * @return Regresa <b>true</b> si pudo agregar la matriz, <b>false</b> en caso contrario.
     */
    public boolean agregarMatrizRenglon(Matriz2D matriz2) {
        if (matriz2 == null) {
            return false;
        }
        if (columnas != matriz2.columnas()) {
            return false;
        }

        int inicio = renglones;
        redimensionar(renglones + matriz2.renglones(), columnas);
        for (int renglon = 0; renglon < matriz2.renglones(); renglon++) {
            for (int columna = 0; columna < columnas; columna++) {
                datos[renglon+inicio][columna] = matriz2.obtenerInfo(renglon, columna);
            }
        }
        return true;
    }

    /**
     * Crea una Matriz3D con la Matriz2D actual y las Matriz2D contenidas en el arreglo.
     * @param matrices Es el arreglo que contiene a las demas Matriz2D.
     * @return Regresa la Matriz3D creada.
     */
    public Matriz3D aMatriz3D(Arreglo matrices) {
        for (int i = 0; i < matrices.numElementos(); i++) {
            Matriz2D recuperada = (Matriz2D) matrices.obtener(i);
            if (renglones != recuperada.renglones() || columnas != recuperada.columnas()) {
                return null;
            }
        }

        int profundidad = matrices.numElementos() + 1;

        Matriz3D matriz3 = new Matriz3D(renglones, columnas, profundidad);
        for (int prof = 0; prof < profundidad; prof++) {
            for (int renglon = 0; renglon < renglones; renglon++) {
                for (int columna = 0; columna < columnas; columna++) {
                    if (prof == 0) {
                        matriz3.cambiarInfo(renglon, columna, prof, datos[renglon][columna]);
                    } else {
                        Matriz2D agregada = (Matriz2D) matrices.obtener(prof-1);
                        matriz3.cambiarInfo(renglon, columna, prof, agregada.obtenerInfo(renglon, columna));
                    }
                }
            }
        }
        return matriz3;
    }

    /**
     * Crea un vector columna que contiene cada columna debajo de otra.
     * @return Regresa el vector columna creado.
     */
    public Matriz2D aVectorColumna() {
        Matriz2D vectorColumna = new Matriz2D(renglones*columnas, 1);

        for (int columna = 0; columna < columnas; columna++) {
            for (int renglon = 0; renglon < renglones; renglon++) {
                vectorColumna.cambiarInfo(columna*renglones + renglon, 0, datos[renglon][columna]);
            }
        }
        return vectorColumna;
    }

    /**
     * Crea un vector renglon que contiene cada renglon uno detras de otro.
     * @return Regresa el vector renglon creado.
     */
    public Matriz2D aVectorRenglon() {
        Matriz2D vectorRenglon = new Matriz2D(1, renglones*columnas);

        for (int renglon = 0; renglon < renglones; renglon++) {
            for (int columna = 0; columna < columnas; columna++) {
                vectorRenglon.cambiarInfo(0, renglon*columnas + columna, datos[renglon][columna]);
            }
        }
        return vectorRenglon;
    }

    /**
     * Elimina la columna en el indice indicado.
     * @param indice Es el indice de la columna a borrar.
     * @return Regresa <b>true</b> si pudo borrarse la columna, <b>false</b> en caso contrario.
     */
    public boolean eliminarColumna(int indice) {
        if (!validarDimension(indice, columnas)) {
            return false;
        }

        if (indice != columnas-1) {
            for (int columna = indice; columna < columnas; columna++) {
                for (int renglon = 0; renglon < renglones; renglon++) {
                    cambiarInfo(renglon, columna, obtenerInfo(renglon, columna+1));
                }
            }
        }
        redimensionar(renglones, columnas-1);

        return true;
    }

    /**
     * Elimina el renglon de la posicion indicada.
     * @param indice Es el indice del renglon a borrar.
     * @return Regresa <b>true</b> si se pudo borrar el renglon, <b>false</b> en caso contrario.
     */
    public boolean eliminarRenglon(int indice) {
        if (!validarDimension(indice, renglones)) {
            return false;
        }

        if (indice != renglones-1) {
            for (int renglon = indice; renglon < renglones; renglon++) {
                for (int columna = 0; columna < columnas; columna++) {
                    cambiarInfo(renglon, columna, obtenerInfo(renglon+1, columna));
                }
            }
        }
        redimensionar(renglones-1, columnas);

        return true;
    }

    /**
     * Elimina la primera o la ultima columna.
     * @param tipoColumna Es el tipo de columna a borrar.
     * @return Regresa <b>true</b> si se pudo borrar la columna, <b>false</b> en caso contrario.
     */
    public boolean eliminarColumna(TipoColumna tipoColumna) {
        if (tipoColumna == TipoColumna.DERECHA) {
            return eliminarColumna(columnas-1);
        } else {
            return eliminarColumna(0);
        }
    }

    /**
     * Elimina el primer o el ultimo renglon.
     * @param tipoRenglon Es el tipo de renglon a borrar.
     * @return Regresa <b>true</b> si se pudo borrar el renglon, <b>false</b> en caso contrario.
     */
    public boolean eliminarRenglon(TipoRenglon tipoRenglon) {
        if (tipoRenglon == TipoRenglon.INFERIOR) {
            return eliminarRenglon(renglones-1);
        } else {
            return eliminarRenglon(0);
        }
    }
}
