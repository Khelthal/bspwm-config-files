package utilerias.fecha;

/**
 * Esta clase es una fecha con dia, mes, anio, hora y minuto.
 */
public class Fecha {
  protected int dia;
  protected int mes;
  protected int anio;
  protected int hora;
  protected int minuto;

  /**
   * Crea una nueva instancia de fecha.
   * @param dia Es el dia de la fecha.
   * @param mes Es el mes de la fecha.
   * @param anio Es el anio de la fecha.
   * @param hora Es la hora de la fecha.
   * @param minuto Es el minuto de la fecha.
   */
  public Fecha(int dia, int mes, int anio, int hora, int minuto) {
    this.dia = (dia >= 1 && dia <= 31) ? dia : 1;
    this.mes = (mes >= 1 && mes <= 12) ? mes : 1;
    this.anio = (anio >= 0) ? anio : 2000;
    this.hora = (hora >= 0 && hora <= 24) ? hora : 0;
    this.minuto = (minuto >= 0 && minuto <= 60) ? minuto : 0;
  }

  /**
   * Obtiene el dia de la fecha.
   * @return Regresa el dia de la fecha.
   */
  public int getDia() {
    return dia;
  }

  /**
   * Obitene el mes de la fecha.
   * @return Regresa el mes de la fecha.
   */
  public int getMes() {
    return mes;
  }

  /**
   * Obtiene el anio de la fecha.
   * @return Regresa el anio de la fecha.
   */
  public int getAnio() {
    return anio;
  }

  /**
   * Obtiene la hora de la fecha.
   * @return Regresa la hora de la fecha.
   */
  public int getHora() {
    return hora;
  }

  /**
   * Obtiene el minuto de la fecha.
   * @return Regresa el minuto de la fecha.
   */
  public int getMinuto() {
    return minuto;
  }

  /**
   * Compara la fecha con otra fecha.
   * @param fecha Es la otra fecha.
   * @return Regresa un enter indicando si la fecha es menor, igual o mayor a la otra fecha.
   */
  public int compareTo(Fecha fecha) {
    if (anio != fecha.getAnio()) {
      return anio - fecha.getAnio();
    }

    if (mes != fecha.getMes()) {
      return mes - fecha.getMes();
    }

    if (dia != fecha.getDia()) {
      return dia - fecha.getDia();
    }
    
    return 0;

    /* Para implementar en futuras caracteristicas.

    if (hora != fecha.getHora()) {
      return hora - fecha.getHora();
    }

    if (minuto != fecha.getMinuto()) {
      return minuto - fecha.getMinuto();
    }

    return 0;
    */
  }

  @Override
  public String toString() {
    String diaTxt = "";
    String mesTxt = "";
    String anioTxt = "";
    String horaTxt = "";
    String minutoTxt = "";

    if (dia < 10) {
      diaTxt += "0";
    }

    if (mes < 10) {
      mesTxt += "0";
    }

    anioTxt = anio + "";

    if (hora < 10) {
      horaTxt += "0";
    }

    if (minuto < 10) {
      minutoTxt += "0";
    }

    diaTxt += dia + "";
    mesTxt += mes + "";
    horaTxt += hora + "";
    minutoTxt += minuto + "";

    return diaTxt + "/" + mesTxt + "/" + anioTxt + " " + horaTxt + ":" + minutoTxt;
  }
}
