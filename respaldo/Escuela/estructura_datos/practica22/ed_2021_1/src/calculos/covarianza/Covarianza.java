package calculos.covarianza;

import edlineal.ListaLigada;
import entradasalida.SalidaEstandar;
import utilerias.matematicas.OperacionesListasLigadas;

/**
 * Esta clase se encarga de calcular la covarianza.
 */
public class Covarianza {

  /**
   * Recibe los datos de dos ListaLigada para calcular la covarianza.
   * @param listaX Es la lista con los datos de X.
   * @param listaY Es la lista con los datos de Y.
   * @return Regresa la covarianza calculada.
   */
  public static double calcularCovarianza(ListaLigada listaX, ListaLigada listaY) {
    double xProm = 0;
    double yProm = 0;
    int n = 0;

    listaX.inicializarIterador();
    while (listaX.hayMas()) {
      n++;
      xProm += (double) listaX.obtenerSigiuente();
    }

    listaY.inicializarIterador();
    while (listaY.hayMas()) {
      yProm += (double) listaY.obtenerSigiuente();
    }

    xProm /= n;
    yProm /= n;

    ListaLigada xResta = OperacionesListasLigadas.sumarConstanteALista(listaX, -xProm);
    ListaLigada yResta = OperacionesListasLigadas.sumarConstanteALista(listaY, -yProm);
    ListaLigada xyResta = OperacionesListasLigadas.multiplicarListas(xResta, yResta);

    double resultado = OperacionesListasLigadas.calcularSumatoriaLista(xyResta) / (n-1.0);
    return resultado;
  }
}
