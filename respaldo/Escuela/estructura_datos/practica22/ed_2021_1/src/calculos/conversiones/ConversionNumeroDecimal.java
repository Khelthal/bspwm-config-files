package calculos.conversiones;

/**
 * Esta clase se encarga de convertir numeros de base 10 a otras bases.
 */
public class ConversionNumeroDecimal {

  /**
   * Cambia la base de un numero base 10 a una nueva base.
   * @param num Es el numero base 10.
   * @param nuevaBase Es la nueva base.
   * @return Regresa una cadena con el numero convertido a una nueva base.
   */
  public static String cambiarBaseNumero(int num, int nuevaBase) {
    if (num/nuevaBase != 0) {
      return cambiarBaseNumero(num/nuevaBase, nuevaBase) + numeroAString(num%nuevaBase);
    } else {
      return "" + numeroAString(num%nuevaBase);
    }
  }

  /**
   * Convierte un numero base 10 a un numero binario.
   * @param num Es el numero a convertir.
   * @return Regresa una cadena con el numero convertido a binario.
   */
  public static String aBinario(int num) {
    if (num == 1) {
      return "1";
    } else {
      if (num % 2 == 0) {
        return aBinario(num/2) + "0";
      } else {
        num--;
        return aBinario(num/2) + "1";
      }
    }
  }

  /**
   * Convierte un numero de base 10 a una nueva base.
   * @param num Es el numero a convertir.
   * @return Regresa el numero convertido a una nueva base.
   */
  private static char numeroAString(int num) {
    String r = "";
    if (num < 10) {
      r += num + "";
      return r.charAt(0);
    } else {
      char c = '7';
      c += num;
      return c;
    }
  }
}
