package calculos.series;

import edlineal.ListaLigada;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de obtener series de numeros primos.
 */
public class SeriePrimos {

  /**
   * Obtiene una lista con los numeros primos desde 2 hasta el numero dado.
   * @param limite Es el numero donde termina la busqueda.
   * @return Regresa una lista con los numeros primos desde 2 hasta el numero dado.
   */
  public static ListaLigada obtenerSeriePrimos(int limite) {
    ListaLigada primos = new ListaLigada();
    generarSeriePrimos(primos, 2, limite);
    return primos;
  }

  /**
   * Obtiene los numeros primos desde 2 hasta el limite.
   * @param lista Es la lista donde se almacenaran los numeros primos.
   * @param num Es el numero a iterar.
   * @param limite Es el numero donde termina la busqueda.
   */
  private static void generarSeriePrimos(ListaLigada lista, int num, int limite) {
    if (num <= limite) {
      if (esPrimo(num)) {
        lista.agregar(num);
      }
      generarSeriePrimos(lista, num+1, limite);
    }
  }

  /**
   * Determina si un numero es primo.
   * @param n Es el numero a evaluar.
   * @return Regresa <b>true</b> si el numero es primo, <b>false</b> en caso contrario.
   */
  public static boolean esPrimo(int n) {
    if (n < 2) {
      return false;
    }
    else if (n == 2) {
      return true;
    }
    return esPrimo(2, n);
  }

  /**
   * Determina si un numero es primo.
   * @param i Es el numero a iterar.
   * @param n Es el numero a evaluar.
   * @return Regresa <b>true</b> si el numero a evaluar es primo, <b>false</b> en caso contrario.
   */
  private static boolean esPrimo(int i, int n) {
    if (i < n) {
      boolean noPrimo = n%i == 0;
      if (noPrimo) {
        return false;
      } else {
        return esPrimo(i+1, n);
      }
    } else {
      return true;
    }
  }
}
