package utilerias.matematicas;

import edlineal.Arreglo;
import edlineal.ArregloNumerico;
import edlineal.PilaArreglo;
import edlineal.PilaLista;
import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de leer expresiones y calcular sus resultados.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class ExpresionAritmetica {

    /**
     * Lee una expresion y cambia las variables por datos numericos dados por el usuario.
     * @param expresion Es la expresion a leer.
     * @return Regresa la expresion modificada.
     */
    public static String convertirExpresion(String expresion) {
        boolean varEncontrada = false;
        int varCount = 0;
        Arreglo variables;
        for (int i = 0; i < expresion.length(); i++) {
            char c = expresion.charAt(i);
            if (!varEncontrada) {
                if (esLetra(c) || c == '$') {
                    varEncontrada = true;
                    varCount++;
                }
            } else {
                if (esSeparador(c)) {
                    varEncontrada = false;
                }
            }
        }

        variables = new Arreglo(varCount);
        String varName = "";
        varEncontrada = false;
        for (int i = 0; i < expresion.length(); i++) {
            char c = expresion.charAt(i);
            if (!varEncontrada) {
                if (esLetra(c) || c == '$') {
                    varName += c;
                    varEncontrada = true;
                }
            } else {
                if (esSeparador(c)) {
                    variables.agregar(varName);
                    varName = "";
                    varEncontrada = false;
                } else {
                    varName += c;
                }
            }
        }
        if (varCount > variables.numElementos()) {
            variables.agregar(varName);
        }

        for (int i = 0; i < varCount; i++) {
            SalidaEstandar.consola("Ingrese el valor de la variable " + variables.obtener(i) + "\n:");
            variables.cambiar(i, EntradaConsola.consolaCadena());
        }

        String nuevaExpresion = "";
        varCount = 0;
        varEncontrada = false;
        for (int i = 0; i < expresion.length(); i++) {
            char c = expresion.charAt(i);
            if (!varEncontrada) {
                if (esLetra(c) || c == '$') {
                    nuevaExpresion += variables.obtener(varCount);
                    varCount++;
                    varEncontrada = true;
                } else {
                    if (c == ' ') {
                        continue;
                    }
                    if (c == '[') {
                        nuevaExpresion += '(';
                        continue;
                    }
                    if (c == ']') {
                        nuevaExpresion += ')';
                        continue;
                    }
                    nuevaExpresion += c;
                }
            } else {
                if (esSeparador(c)) {
                    if (c == ' ') {
                        continue;
                    }
                    if (c == '[') {
                        nuevaExpresion += '(';
                        continue;
                    }
                    if (c == ']') {
                        nuevaExpresion += ')';
                        continue;
                    }
                    nuevaExpresion += c;
                    varEncontrada = false;
                }
            }
        }

        return nuevaExpresion;
    }

    /**
     * Convierte una expresion infija a postfija.
     * @param infija Es la expresion infija a ser convertida.
     * @return Regresa la nueva expresion postfija.
     */
    public static String infijaAPostfija(String infija){
        PilaArreglo pila = new PilaArreglo(infija.length());
        String epos = "";
        int nextIndex = 0;
        for (int i = 0; i < infija.length(); i++) {
            if (i < nextIndex) {
                continue;
            }
            char c = infija.charAt(i);

            if (esOperando(c)) {
                String num = ""+c;
                nextIndex = i+1;
                while (nextIndex < infija.length() && (esOperando(infija.charAt(nextIndex)) || infija.charAt(nextIndex) == '.')) {
                    num += infija.charAt(nextIndex);
                    nextIndex++;
                }
                epos = sumarEPOS(epos, num);
            } else {
                if (c == '(') {
                    pila.poner(c+"");
                }
                else if (c == ')') {
                    while (!pila.verLimite().equals("(")) {
                        epos = sumarEPOS(epos, (String) pila.quitar());
                    }
                    pila.quitar();
                } else {
                    if (pila.vacio()) {
                        pila.poner(c+"");
                        continue;
                    }
                    if (pila.vacio()) {
                        pila.poner(c+"");
                        continue;
                    }
                    while (obtenerPrioridad(c) <= obtenerPrioridad(((String) pila.verLimite()).charAt(0))) {
                        epos = sumarEPOS(epos, (String) pila.quitar());
                        if (pila.vacio()) {
                            break;
                        }
                    }
                    pila.poner(c+"");
                }
            }
        }
        while (!pila.vacio()) {
            epos = sumarEPOS(epos, (String) pila.quitar());
        }
        return epos;
    }

    private static String sumarEPOS(String epos, String element) {
        if (epos.length() > 0) {
            epos += " ";
        }
        epos += element;
        return epos;
    }

    /**
     * Convierte una expresion infija a prefija.
     * @param infija Es la expresion a convertir.
     * @return Regresa la nueva expresion.
     */
    public static String infijaAPrefija(String infija){
        PilaArreglo pila = new PilaArreglo(infija.length());
        String epre = "";
        int nextIndex = infija.length()-1;
        for (int i = infija.length()-1; i >= 0; i--) {
            if (i > nextIndex) {
                continue;
            }
            char c = infija.charAt(i);

            if (esOperando(c)) {
                String num = c+"";
                nextIndex = i-1;
                while (nextIndex > -1 && (esOperando(infija.charAt(nextIndex)) || infija.charAt(nextIndex) == '.')) {
                    String pivote = "";
                    pivote += infija.charAt(nextIndex);
                    pivote += num;
                    num = pivote;
                    nextIndex--;
                }
                epre = sumarEPRE(epre, num);
            } else {
                if (c == ')') {
                    pila.poner(c+"");
                }
                else if (c == '(') {
                    while (!pila.verLimite().equals(")")) {
                        epre = sumarEPRE(epre, (String) pila.quitar());
                    }
                    pila.quitar();
                } else {
                    if (pila.vacio()) {
                        pila.poner(c+"");
                        continue;
                    }
                    if (pila.vacio()) {
                        pila.poner(c+"");
                        continue;
                    }
                    while (obtenerPrioridad(c) < obtenerPrioridad(((String) pila.verLimite()).charAt(0))) {
                        epre = sumarEPRE(epre, (String) pila.quitar());
                        if (pila.vacio()) {
                            break;
                        }
                    }
                    pila.poner(c+"");
                }
            }
        }
        while (!pila.vacio()) {
            epre = sumarEPRE(epre, (String) pila.quitar());
        }
        return epre;
    }

    private static String sumarEPRE(String epre, String element) {
        String pivote = "";
        pivote += element;
        if (epre.length() > 0) {
            pivote += " ";
        }
        pivote += epre;
        return pivote;
    }

    /**
     * Lee una expresion postfija y calcula su resultado.
     * @param postfija Es la expresion a calcular.
     * @return Regresa el resultado de la expresion.
     */
    public static Double evaluarPostfija(String postfija){
        double resultadoE=0.0;
        PilaArreglo pila= new PilaArreglo(postfija.length());

        //0. Tokenizar la cadena en postfija
        int nextIndex = 0;
        for(int posToken=0;posToken<postfija.length();posToken++){
            char token=postfija.charAt(posToken);
            if (token == ' ' || posToken < nextIndex) {
                continue;
            }
            //1. Si es un operando, se mete en la pila
            if(esOperando(token)==true){
                String num = ""+token;
                nextIndex = posToken+1;
                while (nextIndex < postfija.length() && (esOperando(postfija.charAt(nextIndex)) || postfija.charAt(nextIndex) == '.')) {
                    if (postfija.charAt(nextIndex) == ' ') {
                        break;
                    }
                    num += postfija.charAt(nextIndex);
                    nextIndex++;
                }
                pila.poner(num);
            }else{ //es un operador
                //2. Si es un operador, saco dos operandos de la pila, les aplico la operación
                // y el resultado lo meto en la pila.
                String operando2=(String)pila.quitar();
                String operando1=(String)pila.quitar();

                if(operando2==null || operando1==null){
                    return null;
                }else{ //son valores válidos
                    Double resultadoParcial=aplicarOperacion(Double.parseDouble(operando1),
                            Double.parseDouble(operando2),token);
                    if (resultadoParcial!=null) {
                        pila.poner("" + resultadoParcial);
                    }else{ //ocurrión un detalle
                        return null;
                    }
                }
            }
        }
        //3. Regresamos el valor del resultado que se enuentra en la cima de la pila
        String resultadoFinal=(String)pila.quitar();
        if(resultadoFinal==null){
            return null;
        }else { //todo salió bien
            resultadoE=Double.parseDouble(resultadoFinal);
            return resultadoE;
        }
    }

    /**
     * Aplica una operacion entre operadores y operandos.
     * @param op1 Es el operando 1.
     * @param op2 Es el operando 2.
     * @param oper Es el operador.
     * @return Regresa el resultado de la operacion.
     */
    public static Double aplicarOperacion(double op1, double op2, char oper){
        if (oper == '^') {
            return Math.pow(op1,op2);
        }else if (oper == '*') {
            return op1*op2;
        }else if (oper == '/') {
            return op1/op2;
        }else if (oper == '+') {
            return op1+op2;
        }else if (oper == '-') {
            return op1-op2;
        }else{
            return null;
        }
    }

    /**
     * Determina si un dato de tipo char es un operando.
     * @param token Es el dato a evaluar.
     * @return Regresa <b>true</b> si el dato es un operando, <b>false</b> en caso contrario.
     */
    public static boolean esOperando(char token){
        if(token!='^' && token!='*' && token!='/' && token!='+' && token!='-' && token !='(' && token !=')'){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Deterimna si un dato de tipo char es una letra.
     * @param token Es el dato a evaluar.
     * @return Regresa <b>true</b> si el dato es una letra, <b>false</b> en caso contrario.
     */
    public static boolean esLetra(char token) {
        if (token >= 65 && token <= 90 || token >= 97 && token <= 122) {
            return true;
        }
        return false;
    }

    /**
     * Deterimna si un dato de tipo char es un numero.
     * @param token Es el dato a evaluar.
     * @return Regresa <b>true</b> si el dato es un numero, <b>false</b> en caso contrario.
     */
    public static boolean esNumero(char token) {
        if (token >= 48 && token <= 57) {
            return true;
        }
        return false;
    }

    /**
     * Deterimna si un dato de tipo char es un separador.
     * @param token Es el dato a evaluar.
     * @return Regresa <b>true</b> si el dato es un separador, <b>false</b> en caso contrario.
     */
    public static boolean esSeparador(char token) {
        if (!esOperando(token) || token == ' ' || token == '[' || token == ']') {
            return true;
        }
        return false;
    }

    /**
     * Obtiene la prioridad jerarquica de un operador.
     * @param operador Es el operador del cual se obtendra su prioridad.
     * @return Regresa la prioridad del operador.
     */
    public static int obtenerPrioridad(char operador) {
        switch (operador) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            case '^':
                return 3;
        }
        return 0;
    }

    /**
     * Calcula el resultado de una expresion prefija.
     * @param prefija Es la expresion a calcular.
     * @return Regresa el resultado de la expresion.
     */
    public static Double evaluarPrefija(String prefija){
        double resultadoE=0.0;
        PilaArreglo pila= new PilaArreglo(prefija.length());

        //0. Tokenizar la cadena en prefija
        int nextIndex = prefija.length()-1;
        for(int posToken=prefija.length()-1;posToken>=0;posToken--){
            char token=prefija.charAt(posToken);
            if (token == ' ' || posToken > nextIndex) {
                continue;
            }
            //1. Si es un operando, se mete en la pila
            if(esOperando(token)==true){
                String num = token+"";
                nextIndex = posToken-1;
                while (nextIndex > -1 && (esOperando(prefija.charAt(nextIndex)) || prefija.charAt(nextIndex) == '.')) {
                    if (prefija.charAt(nextIndex) == ' ') {
                        break;
                    }
                    String pivote = "";
                    pivote += prefija.charAt(nextIndex);
                    pivote += num;
                    num = pivote;
                    nextIndex--;
                }
                pila.poner(num);
            }else{ //es un operador
                //2. Si es un operador, saco dos operandos de la pila, les aplico la operación
                // y el resultado lo meto en la pila.
                String operando1=(String)pila.quitar();
                String operando2=(String)pila.quitar();

                if(operando2==null || operando1==null){
                    return null;
                }else{ //son valores válidos
                    Double resultadoParcial=aplicarOperacion(Double.parseDouble(operando1),
                            Double.parseDouble(operando2),token);
                    if (resultadoParcial!=null) {
                        pila.poner("" + resultadoParcial);
                    }else{ //ocurrión un detalle
                        return null;
                    }
                }
            }
        }
        //3. Regresamos el valor del resultado que se enuentra en la cima de la pila
        String resultadoFinal=(String)pila.quitar();
        if(resultadoFinal==null){
            return null;
        }else { //todo salió bien
            resultadoE=Double.parseDouble(resultadoFinal);
            return resultadoE;
        }
    }

    /**
     * Valida que los parentesis de la expresion esten bien balanceados.
     * @param expresion Es la expresion a analizar.
     * @return Regresa <b>true</b> si los parentesis estan bien balanceados, <b>false</b> en caso contrario.
     */
    public static boolean validarBalanceoParentesis(String expresion) {
        PilaLista pila = new PilaLista();
        PilaLista pivote = new PilaLista();
        
        for (int i = 0; i < expresion.length(); i++) {
            char c = expresion.charAt(i);

            if (c == '(' || c == ')') {
                if (c == '(') {
                    pila.poner(c);
                }
                else {
                    while (!pila.vacio() && (char) pila.verLimite() != '(') {
                        pivote.poner(pila.quitar());
                    }

                    if (pila.vacio()) {
                        return false;
                    } else {
                        pila.quitar();
                        while (!pivote.vacio()) {
                            pila.poner(pivote.quitar());
                        }
                    }
                }
            }
        }
        return pila.vacio();
    }
}
