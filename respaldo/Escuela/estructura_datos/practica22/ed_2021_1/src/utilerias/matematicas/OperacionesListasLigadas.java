package utilerias.matematicas;

import edlineal.ListaLigada;

/**
 * Esta clase se encarga de realizar operaciones basicas con ListaLigada.
 */
public class OperacionesListasLigadas {

  /**
   * Calcula la suma de los elementos de una ListaLigada.
   * @param lista Es la lista de la que se obtendra la sumatoria.
   * @return Regresa la sumatoria calculada.
   */
  public static double calcularSumatoriaLista(ListaLigada lista) {
    double suma = 0.0;
    lista.inicializarIterador();

    while (lista.hayMas()) {
      suma += (double) lista.obtenerSigiuente();
    }

    return suma;
  }

  /**
   * Crea una nueva ListaLigada a partir del producto de dos ListaLigada.
   * @param lista1 Es la primera lista a multiplicar.
   * @param lista2 Es la segunda lista a multiplicar.
   * @return Regresa la nueva ListaLigada creada.
   */
  public static ListaLigada multiplicarListas(ListaLigada lista1, ListaLigada lista2) {
    double valor;
    ListaLigada resultado = new ListaLigada();

    lista1.inicializarIterador();
    lista2.inicializarIterador();

    while (lista1.hayMas() && lista2.hayMas()) {
      valor = (double) lista1.obtenerSigiuente() * (double) lista2.obtenerSigiuente();
      resultado.agregar(valor);
    }

    return resultado;
  }

  /**
   * Crea una nueva ListaLigada con los valores de una ListaLigada mas una constante.
   * @param lista Es la lista con los elementos deseados.
   * @param constante Es la constante a sumar.
   * @return Regresa la ListaLigada creada.
   */
  public static ListaLigada sumarConstanteALista(ListaLigada lista, double constante) {
    double valor;
    ListaLigada resultado = new ListaLigada();

    lista.inicializarIterador();

    while (lista.hayMas()) {
      valor = (double) lista.obtenerSigiuente();
      resultado.agregar(valor+constante);
    }
    return resultado;
  }
}
