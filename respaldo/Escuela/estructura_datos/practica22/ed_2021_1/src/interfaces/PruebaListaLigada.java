package interfaces;

import edlineal.Arreglo;
import edlineal.ListaLigada;
import edlineal.enumerados.TipoMatriz;
import ednolineal.Matriz2D;
import entradasalida.SalidaEstandar;

/**
 * PruebaListaLigada
 */
public class PruebaListaLigada {
  public static void main(String[] args) {
    ListaLigada ligada = new ListaLigada();
    ligada.agregar("A");
    ligada.agregar("B");
    ligada.agregar("C");
    ligada.agregar("D");
    ligada.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Invirtiendo lista\n");
    ligada.invertir();
    ligada.imprimir();
    SalidaEstandar.consola("\n");
    Arreglo ignorar = new Arreglo(2);
    ignorar.agregar("A");
    ignorar.agregar("D");
    SalidaEstandar.consola("Convirtiendo a arreglo ignorando los elementos A y D\n");
    Arreglo arreglo = ligada.aArreglo(ignorar);
    arreglo.imprimirOI();
    SalidaEstandar.consola("Conviritiendo a Matriz2D de 2 renglones y 3 columnas\n");
    Matriz2D matriz2d = ligada.aMatriz2D(2, 3);
    matriz2d.imprimirR();
    SalidaEstandar.consola("Agregando 5 veces el elemento Z\n");
    ligada.rellenar("Z", 5);
    ligada.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Cambiando 3 veces el elemento Z por el elemento Y\n");
    ligada.cambiar("Z", "Y", 3);
    ligada.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Cambiando el elemento de la posicion 0 por P\n");
    ligada.cambiar(0, "P");
    ligada.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Obteniendo el elemento de la posicion 3\n");
    SalidaEstandar.consola(ligada.obtener(3)+"\n");
    ListaLigada copia = (ListaLigada) ligada.clonar();
    SalidaEstandar.consola("Creando copia de la lista y comprobando si son iguales\n");
    SalidaEstandar.consola(ligada.esIgual(copia)+"\n");
    SalidaEstandar.consola("Redimensionando la copia a tamanio 4\n");
    ListaLigada residuo = (ListaLigada) copia.redimensionar(4);
    copia.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Mostrando elementos eliminados al redimensionar\n");
    residuo.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Redimensionando la copia a tamanio 9\n");
    copia.redimensionar(9);
    copia.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Rellenando la copia con el elemento J\n");
    copia.rellenar("J");
    copia.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Eliminando en la copia el elemento de la posicion 2\n");
    copia.eliminar(2);
    copia.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Insertando elemento W en la posicion 3\n");
    copia.insertar(3, "W");
    copia.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Creando Matriz2D\n");
    Matriz2D agregame = new Matriz2D(2, 2);
    agregame.cambiarInfo(0, 0, 0);
    agregame.cambiarInfo(0, 1, 1);
    agregame.cambiarInfo(1, 0, 2);
    agregame.cambiarInfo(1, 1, 3);
    agregame.imprimirR();
    SalidaEstandar.consola("Agregando Matriz2D por columnas a la copia\n");
    copia.agregarMatriz2D(agregame, TipoMatriz.COLUMNA);
    copia.imprimir();
    SalidaEstandar.consola("\n");
  }
}
