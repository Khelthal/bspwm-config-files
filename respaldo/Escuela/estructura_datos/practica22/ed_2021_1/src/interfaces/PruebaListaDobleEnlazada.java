package interfaces;

import edlineal.ListaDobleLigada;
import edlineal.ListaLigada;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de hacer pruebas con la clase ListaDobleLigada.
 */
public class PruebaListaDobleEnlazada {

  public static void main(String[] args) {
    ListaDobleLigada lista = new ListaDobleLigada();
    lista.rellenar('F');
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Insertando elemento 'X' en la posicion 2\n");
    lista.insertar(2, 'X');
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Imprimiendo la lista en orden inverso\n");
    lista.imprimirOI();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Invirtiendo la lista\n");
    lista.invertir();
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Eliminando el ultimo elemento\n");
    lista.eliminar();
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Eliminando el primer elemento\n");
    lista.eliminarInicio();
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Eliminando el elemento 'X'\n");
    lista.eliminar('X');
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Agregando el elemento 'Y' al inicio de la lista\n");
    lista.agregarInicio('Y');
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Buscando el elemento 'Y' iniciando desde el final\n");
    SalidaEstandar.consola("El resultado de la busqueda fue: " + lista.buscarDesdeFinal('Y') + "\n");
    SalidaEstandar.consola("Buscando el elemento 'B' iniciando desde el principio\n");
    SalidaEstandar.consola("El resultado de la busqueda fue: " + lista.buscar('B') + "\n");
    SalidaEstandar.consola("Clonando la lista actual\n");
    ListaDobleLigada clon = (ListaDobleLigada) lista.clonar();
    clon.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Comprobando si la lista y el clon son iguales: " + lista.esIgual(clon) + "\n");
    SalidaEstandar.consola("Agregando elemento 'hola' a el clon\n");
    clon.agregar("hola");
    clon.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Agregando elemento '32' a el clon\n");
    clon.agregar(32);
    clon.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Comprobando si la lista y el clon son iguales: " + lista.esIgual(clon) + "\n");
    SalidaEstandar.consola("Separando los elementos del clon en distintas categorias\n");
    ListaLigada contenedor = clon.separarElementos();
    ListaDobleLigada numeros = (ListaDobleLigada) contenedor.obtener(0);
    ListaDobleLigada cadenas = (ListaDobleLigada) contenedor.obtener(1);
    ListaDobleLigada otros = (ListaDobleLigada) contenedor.obtener(2);
    SalidaEstandar.consola("Imprimiendo lista de numeros: ");
    numeros.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Imprimiendo lista de cadenas: ");
    cadenas.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Imprimiendo lista de otros objetos: ");
    otros.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Recorriendo 3 posiciones de lista original con iterador izquierdo\n");
    lista.inicializarIteradorIzq();
    SalidaEstandar.consola(lista.obtenerSigiuenteIzq() + "\n");
    SalidaEstandar.consola(lista.obtenerSigiuenteIzq() + "\n");
    SalidaEstandar.consola(lista.obtenerSigiuenteIzq() + "\n");
    SalidaEstandar.consola("Regresando 2 posiciones de lista original con iterador izquierdo\n");
    SalidaEstandar.consola(lista.obtenerProcedenteIzq() + "\n");
    SalidaEstandar.consola(lista.obtenerProcedenteIzq() + "\n");

    SalidaEstandar.consola("Recorriendo 3 posiciones de lista original con iterador derecho\n");
    lista.inicializarIteradorDer();
    SalidaEstandar.consola(lista.obtenerSigiuenteDer() + "\n");
    SalidaEstandar.consola(lista.obtenerSigiuenteDer() + "\n");
    SalidaEstandar.consola(lista.obtenerSigiuenteDer() + "\n");
    SalidaEstandar.consola("Regresando 2 posiciones de lista original con iterador derecho\n");
    SalidaEstandar.consola(lista.obtenerProcedenteDer() + "\n");
    SalidaEstandar.consola(lista.obtenerProcedenteDer() + "\n");
  }
}
