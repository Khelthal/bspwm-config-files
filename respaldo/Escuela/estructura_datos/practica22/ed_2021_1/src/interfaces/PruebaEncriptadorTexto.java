package interfaces;

import edlineal.Arreglo;
import entradasalida.SalidaEstandar;
import entradasalida.archivos.ArchivoTexto;
import utilerias.texto.AnalizadorTexto;
import utilerias.texto.encriptado.EncriptadorTexto;

/**
 * Esta clase se encarga de hacer pruebas con cadenas de texto y encriptado.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class PruebaEncriptadorTexto {
    public static void main(String[] args) {
        String texto = "Estructuras de Datos";
        String encriptado;
        String desencriptado;

        SalidaEstandar.consola("Texto original: " + texto + "\n");
        encriptado = EncriptadorTexto.encriptarTexto(texto);
        SalidaEstandar.consola(encriptado+"\n");

        desencriptado = EncriptadorTexto.desencriptarTexto(encriptado);
        SalidaEstandar.consola(desencriptado+"\n");

        SalidaEstandar.consola("\n" + "Actividad 3" + "\n");
        Arreglo archivo = ArchivoTexto.leer("src/textoEncriptado.txt", 7);
        SalidaEstandar.consola(archivo.obtener(0)+"\n");
        SalidaEstandar.consola(archivo.obtener(1)+"\n");
        SalidaEstandar.consola(archivo.obtener(2)+"\n");
        SalidaEstandar.consola(archivo.obtener(3)+"\n");
        SalidaEstandar.consola(archivo.obtener(4)+"\n");
        SalidaEstandar.consola(archivo.obtener(5)+"\n");
        SalidaEstandar.consola(archivo.obtener(6)+"\n");
        SalidaEstandar.consola("\nDesencriptando\n\n");
        String ndesencriptado = EncriptadorTexto.desencriptarArchivo("src/textoEncriptado.txt", 7);
        SalidaEstandar.consola(ndesencriptado+"\n");

        SalidaEstandar.consola("\n" + "Actividad 4" + "\n");
        String palindromo = "oso";
        SalidaEstandar.consola(palindromo + "\n");
        SalidaEstandar.consola("La palabra " + palindromo + " es un palindromo: " + AnalizadorTexto.esPalindromo(palindromo)+"\n");
    }
}
