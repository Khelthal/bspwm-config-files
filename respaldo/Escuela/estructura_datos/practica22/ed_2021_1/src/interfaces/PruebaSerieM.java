package interfaces;

import calculos.series.SerieM;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de realizar pruebas con la clase SerieM.
 */
public class PruebaSerieM {

  public static void main(String[] args) {
    int x = 3;
    int m = 7;
    SalidaEstandar.consola("Calculando la serie con los valores x = " + x + " y m = " + m + "\n");
    SalidaEstandar.consola(SerieM.calcularSerieM(3, 7) + "\n");
  }
}
