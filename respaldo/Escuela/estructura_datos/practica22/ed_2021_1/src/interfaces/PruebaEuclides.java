package interfaces;

import calculos.mcd.Euclides;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de realizar pruebas con la clase Euclides.
 */
public class PruebaEuclides {

  public static void main(String[] args) {
    int a=25;
    int b=5;
    SalidaEstandar.consola("Calculando el MCD de " + a + " y " + b + "\n");
    SalidaEstandar.consola(Euclides.calcularMCD(a, b) + "\n");
  }
}
