package entradasalida;

import java.io.*;

/**
 * Esta clase se encarga de manejar la entrada de informacion del usuario.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class EntradaConsola {
    /**
     * Recibe la entrada de texto del usuario.
     * @return Regresa el texto ingresado por el usuario.
     */
    public static String consolaCadena() {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(isr);
        String cadenaEntrada="";
        try {
            cadenaEntrada = buffer.readLine();
        }catch(IOException e){
            e.printStackTrace();
        }finally{
            return cadenaEntrada;
        }
    }

    /**
     * Recibe la entrada de un dato tipo <b>Double</b> del usuario.
     * @return Regresa el dato de tipo <b>Double</b> ingresado por el usuario.
     */
    public static Double consolaDouble() {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(isr);
        double cadenaEntrada=0;
        try {
            cadenaEntrada = Double.parseDouble(buffer.readLine());
        }catch(IOException e){
            e.printStackTrace();
        }finally{
            return cadenaEntrada;
        }
    }

    /**
     * Recibe la entrada de un dato tipo <b>int</b> del usuario.
     * @return Regresa el dato de tipo <b>int</b> ingresado por el usuario.
     */
    public static int consolaInt() {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(isr);
        int cadenaEntrada=0;
        try {
            cadenaEntrada = Integer.parseInt(buffer.readLine());
        }catch(IOException e){
            e.printStackTrace();
        }finally{
            return cadenaEntrada;
        }
    }
}
