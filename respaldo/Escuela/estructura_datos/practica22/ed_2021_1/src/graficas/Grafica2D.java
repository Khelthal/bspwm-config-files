package graficas;

import edlineal.ListaLigada;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;

/**
 * Crea una grafica de 2 dimensiones.
 */
public class Grafica2D {
  ListaLigada listaX;
  ListaLigada listaY;

  /**
   * Crea una nueva instancia de Grafica2D.
   * @param listaX Es la lista que contiene los datos de X.
   * @param listaY Es la lista que contiene los datos de Y.
   */
  public Grafica2D(ListaLigada listaX, ListaLigada listaY) {
    jFrameMain = new JFrame();
    jPanelMain = new JPanel();
    jFrameMain.add(jPanelMain);
    this.listaX = listaX;
    this.listaY = listaY;
  }

  /**
   * Inicializa la Grafica2D.
   * @param key Es el nombre de la relacion entre X y Y.
   * @param titulo Es el titulo de la grafica.
   * @param ejeX Es el titulo del eje X.
   * @param ejeY Es el titulo del eje Y.
   */
  public void init(String key, String titulo, String ejeX, String ejeY) {
    jFrameMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    jFrameMain.setSize(800, 500);
    XYSeries xySeries = new XYSeries(key);

    listaX.inicializarIterador();
    listaY.inicializarIterador();

    while (listaX.hayMas() && listaY.hayMas()) {
      xySeries.add((double) listaX.obtenerSigiuente(), (double) listaY.obtenerSigiuente());
    }

    XYSeriesCollection xySeriesCollection = new XYSeriesCollection();
    xySeriesCollection.addSeries(xySeries);

    JFreeChart grafica = ChartFactory.createScatterPlot(titulo, ejeX, ejeY, xySeriesCollection,
        PlotOrientation.VERTICAL, true, false, false);

    ChartPanel panel = new ChartPanel(grafica);
    jPanelMain.setLayout(new java.awt.BorderLayout());
    jPanelMain.add(panel);
    jPanelMain.validate();

    jFrameMain.setVisible(true);
  }

  JFrame jFrameMain;
  JPanel jPanelMain;
}
