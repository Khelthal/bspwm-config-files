package registros.procesos;

/**
 * Esta clase enumerada contiene todos los tipos de procesos.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public enum TipoProceso {
    SISTEMA_OPERATIVO,
    DESCARGA,
    IMPRESION,
    COPIADO
}
