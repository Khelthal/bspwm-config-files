package edlineal;

import comparadores.ComparadorObject;

/**
 * Esta clase maneja arreglos y distintas operaciones con arreglos ordenados.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class ArregloOrdenado extends Arreglo {
    protected TipoOrden orden;

    /**
     * Crea un ArregloOrdenado del tamanio y orden especificados.
     * @param maxima Es el numero maximo de elementos que pueden entrar en el arreglo.
     * @param orden Indica si los elementos se ordenan en orden ascendente o descendente.
     */
    public ArregloOrdenado(int maxima, TipoOrden orden) {
        super(maxima);
        this.orden = orden;
    }

    /**
     * Verifica el tipo de ordenaimento del ArregloOrdenado.
     * @return Regresa el orden del ArregloOrdenado.
     */
    public TipoOrden getOrden() {
        return orden;
    }

    @Override
    public Object buscar(Object info) {
        int posicionB = 0;
        if (orden == TipoOrden.CRECIENTE) {
            while (posicionB <= limite && ComparadorObject.compare(datos[posicionB], info) < 0) {
                posicionB++;
            }
            if (posicionB > limite || ComparadorObject.compare(datos[posicionB], info) != 0) {
                return (posicionB+1)*-1;
            } else {
                return posicionB+1;
            }
        } else {
            while (posicionB <= limite && ComparadorObject.compare(datos[posicionB], info) > 0) {
                posicionB++;
            }
            if (posicionB > limite || ComparadorObject.compare(datos[posicionB], info) != 0) {
                return (posicionB+1)*-1;
            } else {
                return posicionB+1;
            }
        }
    }

    @Override
    public int agregar(Object info) {
        if (lleno()) {
            return -1;
        }
        int posicionI = (Integer) buscar(info);
        if (posicionI > 0) {
            return -1;
        }
        posicionI = posicionI*-1;
        posicionI--;

        limite++;

        for (int i = limite; i > posicionI; i--) {
            datos[i] = datos[i-1];
        }

        datos[posicionI] = info;
        return posicionI;
    }

    @Override
    public boolean cambiar(Object infoVieja, Object infoNueva, int numOcurrencias) {
        boolean cambio = false;
        while (numOcurrencias > 0) {
            Object elemBorrado = eliminar(infoVieja);
            if (elemBorrado == null) {
                break;
            }
            int posicionAgregado = agregar(infoNueva);
            if (posicionAgregado != -1){
                cambio = true;
            }
            numOcurrencias--;
        }

        return cambio;
    }

    @Override
    public boolean cambiar(int indice, Object info) {
        boolean cambio = false;
        Object elemBorrado = eliminar(indice);
        if (elemBorrado == null) {
            return false;
        }
        int posicionAgregado = agregar(info);
        if (posicionAgregado != -1){
            cambio = true;
        }

        return cambio;
    }

    @Override
    public Object eliminar(Object info) {
        int posicionE = (Integer) buscar(info);
        if (posicionE < 0) {
            return null;
        }
        posicionE--;
        return super.eliminar(posicionE);
    }

    @Override
    public boolean agregarLista(Object lista2) {
        return super.agregarLista(lista2);
    }

    @Override
    public void invertir() {
        orden = (orden == TipoOrden.CRECIENTE)?TipoOrden.DECRECIENTE:TipoOrden.CRECIENTE;
        super.invertir();
    }

    /**
     * Crea un Arreglo desordenado a partir de los elementos del ArregloOrdenado actual.
     * @return Regresa el Arreglo desordenado creado.
     */
    public Object arregloDesordenado() {
        Arreglo desordenado = new Arreglo(MAXIMO);
        int mitad = limite/2;
        for (int i = mitad; i < numElementos(); i++) {
            desordenado.agregar(obtener(i));
        }
        for (int i = 0; i < mitad; i++) {
            desordenado.agregar(obtener(i));
        }
        return desordenado;
    }

    /**
     * Determina si lista2 es una sub lista del arreglo.
     * @param lista2 Es la sublista a examinar.
     * @return Regresa <b>true</b> si lista2 es una sublista del arreglo, <b>false</b> en caso contrario.
     */
    public boolean esSubLista(Object lista2) {
        Arreglo lista = (Arreglo) lista2;
        int inicio = (Integer) buscar(lista.obtener(0));
        if (inicio < 0) {
            return false;
        }
        inicio--;
        Arreglo subLista = (Arreglo) subLista(inicio, inicio+lista.numElementos()-1);
        if (subLista == null) {
            return false;
        }
        subLista.redimensionar(lista.longitud());
        return lista.esIgual(subLista);
    }

    /**
     * Cambia los elementos de lista2 que se encuentren en el arreglo y los reemplaza con los elementos de lista2Cambios.
     * @param lista2 Es la lista que contiene los elementos que se reemplazaran.
     * @param lista2Cambios Es la lista que contiene los nuevos elementos.
     * @return Regresa <b>true</b> si se cambio al menos un elemento, <b>false</b> en caso contrario.
     */
    public boolean cambiarLista(Object lista2, Object lista2Cambios) {
        Arreglo arreglo2 = (Arreglo) lista2;
        Arreglo arreglo2Cambios = (Arreglo) lista2Cambios;
        if (arreglo2.numElementos() != arreglo2Cambios.numElementos()) {
            return false;
        }
        boolean cambio = false;

        for (int i = 0; i < arreglo2.numElementos(); i++) {
            if (cambiar(arreglo2.obtener(i), arreglo2Cambios.obtener(i), 1)) {
                cambio = true;
            }
        }

        return cambio;
    }

    /**
     * Elimina del arreglo todos los elementos que no esten contenidos en lista2.
     * @param lista2 Es la lista con los elementos a conservar.
     * @return Regresa <b>true</b> si se elimino al menos un elemento, <b>false</b> en caso contrario;
     */
    public boolean retenerLista(Object lista2) {
        Arreglo lista = (Arreglo) lista2;
        Arreglo clon = (Arreglo) clonar();
        boolean cambio = false;
        for (int i = 0; i < clon.numElementos(); i++) {
            if ((Integer) lista.buscar(clon.obtener(i)) < 0) {
                eliminar(clon.obtener(i));
            }
        }
        return cambio;
    }

    public boolean insertar(int indice, Object info) {
        if (lleno()) {
            return false;
        }
        if (indice > limite || indice < 0) {
            return false;
        }

        int posicion = (Integer) buscar(info);

        if (posicion > 0) {
            return false;
        } else {
            posicion *= -1;
            posicion--;
            if (indice == posicion) {
                agregar(info);
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public boolean copiarArreglo(Arreglo arreglo2) {
        if (arreglo2 instanceof ArregloOrdenado) {
            ArregloOrdenado arreglo = (ArregloOrdenado) arreglo2;
            if (arreglo.getOrden() == orden) {
                return super.copiarArreglo(arreglo2);
            } else {
                return false;
            }
        } else {
            return super.copiarArreglo(arreglo2);
        }
    }
}
