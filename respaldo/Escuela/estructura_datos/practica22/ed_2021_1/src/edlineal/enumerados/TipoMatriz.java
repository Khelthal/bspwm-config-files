package edlineal.enumerados;

/**
 * Esta clase enumerada indica si una matriz debe recorrerse por renglones o por columnas.
 * */
public enum TipoMatriz {
  COLUMNA,
  RENGLON
}
