package edlineal;

import comparadores.ComparadorObject;
import edlineal.catalogo.Nodo;
import edlineal.enumerados.TipoMatriz;
import ednolineal.Matriz2D;
import entradasalida.SalidaEstandar;

/**
 * Esta clase almacena elementos de forma dinamica.
 */
public class ListaLigada implements Lista {
  protected Nodo primero; // es obligatoria
  protected Nodo ultimo; // es opcional
  protected Nodo iterador;

  /**
   * Crea una nueva instancia de ListaLigada.
   */
  public ListaLigada() {
    primero = null;
    ultimo = null;
  }

  /**
   * Obtiene el elemento almacenado en el primer nodo de la lista.
   * 
   * @return Regresa el elemento almacenado en el primer nodo de la lista.
   */
  public Object getPrimero() {
    if (primero == null) {
      return null;
    }
    return primero.getInfo();
  }

  /**
   * Obtiene el elemento almacenado en el ultimo nodo de la lista.
   * 
   * @return Regresa el elemento almacenado en el ultimo nodo de la lista.
   */
  public Object getUltimo() {
    if (ultimo == null) {
      return null;
    }
    return ultimo.getInfo();
  }

  @Override
  public boolean vacia() {
    if (primero == null) {
      return true;
    } else {
      return false;
    }
  }

  @Override
  public int agregar(Object info) {
    Nodo nuevoNodo = new Nodo(info);

    if (nuevoNodo != null) { // que si puedo reservar memoria la MVJ
      if (vacia() == true) { // vcuando no hay nada
        primero = nuevoNodo;
        ultimo = nuevoNodo;
      } else { // cuando ya hay elementos
        ultimo.setLigaDer(nuevoNodo);
        ultimo = nuevoNodo;
      }
      return 1;
    } else { // que no hay más espacio o hay error
      return -1;
    }
  }

  public int agregarInicio(Object info) {
    Nodo nuevoNodo = new Nodo(info);

    if (nuevoNodo != null) {// si hay espacio
      if (vacia() == true) { // no hay nada
        primero = nuevoNodo;
        ultimo = nuevoNodo;
      } else { // hay varios
        nuevoNodo.setLigaDer(primero);
        primero = nuevoNodo;
      }
      return 1;
    } else { // no hay espacio
      return -1;
    }
  }

  @Override
  public void imprimir() {
    Nodo temp = primero;

    while (temp != null) {
      SalidaEstandar.consola(temp.getInfo() + " -> ");
      temp = temp.getLigaDer();
    }
    SalidaEstandar.consola("null");
  }

  public Object eliminarInicio() {
    Object elementoBorrado = null;

    if (vacia() == false) { // no está vacía
      elementoBorrado = primero.getInfo();// 1
      if (primero == ultimo) { // único, b)
        // elementoBorrado=primero.getInfo();//1
        primero = null;// 2
        ultimo = null;
      } else { // varios c)
        // elementoBorrado=primero.getInfo();//1
        primero = primero.getLigaDer(); // 2
      }
      return elementoBorrado;
    } else { // está vacía a)
      return null;
    }
  }

  @Override
  public void imprimirOI() {
    PilaLista pilaLista = new PilaLista();
    Nodo recorrer = primero;
    while (recorrer != null) {
      pilaLista.poner(recorrer);
      recorrer = recorrer.getLigaDer();
    }
    while (!pilaLista.vacio()) {
      SalidaEstandar.consola(pilaLista.quitar() + " -> ");
    }
    SalidaEstandar.consola("null");
  }

  @Override
  public Object buscar(Object info) {
    Nodo recorrer = primero;

    while (recorrer != null && !recorrer.getInfo().toString().equalsIgnoreCase(info.toString())) {
      recorrer = recorrer.getLigaDer();
    }
    if (recorrer == null) {
      return null;
    } else {
      return recorrer.getInfo();
    }
  }

  @Override
  public Object eliminar(Object info) {
    Object elementoBorrado = null;
    if (vacia() == false) { // no está vacía, hay algo
      Nodo anterior = primero;
      Nodo encontrado = primero;
      while (encontrado != null && !encontrado.getInfo().toString().equalsIgnoreCase(info.toString())) {
        anterior = encontrado;
        encontrado = encontrado.getLigaDer();
      }
      if (encontrado == null) { // no existe, f)
        return null;
      } else { // si existe,
        elementoBorrado = encontrado.getInfo(); // 1
        if (primero == ultimo) { // único, b)
          primero = null; // 2
          ultimo = null;
        } else if (primero == encontrado) { // c(, el primero
          primero = primero.getLigaDer(); // 2
        } else if (ultimo == encontrado) { // d), el último
          anterior.setLigaDer(null);// 2
          ultimo = anterior;// 3
        } else { // cualquier otro caso, es decir, en medio, e)
          Nodo siguiente = encontrado.getLigaDer();
          anterior.setLigaDer(siguiente);// 2
        }
        return elementoBorrado;
      }
    } else { // está vacía la lista, a)
      return null;
    }
  }

  @Override
  public Object eliminar() {
    Object elementoBorrado = null;
    if (vacia() == false) { // hay algo
      elementoBorrado = ultimo.getInfo(); // 1
      if (primero == ultimo) { // único b)
        // elementoBorrado=ultimo.getInfo(); //1
        primero = null;// 2
        ultimo = null;
      } else { // hay varios c)
        // elementoBorrado=ultimo.getInfo(); //1
        Nodo penultimo = primero;
        while (penultimo.getLigaDer() != ultimo) {
          penultimo = penultimo.getLigaDer();
        }
        ultimo = penultimo;// 2
        ultimo.setLigaDer(null);// 3
      }
      return elementoBorrado;
    } else { // vacía a)
      return null;
    }
  }

  @Override
  public boolean esIgual(Object lista2) {
    ListaLigada lista = (ListaLigada) lista2;
    if (vacia()) {
      return lista.vacia();
    } else {
      if (lista.vacia()) {
        return false;
      }
    }
    Nodo recorrer = primero;
    int indice = 0;

    while (recorrer != null) {
      if (ComparadorObject.compare(recorrer.getInfo(), lista.obtener(indice)) != 0) {
        return false;
      }
      indice++;
      recorrer = recorrer.getLigaDer();
    }

    if (lista.obtener(indice) != null) {
      return false;
    }

    return true;
  }

  @Override
  public boolean cambiar(Object infoVieja, Object infoNueva, int numOcurrencias) {
    boolean cambio = false;
    Nodo recorrer = primero;
    while (recorrer != null) {
      if (numOcurrencias <= 0) {
        break;
      }
      if (ComparadorObject.compare(recorrer.getInfo(), infoVieja) == 0) {
        recorrer.setInfo(infoNueva);
        cambio = true;
        numOcurrencias--;
      }
      recorrer = recorrer.getLigaDer();
    }
    return cambio;
  }

  @Override
  public Arreglo buscarValores(Object info) {
    ListaLigada indices = new ListaLigada();
    if (vacia()) {
      return null;
    }
    Nodo recorrer = primero;
    int actual = 0;

    while (recorrer != null) {
      if (ComparadorObject.compare(recorrer.getInfo(), info) == 0) {
        indices.agregar(actual);
      }
      actual++;
      recorrer = recorrer.getLigaDer();
    }

    return indices.aArreglo();
  }

  @Override
  public void vaciar() {
    primero = null;
    ultimo = null;
  }

  @Override
  public boolean agregarLista(Object lista2) {
    Arreglo arreglo = (Arreglo) lista2;
    boolean logrado = true;

    for (int i = 0; i < arreglo.numElementos(); i++) {
      if (agregar(arreglo.obtener(i)) == -1) {
        logrado = false;
      }
    }

    return logrado;
  }

  @Override
  public void invertir() {
    if (vacia()) {
      return;
    }
    ColaLista colaLista = new ColaLista();
    while (!vacia()) {
      colaLista.poner(eliminar());
    }

    while (!colaLista.vacio()) {
      agregar(colaLista.quitar());
    }
  }

  @Override
  public int contar(Object info) {
    int contador = 0;
    Nodo recorrer = primero;
    while (recorrer != null) {
      if (ComparadorObject.compare(recorrer.getInfo(), info) == 0) {
        contador++;
      }
      recorrer = recorrer.getLigaDer();
    }
    return contador;
  }

  @Override
  public boolean eliminarLista(Object lista2) {
    boolean cambiado = false;
    Arreglo lista = (Arreglo) lista2;

    for (int i = 0; i < lista.numElementos(); i++) {
      if (eliminar(lista.obtener(i)) != null) {
        cambiado = true;
      }
    }

    return cambiado;
  }

  @Override
  public void rellenar(Object info, int cantidad) {
    for (int i = 0; i < cantidad; i++) {
      agregar(info);
    }
  }

  @Override
  public void rellenar(Object info) {
    Nodo recorrer = primero;
    while (recorrer != null) {
      if (recorrer.getInfo() == null) {
        recorrer.setInfo(info);
      }
      recorrer = recorrer.getLigaDer();
    }
  }

  @Override
  public Object clonar() {
    ListaLigada copia = new ListaLigada();
    Nodo recorrer = primero;
    while (recorrer != null) {
      copia.agregar(recorrer.getInfo());
      recorrer = recorrer.getLigaDer();
    }

    return copia;
  }

  @Override
  public Object subLista(int indiceInicial, int indiceFinal) {
    if (vacia() || indiceInicial < 0) {
      return null;
    }

    Nodo recorrer = primero;
    int actual = 0;
    ListaLigada subLista = new ListaLigada();

    while (recorrer != null) {
      if (actual >= indiceInicial && actual <= indiceFinal) {
        subLista.agregar(recorrer.getInfo());
      }
      actual++;
      recorrer = recorrer.getLigaDer();
    }

    if (subLista.vacia()) {
      return null;
    }
    return subLista;
  }

  @Override
  public boolean insertar(int indice, Object info) {
    if (indice < 0) {
      return false;
    }

    Nodo encontrado = primero;
    Nodo anterior = primero;
    int actual = -1;

    while (encontrado != null && actual != indice) {
      actual++;
      if (actual != indice) {
        anterior = encontrado;
        encontrado = encontrado.getLigaDer();
      }
    }

    Nodo nuevoNodo = new Nodo(info);
    if (nuevoNodo == null) {
      return false;
    }

    if (actual != indice) {
      if (actual == -1) {
        primero = nuevoNodo;
        ultimo = nuevoNodo;
        return true;
      }
      else if (indice == actual+1) {
        ultimo.setLigaDer(nuevoNodo);
        ultimo = nuevoNodo;
      }
      return false;
    } else {
      if (primero == ultimo) {
        nuevoNodo.setLigaDer(primero);
        ultimo = primero;
        primero = nuevoNodo;
      } else if (primero == encontrado) {
        nuevoNodo.setLigaDer(primero);
        primero = nuevoNodo;
      } else if (ultimo == encontrado) {
        anterior.setLigaDer(nuevoNodo);
        nuevoNodo.setLigaDer(ultimo);
      } else {
        anterior.setLigaDer(nuevoNodo);
        nuevoNodo.setLigaDer(encontrado);
      }
      return true;
    }
  }

  public void inicializarIterador() {
    iterador = primero;
  }

  public boolean hayMas() {
    if (iterador != null) {
      return true;
    } else {
      return false;
    }
  }

  public Object obtenerSigiuente() {
    if (hayMas() == true) {
      Object info = iterador.getInfo();
      iterador = iterador.getLigaDer();
      return info;
    } else {
      return null;
    }
  }

  /**
   * Guarda los elementos de la ListaLigada en un arreglo.
   * 
   * @return Regresa un arreglo con los elementos de la lista ligada.
   */
  public Arreglo aArreglo() {
    Arreglo elementos;
    int contador = 0;
    Nodo recorrer;
    recorrer = primero;

    while (recorrer != null) {
      contador++;
      recorrer = recorrer.getLigaDer();
    }
    elementos = new Arreglo(contador);
    recorrer = primero;

    while (recorrer != null) {
      elementos.agregar(recorrer.getInfo());
      recorrer = recorrer.getLigaDer();
    }

    return elementos;
  }

  /**
   * Guarda los elementos de la ListaLigada en un arreglo, excepto aquellos que
   * esten contenidos en arregloADescartar.
   * 
   * @param arregloADescartar Es un arreglo con elementos que no deben ser
   *                          incluidos en el arreglo generado.
   * @return Regresa un arreglo con los elementos de la lista ligada, excepto
   *         aquellos que esten contenidos en arregloADescartar.
   */
  public Arreglo aArreglo(Arreglo arregloADescartar) {
    Arreglo elementos;
    int contador = 0;
    Nodo recorrer;
    recorrer = primero;

    while (recorrer != null) {
      if (arregloADescartar.buscar(recorrer.getInfo()) == null) {
        contador++;
      }
      recorrer = recorrer.getLigaDer();
    }
    elementos = new Arreglo(contador);
    recorrer = primero;

    while (recorrer != null) {
      if (arregloADescartar.buscar(recorrer.getInfo()) == null) {
        elementos.agregar(recorrer.getInfo());
      }
      recorrer = recorrer.getLigaDer();
    }

    return elementos;
  }

  /**
   * Guarda los elementos de la ListaLigada en una Matriz2D.
   * 
   * @param renglones Es el numero de renglones de la Matriz2D.
   * @param columnas  Es el numero de columnas de la Matriz2D.
   * @return Regresa una Matriz2D con los elementos de la ListaLigada.
   */
  public Matriz2D aMatriz2D(int renglones, int columnas) {
    Matriz2D matriz2d = new Matriz2D(renglones, columnas);
    Nodo recorrer = primero;
    for (int renglon = 0; renglon < matriz2d.renglones(); renglon++) {
      for (int columna = 0; columna < matriz2d.columnas(); columna++) {
        if (recorrer == null) {
          break;
        }
        matriz2d.cambiarInfo(renglon, columna, recorrer.getInfo());
        recorrer = recorrer.getLigaDer();
      }
    }

    return matriz2d;
  }

  /**
   * Agrega los elementos de una Matriz2D al final de la ListaLigada.
   * 
   * @param matriz         Es la matriz de la cual se obtendran los elementos.
   * @param enumTipoMatriz Indica si los elementos se agregaran columna por
   *                       columna o renglon por renglon.
   * @return Regresa <b>true</b> si se pudieron agregar todos los elementos de la
   *         lista ligada, <b>false</b> en caso contrario.
   */
  public boolean agregarMatriz2D(Matriz2D matriz, TipoMatriz enumTipoMatriz) {
    boolean logrado = true;
    if (enumTipoMatriz == TipoMatriz.COLUMNA) {
      for (int columna = 0; columna < matriz.columnas(); columna++) {
        for (int renglon = 0; renglon < matriz.renglones(); renglon++) {
          if (agregar(matriz.obtenerInfo(renglon, columna)) == -1) {
            logrado = false;
          }
        }
      }
    } else {
      for (int renglon = 0; renglon < matriz.renglones(); renglon++) {
        for (int columna = 0; columna < matriz.columnas(); columna++) {
          if (agregar(matriz.obtenerInfo(renglon, columna)) == -1) {
            logrado = false;
          }
        }
      }
    }
    return logrado;
  }

  /**
   * Cambia el elemento de la posicion especificada por un nuevo elemento.
   * 
   * @param indice Es la posicion donde se cambiara el elemento.
   * @param info   Es el nuevo elemento.
   * @return Regresa <b>true</b> si se pudo hacer el cambio, <b>false</b> en caso
   *         contrario.
   */
  public boolean cambiar(int indice, Object info) {
    if (indice < 0) {
      return false;
    }

    Nodo recorrer = primero;
    int actual = -1;

    while (recorrer != null && actual != indice) {
      actual++;
      if (actual != indice) {
        recorrer = recorrer.getLigaDer();
      }
    }

    if (actual != indice) {
      return false;
    } else {
      recorrer.setInfo(info);
      return true;
    }
  }

  /**
   * Obtiene el elemento de la posicion especificada.
   * 
   * @param indice Es la posicion de la cual se obtendra el elemento.
   * @return Regresa el elemento si pudo encontrarlo, null en caso contrario.
   */
  public Object obtener(int indice) {
    if (indice < 0) {
      return null;
    }

    Nodo recorrer = primero;
    int actual = -1;

    while (recorrer != null && actual != indice) {
      actual++;
      if (actual != indice) {
        recorrer = recorrer.getLigaDer();
      }
    }

    if (actual != indice) {
      return null;
    } else {
      return recorrer.getInfo();
    }
  }

  /**
   * Redimensiona el tamanio de la ListaLigada.
   * 
   * @param maximo Es el nuevo tamanio de la ListaLigada.
   * @return Regresa una ListaLigada con los elementos que fueron borrados, null
   *         en caso contrario.
   */
  public Object redimensionar(int maximo) {
    Nodo recorrer = primero;
    int tamanio = 0;

    while (recorrer != null) {
      tamanio++;
      recorrer = recorrer.getLigaDer();
    }

    if (tamanio > maximo) {
      ListaLigada eliminados = new ListaLigada();
      while (tamanio > maximo) {
        eliminados.agregar(eliminar());
        tamanio--;
      }
      eliminados.invertir();
      return eliminados;
    } else if (tamanio < maximo) {
      while (tamanio < maximo) {
        agregar(null);
        tamanio++;
      }
    }
    return null;
  }

  /**
   * Elimina el elemento en la posicion especificada.
   * @param indice Es la posicion del elemento a eliminar.
   * @return Regresa el elemento eliminado.
   */
  public Object eliminar(int indice) {
    if (indice < 0) {
      return null;
    }

    Nodo encontrado = primero;
    Nodo anterior = primero;
    int actual = -1;

    while (encontrado != null && actual != indice) {
      actual++;
      if (actual != indice) {
        anterior = encontrado;
        encontrado = encontrado.getLigaDer();
      }
    }

    if (actual != indice) {
      return null;
    } else {
      Object elementoBorrado = encontrado.getInfo(); // 1
      if (primero == ultimo) { // único, b)
        primero = null; // 2
        ultimo = null;
      } else if (primero == encontrado) { // c(, el primero
        primero = primero.getLigaDer(); // 2
      } else if (ultimo == encontrado) { // d), el último
        anterior.setLigaDer(null);// 2
        ultimo = anterior;// 3
      } else { // cualquier otro caso, es decir, en medio, e)
        Nodo siguiente = encontrado.getLigaDer();
        anterior.setLigaDer(siguiente);// 2
      }
      return elementoBorrado;
    }

  }

}
