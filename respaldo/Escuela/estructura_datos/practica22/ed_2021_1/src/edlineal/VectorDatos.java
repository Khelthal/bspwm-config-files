package edlineal;

/**
 * Esta interface administra las operaciones sobre vectores.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public interface VectorDatos extends Lista {
    /**
     * Determina si el arreglo esta lleno.
     * @return Regresa <b>true</b> si el arreglo esta lleno, false en caso contrario.
     */
    public boolean lleno();

    /**
     * Obtiene el tamanio maximo del arreglo.
     * @return Regresa el tamanio maximo del arreglo.
     */
    public int longitud();

    /**
     * Obtiene el numero de elementos contenidos en el arreglo.
     * @return Regresa el numero de elementos contenidos en el arreglo.
     */
    public int numElementos();

    /**
     * Regresa el elemento ubicado en el indice indicado.
     * @param indice Es el indice del elemento a regresar.
     * @return Regresa un Objeto si el indice es valido, <b>null</b> en caso contrario.
     */
    public Object obtener(int indice);

    /**
     * Reemplaza el Objeto de la posicion indicada con un nuevo Objeto.
     * @param indice Es el indice donde se va a reemplazar el Objeto.
     * @param info Es el objeto a insertar.
     * @return Regresa true si pudo realizar el cambio, <b>false</b> en caso contrario.
     */
    public boolean cambiar(int indice, Object info);

    /**
     * Elimina el Objeto en la posicion indicada.
     * @param indice Es la posicion donde se borrara el Objeto.
     * @return Regresa el objeto eliminado si pudo borrar alguno, <b>null</b> en caso contrario.
     */
    public Object eliminar(int indice);

    /**
     * Redimensiona el arreglo.
     * @param maximo Es el nuevo tamanio del arreglo.
     */
    public void redimensionar(int maximo);

    /**
     * Copia el contenido del arreglo recibido al arreglo actual.
     * @param arreglo2 Es el arreglo del cual se copiara el contenido.
     * @return Regresa la copia del arreglo.
     */
    public boolean copiarArreglo(Arreglo arreglo2);
}
