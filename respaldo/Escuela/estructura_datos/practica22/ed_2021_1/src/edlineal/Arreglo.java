package edlineal;

import entradasalida.SalidaEstandar;

/**
 * Esta clase maneja arreglos y distintas operaciones con arreglos.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class Arreglo implements VectorDatos{
    protected Object datos[];
    protected int MAXIMO;
    protected int limite;

    /**
     * Crea un nuevo arreglo con el tamanio maximo indicado.
     * @param maxima Es el tamanio maximo del arreglo.
     */
    public Arreglo(int maxima){
        MAXIMO=maxima;
        datos=new Object[MAXIMO];
        limite=-1;
    }

    @Override
    public boolean vacia(){
        if (limite==-1){
            return true;
        }else{
            return false;
        }
    }
    @Override
    public boolean lleno(){
        if (limite== (MAXIMO - 1)){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public int agregar(Object info){
        if (lleno()==false){
            limite=limite+1;
            datos[limite]=info;
            return limite;
        }else{
            return -1;
        }
    }

    @Override
    public void imprimir(){
        for(int posicion=limite;posicion>=0;posicion --){
            SalidaEstandar.consola(datos[posicion]+ "\n");
        }
    }

    @Override
    public void imprimirOI(){
        for(int posicion=0;posicion<=limite;posicion ++){
            SalidaEstandar.consola(datos[posicion]+ "\n");
        }
    }

    @Override
    public Object buscar(Object info){
        int localidad=0;
        while(localidad<=limite && !info.toString().equalsIgnoreCase(datos[localidad].toString())){
            localidad++;
        }
        if (localidad > limite) {
            return null;
        }else{
            return localidad;
        }
    }

    @Override
    public Object eliminar(Object info){
        Integer posicion=(Integer)buscar(info);
        if(posicion!=null){  //si lo encontro
            Object elementoBorrado=datos[posicion];
            limite = limite - 1;
            for(int mov=posicion; mov <= limite; mov ++){
                datos[mov]=datos[mov+1];
            }
            return elementoBorrado;
        }else{ //no lo encontroo
            return null;
        }
    }

    @Override
    public int longitud(){
        return MAXIMO;
    }

    @Override
    public int numElementos(){
        return limite+1;
    }

    @Override
    public boolean esIgual(Object lista2) {
        if (!(lista2 instanceof Arreglo)) {
            return false;
        }

        Arreglo lista = (Arreglo) lista2;
        if (MAXIMO != lista.longitud() || limite != lista.numElementos()-1) {
            return false;
        }

        for (int i = 0; i <= limite; i++) {
            if (!(obtener(i).toString().equalsIgnoreCase(lista.obtener(i).toString()))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Object obtener(int indice) {
        if (indice > limite || indice < 0) {
            return null;
        }

        return datos[indice];
    }

    @Override
    public boolean cambiar(Object infoVieja, Object infoNueva, int numOcurrencias) {
        boolean cambio = false;
        for (int i = 0; i <= limite; i++) {
            if (numOcurrencias == 0) {
                break;
            }
            if (obtener(i).toString().equalsIgnoreCase(infoVieja.toString())) {
                cambio = cambiar(i, infoNueva);
                numOcurrencias--;
            }
        }
        return cambio;
    }

    @Override
    public boolean cambiar(int indice, Object info) {
        if (indice > limite || indice < 0) {
            return false;
        }
        datos[indice] = info;
        return true;
    }

    @Override
    public Arreglo buscarValores(Object info) {
        Arreglo lista = new Arreglo(contar(info));
        if (lista.longitud() == 0) {
            return null;
        }
        for (int i = 0; i <= limite; i++) {
            if (obtener(i).toString().equalsIgnoreCase(info.toString())) {
                lista.agregar(i);
            }
        }

        return lista;
    }

    @Override
    public Object eliminar(int indice){
        if (indice > limite || indice < 0) {
            return null;
        }
        Object elementoBorrado=obtener(indice);
        for(int mov=indice; mov < limite; mov ++){
            datos[mov] = datos[mov+1];
        }
        limite = limite - 1;
        return elementoBorrado;
    }

    @Override
    public Object eliminar() {
        return eliminar(limite);
    }

    @Override
    public void vaciar() {
        while (!vacia()) {
            eliminar();
        }
    }

    @Override
    public boolean agregarLista(Object lista2) {
        if (!(lista2 instanceof Arreglo)) {
            return false;
        }
        Arreglo lista = (Arreglo) lista2;

        if (MAXIMO - (limite+1) < lista.longitud()) {
            return false;
        }

        for (int i = 0; i < lista.numElementos(); i++) {
            agregar(lista.obtener(i));
        }

        return true;
    }

    @Override
    public void invertir() {
        Arreglo copia = (Arreglo) clonar();
        vaciar();
        int contador = 0;
        for (int i = copia.numElementos()-1; i >= 0; i--) {
            agregar(copia.obtener(i));
        }
    }

    @Override
    public int contar(Object info) {
        int conteoElementos = 0;

        for (int i = 0; i <= limite; i++) {
            if (obtener(i).toString().equalsIgnoreCase(info.toString())) {
                conteoElementos++;
            }
        }

        return conteoElementos;
    }

    @Override
    public boolean eliminarLista(Object lista2) {
        if (!(lista2 instanceof Arreglo)) {
            return false;
        }

        Arreglo lista = (Arreglo) lista2;

        for (int i = 0; i < lista.numElementos(); i++) {
            Arreglo posiciones = buscarValores(lista.obtener(i));
            if (posiciones == null) {
                continue;
            }
            int desplazamientos = 0;
            for (int j = 0; j < posiciones.numElementos(); j++) {
                int eliminar = (Integer) posiciones.obtener(j);
                eliminar -= desplazamientos;
                eliminar(eliminar);
                desplazamientos++;
            }
        }

        return true;
    }

    @Override
    public void rellenar(Object info, int cantidad) {
        for (int i = 0; i < cantidad; i++) {
            agregar(info);
        }
    }

    @Override
    public void rellenar(Object info) {
        vaciar();
        if (info instanceof Integer) {
            int num = (Integer) info;
            if (num > 0) {
                for (int i = 0; i < num; i++) {
                    agregar(i+1);
                }
            } else {
                for (int i = 0; i > num; i--) {
                    agregar(i-1);
                }
            }
        } else {
            if (info instanceof Character) {
                char letra = (Character) info;
                for (char i = 'A'; i <= letra; i++) {
                    agregar(i);
                }
            } else {
                agregar(info);
            }
        }
    }

    @Override
    public Object clonar() {
        Arreglo copia = new Arreglo(MAXIMO);
        for (int i = 0; i < numElementos(); i++) {
            copia.agregar(obtener(i));
        }
        return copia;
    }

    @Override
    public Object subLista(int indiceInicial, int indiceFinal) {
        if (indiceInicial < 0 || indiceFinal > limite) {
            return null;
        }
        if (indiceInicial > indiceFinal) {
            return null;
        }
        Arreglo sublista = new Arreglo(indiceFinal-indiceInicial+1);

        for (int i = indiceInicial; i <= indiceFinal; i++) {
            sublista.agregar(obtener(i));
        }

        return sublista;
    }

    @Override
    public void redimensionar(int maximo) {
        if (maximo < 0) {
            return;
        }
        Arreglo copia = (Arreglo) clonar();
        datos = new Object[maximo];
        limite = -1;
        MAXIMO = maximo;
        for (int i = 0; i < copia.numElementos(); i++) {
            agregar(copia.obtener(i));
        }
    }

    @Override
    public boolean insertar(int indice, Object info) {
        if (lleno()) {
            return false;
        }
        if (indice > limite || indice < 0) {
            return false;
        }

        limite++;
        for (int i = limite; i > indice; i--) {
            datos[i] = datos[i-1];
        }

        datos[indice] = info;
        return true;
    }

    @Override
    public boolean copiarArreglo(Arreglo arreglo2) {
        Arreglo lista = (Arreglo) arreglo2;
        if (lista.longitud() != longitud()) {
            return false;
        }

        redimensionar(lista.longitud());
        vaciar();
        agregarLista(arreglo2);
        return true;
    }

    /**
     * Crea una sublista con los elementos de los indices contenidos en arregloIndices.
     * @param arregloIndices Es el arreglo con buffer2.vaciar();las posiciones de los elementos de la sublista.
     * @return Regresa la sublista con los elementos de las posiciones indicadas.
     */
    public Arreglo subLista(ArregloNumerico arregloIndices) {
        Arreglo arreglo = new Arreglo(arregloIndices.numElementos());
        for (int i = 0; i < arregloIndices.numElementos(); i++) {
            Object o = obtener((int) arregloIndices.obtener(i));
            if (o == null) {
                continue;
            }
            arreglo.agregar(o);
        }
        return arreglo;
    }

    /**
     * Cambia el contenido de datos por el contenido del arreglo.
     * @param arreglo Es el arreglo que contiene los nuevos datos.
     */
    public void recibirArreglo(Object[] arreglo) {
        vaciar();
        redimensionar(arreglo.length);
        for (int i = 0; i < arreglo.length; i++) {
            agregar(arreglo[i]);
        }
    }

    /**
     * Obtiene una copia de datos.
     * @return Regresa una copia de datos.
     */
    public Object[] leerArreglo() {
        Object[] arreglo = new Object[numElementos()];
        for (int i = 0; i < numElementos(); i++) {
            arreglo[i] = datos[i];
        }
        return arreglo;
    }

    @Override
    public String toString() {
        String elementos = "[";
        for (int i = 0; i < numElementos()-1; i++) {
            elementos += datos[i];
            elementos += ", ";
        }

        if (numElementos() > 0) {
            elementos += datos[numElementos()-1];
        }
        elementos += "]";
        return elementos;
    }
}
