package ednolineal;

import edlineal.Arreglo;
import edlineal.ArregloNumerico;
import static java.lang.Math.pow;
import static java.lang.Math.log;
import static java.lang.Math.log10;
import entradasalida.SalidaEstandar;

/**
 * Esta clase maneja arreglos de 2 dimensiones con datos numericos y distintas operaciones con arreglos de 2 dimensiones con datos numericos.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class Matriz2DNumerica extends Matriz2D {
    public Matriz2DNumerica(int reng, int colum) {
        super(reng, colum, 0.0);
    }

    public Matriz2DNumerica(int reng, int colum, Object info) {
        super(reng, colum, (info instanceof Number)?info:0.0);
    }

    @Override
    public boolean rellenar(Object info) {
        if (info instanceof Number){
            info = numberToDouble((Number) info);
            return super.rellenar(info);
        }
        return false;
    }

    @Override
    public boolean cambiarInfo(int renglon, int columna, Object info) {
        if (info instanceof Number) {
            info = numberToDouble((Number) info);
            return super.cambiarInfo(renglon, columna, info);
        }
        return false;
    }

    @Override
    public Number obtenerInfo(int renglon, int columna) {
        return (Number) super.obtenerInfo(renglon, columna);
    }

    @Override
    public boolean redefinirMatriz(Matriz2D matriz2) {
        if (matriz2 instanceof Matriz2DNumerica) {
            return super.redefinirMatriz(matriz2);
        }
        return false;
    }

    @Override
    public boolean agregarRenglon(Arreglo arreglo) {
        if (arreglo instanceof ArregloNumerico) {
            return super.agregarRenglon(arreglo);
        }
        return false;
    }

    @Override
    public boolean agregarColumna(Arreglo arreglo) {
        if (arreglo instanceof ArregloNumerico) {
            return super.agregarColumna(arreglo);
        }
        return false;
    }

    @Override
    public boolean agregarMatrizColumna(Matriz2D matriz2) {
        if (matriz2 instanceof Matriz2DNumerica) {
            return super.agregarMatrizColumna(matriz2);
        }
        return false;
    }

    @Override
    public boolean agregarMatrizRenglon(Matriz2D matriz2) {
        if (matriz2 instanceof  Matriz2DNumerica) {
            return super.agregarMatrizRenglon(matriz2);
        }
        return false;
    }

    @Override
    public Matriz3D aMatriz3D(Arreglo matrices) {
        for (int i = 0; i < matrices.numElementos(); i++) {
            if (!(matrices.obtener(i) instanceof Matriz2DNumerica)) {
                return null;
            }
        }
        return super.aMatriz3D(matrices);
    }

    @Override
    public Matriz2DNumerica clonar() {
        Matriz2DNumerica copia = new Matriz2DNumerica(renglones, columnas);
        for (int renglon = 0; renglon < renglones; renglon++) {
            for (int columna = 0; columna < columnas; columna++) {
                copia.cambiarInfo(renglon, columna, datos[renglon][columna]);
            }
        }
        return copia;
    }

    /**
     * Convierte enteros y flotantes a Double.
     * @param n Es el numero a ser convertido.
     * @return Regresa el numero convertido a Double.
     */
    public Number numberToDouble(Number n) {
        if (n instanceof Integer) {
            int pivote = (int) n;
            Double nuevo = (double) pivote;
            n = nuevo;
        }

        if (n instanceof Float) {
            float pivote = (float) n;
            Double nuevo = (double) pivote;
            n = nuevo;
        }
        return n;
    }

    /**
     * Multiplica cada elemento de la matriz por el escalar dado.
     * @param escalar Es el escalar que multiplicara a los elementos de la matriz.
     */
    public void porEscalar(double escalar) {
        for (int renglon = 0; renglon < renglones; renglon++) {
            for (int columna = 0; columna < columnas; columna++) {
                datos[renglon][columna] = (double) datos[renglon][columna] * escalar;
            }
        }
    }

    /**
     * Multiplica a la matriz por otra matriz.
     * @param matriz2 Es la matriz con la que se va a multiplicar.
     * @return Regresa <b>true</b> si fue posible hacer la multiplicacion, <b>false</b> en caso contrario.
     */
    public boolean porMatriz(Matriz2DNumerica matriz2) {
        if (matriz2 == null) {
            return false;
        }
        if (columnas != matriz2.renglones()) {
            return false;
        }

        Matriz2DNumerica resultado = new Matriz2DNumerica(renglones, matriz2.columnas());

        for (int renglonA = 0; renglonA < renglones; renglonA++) {
            for (int columnaB = 0; columnaB < matriz2.columnas(); columnaB++) {
                double suma = 0.0;
                for (int columnaA = 0; columnaA < columnas; columnaA++) {
                    suma += (double) datos[renglonA][columnaA] * (double) matriz2.obtenerInfo(columnaA, columnaB);
                }
                resultado.cambiarInfo(renglonA, columnaB, suma);
            }
        }

        redefinirMatriz(resultado);
        return true;
    }

    /**
     * Suma a los elementos de la matriz actual los elementos de otra matriz.
     * @param matriz2 Es la matriz que se va a sumar.
     * @return Regresa <b>true</b> si se pudo hacer la suma, <b>false</b> en caso contrario.
     */
    public boolean sumarMatriz(Matriz2DNumerica matriz2) {
        if (matriz2 == null) {
            return false;
        }
        if (renglones != matriz2.renglones() || columnas != matriz2.columnas()) {
            return false;
        }

        for (int renglon = 0; renglon < renglones; renglon++) {
            for (int columna = 0; columna < columnas; columna++) {
                datos[renglon][columna] = (double) matriz2.obtenerInfo(renglon, columna) + (double) datos[renglon][columna];
            }
        }
        return true;
    }

    /**
     * Eleva cada elemento de la matriz a la potencia indicada.
     * @param escalar Es la potencia a la que se elevaran los elementos de la matriz.
     */
    public void aplicarPotencia(double escalar) {
        for (int renglon = 0; renglon < renglones; renglon++) {
            for (int columna = 0; columna < columnas; columna++) {
                datos[renglon][columna] = pow((double) datos[renglon][columna], escalar);
            }
        }
    }

    /**
     * Aplica un logaritmo a cada elemento de la matriz.
     * @param tipoLog Es el tipo de logaritmo que se va a aplicar.
     * @return Regresa <b>true</b> si se pudo aplicar el logaritmo, <b>false</b> en caso contrario.
     */
    public boolean aplicarLog(TipoLog tipoLog) {
        for (int renglon = 0; renglon < renglones; renglon++) {
            for (int columna = 0; columna < columnas; columna++) {
                if ((double) datos[renglon][columna] <= 0.0) {
                    return false;
                }
            }
        }
        if (tipoLog == TipoLog.NATURAL) {
            for (int renglon = 0; renglon < renglones; renglon++) {
                for (int columna = 0; columna < columnas; columna++) {
                    datos[renglon][columna] = log((double) datos[renglon][columna]);
                }
            }
        }

        if (tipoLog == TipoLog.BASE10) {
            for (int renglon = 0; renglon < renglones; renglon++) {
                for (int columna = 0; columna < columnas; columna++) {
                    datos[renglon][columna] = log10((double) datos[renglon][columna]);
                }
            }
        }

        if (tipoLog == TipoLog.BASE2) {
            for (int renglon = 0; renglon < renglones; renglon++) {
                for (int columna = 0; columna < columnas; columna++) {
                    datos[renglon][columna] = log((double) datos[renglon][columna])/log(2.0);
                }
            }
        }

        return true;
    }

    /**
     * Convierte a una matriz cuadrada en una matriz diagonal.
     * @param info Es el objeto que llenara la diagonal de la matriz.
     */
    public void matrizDiagonal(Object info) {
        if (columnas != renglones) {
            return;
        }
        if (!(info instanceof Number)) {
            return;
        }
        info = numberToDouble((Number) info);
        rellenar(0);

        for (int renglon = 0; renglon < renglones; renglon ++) {
            for (int columna = 0; columna < columnas; columna++) {
                if (renglon == columna) {
                    datos[renglon][columna] = info;
                } else {
                    datos[renglon][columna] = 0.0;
                }
            }
        }
    }

    /**
     * Determina si la matriz cuadrada es una matriz diagonal superior.
     * @return Regresa <b>true</b> si la matriz es una matriz diagonal superior, <b>false</b> en caso contrario.
     */
    public boolean esDiagonalSup() {
        if (columnas != renglones) {
            return false;
        }

        for (int renglon = 1; renglon < renglones; renglon++) {
            for (int columna = 0; columna < renglon; columna++) {
                if ((double) datos[renglon][columna] != 0.0) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Determina si la matriz cuadrada es una matriz diagonal inferior.
     * @return Regresa <b>true</b> si la matriz es una matriz diagonal inferior, <b>false</b> en caso contrario.
     */
    public boolean esDiagonalInf() {
        if (columnas != renglones) {
            return false;
        }

        for (int renglon = 0; renglon < renglones-1; renglon++) {
            for (int columna = renglon+1; columna < columnas; columna++) {
                if ((double) datos[renglon][columna] != 0.0) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Eleva la matriz a la potencia indicada.
     * @param exponente Es el exponente al que se elevara la matriz.
     * @return Regresa <b>true</b> si se pudo elevar la matriz a la potencia indicada, <b>false</b> en caso contrario.
     */
    public boolean potencia(int exponente) {
        if (exponente < 1) {
            return false;
        }
        if (columnas != renglones) {
            return false;
        }
        Matriz2DNumerica copia = clonar();
        copia.redefinirMatriz(this);

        for (int i = 1; i < exponente; i++) {
            porMatriz(copia);
        }
        return true;
    }

    /**
     * Reduce el numero de columnas de la matriz a la mitad, sumando las columnas que estan juntas.
     * @return Regresa <b>true</b> si fue posible doblar el ancho de la matriz, <b>false</b> en caso contrario.
     */
    public boolean doblarAncho() {
        if (columnas <= 1) {
            return false;
        }

        if (columnas%2 == 0) {
            for (int columna = 0; columna < columnas; columna++) {
                for (int renglon = 0; renglon < renglones; renglon++) {
                    datos[renglon][columna] = (double) datos[renglon][columna] + (double) datos[renglon][columna+1];
                }
                eliminarColumna(columna+1);
            }
        } else {
            int mitad = columnas/2;
            for (int columna = 0; columna < columnas; columna++) {
                for (int renglon = 0; renglon < renglones; renglon++) {
                    if (columna == mitad || columna == mitad-1) {
                        break;
                    }
                    datos[renglon][columna] = (double) datos[renglon][columna] + (double) datos[renglon][columna+1];
                }
                if (columna == mitad || columna == mitad-1) {
                    continue;
                }
                eliminarColumna(columna+1);
                mitad--;
            }
        }

        return true;
    }

    /**
     * Reduce el numero de renglones de la matriz a la mitad, sumando los renglones que estan juntos.
     * @return Regresa <b>true</b> si fue posible doblar el largo de la matriz, <b>false</b> en caso contrario.
     */
    public boolean doblarLargo() {
        if (renglones <= 1) {
            return false;
        }

        if (renglones%2 == 0) {
            for (int renglon = 0; renglon < renglones; renglon++) {
                for (int columna = 0; columna < columnas; columna++) {
                    datos[renglon][columna] = (double) datos[renglon][columna] + (double) datos[renglon+1][columna];
                }
                eliminarRenglon(renglon+1);
            }
        } else {
            int mitad = renglones/2;
            for (int renglon = 0; renglon < renglones; renglon++) {
                for (int columna = 0; columna < columnas; columna++) {
                    if (renglon == mitad || renglon == mitad-1) {
                        break;
                    }
                    datos[renglon][columna] = (double) datos[renglon][columna] + (double) datos[renglon+1][columna];
                }
                if (renglon == mitad || renglon == mitad-1) {
                    continue;
                }
                eliminarRenglon(renglon+1);
                mitad--;
            }
        }

        return true;
    }
}
