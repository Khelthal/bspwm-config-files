package analisis.cadenas;

import edlineal.Arreglo;
import edlineal.ArregloNumerico;
import entradasalida.SalidaEstandar;
import entradasalida.archivos.ArchivoTexto;
import utilerias.texto.AnalizadorTexto;

/**
 * Esta clase se encarga de mostrar correcciones a texto de archivos y cadenas.
 */
public class CorrectorTexto {
    /**
     * Analiza una cadena de texto y muestra los errores que contiene.
     * @param cadena Es la cadena de texto a analizar.
     * @param inicioParrafo Indica si la cadena es un inicio de parrafo.
     */
    public static void buscarErroresCadena(String cadena, boolean inicioParrafo) {
        ArregloNumerico puntuacion = AnalizadorTexto.puntuacionIncorrecta(cadena);
        ArregloNumerico mayusculas = AnalizadorTexto.mayusculasIncorrectas(cadena);
        ArregloNumerico agrupadores = AnalizadorTexto.agrupadoresIncorrectos(cadena);
        ArregloNumerico erroresTotales = new ArregloNumerico(cadena.length());
        if (inicioParrafo) {
            if (!AnalizadorTexto.esMayuscula(cadena.charAt(0)) && AnalizadorTexto.esLetra(cadena.charAt(0))) {
                if (!mayusculas.vacia()) {
                    mayusculas.insertar(0, 0.0);
                }
                else {
                    mayusculas.agregar(0.0);
                }
            }
        }
        SalidaEstandar.consola(cadena+"\n");
        for (int i = 0; i < cadena.length(); i++) {
            int insertar = cadena.length();
            ArregloNumerico elegido = null;
            if (!puntuacion.vacia()) {
                int siguiente = (int) (double) puntuacion.obtener(0);
                if (siguiente < insertar) {
                    insertar = siguiente;
                    elegido = puntuacion;
                }
            }
            if (!mayusculas.vacia()) {
                int siguiente = (int) (double) mayusculas.obtener(0);
                if (siguiente < insertar) {
                    insertar = siguiente;
                    elegido = mayusculas;
                }
            }
            if (!agrupadores.vacia()) {
                int siguiente = (int) (double) agrupadores.obtener(0);
                if (siguiente < insertar) {
                    insertar = siguiente;
                    elegido = agrupadores;
                }
            }
            if (insertar == cadena.length()) {
                break;
            }
            elegido.eliminar(0);
            erroresTotales.agregar(insertar);
        }

        if (!erroresTotales.vacia()) {
            mostrarErrores(erroresTotales, cadena);
        }
    }

    /**
     * Busca y muestra los errores que existen en un archivo de texto.
     * @param nombreArchivo Es el nombre del archivo a analizar.
     * @param numLineas Es el numero de lineas a analizar.
     */
    public static void buscarErroresArchivo(String nombreArchivo, int numLineas) {
        Arreglo lineas = ArchivoTexto.leer(nombreArchivo, 2);
        boolean inicioParrafo = true;
        for (int i = 0; i < numLineas; i++) {
            String linea = lineas.obtener(i)+"";
            buscarErroresCadena(linea, inicioParrafo);
            if (linea.charAt(linea.length()-1) == '.') {
                inicioParrafo = true;
            }
            else {
                inicioParrafo = false;
            }
        }
    }

    /**
     * Muestra los errores que contiene una cadena.
     * @param errores Es un arreglo con los indices de los errores.
     * @param cadena Es la cadena analizada.
     */
    private static void mostrarErrores(ArregloNumerico errores, String cadena){
        String erroresPuntuacion = "";
        int ultimoErrorPuntuacion = (int) (double) errores.obtener(errores.numElementos()-1);
        int ultimoEncontrado = 0;
        for (int i = 0; i <= ultimoErrorPuntuacion; i++) {
            if (i == (int) (double) errores.obtener(ultimoEncontrado)) {
                ultimoEncontrado++;
                erroresPuntuacion += "^";
            }
            else {
                erroresPuntuacion += " ";
            }
        }
        SalidaEstandar.consola(erroresPuntuacion+"\n");

        for (int i = 0; i < errores.numElementos(); i++) {
            int error = (int) (double) errores.obtener(i);
            String mensajeError = "";
            for (int j = 0; j < error; j++) {
                mensajeError += " ";
            }
            char c = cadena.charAt(error);
            if (c == '.') {
                mensajeError += "Este punto no deberia existir.";
            }
            else if (AnalizadorTexto.esLetra(c)) {
                mensajeError += "La letra deberia ser mayuscula.";
            }
            else {
                if (AnalizadorTexto.esAgrupadorApertura(c)) {
                    mensajeError += "Falta cerrar este agrupador";
                }
                else {
                    mensajeError += "Este agrupador nunca se abrio";
                }
            }
            SalidaEstandar.consola(mensajeError+"\n");
        }
    }
}
