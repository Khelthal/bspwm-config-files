package interfaces;

import edlineal.PilaLista;
import entradasalida.SalidaEstandar;

/**
 * Esta clase es una pila que almacena elementos de forma dinamica.
 */
public class PruebaPilaLista {
    public static void main(String[] args) {
        PilaLista prueba = new PilaLista();

        prueba.poner("a");
        prueba.poner("b");
        prueba.poner("c");
        prueba.poner("d");
        prueba.poner("e");

        prueba.imprimir();
        SalidaEstandar.consola("\n");

        SalidaEstandar.consola("Eliminando el siguiente elemento en la pila: " + prueba.quitar()+  "\n");
        prueba.imprimir();
        SalidaEstandar.consola("\n");
        SalidaEstandar.consola("Eliminando el siguiente elemento en la pila: " + prueba.quitar()+  "\n");
        prueba.imprimir();
        SalidaEstandar.consola("\n");
        SalidaEstandar.consola("Eliminando el siguiente elemento en la pila: " + prueba.quitar()+  "\n");
        prueba.imprimir();
        SalidaEstandar.consola("\n");
        
    }
}
