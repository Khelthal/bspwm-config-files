package interfaces;

import audio.AudioFileRecord;

/**
 * Esta clase se encarga de realizar pruebas con AudioFileRecord.
 */
public class PruebaAudioFileRecord {
    public static void main(String[] args) {
        AudioFileRecord audioFileRecord = new AudioFileRecord("src/audio/cancion.wav");
        audioFileRecord.leerAudio();
        audioFileRecord.invertirEjeY();
        audioFileRecord.escribirAudio();
    }
}
