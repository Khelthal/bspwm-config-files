package interfaces;

import entradasalida.SalidaEstandar;
import utilerias.matematicas.ExpresionAritmetica;

/**
 * Esta clase se encarga de hacer pruebas con las clases PilaLista y ExpresionAritmetica.
 * */
public class PruebaExpresionAritmeticaLista {
    public static void main(String[] args) {
        String expresion = "45 + f * (h - 1) + ((a/b) -c)";

        SalidaEstandar.consola("Expresion original: " + expresion + "\n");

        if (ExpresionAritmetica.validarBalanceoParentesis(expresion)) {
            SalidaEstandar.consola("Los parentesis estan bien balanceados\n");
            expresion = ExpresionAritmetica.convertirExpresion(expresion);
            String expresionPre = ExpresionAritmetica.infijaAPrefija(expresion);
            String expresionPos = ExpresionAritmetica.infijaAPostfija(expresion);
            SalidaEstandar.consola("Expresion infija\n");
            SalidaEstandar.consola(expresion + "\n\n");

            SalidaEstandar.consola("Expresion prefija\n");
            SalidaEstandar.consola(expresionPre + "\n");
            SalidaEstandar.consola("Resultado: "+ExpresionAritmetica.evaluarPrefija(expresionPre)+"\n\n");

            SalidaEstandar.consola("Expresion postfija\n");
            SalidaEstandar.consola(expresionPos + "\n");
            SalidaEstandar.consola("Resultado: "+ExpresionAritmetica.evaluarPostfija(expresionPos)+"\n");
        }
        else {
            SalidaEstandar.consola("La expresion no es valida\n");
        }
    }
}
