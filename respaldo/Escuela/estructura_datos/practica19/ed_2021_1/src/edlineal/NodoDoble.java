package edlineal;

public class NodoDoble {
    protected NodoDoble ligaIzq;
    protected Object info;
    protected NodoDoble ligaDer;

    public NodoDoble(Object info){
        ligaIzq=null;
        ligaDer=null;
        this.info=info;
    }

    public NodoDoble getLigaIzq() {
        return ligaIzq;
    }

    public Object getInfo() {
        return info;
    }

    public NodoDoble getLigaDer() {
        return ligaDer;
    }

    public void setLigaIzq(NodoDoble ligaIzq) {
        this.ligaIzq = ligaIzq;
    }

    public void setInfo(Object info) {
        this.info = info;
    }

    public void setLigaDer(NodoDoble ligaDer) {
        this.ligaDer = ligaDer;
    }

    @Override
    public String toString(){
        return info.toString();
    }
}
