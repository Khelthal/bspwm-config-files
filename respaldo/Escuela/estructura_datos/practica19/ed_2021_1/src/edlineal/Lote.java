package edlineal;

/**
 * Esta interface administra las operaciones sobre un lote de datos.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public interface Lote {
    /**
     * Determina si un lote esta vacio.
     * @return Regresa <b>true</b> si el lote esta vacio, <b>false</b> en caso contrario.
     */
    public boolean vacio();

    /**
     * Determina si un lote esta lleno.
     * @return Regresa <b>true</b> si el lote esta lleno, <b>false</b> en caso contrario.
     */
    public boolean lleno();

    /**
     * Pone un nuevo elemento en el lote.
     * @param info Es el nuevo elemento.
     * @return Regresa <b>true</b> si se pudo poner el nuevo elemento, <b>false</b> en caso contrario.
     */
    public boolean poner(Object info);

    /**
     * Quita el elemento visible del lote.
     * @return Regresa el elemento quitado, <b>null</b> si no pudo quitar ningun elemento.
     */
    public Object quitar();

    /**
     * Imprime el contenido del lote.
     */
    public void imprimir();

    /**
     * Obtiene el elemento visible del lote.
     * @return Regresa el elemento visible del lote, <b>null</b> si no hay elemento visible.
     */
    public Object verLimite();
}
