package edlineal;

import entradasalida.SalidaEstandar;
import comparadores.ComparadorObject;

/**
 * Esta clase es una lista doblemente ligada.
 * */
public class ListaDobleLigada implements Lista {
  protected NodoDoble primero;
  protected NodoDoble ultimo;
  protected NodoDoble iteradorIzq;
  protected NodoDoble iteradorDer;

  /**
   * Crea una nueva lista doblemente ligada.
   * */
  public ListaDobleLigada() {
    primero = null;
    ultimo = null;
  }

  @Override
  public boolean vacia() {
    if (primero == null) {
      return true;
    } else {
      return false;
    }
  }

  @Override
  public int agregar(Object info) {
    NodoDoble nuevoNodo = new NodoDoble(info); // 1

    if (nuevoNodo != null) { // si hay espacio
      if (vacia() == true) { // está vacía, a)
        primero = nuevoNodo;// 2
        ultimo = nuevoNodo;
      } else { // no está vacía, b)
        ultimo.setLigaDer(nuevoNodo);// 2
        nuevoNodo.setLigaIzq(ultimo);// 3
        ultimo = nuevoNodo;// 4
      }
      return 1;
    } else { // hay error, o no hay memoria
      return -1;
    }
  }

  /**
   * Agrega un nuevo elemento al inicio de la lista doblemente ligada.
   * 
   * @param info Es el elemento a agregar.
   * @return Regresa 1 si pudo agregar el elemento, -1 en caso contrario.
   */
  public int agregarInicio(Object info) {
    NodoDoble nuevoNodo = new NodoDoble(info);

    if (nuevoNodo != null) { // si hay espacio
      if (vacia() == true) { // está vacía, a)
        primero = nuevoNodo;// 2
        ultimo = nuevoNodo;
      } else { // no está vacía, b)
        primero.setLigaIzq(nuevoNodo);
        nuevoNodo.setLigaDer(primero);
        primero = nuevoNodo;
      }
      return 1;
    } else { // hay error, o no hay memoria
      return -1;
    }
  }

  @Override
  public Object eliminar() {
    if (vacia() == false) {// hay algo
      Object elementoBorrado = ultimo.getInfo();// 1
      if (primero == ultimo) { // es el único, c)
        primero = null;// 2
        ultimo = null;
      } else { // hay más de uno, b)
        ultimo = ultimo.getLigaIzq();// 2
        ultimo.setLigaDer(null);// 3
      }
      return elementoBorrado;
    } else { // no hay nada, a)
      return null;
    }
  }

  @Override
  public void imprimir() {
    if (vacia() == false) { // hay algo
      SalidaEstandar.consola("null <- "); // 1
      NodoDoble temporal = primero;
      while (temporal != ultimo) {
        SalidaEstandar.consola(temporal.getInfo() + " <--> "); // 2
        temporal = temporal.getLigaDer();
      }
      SalidaEstandar.consola(temporal.getInfo() + " -> null");// 3
    } else { // no hay nada
      SalidaEstandar.consola("null");
    }
  }

  @Override
  public void imprimirOI() {
    if (vacia() == false) { // hay algo
      SalidaEstandar.consola("null <- "); // 1
      NodoDoble temporal = ultimo;
      while (temporal != primero) {
        SalidaEstandar.consola(temporal.getInfo() + " <--> "); // 2
        temporal = temporal.getLigaIzq();
      }
      SalidaEstandar.consola(temporal.getInfo() + " -> null");// 3
    } else { // no hay nada
      SalidaEstandar.consola("null");
    }
  }

  @Override
  public Object buscar(Object info) {
    if (vacia()) {
      return null;
    }
    NodoDoble recorrer = primero;

    while (recorrer != null && ComparadorObject.compare(recorrer.getInfo(), info) != 0) {
      recorrer = recorrer.getLigaDer();
    }

    if (recorrer == null) {
      return null;
    } else {
      return recorrer.getInfo();
    }
  }

  /**
   * Busca un elemento en la lista comenzando desde el ultimo nodo.
   * @param info Es el elemento buscado.
   * @return Regresa el elemento encontrado, null en caso contrario.
   */
  public Object buscarDesdeFinal(Object info) {
    if (vacia()) {
      return null;
    }
    NodoDoble recorrer = ultimo;

    while (recorrer != null && ComparadorObject.compare(recorrer.getInfo(), info) != 0) {
      recorrer = recorrer.getLigaIzq();
    }

    if (recorrer == null) {
      return null;
    } else {
      return recorrer.getInfo();
    }
  }

  @Override
  public Object eliminar(Object info) {
    if (vacia()) {
      return null;
    }
    NodoDoble recorrer = primero;

    while (recorrer != null && ComparadorObject.compare(recorrer.getInfo(), info) != 0) {
      recorrer = recorrer.getLigaDer();
    }

    if (recorrer == null) {
      return null;
    } else {
      Object eliminado = recorrer.getInfo();
      if (primero == ultimo) {
        primero = null;
        ultimo = null;
      } else if (recorrer == primero) {
        primero.getLigaDer().setLigaIzq(null);
        primero = primero.getLigaDer();
      } else if (recorrer == ultimo) {
        ultimo.getLigaIzq().setLigaDer(null);
        ultimo = ultimo.getLigaIzq();
      } else {
        recorrer.getLigaIzq().setLigaDer(recorrer.getLigaDer());
        recorrer.getLigaDer().setLigaIzq(recorrer.getLigaIzq());
      }

      return eliminado;
    }
  }

  /**
   * Elimina el elemento al inicio de la lista doblemente ligada.
   * @return Regresa el elemento borrado, null si la lista estaba vacia.
   */
  public Object eliminarInicio() {
    if (vacia()) {
      return null;
    }

    Object eliminado = primero.getInfo();

    if (primero == ultimo) {
      primero = null;
      ultimo = null;
    } else {
      primero.getLigaDer().setLigaIzq(null);
      primero = primero.getLigaDer();
    }
    return eliminado;
  }

  /**
   * Separa de la lista actual los elementos de tipo Number, String y otros en 3 listas doblemente ligadas.
   * @return Regresa una ListaLigada con las 3 listas doblemente enlazadas que contienen los datos separados.
   */
  public ListaLigada separarElementos() {
    ListaLigada resultado = new ListaLigada();
    ListaDobleLigada numeros = new ListaDobleLigada();
    ListaDobleLigada cadenas = new ListaDobleLigada();
    ListaDobleLigada otro = new ListaDobleLigada();
    resultado.agregar(numeros);
    resultado.agregar(cadenas);
    resultado.agregar(otro);

    NodoDoble recorrer = primero;

    while(recorrer != null) {
      Object analizado = recorrer.getInfo();
      if (analizado == null) {
        continue;
      }
      if (analizado instanceof Number) {
        numeros.agregar(analizado);
      }
      else if (analizado instanceof String) {
        cadenas.agregar(analizado);
      }
      else {
        otro.agregar(analizado);
      }
      recorrer = recorrer.getLigaDer();
    }

    return resultado;
  }

  /**
   * Inicializa el iterador izquierdo para recorrer la lista desde el inicio.
   * */
  public void inicializarIteradorIzq() {
    iteradorIzq = primero;
  }

  /**
   * Inicializa el iterador derecho para recorrer la lista comenzando por el ultimo elemento.
   * */
  public void inicializarIteradorDer() {
    iteradorDer = ultimo;
  }

  /**
   * Determina si existen mas elementos en el iterador izquierdo.
   * @return Regresa <b>true</b> si existen mas elementos en el iterador izquierdo, <b>false</b> en caso contrario.
   */
  public boolean hayMasIzq() {
    return iteradorIzq != null;
  }

  /**
   * Determina si existen mas elementos en el iterador derecho.
   * @return Regresa <b>true</b> si existen mas elementos en el iterador derecho, <b>false</b> en caso contrario.
   */
  public boolean hayMasDer() {
    return iteradorDer != null;
  }

  /**
   * Obtiene el siguiente elemento en el iterador izquierdo.
   * @return Regresa el siguiente elemento, null en caso contrario.
   */
  public Object obtenerSigiuenteIzq() {
    if (hayMasIzq()) {
      Object info = iteradorIzq.getInfo();
      iteradorIzq = iteradorIzq.getLigaDer();
      return info;
    } else {
      return null;
    }
  }

  /**
   * Obtiene el elemento anterior en el iterador izquierdo.
   * @return Regresa el elemento anterior, null en caso contrario.
   */
  public Object obtenerProcedenteIzq() {
    if (hayMasIzq()) {
      Object info = iteradorIzq.getInfo();
      iteradorIzq = iteradorIzq.getLigaIzq();
      return info;
    } else {
      return null;
    }
  }

  /**
   * Obtiene el siguiente elemento en el iterador derecho.
   * @return Regresa el siguiente elemento, null en caso contrario.
   */
  public Object obtenerSigiuenteDer() {
    if (hayMasDer()) {
      Object info = iteradorDer.getInfo();
      iteradorDer = iteradorDer.getLigaIzq();
      return info;
    } else {
      return null;
    }
  }

  /**
   * Obtiene el elemento anterior en el iterador derecho.
   * @return Regresa el elemento anterior, null en caso contrario.
   */
  public Object obtenerProcedenteDer() {
    if (hayMasDer()) {
      Object info = iteradorDer.getInfo();
      iteradorDer = iteradorDer.getLigaDer();
      return info;
    } else {
      return null;
    }
  }
  
  @Override
  public boolean esIgual(Object lista2) {
    if (!(lista2 instanceof ListaDobleLigada)) {
      return false;
    }
    ListaDobleLigada lista = (ListaDobleLigada) lista2;
    if (vacia()) {
      return lista.vacia();
    } else {
      if (lista.vacia()) {
        return false;
      }
    }
    NodoDoble recorrer = primero;
    int indice = 0;
    while (recorrer != null) {
      if (ComparadorObject.compare(recorrer.getInfo(), lista.obtener(indice)) != 0) {
        return false;
      }
      indice++;
      recorrer = recorrer.getLigaDer();
    }

    if (lista.obtener(indice) != null) {
      return false;
    }

    return true;
  }

  @Override
  public boolean cambiar(Object infoVieja, Object infoNueva, int numOcurrencias) {
    boolean cambio = false;
    NodoDoble recorrer = primero;
    while (recorrer != null) {
      if (numOcurrencias <= 0) {
        break;
      }
      if (ComparadorObject.compare(recorrer.getInfo(), infoVieja) == 0) {
        recorrer.setInfo(infoNueva);
        cambio = true;
        numOcurrencias--;
      }
      recorrer = recorrer.getLigaDer();
    }
    return cambio;
  }

  @Override
  public Arreglo buscarValores(Object info) {
    ListaLigada indices = new ListaLigada();
    if (vacia()) {
      return null;
    }
    NodoDoble recorrer = primero;
    int actual = 0;

    while (recorrer != null) {
      if (ComparadorObject.compare(recorrer.getInfo(), info) == 0) {
        indices.agregar(actual);
      }
      actual++;
      recorrer = recorrer.getLigaDer();
    }

    return indices.aArreglo();
  }

  @Override
  public void vaciar() {
    primero = null;
    ultimo = null;
  }

  @Override
  public boolean agregarLista(Object lista2) {
    Arreglo arreglo = (Arreglo) lista2;
    boolean logrado = true;

    for (int i = 0; i < arreglo.numElementos(); i++) {
      if (agregar(arreglo.obtener(i)) == -1) {
        logrado = false;
      }
    }

    return logrado;
  }

  @Override
  public void invertir() {
    if (!vacia()) {
      NodoDoble recorrer = primero;
      NodoDoble temp;
      while (recorrer != null) {
        temp = recorrer.getLigaDer();
        recorrer.setLigaDer(recorrer.getLigaIzq());
        recorrer.setLigaIzq(temp);
        recorrer = recorrer.getLigaIzq();
      }
      temp = primero;
      primero = ultimo;
      ultimo = temp;
    }
  }

  @Override
  public int contar(Object info) {
    int contador = 0;
    NodoDoble recorrer = primero;
    while (recorrer != null) {
      if (ComparadorObject.compare(recorrer.getInfo(), info) == 0) {
        contador++;
      }
      recorrer = recorrer.getLigaDer();
    }
    return contador;
  }

  @Override
  public boolean eliminarLista(Object lista2) {
    boolean cambiado = false;
    Arreglo lista = (Arreglo) lista2;

    for (int i = 0; i < lista.numElementos(); i++) {
      if (eliminar(lista.obtener(i)) != null) {
        cambiado = true;
      }
    }

    return cambiado;
  }

  @Override
  public void rellenar(Object info, int cantidad) {
    for (int i = 0; i < cantidad; i++) {
      agregar(info);
    }
  }

  @Override
  public void rellenar(Object info) {
    vaciar();
    if (info instanceof Integer) {
      int num = (Integer) info;
      if (num > 0) {
        for (int i = 0; i < num; i++) {
          agregar(i + 1);
        }
      } else {
        for (int i = 0; i > num; i--) {
          agregar(i - 1);
        }
      }
    } else {
      if (info instanceof Character) {
        char letra = (Character) info;
        for (char i = 'A'; i <= letra; i++) {
          agregar(i);
        }
      } else {
        agregar(info);
      }
    }
  }

  @Override
  public Object clonar() {
    ListaDobleLigada copia = new ListaDobleLigada();
    NodoDoble recorrer = primero;
    while (recorrer != null) {
      copia.agregar(recorrer.getInfo());
      recorrer = recorrer.getLigaDer();
    }

    return copia;
  }

  @Override
  public Object subLista(int indiceInicial, int indiceFinal) {
    if (vacia() || indiceInicial < 0) {
      return null;
    }

    NodoDoble recorrer = primero;
    int actual = 0;
    ListaDobleLigada subLista = new ListaDobleLigada();

    while (recorrer != null) {
      if (actual >= indiceInicial && actual <= indiceFinal) {
        subLista.agregar(recorrer.getInfo());
      }
      actual++;
      recorrer = recorrer.getLigaDer();
    }

    if (subLista.vacia()) {
      return null;
    }
    return subLista;
  }

  @Override
  public boolean insertar(int indice, Object info) {
    if (indice < 0) {
      return false;
    }

    NodoDoble recorrer = primero;
    int actual = -1;

    while (recorrer != null && actual != indice) {
      actual++;
      if (actual != indice) {
        recorrer = recorrer.getLigaDer();
      }
    }

    NodoDoble nuevoNodo = new NodoDoble(info);
    if (nuevoNodo == null) {
      return false;
    }

    if (actual != indice) {
      if (actual == -1) {
        primero = nuevoNodo;
        ultimo = nuevoNodo;
        return true;
      }
      else if (indice == actual+1) {
        ultimo.setLigaDer(nuevoNodo);
        nuevoNodo.setLigaIzq(ultimo);
        ultimo = nuevoNodo;
      }
      return false;
    } else {
      if (primero == recorrer) {
        nuevoNodo.setLigaDer(primero);
        primero.setLigaIzq(nuevoNodo);
        primero = nuevoNodo;
      } else {
        nuevoNodo.setLigaDer(recorrer);
        nuevoNodo.setLigaIzq(recorrer.getLigaIzq());
        recorrer.setLigaIzq(nuevoNodo);
        nuevoNodo.getLigaIzq().setLigaDer(nuevoNodo);
      }
      return true;
    }
  }

  /**
   * Obtiene el elemento de la posicion especificada.
   * 
   * @param indice Es la posicion de la cual se obtendra el elemento.
   * @return Regresa el elemento si pudo encontrarlo, null en caso contrario.
   */
  public Object obtener(int indice) {
    if (indice < 0) {
      return null;
    }

    NodoDoble recorrer = primero;
    int actual = -1;

    while (recorrer != null && actual != indice) {
      actual++;
      if (actual != indice) {
        recorrer = recorrer.getLigaDer();
      }
    }

    if (actual != indice) {
      return null;
    } else {
      return recorrer.getInfo();
    }
  }

}
