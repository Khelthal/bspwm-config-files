package edlineal;

import entradasalida.SalidaEstandar;

public class ColaArreglo implements Lote{
    protected Object datosCola[];
    protected int MAXIMO;
    protected int primero;
    protected int ultimo;

    public  ColaArreglo(int tam){
        datosCola=new Object[tam];
        MAXIMO=tam;
        primero=-1;
        ultimo=-1;
    }

    @Override
    public boolean vacio(){
        if(primero==-1) {
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean lleno(){
        if( (primero==0 && ultimo==(MAXIMO-1)) || primero==(ultimo+1) ) {
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean poner(Object info){
        if(lleno()==false){
            if (vacio()==true){ //coloca la s variables en un valor apropiado
                primero=0; //coloca la s variables en un valor apropiado
                ultimo=0;
                //datosCola[ultimo]=info;//asigna
            }else if(ultimo==(MAXIMO-1)){
                ultimo=0;//coloca la s variables en un valor apropiado
                //datosCola[ultimo]=info;//asigna
            }else {
                ultimo++;//coloca la s variables en un valor apropiado
                //datosCola[ultimo] = info;//asigna
            }
            datosCola[ultimo] = info;//asigna
            return true;
        }else{
            return false;
        }
    }

    @Override
    public Object quitar(){
        Object elementoBorrado=null;

        if(vacio()==false){ //hay algo
            //elementoBorrado=datosCola[primero];
            if (primero == ultimo) {
                elementoBorrado=datosCola[primero];
                primero=-1; //ahora va a quedar vacía, solo había un elemento
                ultimo=-1;
            }else if (primero==(MAXIMO-1)) { //va a comportarse circular
                elementoBorrado=datosCola[primero];
                primero=0;
            }else{ //para los demás casos
                elementoBorrado=datosCola[primero];
                primero++;
            }
            return elementoBorrado;
        }else{ //no hay nada
            return null;
        }
    }

    @Override
    public void imprimir(){
        if (vacio()==false){//hay elementos
            if (primero <= ultimo) { //comportamiento habitual
                for(int pos=primero; pos<=ultimo;pos++){
                    SalidaEstandar.consola(datosCola[pos]+ " ");
                }
            }else{ //comportamiento circular
                for(int pos=primero; pos<=(MAXIMO-1);pos++){
                    SalidaEstandar.consola(datosCola[pos]+ " ");
                }
                for(int pos=0; pos<=ultimo;pos++){
                    SalidaEstandar.consola(datosCola[pos]+ " ");
                }
            }
        }//else, pero no hacemos nada
    }

    @Override
    public Object verLimite(){
        if(vacio()==false) {
            return datosCola[primero];
        }else{
            return null;
        }
    }
}
