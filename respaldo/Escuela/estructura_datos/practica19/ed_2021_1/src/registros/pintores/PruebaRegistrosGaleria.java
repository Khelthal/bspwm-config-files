package registros.pintores;

import edlineal.ArregloOrdenado;
import edlineal.TipoOrden;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de inicializar los datos de la instancia de RegistrosGaleria e iniciar el menu.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class PruebaRegistrosGaleria {
    public static void main(String[] args) {
        Pintor p1 = new Pintor("Erik", 27, "Vall Martinez de las Torres");
        Pintor p2 = new Pintor("Jesus", 32, "Brisenio del Puerto");
        Pintor p3 = new Pintor("Esquivel", 28, "El Orosco de Ulla");
        Pintor p4 = new Pintor("Salma", 26, "Los Anaya");
        Pintor p5 = new Pintor("Nayara", 37, "Villa Torrez de Ulla");
        Pintor p6 = new Pintor("Mireia", 30, "A Avila Medio");
        Pintor p7 = new Pintor("Silvia", 31, "Las Parra");
        Pintor p8 = new Pintor("Francisco", 28, "La Arellano");
        Pintor p9 = new Pintor("Florez", 32, "A Deleon del Valles");
        Pintor p10 = new Pintor("Deleon", 34, "La Arroyo");
        Pintor p11 = new Pintor("Valeria", 28, "Armendariz Alta");


        Presentacion c1 = new Presentacion("Las murallas de Orgrimmar", "Durotar", 250);
        Presentacion c2 = new Presentacion("Los bosques de Ventormenta", "Elwymm", 400);
        Presentacion c3 = new Presentacion("La ciudadela de hielo", "Northrend", 125);
        Presentacion c4 = new Presentacion("Los arboles del mundo", "Hyjal", 50);
        Presentacion c5 = new Presentacion("La ciudad flotante", "Dalaran", 360);
        Presentacion c6 = new Presentacion("Las cuevas de cristal", "Infralar", 210);
        Presentacion c7 = new Presentacion("Las antiguas piramides", "Uldum", 40);
        Presentacion c8 = new Presentacion("Secretos de la arena", "Zandalar", 500);
        Presentacion c9 = new Presentacion("El buen vino", "Suramar", 120);
        Presentacion c10 = new Presentacion("Las tierras de fuego", "Voldun", 480);
        Presentacion c11 = new Presentacion("Las cavernas del tiempo", "Tanaris", 650);
        Presentacion c12 = new Presentacion("Las mil agujas", "Lunargenta", 900);
        Presentacion c13 = new Presentacion("El templo oscuro", "Shattrath", 100);
        Presentacion c14 = new Presentacion("La necropolis", "Zuldrak", 700);
        Presentacion c15 = new Presentacion("El reflejo de plata", "Zacatecas", 100);

        RegistrosGaleria registrosGaleria = new RegistrosGaleria(11, 15);
        registrosGaleria.agregarPintor(p1);
        registrosGaleria.agregarPintor(p2);
        registrosGaleria.agregarPintor(p3);
        registrosGaleria.agregarPintor(p4);
        registrosGaleria.agregarPintor(p5);
        registrosGaleria.agregarPintor(p6);
        registrosGaleria.agregarPintor(p7);
        registrosGaleria.agregarPintor(p8);
        registrosGaleria.agregarPintor(p9);
        registrosGaleria.agregarPintor(p10);
        registrosGaleria.agregarPintor(p11);

        registrosGaleria.agregarPresentacion(c1);
        registrosGaleria.agregarPresentacion(c2);
        registrosGaleria.agregarPresentacion(c3);
        registrosGaleria.agregarPresentacion(c4);
        registrosGaleria.agregarPresentacion(c5);
        registrosGaleria.agregarPresentacion(c6);
        registrosGaleria.agregarPresentacion(c7);
        registrosGaleria.agregarPresentacion(c8);
        registrosGaleria.agregarPresentacion(c9);
        registrosGaleria.agregarPresentacion(c10);
        registrosGaleria.agregarPresentacion(c11);
        registrosGaleria.agregarPresentacion(c12);
        registrosGaleria.agregarPresentacion(c13);
        registrosGaleria.agregarPresentacion(c14);
        registrosGaleria.agregarPresentacion(c15);

        registrosGaleria.agregarActividad("Enero", "Erik", "Las murallas de Orgrimmar", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Febrero", "Erik", "Las murallas de Orgrimmar", ActividadPintor.EXPONER);
        registrosGaleria.agregarActividad("Marzo", "Erik", "Las murallas de Orgrimmar", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Abril", "Erik", "Las murallas de Orgrimmar", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Mayo", "Erik", "Los bosques de Ventormenta", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Junio", "Erik", "Los bosques de Ventormenta", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Julio", "Erik", "Los arboles del mundo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Agosto", "Erik", "Las cuevas de cristal", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Septiembre", "Erik", "El reflejo de plata", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Octubre", "Erik", "Las mil agujas", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Noviembre", "Erik", "Las murallas de Orgrimmar", ActividadPintor.PINTAR);
        registrosGaleria.agregarActividad("Diciembre", "Erik", "Los bosques de Ventormenta", ActividadPintor.PINTAR);

        registrosGaleria.agregarActividad("Enero", "Jesus", "La ciudadela de hielo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Febrero", "Jesus", "La ciudad flotante", ActividadPintor.EXPONER);
        registrosGaleria.agregarActividad("Marzo", "Jesus", "Los arboles del mundo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Abril", "Jesus", "Las tierras de fuego", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Mayo", "Jesus", "La ciudadela de hielo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Junio", "Jesus", "Las murallas de Orgrimmar", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Julio", "Jesus", "Las antiguas piramides", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Agosto", "Jesus", "Los bosques de Ventormenta", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Septiembre", "Jesus", "Las cavernas del tiempo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Octubre", "Jesus", "Secretos de la arena", ActividadPintor.PINTAR);
        registrosGaleria.agregarActividad("Noviembre", "Jesus", "La ciudadela de hielo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Diciembre", "Jesus", "Los arboles del mundo", ActividadPintor.OBSERVAR);

        registrosGaleria.agregarActividad("Enero", "Esquivel", "La ciudad flotante", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Febrero", "Esquivel", "Los bosques de Ventormenta", ActividadPintor.EXPONER);
        registrosGaleria.agregarActividad("Marzo", "Esquivel", "Las murallas de Orgrimmar", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Abril", "Esquivel", "Las cuevas de cristal", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Mayo", "Esquivel", "La ciudadela de hielo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Junio", "Esquivel", "El buen vino", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Julio", "Esquivel", "Los bosques de Ventormenta", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Agosto", "Esquivel", "La necropolis", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Septiembre", "Esquivel", "El templo oscuro", ActividadPintor.PINTAR);
        registrosGaleria.agregarActividad("Octubre", "Esquivel", "La ciudadela de hielo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Noviembre", "Esquivel", "Las murallas de Orgrimmar", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Diciembre", "Esquivel", "El reflejo de plata", ActividadPintor.OBSERVAR);

        registrosGaleria.agregarActividad("Enero", "Salma", "Las antiguas piramides", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Febrero", "Salma", "Los bosques de Ventormenta", ActividadPintor.EXPONER);
        registrosGaleria.agregarActividad("Marzo", "Salma", "La ciudad flotante", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Abril", "Salma", "Los bosques de Ventormenta", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Mayo", "Salma", "Secretos de la arena", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Junio", "Salma", "Las murallas de Orgrimmar", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Julio", "Salma", "Las tierras de fuego", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Agosto", "Salma", "Las cuevas de cristal", ActividadPintor.PINTAR);
        registrosGaleria.agregarActividad("Septiembre", "Salma", "La ciudadela de hielo", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Octubre", "Salma", "El reflejo de plata", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Noviembre", "Salma", "El buen vino", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Diciembre", "Salma", "Las murallas de Orgrimmar", ActividadPintor.OBSERVAR);

        registrosGaleria.agregarActividad("Enero", "Nayara", "Las murallas de Orgrimmar", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Febrero", "Nayara", "Secretos de la arena", ActividadPintor.EXPONER);
        registrosGaleria.agregarActividad("Marzo", "Nayara", "Los bosques de Ventormenta", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Abril", "Nayara", "Las antiguas piramides", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Mayo", "Nayara", "Los arboles del mundo", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Junio", "Nayara", "La ciudad flotante", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Julio", "Nayara", "Las murallas de Orgrimmar", ActividadPintor.PINTAR);
        registrosGaleria.agregarActividad("Agosto", "Nayara", "Las cavernas del tiempo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Septiembre", "Nayara", "La ciudadela de hielo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Octubre", "Nayara", "Las mil agujas", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Noviembre", "Nayara", "Las tierras de fuego", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Diciembre", "Nayara", "El templo oscuro", ActividadPintor.OBSERVAR);

        registrosGaleria.agregarActividad("Enero", "Mireia", "La ciudadela de hielo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Febrero", "Mireia", "Las murallas de Orgrimmar", ActividadPintor.EXPONER);
        registrosGaleria.agregarActividad("Marzo", "Mireia", "La necropolis", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Abril", "Mireia", "Los bosques de Ventormenta", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Mayo", "Mireia", "Las antiguas piramides", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Junio", "Mireia", "Los arboles del mundo", ActividadPintor.PINTAR);
        registrosGaleria.agregarActividad("Julio", "Mireia", "Las murallas de Orgrimmar", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Agosto", "Mireia", "La ciudad flotante", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Septiembre", "Mireia", "El buen vino", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Octubre", "Mireia", "La ciudadela de hielo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Noviembre", "Mireia", "Las cavernas del tiempo", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Diciembre", "Mireia", "El reflejo de plata", ActividadPintor.OBSERVAR);

        registrosGaleria.agregarActividad("Enero", "Silvia", "Las cuevas de cristal", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Febrero", "Silvia", "Las murallas de Orgrimmar", ActividadPintor.EXPONER);
        registrosGaleria.agregarActividad("Marzo", "Silvia", "Secretos de la arena", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Abril", "Silvia", "Los bosques de Ventormenta", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Mayo", "Silvia", "Secretos de la arena", ActividadPintor.PINTAR);
        registrosGaleria.agregarActividad("Junio", "Silvia", "Los arboles del mundo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Julio", "Silvia", "La ciudadela de hielo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Agosto", "Silvia", "Las antiguas piramides", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Septiembre", "Silvia", "El reflejo de plata", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Octubre", "Silvia", "Las murallas de Orgrimmar", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Noviembre", "Silvia", "Las mil agujas", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Diciembre", "Silvia", "La ciudad flotante", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Diciembre", "Silvia", "La ciudad flotante", ActividadPintor.FIRMAR_AUTOGRAFOS);

        registrosGaleria.agregarActividad("Enero", "Francisco", "Las tierras de fuego", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Febrero", "Francisco", "Los bosques de Ventormenta", ActividadPintor.EXPONER);
        registrosGaleria.agregarActividad("Marzo", "Francisco", "Los bosques de Ventormenta", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Abril", "Francisco", "Las cuevas de cristal", ActividadPintor.PINTAR);
        registrosGaleria.agregarActividad("Mayo", "Francisco", "El buen vino", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Junio", "Francisco", "Las murallas de Orgrimmar", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Julio", "Francisco", "El templo oscuro", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Agosto", "Francisco", "La ciudadela de hielo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Septiembre", "Francisco", "Los arboles del mundo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Octubre", "Francisco", "Los arboles del mundo", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Noviembre", "Francisco", "La necropolis", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Diciembre", "Francisco", "La ciudad flotante", ActividadPintor.OBSERVAR);

        registrosGaleria.agregarActividad("Enero", "Florez", "El buen vino", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Febrero", "Florez", "Las antiguas piramides", ActividadPintor.EXPONER);
        registrosGaleria.agregarActividad("Marzo", "Florez", "Las murallas de Orgrimmar", ActividadPintor.PINTAR);
        registrosGaleria.agregarActividad("Abril", "Florez", "Las murallas de Orgrimmar", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Mayo", "Florez", "El reflejo de plata", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Junio", "Florez", "Las tierras de fuego", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Julio", "Florez", "Los bosques de Ventormenta", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Agosto", "Florez", "Los bosques de Ventormenta", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Septiembre", "Florez", "Las murallas de Orgrimmar", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Octubre", "Florez", "Las murallas de Orgrimmar", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Noviembre", "Florez", "El reflejo de plata", ActividadPintor.OBSERVAR);
        registrosGaleria.agregarActividad("Diciembre", "Florez", "Las antiguas piramides", ActividadPintor.OBSERVAR);

        registrosGaleria.agregarActividad("Enero", "Deleon", "La ciudadela de hielo", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Enero", "Deleon", "La ciudadela de hielo", ActividadPintor.PINTAR);
        registrosGaleria.agregarActividad("Febrero", "Deleon", "Las mil agujas", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Febrero", "Deleon", "Los arboles del mundo", ActividadPintor.EXPONER);
        registrosGaleria.agregarActividad("Junio", "Deleon", "La necropolis", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Julio", "Deleon", "Los bosques de Ventormenta", ActividadPintor.CONVIVIR);

        registrosGaleria.agregarActividad("Febrero", "Valeria", "La ciudadela de hielo", ActividadPintor.CONVIVIR);
        registrosGaleria.agregarActividad("Febrero", "Valeria", "La ciudadela de hielo", ActividadPintor.EXPONER);


        MenuRegistrosGaleria menuRegistrosGaleria = new MenuRegistrosGaleria(registrosGaleria);
        menuRegistrosGaleria.iniciar();

    }
}
