package registros.pintores;

import edlineal.Arreglo;
import edlineal.ArregloNumerico;
import edlineal.ArregloOrdenado;
import edlineal.TipoOrden;
import ednolineal.Matriz3D;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de manejar todos los datos de los meses, pintores, presentaciones
 * y las actividades de los pintores.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class RegistrosGaleria {
    protected Matriz3D actividades;
    protected Arreglo meses;
    protected Arreglo pintores;
    protected Arreglo presentaciones;
    protected ArregloNumerico conteoActividades;

    /**
     * Crea una nueva instancia de RegistrosGaleria con el numero de pintores y numero de presentaciones pasados como argumento.
     * @param numPintores Es el numero maximo de pintores a registrar.
     * @param numPresentaciones Es el numero maximo de presentaciones a registrar.
     */
    public RegistrosGaleria(int numPintores, int numPresentaciones) {
        meses = new Arreglo(12);
        meses.agregar("Enero");
        meses.agregar("Febrero");
        meses.agregar("Marzo");
        meses.agregar("Abril");
        meses.agregar("Mayo");
        meses.agregar("Junio");
        meses.agregar("Julio");
        meses.agregar("Agosto");
        meses.agregar("Septiembre");
        meses.agregar("Octubre");
        meses.agregar("Noviembre");
        meses.agregar("Diciembre");

        conteoActividades = new ArregloNumerico(5);
        conteoActividades.rellenar(0.0, 5);

        ArregloOrdenado actividad = new ArregloOrdenado(5, TipoOrden.CRECIENTE);
        actividades = new Matriz3D(12, numPintores, numPresentaciones, actividad);
        pintores = new Arreglo(numPintores);
        presentaciones = new Arreglo(numPresentaciones);
    }

    /**
     * Agrega un pintor al registro.
     * @param pintor Es el pintor a agregar.
     * @return Regresa <b>true</b> si pudo agregar al pintor, <b>false</b> en caso contrario.
     */
    public boolean agregarPintor(Pintor pintor) {
        return (pintores.agregar(pintor) != -1);
    }

    /**
     * Agrega una presentacion al registro.
     * @param presentacion Es la presentacion a agregar.
     * @return Regresa <b>true</b> si pudo agregar la presentacion, <b>false</b> en caso contrario.
     */
    public boolean agregarPresentacion(Presentacion presentacion) {
        return (presentaciones.agregar(presentacion) != -1);
    }

    /**
     * Agrega una actividad en la posicion pasada como argumento.
     * @param mes Es el mes donde se agregara la actividad.
     * @param nombrePintor Es el pintor al que se le agregara la actividad.
     * @param nombrePresentacion Es la presentacion a la que se le agregara la actividad.
     * @param actividadesPintor Es la actividad a agregar.
     * @return Regresa <b>true</b> si se pudo agregar la actividad, <b>false</b> en caso contrario.
     */
    public boolean agregarActividad(String mes, String nombrePintor, String nombrePresentacion, ActividadPintor actividadesPintor) {
        Integer numMes = (Integer) meses.buscar(mes);
        Integer numPintor = (Integer) pintores.buscar(nombrePintor);
        Integer numPresentacion = (Integer) presentaciones.buscar(nombrePresentacion);

        if (numMes == null || numPintor == null || numPresentacion == null) {
            return false;
        }
        ArregloOrdenado arregloOrdenado = (ArregloOrdenado) actividades.obtenerInfo(numMes, numPintor, numPresentacion);
        Arreglo temporal = (Arreglo) arregloOrdenado.clonar();
        arregloOrdenado = new ArregloOrdenado(temporal.longitud(), TipoOrden.CRECIENTE);
        arregloOrdenado.agregarLista(temporal);
        if (arregloOrdenado.agregar(actividadesPintor) != -1) {
            sumarActividad(actividadesPintor);
        }
        return actividades.cambiarInfo(numMes, numPintor, numPresentacion, arregloOrdenado);
    }

    /**
     * Suma el conteo del numero de repeticiones de una actividad.
     * @param actividad Es la actividad cuyo conteo se sumara.
     */
    public void sumarActividad(ActividadPintor actividad) {
        switch (actividad) {
            case CONVIVIR:
                conteoActividades.cambiar(0, (Double) conteoActividades.obtener(0) + 1);
                break;
            case EXPONER:
                conteoActividades.cambiar(1, (Double) conteoActividades.obtener(1) + 1);
                break;
            case FIRMAR_AUTOGRAFOS:
                conteoActividades.cambiar(2, (Double) conteoActividades.obtener(2) + 1);
                break;
            case OBSERVAR:
                conteoActividades.cambiar(3, (Double) conteoActividades.obtener(3) + 1);
                break;
            case PINTAR:
                conteoActividades.cambiar(4, (Double) conteoActividades.obtener(4) + 1);
                break;
        }
    }

    /**
     * Imprime los meses.
     */
    public void imprimirMeses() {
        for (int i = 0; i < meses.numElementos(); i++) {
            SalidaEstandar.consola(meses.obtener(i) + "\n");
        }
    }

    /**
     * Imprime los pintores registrados.
     */
    public void imprimirPintores() {
        for (int i = 0; i < pintores.numElementos(); i++) {
            SalidaEstandar.consola(pintores.obtener(i) + "\n");
        }
    }

    /**
     * Imprime las presentaciones registradas.
     */
    public void imprimirPresentaciones() {
        for (int i = 0; i < presentaciones.numElementos(); i++) {
            SalidaEstandar.consola(((Presentacion) presentaciones.obtener(i)).obtenerDatos() + "\n");
        }
    }

    /**
     * Imprime las actividades registradas.
     */
    public void imprimirActividades() {
        actividades.imprimirColumnas();
    }

    /**
     * Imprime una actividad por indice.
     * @param indice Es el indice de la actividad a imprimir.
     */
    public void imprimirActividad(int indice) {
        switch (indice) {
            case 0:
                SalidaEstandar.consola(ActividadPintor.CONVIVIR + "\n");
                break;
            case 1:
                SalidaEstandar.consola(ActividadPintor.EXPONER + "\n");
                break;
            case 2:
                SalidaEstandar.consola(ActividadPintor.FIRMAR_AUTOGRAFOS + "\n");
                break;
            case 3:
                SalidaEstandar.consola(ActividadPintor.OBSERVAR + "\n");
                break;
            case 4:
                SalidaEstandar.consola(ActividadPintor.PINTAR + "\n");
                break;
        }
    }

    /**
     * Imprime la activdad que mas se repitio en el registro.
     */
    public void imprimirActividadMaxima() {
        int maximo = 0;
        for (int i = 0; i < conteoActividades.numElementos(); i++) {
            if ((Double) conteoActividades.obtener(i) > (Double) conteoActividades.obtener(maximo)) {
                maximo = i;
            }
        }

        imprimirActividad(maximo);
    }

    /**
     * Imprime el pintor que realizo mas actividades.
     */
    public void imprimirPintorMaximo() {
        int pintorMax = 0;
        int maxTrabajo = 0;
        for (int pintor = 0; pintor < pintores.numElementos(); pintor++) {
            int trabajoPintor = 0;
            for (int mes = 0; mes < meses.numElementos(); mes++) {
                for (int presentacion = 0; presentacion < presentaciones.numElementos(); presentacion++) {
                    trabajoPintor += ((ArregloOrdenado) actividades.obtenerInfo(mes, pintor, presentacion)).numElementos();
                }
            }
            if (trabajoPintor > maxTrabajo) {
                maxTrabajo = trabajoPintor;
                pintorMax = pintor;
            }
        }
        SalidaEstandar.consola(pintores.obtener(pintorMax)+"\n");
    }

    /**
     * Imprime el nombre y edad de los pintores que realizaron una actividad especifica en un mes especifico.
     * @param mes Es el mes a consultar.
     * @param actividadPintor Es la actividad a consultar.
     */
    public void imprimirPintorActividadMes(String mes, ActividadPintor actividadPintor) {
        Integer numMes = (Integer) meses.buscar(mes);
        if (numMes == null) {
            return;
        }
        ArregloOrdenado pintoresEncontrados = new ArregloOrdenado(pintores.numElementos(), TipoOrden.CRECIENTE);
        for (int pintor = 0; pintor < pintores.numElementos(); pintor++) {
            for (int presentacion = 0; presentacion < presentaciones.numElementos(); presentacion++) {
                ArregloOrdenado arregloOrdenado = (ArregloOrdenado) actividades.obtenerInfo(numMes, pintor, presentacion);
                Integer posEncontrado = (Integer) arregloOrdenado.buscar(actividadPintor);
                if (posEncontrado > 0) {
                    pintoresEncontrados.agregar(pintores.obtener(pintor));
                }
            }
        }

        for (int i = 0; i < pintoresEncontrados.numElementos(); i++) {
            Pintor p = (Pintor) pintoresEncontrados.obtener(i);
            SalidaEstandar.consola(p.getNombre() + ", edad: " + p.getEdad() + "\n");
        }
    }

    /**
     * Imprime el mes y la presentacion en donde un pintor especifico realizo una actividad especifica.
     * @param nombrePintor Es el nombre del pintor a consultar.
     * @param actividadPintor Es la actividad a consultar.
     */
    public void imprimirMesPresentacionPintorActividad(String nombrePintor, ActividadPintor actividadPintor) {
        Integer numPintor = (Integer) pintores.buscar(nombrePintor);

        if (numPintor == null) {
            return;
        }
        for (int mes = 0; mes < meses.numElementos(); mes++) {
            for (int presentacion = 0; presentacion < presentaciones.numElementos(); presentacion++) {
                ArregloOrdenado arregloOrdenado = (ArregloOrdenado) actividades.obtenerInfo(mes, numPintor, presentacion);
                Integer posEncontrado = (Integer) arregloOrdenado.buscar(actividadPintor);
                if (posEncontrado > 0) {
                    SalidaEstandar.consola(meses.obtener(mes) + ", " + ((Presentacion) presentaciones.obtener(presentacion)).obtenerDatos() + "\n");
                }
            }
        }
    }

    /**
     * Imprime el mes donde se realizo en menor cantidad una actividad especifica.
     * @param actividadPintor Es la actividad a evaluar.
     */
    public void imprimirMesMenorActividad(ActividadPintor actividadPintor) {
        int minMes = 0;
        int minActividad = 0;

        for (int mes = 0; mes < meses.numElementos(); mes++) {
            int actividadMensual = 0;
            for (int pintor = 0; pintor < pintores.numElementos(); pintor++) {
                for (int presentacion = 0; presentacion < presentaciones.numElementos(); presentacion++) {
                    ArregloOrdenado arregloOrdenado = (ArregloOrdenado) actividades.obtenerInfo(mes, pintor, presentacion);
                    Integer posEncontrado = (Integer) arregloOrdenado.buscar(actividadPintor);
                    if (mes == 0) {
                        if (posEncontrado > 0) {
                            minActividad++;
                            actividadMensual++;
                        }
                    } else {
                        if (posEncontrado > 0) {
                            actividadMensual++;
                        }
                    }
                }
            }
            if (actividadMensual < minActividad) {
                minMes = mes;
                minActividad = actividadMensual;
            }
        }

        SalidaEstandar.consola(meses.obtener(minMes)+"\n");
    }

    /**
     * Imprime el nombre y domicilio de los pintores que convivieron o descansaron todos los meses.
     */
    public void imprimirPintoresDescansados() {
        busquedaPintor:
        for (int pintor = 0; pintor < pintores.numElementos(); pintor++) {
            for (int mes = 0; mes < meses.numElementos(); mes++) {
                boolean trabajo = false;
                for (int presentacion = 0; presentacion < presentaciones.numElementos(); presentacion++) {
                    ArregloOrdenado arregloOrdenado = (ArregloOrdenado) actividades.obtenerInfo(mes, pintor, presentacion);
                    Integer posEncontrado = (Integer) arregloOrdenado.buscar(ActividadPintor.CONVIVIR);
                    if (arregloOrdenado.numElementos() > 0) {
                        if (posEncontrado > 0) {
                            trabajo = false;
                            break;
                        } else {
                            trabajo = true;
                        }
                    }
                }
                if (trabajo) {
                    continue busquedaPintor;
                }
            }
            Pintor p = (Pintor) pintores.obtener(pintor);
            SalidaEstandar.consola(p.getNombre()+", direccion: "+p.getDireccion()+"\n");
        }
    }

    /**
     * Imprime el mes en el que todos los pintores realizaron la actividad especificada.
     * @param actividadPintor Es la actividad a evaluar.
     */
    public void imprimirMesActividadTotal(ActividadPintor actividadPintor) {
        buscarMes:
        for (int mes = 0; mes < meses.numElementos(); mes++) {
            for (int pintor = 0; pintor < pintores.numElementos(); pintor++) {
                boolean pintorActivo = false;
                for (int presentacion = 0; presentacion < presentaciones.numElementos(); presentacion++) {
                    ArregloOrdenado arregloOrdenado = (ArregloOrdenado) actividades.obtenerInfo(mes, pintor, presentacion);
                    Integer posEncontrado = (Integer) arregloOrdenado.buscar(actividadPintor);
                    if (posEncontrado > 0) {
                        pintorActivo = true;
                    }
                }
                if (!pintorActivo) {
                    continue buscarMes;
                }
            }
            SalidaEstandar.consola(meses.obtener(mes)+"\n");
        }
    }

    /**
     * Imprime el mes en donde un pintor realizo mas veces una actividad especifica.
     * @param nombrePintor Es el nombre del pintor a evaluar.
     * @param actividadPintor Es la actividad a evaluar.
     */
    public void imprimirMaximaActividadMesPintor(String nombrePintor, ActividadPintor actividadPintor) {
        Integer numPintor = (Integer) pintores.buscar(nombrePintor);

        if (numPintor == null) {
            return;
        }
        int maxMes = 0;
        int maxActividad = 0;
        for (int mes = 0; mes < meses.numElementos(); mes++) {
            int actividadMensual = 0;
            for (int presentacion = 0; presentacion < presentaciones.numElementos(); presentacion++) {
                ArregloOrdenado arregloOrdenado = (ArregloOrdenado) actividades.obtenerInfo(mes, numPintor, presentacion);
                Integer posEncontrado = (Integer) arregloOrdenado.buscar(actividadPintor);
                if (posEncontrado > 0) {
                    actividadMensual++;
                }
            }
            if (actividadMensual > maxActividad) {
                maxMes = mes;
                maxActividad = actividadMensual;
            }
        }

        SalidaEstandar.consola(meses.obtener(maxMes)+"\n");
    }

    /**
     * Muestra los eventos en donde no se realizo la actividad especificada.
     * @param actividadPintor Es la actividad a evaluar.
     */
    public void imprimirPresentacionSinActividad(ActividadPintor actividadPintor) {
        busquedaPresentacion:
        for (int presentacion = 0; presentacion < presentaciones.numElementos(); presentacion++) {
            for (int mes = 0; mes < meses.numElementos(); mes++) {
                for (int pintor = 0; pintor < pintores.numElementos(); pintor++) {
                    ArregloOrdenado arregloOrdenado = (ArregloOrdenado) actividades.obtenerInfo(mes, pintor, presentacion);
                    Integer posEncontrado = (Integer) arregloOrdenado.buscar(actividadPintor);
                    if (posEncontrado > 0) {
                        continue busquedaPresentacion;
                    }
                }
            }
            SalidaEstandar.consola(((Presentacion)presentaciones.obtener(presentacion)).obtenerDatos()+"\n");
        }
    }
}
