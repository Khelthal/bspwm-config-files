package registros.computadoras;

import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;
import utilerias.fecha.Fecha;

/**
 * Esta clase se encarga de manipular a la clase RegistroCentroComputo.
 * */
public class MenuRegistroCentroComputo {
  protected RegistroCentroComputo registroCentroComputo;

  public MenuRegistroCentroComputo(RegistroCentroComputo registroCentroComputo) {
    this.registroCentroComputo = registroCentroComputo;
  }

  /**
   * Inicia el menu.
   */
  public void iniciar() {
    while (true) {
      imprimirMenu();
      SalidaEstandar.consola("\n");
      seleccionarOpcion();
      SalidaEstandar.consola("Presiona enter para continuar\n");
      EntradaConsola.consolaCadena();
    }
  }

  /**
   * Imprime las opciones del menu.
   */
  public void imprimirMenu() {
    SalidaEstandar.consola("¿Que desea hacer?\n");
    SalidaEstandar.consola("a) Imprimir lista de computadoras.\n");
    SalidaEstandar.consola("b) Imprimir computadoras con ram de 16GB.\n");
    SalidaEstandar.consola("c) Imprimir caracteristicas de computadora especifica.\n");
    SalidaEstandar.consola("d) Agregar aplicaciones a computadora especifica.\n");
    SalidaEstandar.consola("e) Eliminar aplicaciones de computadora especifica.\n");
    SalidaEstandar.consola("f) Dar de alta una nueva computadora.\n");
    SalidaEstandar.consola("g) Dar de baja una computadora.\n");
    SalidaEstandar.consola("h) Buscar las computadoras que tengan instalado Firefox.\n");
    SalidaEstandar.consola("i) Mostrar computadoras que puedan correr una aplicacion especifica instalada.\n");
    SalidaEstandar.consola("j) Imprimir computadoras con aplicaciones que requieran 16GB de ram o mas.\n");
    SalidaEstandar.consola("k) Imprimir usuarios que han usado computadora especifica.\n");
    SalidaEstandar.consola("l) Mostrar horarios en los que un usuario especifico ha usado cierta computadora.\n");
    SalidaEstandar.consola("m) Imprimir las aplicaciones que un usuario especifico abrio en una fecha especifica.\n");
    SalidaEstandar.consola("n) Imprimir aplicaciones mas populares y menos populares entre los usuarios.\n");
    SalidaEstandar.consola("o) Imprimir usuarios que no usan el centro de computo.\n");
    SalidaEstandar.consola("p) Imprimir usuarios que necesitan su propio centro de computo.\n");
    SalidaEstandar.consola("q) Salir.\n");
  }

  /**
   * Recibe la entrada del usuario para elegir una de las opciones.
   */
  public void seleccionarOpcion() {
    String respuesta = EntradaConsola.consolaCadena();
    switch (respuesta) {
    case "a":
      registroCentroComputo.imprimirComputadoras();
      break;
    case "b":
      registroCentroComputo.imprimirComputadorasRamEspecifica(16);
      break;
    case "c":
      registroCentroComputo.imprimirComputadoraEspecifica(registroCentroComputo.buscarComputadora(seleccionarComputadora()));
      break;
    case "d":
      registroCentroComputo.agregarAplicacionComputadora(seleccionarComputadora(), seleccionarAplicacion());
      break;
    case "e":
      registroCentroComputo.eliminarAplicacionComputadora(seleccionarComputadora(), seleccionarAplicacion());
      break;
    case "f":
      CentroComputo centro = registroCentroComputo.buscarCentroComputo(seleccionarCentroComputo());
      if (centro == null) {
        SalidaEstandar.consola("No existe ese centro de computo\n");
        break;
      }
      registroCentroComputo.agregarComputadora(crearComputadora(centro));
      break;
    case "g":
      registroCentroComputo.eliminarComputadora(seleccionarComputadora());
      break;
    case "h":
      registroCentroComputo.buscarComputadoraConAplicacion("Firefox");
      break;
    case "i":
      registroCentroComputo.buscarComputadoraCorreAplicacion(seleccionarAplicacion());
      break;
    case "j":
      registroCentroComputo.buscarComputadoraConAplicacionRamMinima(16);
      break;
    case "k":
      registroCentroComputo.imprimirUsuariosComputadora(seleccionarComputadora());
      break;
    case "l":
      registroCentroComputo.imprimirUsosUsuarioComputadora(seleccionarComputadora(), seleccionarUsuario());
      break;
    case "m":
      registroCentroComputo.imprimirAplicacionesUsuarioFecha(seleccionarUsuario(), seleccionarFecha());
      break;
    case "n":
      registroCentroComputo.imprimirAplicacionMasUtilizada();
      registroCentroComputo.imprimirAplicacionMenosUtilizada();
      break;
    case "o":
      registroCentroComputo.imprimirUsuariosInactivos();
      break;
    case "p":
      registroCentroComputo.imprimirUsuariosDemasiadoActivos();
      break;
    case "q":
      System.exit(0);
    }
  }

  /**
   * Permite al usuario seleccionar el indice de una computadora.
   * @return Regresa el indice seleccionado por el usuario.
   */
  public int seleccionarComputadora() {
    registroCentroComputo.listarComputadoras();
    SalidaEstandar.consola("Ingrese el numero de la computadora deseada\n:");
    return EntradaConsola.consolaInt();
  }

  /**
   * Permite al usuario seleccionar el nombre de una aplicacion.
   * @return Regresa el nombre seleccionado por el usuario.
   */
  public String seleccionarAplicacion() {
    registroCentroComputo.listarAplicaciones();
    SalidaEstandar.consola("Ingrese el nombre de la aplicacion deseada\n:");
    return EntradaConsola.consolaCadena();
  }

  /**
   * Permite al usuario seleccionar el indice de un usuario.
   * @return Regresa el indice seleccionado.
   */
  public int seleccionarUsuario() {
    registroCentroComputo.listarUsuarios();
    SalidaEstandar.consola("Ingrese el numero del usuario deseado\n:");
    return EntradaConsola.consolaInt();
  }

  /**
   * Permite al usuario seleccionar el indice de un centro de computo.
   * @return Regresa el indice seleccionado por el usuario.
   */
  public int seleccionarCentroComputo() {
    registroCentroComputo.listarCentrosComputo();
    SalidaEstandar.consola("Ingrese el numero del centro de computo deseado\n:");
    return EntradaConsola.consolaInt();
  }

  /**
   * Permite al usuario ingresar una fecha.
   * @return Regresa la fecha creada.
   */
  public Fecha seleccionarFecha() {
    SalidaEstandar.consola("Ingrese el numero de dia\n:");
    int dia = EntradaConsola.consolaInt();
    SalidaEstandar.consola("Ingrese el numero de mes\n:");
    int mes = EntradaConsola.consolaInt();
    SalidaEstandar.consola("Ingrese el numero de anio\n:");
    int anio = EntradaConsola.consolaInt();
    return new Fecha(dia, mes, anio, 0, 0);
  }

  /**
   * Permite al usuario ingresar los datos de una nueva computadora.
   * @param centroComputo Es el centro de computo al cual pertenece la computadora.
   * @return Regresa el nueva computadora creada.
   */
  public Computadora crearComputadora(CentroComputo centroComputo) {
    SalidaEstandar.consola("Ingrese la cantidad de ram (GB) de la computadora\n:");
    int ram = EntradaConsola.consolaInt();
    SalidaEstandar.consola("Ingrese la capacidad del disco (GB) duro de la computadora\n:");
    int discoDuro = EntradaConsola.consolaInt();
    SalidaEstandar.consola("Ingrese el nombre del procesador de la computadora\n:");
    String procesador = EntradaConsola.consolaCadena();
    SalidaEstandar.consola("Ingrese la marca de la computadora\n:");
    String marca = EntradaConsola.consolaCadena();
    return new Computadora(centroComputo, ram, discoDuro, procesador, marca);
  }
}
