package imagen;

import edlineal.ArregloNumerico;
import ednolineal.Matriz2DNumerica;
import entradasalida.SalidaEstandar;
import imagen.lector.LectorImagen;

/**
 * Esta clase se encarga de hacer operaciones con imagenes.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class ImageEditor {
    Matriz2DNumerica buffer;
    LectorImagen imagen;

    /**
     * Crea una nueva instancia para modificar la imagen con el nombre dado.
     * @param nombreImagen Es el nombre de la imagen.
     */
    public void leerImagen(String nombreImagen) {
        imagen = new LectorImagen(nombreImagen);
        buffer = new Matriz2DNumerica(imagen.getWidth(), imagen.getHeight());
        for (int renglon = 0; renglon < buffer.renglones(); renglon++) {
            for (int columna = 0; columna < buffer.columnas(); columna++) {
                buffer.cambiarInfo(renglon, columna, imagen.getPixel(renglon, columna));
            }
        }
    }

    /**
     * Escribe los cambios hechos en el buffer en el archivo de la imagen.
     */
    public void escribirImagen() {
        for (int renglon = 0; renglon < buffer.renglones(); renglon++) {
            for (int columna = 0; columna < buffer.columnas(); columna++) {
                double recuperado = (double) buffer.obtenerInfo(renglon, columna);
                int pixel = (int) recuperado;
                imagen.setRGB(renglon, columna, pixel);
            }
        }
        imagen.escribirImagen();
    }

    /**
     * Obtiene el canal alfa de un pixel.
     * @param pixel Es el pixel del cual se obtendra el canal.
     * @return Regresa el canal obtenido.
     */
    public int pixelToAlfa(int pixel) {
        byte a;
        pixel = pixel >> 24;
        a = (byte) pixel;

        int alfa = a;
        if (alfa < 0) {
            alfa = 256+alfa;
        }
        return alfa;
    }

    /**
     * Obtiene el canal rojo de un pixel.
     * @param pixel Es el pixel del cual se obtendra el canal.
     * @return Regresa el canal obtenido.
     */
    public int pixelToRed(int pixel) {
        byte r;
        pixel = pixel >> 16;
        r = (byte) pixel;

        int red = r;
        if (red < 0) {
            red = 256+red;
        }
        return red;
    }

    /**
     * Obtiene el canal verde de un pixel.
     * @param pixel Es el pixel del cual se obtendra el canal.
     * @return Regresa el canal obtenido.
     */
    public int pixelToGreen(int pixel) {
        byte g;
        pixel = pixel >> 8;
        g = (byte) pixel;

        int green = g;
        if (green < 0) {
            green = 256+green;
        }
        return green;
    }

    /**
     * Obtiene el canal azul de un pixel.
     * @param pixel Es el pixel del cual se obtendra el canal.
     * @return Regresa el canal obtenido.
     */
    public int pixelToBlue(int pixel) {
        byte b;
        b = (byte) pixel;

        int blue = b;
        if (blue < 0) {
            blue = 256+blue;
        }
        return blue;
    }

    /**
     * Convierte la imagen a escala de grises.
     */
    public void imageToGrises() {
        for (int renglon = 0; renglon < buffer.renglones(); renglon++) {
            for (int columna = 0; columna < buffer.columnas(); columna++) {
                double recuperado = (double) buffer.obtenerInfo(renglon, columna);
                int pixel = (int) recuperado;
                int alfa = pixelToAlfa(pixel)<<24;
                int red = pixelToRed(pixel);
                int green = pixelToGreen(pixel);
                int blue = pixelToBlue(pixel);
                int fusion = (red+green+blue)/3;
                red = fusion<<16;
                green = fusion<<8;
                blue = fusion;

                buffer.cambiarInfo(renglon, columna, red+green+blue+alfa);
            }
        }
    }

    /**
     * Cambia el brillo de la imagen en un valor dado.
     * @param cambio Es el valor a incrementar/decrementar.
     */
    public void modificarBrillo(int cambio) {
        for (int renglon = 0; renglon < buffer.renglones(); renglon++) {
            for (int columna = 0; columna < buffer.columnas(); columna++) {
                double recuperado = (double) buffer.obtenerInfo(renglon, columna);
                int pixel = (int) recuperado;
                int alfa = pixelToAlfa(pixel)<<24;
                int red = pixelToRed(pixel);
                int green = pixelToGreen(pixel);
                int blue = pixelToBlue(pixel);
                red += cambio;
                green += cambio;
                blue += cambio;
                if (red > 255) {
                    red = 255;
                }
                if (red < 0) {
                    red = 0;
                }
                if (green > 255) {
                    green = 255;
                }
                if (green < 0) {
                    green = 0;
                }
                if (blue > 255) {
                    blue = 255;
                }
                if (blue < 0) {
                    blue = 0;
                }
                red = red<<16;
                green = green<<8;

                buffer.cambiarInfo(renglon, columna, red+green+blue+alfa);
            }
        }
    }

    /**
     * Voltea la imagen horizontalmente.
     */
    public void invertirX() {
        Matriz2DNumerica bufferCopy = buffer.clonar();
        int count = 0;
        for (int columna = 0; columna < buffer.columnas(); columna++) {
            for (int renglon = buffer.renglones()-1; renglon >= 0; renglon--) {
                double recuperado = (double) bufferCopy.obtenerInfo(renglon, columna);
                int pixel = (int) recuperado;
                buffer.cambiarInfo(count, columna, pixel);
                count++;
            }
            count = 0;
        }
    }

    /**
     * Voltea la imagen verticalmente.
     */
    public void invertirY() {
        Matriz2DNumerica bufferCopy = buffer.clonar();
        int count = 0;
        for (int renglon = 0; renglon < buffer.renglones(); renglon++) {
            for (int columna = buffer.columnas()-1; columna >= 0; columna--) {
                double recuperado = (double) bufferCopy.obtenerInfo(renglon, columna);
                int pixel = (int) recuperado;
                buffer.cambiarInfo(renglon, count, pixel);
                count++;
            }
            count = 0;
        }
    }

    /**
     * Convierte la imagen a su forma transpuesta.
     */
    public void imagenTranspuesta() {
        buffer.aplicarTranspuesta();
        imagen.redimensionarImagen(buffer.renglones(), buffer.columnas());
    }

    /**
     * Cambia el tamanio de la imagen.
     * @param tipoTamanio Es el nuevo tamanio que tendra.
     */
    public void redimensionarImagen(TipoTamanio tipoTamanio) {
        int num = 1;
        int div = 1;

        switch (tipoTamanio) {
            case TERCIO:
                div = 3;
                break;
            case MITAD:
                div = 2;
                break;
            case DOBLE:
                num = 2;
                break;
            case TRIPLE:
                num = 3;
                break;
            case CUADRUPLE:
                num = 4;
                break;
        }
        redimensionarImagen(imagen.getWidth()*num/div, imagen.getHeight()*num/div);
    }

    /**
     * Cambia el tamanio de la imagen.
     * @param width Es el nuevo ancho de la imagen.
     * @param height Es la nueva altura de la imagen.
     */
    public void redimensionarImagen(int width, int height) {
        resizeWidth(width);
        resizeHeight(height);
    }

    private void resizeWidth(int width) {
        double step = (double) width/buffer.renglones();

        Matriz2DNumerica bufferCopy = buffer.clonar();
        imagen.redimensionarImagen(width, imagen.getHeight());
        buffer.redimensionar(width, buffer.columnas());

        for (int columna = 0; columna < buffer.columnas(); columna++) {
            for (int renglon = 0; renglon < buffer.renglones(); renglon++) {
                buffer.cambiarInfo(renglon, columna, bufferCopy.obtenerInfo((int) ((double)renglon/step), columna));
            }
        }
    }

    private void resizeHeight(int height) {
        double step = (double) height/ buffer.columnas();

        Matriz2DNumerica bufferCopy = buffer.clonar();
        imagen.redimensionarImagen(imagen.getWidth(), height);
        buffer.redimensionar(buffer.renglones(), height);

        for (int renglon = 0; renglon < buffer.renglones(); renglon++) {
            for (int columna = 0; columna < buffer.columnas(); columna++) {
                buffer.cambiarInfo(renglon, columna, bufferCopy.obtenerInfo(renglon, (int) ((double)columna/step)));
            }
        }
    }

    /**
     * Invierte los colores de la imagen.
     */
    public void invertirColores() {
        for (int renglon = 0; renglon < buffer.renglones(); renglon++) {
            for (int columna = 0; columna < buffer.columnas(); columna++) {
                double recuperado = (double) buffer.obtenerInfo(renglon, columna);
                int pixel = (int) recuperado;
                int alfa = pixelToAlfa(pixel)<<24;
                int red = pixelToRed(pixel);
                int green = pixelToGreen(pixel);
                int blue = pixelToBlue(pixel);
                red = 255-red;
                green = 255-green;
                blue = 255-blue;
                red = red<<16;
                green = green<<8;
                blue = blue;

                buffer.cambiarInfo(renglon, columna, red+green+blue+alfa);
            }
        }
    }

    /**
     * Agrega un marco a la imagen.
     * @param grosor Es el grosor en pixeles del marco.
     * @param color Es el color del marco.
     */
    public void generarMarco(int grosor, int color) {
        Matriz2DNumerica marco = new Matriz2DNumerica(buffer.renglones()+grosor*2, buffer.columnas()+grosor*2, color);
        for (int renglon = 0; renglon < buffer.renglones(); renglon++) {
            for (int columna = 0; columna < buffer.columnas(); columna++) {
                marco.cambiarInfo(grosor+renglon, grosor+columna, buffer.obtenerInfo(renglon, columna));
            }
        }
        buffer.redefinirMatriz(marco);
        imagen.redimensionarImagen(buffer.renglones(), buffer.columnas());
    }
}
