package edlineal;

import edlineal.catalogo.Nodo;

/**
 * Esta clase es una cola con almacenamiento dinamico.
 * */
public class ColaLista implements Lote{
    protected ListaLigada datosCola;

    /**
     * Crea una nueva instancia de ColaLista.
     * */
    public  ColaLista(){
        datosCola=new ListaLigada();
    }

    @Override
    public boolean vacio(){
        return datosCola.vacia();
    }

    @Override
    public boolean lleno(){
        Nodo nodo = new Nodo("x");
        return (nodo == null);
    }

    @Override
    public boolean poner(Object info){
        return (datosCola.agregar(info) != -1);
    }

    @Override
    public Object quitar(){
        return datosCola.eliminarInicio();
    }

    @Override
    public void imprimir(){
        datosCola.imprimir();
    }

    @Override
    public Object verLimite(){
        return datosCola.getPrimero();
    }
}

