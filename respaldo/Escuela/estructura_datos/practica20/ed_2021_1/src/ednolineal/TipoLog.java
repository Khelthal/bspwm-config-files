package ednolineal;

/**
 * Esta clase enumerada contiene los distintos tipos de logaritmo utilizados.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public enum TipoLog {
    NATURAL,
    BASE10,
    BASE2
}
