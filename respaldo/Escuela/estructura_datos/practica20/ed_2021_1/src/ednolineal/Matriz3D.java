package ednolineal;

import edlineal.Arreglo;
import entradasalida.SalidaEstandar;

/**
 * Esta clase maneja arreglos de 3 dimensiones y distintas operaciones con arreglos de 3 dimensiones.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class Matriz3D {
    protected int renglones;
    protected int columnas;
    protected int profundidad;
    protected Object datos[][][];

    /**
     * Crea una nueva instancia de Matriz3D con la cantidad de renglones, columnas y profundidad indicadas.
     * @param reng Es el numero de renglones de la matriz.
     * @param colum Es el numero de columnas de la matriz.
     * @param prof Es el numero de profundidad de la matriz.
     */
    public Matriz3D(int reng, int colum, int prof) {
        renglones = reng;
        columnas = colum;
        profundidad = prof;
        datos = new Object[renglones][columnas][profundidad];
    }

    /**
     * Crea una nueva instancia de Matriz3D con la cantidad de renglones, columnas y profundidad indicadas y la rellena
     * con el objeto seleccionado.
     * @param reng Es el numero de renglones de la matriz.
     * @param colum Es el numero de columnas de la matriz.
     * @param prof Es el numero de profundidad de la matriz.
     * @param info Es el objeto con el que se rellenara la matriz.
     */
    public Matriz3D(int reng, int colum, int prof, Object info) {
        renglones = reng;
        columnas = colum;
        profundidad = prof;
        datos = new Object[renglones][columnas][profundidad];
        //tendré que invocar a un método que rellene toda la matriz con datos de tipo "info"
        rellenar(info);
    }

    /**
     * Obtiene el numero de renglones de la matriz.
     * @return Regresa el numero de renglones de la matriz.
     */
    public int renglones() {
        return renglones;
    }

    /**
     * Obtiene el numero de columnas de la matriz.
     * @return Regresa el numero de columnas de la matriz.
     */
    public int columnas() {
        return columnas;
    }

    /**
     * Obtiene la profundidad de la matriz.
     * @return Regresa la profundidad de la matriz.
     */
    public int profundidad() {
        return profundidad;
    }

    /**
     * Cambia el elemento de la posicion seleccionada por un nuevo objeto.
     * @param renglon Es el renglon del elemento a cambiar.
     * @param columna Es la columna del elemento a cambiar.
     * @param prof Es la profundidad del elemento a cambiar.
     * @param info Es el objeto nuevo.
     * @return Regresa <b>true</b> si se pudo cambiar el objeto, <b>false</b> en caso contrario.
     */
    public boolean cambiarInfo(int renglon, int columna, int prof, Object info){
        if(validarDimension(renglon, renglones)==true && validarDimension(columna, columnas)==true &&
                validarDimension(prof, profundidad)==true){
            datos[renglon][columna][prof]=info;
            return true;
        }else{
            return false;
        }
    }

    /**
     * Recupera el objeto ubicado en la posicion seleccionada.
     * @param renglon Es el renglon del elemento a obtener.
     * @param columna Es la columna del elemento a obtener.
     * @param prof Es la profundidad del elemento a obtener.
     * @return Regresa el objeto si pudo encontrarlo, <b>null</b> en caso contrario.
     */
    public Object obtenerInfo(int renglon, int columna, int prof){
        if(validarDimension(renglon, renglones)==true && validarDimension(columna, columnas)==true &&
                validarDimension(prof, profundidad)==true) {
            return datos[renglon][columna][prof];
        }else{
            return null;
        }
    }

    /**
     * Determina si un valor esta en dentro de un rango valido.
     * @param dimension Es el valor a evaluar.
     * @param limiteDimension Es el limite del rango.
     * @return Regresa <b>true</b> si el valor se encuentra dentro del rango, <b>false</b> en caso contrario.
     */
    private boolean validarDimension(int dimension, int limiteDimension){
        if(dimension>=0 && dimension<limiteDimension){ //cumple los límites
            return true;
        }else{
            return false;
        }
    }

    /**
     * Rellena la matriz con el objeto indicado.
     * @param info Es el objeto con el que se rellenara la matriz.
     */
    public void rellenar(Object info){
        for(int renglon=0;renglon<renglones;renglon++){ //recorrer cada renglón
            for(int columna=0; columna<columnas;columna++){ //recorrer todas las columnas de una sola fila o renglón
                for(int prof=0;prof<profundidad;prof++) { //recorro todo los elementos de la profundidad de un solo renglon
                    // de una sola columna
                    datos[renglon][columna][prof]=info;
                }
            }
        }
    }

    /**
     * Imprime la Matriz3D columna por columna.
     */
    public void imprimirColumnas(){
        for(int columna=0;columna<columnas;columna++){ //es del de las rebanadas (campesino), columnas en nuestra matriz original
            //cada rebanada es una matriz 2D
            for(int renglon=0;renglon<renglones;renglon++) { //este es por cada año, renglones
                for (int prof = 0; prof < profundidad; prof++) { //este es por cada tierra, profundidad
                    SalidaEstandar.consola(datos[renglon][columna][prof] + " ");
                }
                //aquí termina un renglón
                SalidaEstandar.consola("\n");
            }
            //cuando acabe todos los renglones, que haga otro salto
            SalidaEstandar.consola("\n");
        }
    }

    /**
     * Imprime la Matriz3D por capas de profundidad.
     */
    public void imprimirProfundidades() {
        for (int prof = 0; prof < profundidad; prof++) {
            for (int renglon = 0; renglon < renglones; renglon++) {
                for (int columna = 0; columna < columnas; columna++) {
                    SalidaEstandar.consola(datos[renglon][columna][prof] + " ");
                }
                SalidaEstandar.consola("\n");
            }
            SalidaEstandar.consola("\n");
        }
    }

    /**
     * Extrae un conjunto de matrices 2D en un arreglo, en formato de columnas.
     * @return Regresa un arreglo que contiene una matriz 2D en cada posición.
     */
    public Arreglo aMatriz2DColumnas(){
        Arreglo matrices2D=new Arreglo(columnas);

        for(int rebanada=0; rebanada<columnas;rebanada++){
            Matriz2D matrizRebanada= new Matriz2D(renglones,profundidad);
            //ahora hay que recorrer la matriz 2d a formar
            for(int reng=0;reng<renglones;reng++){
                for(int colum=0;colum<profundidad;colum++){
                    matrizRebanada.cambiarInfo(reng,colum,datos[reng][rebanada][colum]);
                }
            }
            //aquí a este punto, ya llenamos una rebanada en la matriz, de una por una
            matrices2D.agregar(matrizRebanada);
        }
        return matrices2D;
    }

}
