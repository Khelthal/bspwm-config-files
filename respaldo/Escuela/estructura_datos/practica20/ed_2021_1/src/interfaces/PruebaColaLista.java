package interfaces;

import edlineal.ColaLista;
import edlineal.Monticulo;
import edlineal.TipoOrden;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de hacer pruebas con la clase ColaLista.
 */
public class PruebaColaLista {
    public static void main(String[] args) {
        ColaLista prueba = new ColaLista();

        prueba.poner("a");
        prueba.poner("b");
        prueba.poner("c");
        prueba.poner("d");
        prueba.poner("e");

        prueba.imprimir();
        SalidaEstandar.consola("\n");

        SalidaEstandar.consola("Eliminando el siguiente elemento en la cola: " + prueba.quitar()+  "\n");
        prueba.imprimir();
        SalidaEstandar.consola("\n");
        SalidaEstandar.consola("Eliminando el siguiente elemento en la cola: " + prueba.quitar()+  "\n");
        prueba.imprimir();
        SalidaEstandar.consola("\n");
        SalidaEstandar.consola("Eliminando el siguiente elemento en la cola: " + prueba.quitar()+  "\n");
        prueba.imprimir();
        SalidaEstandar.consola("\n");
        SalidaEstandar.consola("\n\n\n");
        
        Monticulo monticuloAsc = new Monticulo(10, TipoOrden.CRECIENTE);
        Monticulo monticuloDes = new Monticulo(10, TipoOrden.DECRECIENTE);
        ColaLista colaAsc = new ColaLista();
        ColaLista colaDes = new ColaLista();

        monticuloAsc.agregar(10);
        monticuloAsc.agregar(2);
        monticuloAsc.agregar(1);
        monticuloAsc.agregar(8);
        monticuloAsc.agregar(9);
        monticuloAsc.agregar(5);
        monticuloAsc.agregar(7);
        monticuloAsc.agregar(4);
        monticuloAsc.agregar(3);
        monticuloAsc.agregar(6);

        SalidaEstandar.consola("Monticulo ascendente\n");
        monticuloAsc.imprimirOI();

        while (!monticuloAsc.vacio()) {
            Object elem = monticuloAsc.obtener();
            monticuloAsc.eliminar();
            colaAsc.poner(elem);
        }
        
        colaAsc.imprimir();
        SalidaEstandar.consola("\n\n");

        monticuloDes.agregar(10);
        monticuloDes.agregar(2);
        monticuloDes.agregar(1);
        monticuloDes.agregar(8);
        monticuloDes.agregar(9);
        monticuloDes.agregar(5);
        monticuloDes.agregar(7);
        monticuloDes.agregar(4);
        monticuloDes.agregar(3);
        monticuloDes.agregar(6);

        SalidaEstandar.consola("Monticulo descendente\n");
        monticuloDes.imprimirOI();

        while (!monticuloDes.vacio()) {
            Object elem = monticuloDes.obtener();
            monticuloDes.eliminar();
            colaDes.poner(elem);
        }

        colaDes.imprimir();
        SalidaEstandar.consola("\n");
        
    }
}
