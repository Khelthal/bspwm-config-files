package interfaces;

import edlineal.Arreglo;
import edlineal.ArregloNumerico;
import entradasalida.SalidaEstandar;

/**
 * Esta clase sirve para hacer pruebas con arreglos numericos.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class PruebaArregloNumerico {
    public static void main(String[] args) {
        ArregloNumerico a = new ArregloNumerico(10);
        ArregloNumerico b = new ArregloNumerico(10);
        ArregloNumerico c = new ArregloNumerico(10);
        ArregloNumerico d = new ArregloNumerico(10);
        ArregloNumerico e = new ArregloNumerico(10);
        d.agregar(5);
        d.agregar(10);
        d.agregar(2);

        e.agregar(3);
        e.agregar(4);
        e.agregar(1);

        SalidaEstandar.consola(d.productoPunto(e)+"\n");
        d.imprimirOI();
    }
}
