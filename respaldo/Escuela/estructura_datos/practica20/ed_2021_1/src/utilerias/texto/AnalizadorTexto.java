package utilerias.texto;

import edlineal.ArregloNumerico;
import edlineal.PilaArreglo;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de hacer operaciones con cadenas de texto.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class AnalizadorTexto {
    /**
     * Determina si una cadena de texto es un palindromo.
     * @param string Es la cadena de texto a analizar.
     * @return Regresa <b>true</b> si la palabra es un palindromo, <b>false</b> en caso contrario.
     */
    public static boolean esPalindromo(String string) {
        PilaArreglo pila = new PilaArreglo(string.length());
        String invertido = "";
        for (int i = 0; i < string.length(); i++) {
            pila.poner(string.charAt(i));
        }
        while (!pila.vacio()) {
            invertido += pila.quitar();
        }
        return string.compareToIgnoreCase(invertido) == 0;
    }

    /**
     * Busca si existen puntos '.' despues de algun signo '?' o '!'.
     * @param string Es la cadena de texto a analizar.
     * @return Regresa un ArregloNumerico con los indices donde existen errores
     */
    public static ArregloNumerico puntuacionIncorrecta(String string) {
        PilaArreglo pila = new PilaArreglo(string.length());
        ArregloNumerico indices = new ArregloNumerico(string.length());
        for (int i = 0; i < string.length(); i++) {
            char c = string.charAt(i);
            if (c == '.') {
                char s = (char) pila.verLimite();
                if (s == '!' || s == '?') {
                    indices.agregar(i);
                    pila.poner(c);
                }
            }
            else {
                pila.poner(c);
            }
        }
        return indices;
    }

    /**
     * Obtiene los indices de mayusculas incorrectas en una cadena.
     * @param string Es la cadena a analizar.
     * @return Regresa un arreglo con las posiciones donde deberia haber mayusculas.
     */
    public static ArregloNumerico mayusculasIncorrectas(String string) {
        PilaArreglo pila = new PilaArreglo(string.length());
        ArregloNumerico indices = new ArregloNumerico(string.length());
        for (int i = 0; i < string.length(); i++) {
            char c = string.charAt(i);
            if (!esLetra(c) && c != '.') {
                continue;
            }
            if (pila.vacio()) {
                pila.poner(c);
            }
            else {
                if ((char) pila.verLimite() == '.') {
                    if (!esMayuscula(c)) {
                        indices.agregar(i);
                    }
                }
                pila.poner(c);
            }
        }
        return indices;
    }

    /**
     * Obtiene los indices donde hay agrupadores incorrectos.
     * @param string Es la cadena a analizar.
     * @return Regresa un arreglo con los agrupadores corregidos.
     */
    public static ArregloNumerico agrupadoresIncorrectos(String string) {
        PilaArreglo incorrectos = new PilaArreglo(string.length());
        PilaArreglo pivote = new PilaArreglo(string.length());
        ArregloNumerico indices = new ArregloNumerico(string.length());

        for (int i = 0; i < string.length(); i++) {
            char c = string.charAt(i);

            if (obtenerTipoAgrupacion(c) > 0) {
                if (obtenerTipoAgrupacion(c) >= 7) {
                    boolean correcto = false;
                    while (!incorrectos.vacio()) {
                        if (obtenerTipoAgrupacion(string.charAt((int) incorrectos.verLimite())) == obtenerTipoAgrupacion(c)) {
                            correcto = true;
                            incorrectos.quitar();
                            break;
                        }
                        else {
                            pivote.poner(incorrectos.quitar());
                        }
                    }
                    while (!pivote.vacio()) {
                        incorrectos.poner(pivote.quitar());
                    }
                    if (!correcto) {
                        incorrectos.poner(i);
                    }
                }
                else {
                    if (esAgrupadorApertura(c)) {
                        incorrectos.poner(i);
                    }
                    else {
                        boolean correcto = false;
                        while (!incorrectos.vacio()) {
                            if (obtenerTipoAgrupacion(string.charAt((int) incorrectos.verLimite())) == obtenerTipoAgrupacion(c) && esAgrupadorApertura(string.charAt((int) incorrectos.verLimite()))) {
                                correcto = true;
                                incorrectos.quitar();
                                break;
                            }
                            else {
                                pivote.poner(incorrectos.quitar());
                            }
                        }
                        while (!pivote.vacio()) {
                            incorrectos.poner(pivote.quitar());
                        }
                        if (!correcto) {
                            incorrectos.poner(i);
                        }
                    }
                }
            }
        }

        while (!incorrectos.vacio()) {
            pivote.poner(incorrectos.quitar());
        }
        while (!pivote.vacio()) {
            indices.agregar((int) pivote.quitar());
        }
        return indices;
    }

    /**
     * Determina si una letra es mayuscula.
     * @param c Es la letra a analizar.
     * @return Regresa <b>true</b> si la letra era mayuscula.
     */
    public static boolean esMayuscula(char c) {
        if (c >= 65 && c <= 90) {
            return true;
        }
        return false;
    }

    /**
     * Determina si el char es una letra.
     * @param token Es la letra a analizar.
     * @return Regresa <b>true</b> si era una letra.
     */
    public static boolean esLetra(char token) {
        if (token >= 65 && token <= 90 || token >= 97 && token <= 122) {
            return true;
        }
        return false;
    }

    /**
     * Obtiene el tipo de un agrupador.
     * @param c Es el agrupador a analizar.
     * @return Regresa el tipo del agrupador.
     */
    private static int obtenerTipoAgrupacion(char c) {
        switch (c) {
            default:
                return 0;
            case '<':
            case '>':
                return 1;
            case '[':
            case ']':
                return 2;
            case '{':
            case '}':
                return 3;
            case '«':
            case '»':
                return 4;
            case '¡':
            case '!':
                return 5;
            case '¿':
            case '?':
                return 6;
            case '\"':
                return 7;
            case '\'':
                return 8;
            case '_':
                return 9;
        }
    }

    /**
     * Determina si un agrupador es de apertura.
     * @param c Es el agrupador a analizar.
     * @return Regresa <b>true</b> si el agrupador es de apertura, <b>false</b> en caso contrario.
     */
    public static boolean esAgrupadorApertura(char c) {
        if (c == '(' || c == '[' || c == '{' || c == '\"' || c == '«' || c == '¿' || c == '¡' || c == '<') {
            return true;
        }
        return false;
    }
}
