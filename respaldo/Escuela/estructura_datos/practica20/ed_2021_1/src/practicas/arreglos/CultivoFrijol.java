package practicas.arreglos;

import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;

/**
 * Esta es una clase para probar la clase Campesino y la clase MenuCultivoFrijol.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class CultivoFrijol {
    public static void main(String[] args) {
        Campesino juan = new Campesino("Juan");
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + juan + " en " + juan.obtenerTerreno(0) + ": ");
        juan.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + juan + " en " + juan.obtenerTerreno(1) + ": ");
        juan.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + juan + " en " + juan.obtenerTerreno(2) + ": ");
        juan.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + juan + " en " + juan.obtenerTerreno(3) + ": ");
        juan.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + juan + " en " + juan.obtenerTerreno(4) + ": ");
        juan.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + juan + " en " + juan.obtenerTerreno(5) + ": ");
        juan.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + juan + " en " + juan.obtenerTerreno(6) + ": ");
        juan.agregar(EntradaConsola.consolaDouble());

        Campesino jose = new Campesino("Jose");
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + jose + " en " + jose.obtenerTerreno(0) + ": ");
        jose.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + jose + " en " + jose.obtenerTerreno(1) + ": ");
        jose.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + jose + " en " + jose.obtenerTerreno(2) + ": ");
        jose.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + jose + " en " + jose.obtenerTerreno(3) + ": ");
        jose.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + jose + " en " + jose.obtenerTerreno(4) + ": ");
        jose.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + jose + " en " + jose.obtenerTerreno(5) + ": ");
        jose.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + jose + " en " + jose.obtenerTerreno(6) + ": ");
        jose.agregar(EntradaConsola.consolaDouble());

        Campesino pedro = new Campesino("Pedro");
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + pedro + " en " + pedro.obtenerTerreno(0) + ": ");
        pedro.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + pedro + " en " + pedro.obtenerTerreno(1) + ": ");
        pedro.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + pedro + " en " + pedro.obtenerTerreno(2) + ": ");
        pedro.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + pedro + " en " + pedro.obtenerTerreno(3) + ": ");
        pedro.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + pedro + " en " + pedro.obtenerTerreno(4) + ": ");
        pedro.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + pedro + " en " + pedro.obtenerTerreno(5) + ": ");
        pedro.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + pedro + " en " + pedro.obtenerTerreno(6) + ": ");
        pedro.agregar(EntradaConsola.consolaDouble());

        Campesino miguel = new Campesino("Miguel");
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + miguel + " en " + miguel.obtenerTerreno(0) + ": ");
        miguel.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + miguel + " en " + miguel.obtenerTerreno(1) + ": ");
        miguel.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + miguel + " en " + miguel.obtenerTerreno(2) + ": ");
        miguel.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + miguel + " en " + miguel.obtenerTerreno(3) + ": ");
        miguel.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + miguel + " en " + miguel.obtenerTerreno(4) + ": ");
        miguel.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + miguel + " en " + miguel.obtenerTerreno(5) + ": ");
        miguel.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + miguel + " en " + miguel.obtenerTerreno(6) + ": ");
        miguel.agregar(EntradaConsola.consolaDouble());

        Campesino ramiro = new Campesino("Ramiro");
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + ramiro + " en " + ramiro.obtenerTerreno(0) + ": ");
        ramiro.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + ramiro + " en " + ramiro.obtenerTerreno(1) + ": ");
        ramiro.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + ramiro + " en " + ramiro.obtenerTerreno(2) + ": ");
        ramiro.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + ramiro + " en " + ramiro.obtenerTerreno(3) + ": ");
        ramiro.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + ramiro + " en " + ramiro.obtenerTerreno(4) + ": ");
        ramiro.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + ramiro + " en " + ramiro.obtenerTerreno(5) + ": ");
        ramiro.agregar(EntradaConsola.consolaDouble());
        SalidaEstandar.consola("Ingrese el numero de toneladas producidas por " + ramiro + " en " + ramiro.obtenerTerreno(6) + ": ");
        ramiro.agregar(EntradaConsola.consolaDouble());

        MenuCultivoFrijol menu = new MenuCultivoFrijol();
        menu.agregar(juan);
        menu.agregar(jose);
        menu.agregar(pedro);
        menu.agregar(miguel);
        menu.agregar(ramiro);

        menu.iniciar();
    }
}
