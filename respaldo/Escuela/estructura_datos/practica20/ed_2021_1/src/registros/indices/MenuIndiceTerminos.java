package registros.indices;

import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de imprimir el menu del diccionario y de hacer llamadas a la clase RegistroPalabras.
 */
public class MenuIndiceTerminos {
    protected IndiceTerminos indice = new IndiceTerminos();

    /**
     * Inicia el menu
     */
    public void iniciar() {
        while (true) {
            imprimirMenu();
            SalidaEstandar.consola("\n");
            seleccionarOpcion();
            SalidaEstandar.consola("Presiona enter para continuar\n");
            EntradaConsola.consolaCadena();
        }
    }

    /**
     * Imprime las opciones del menu.
     */
    public void imprimirMenu() {
        SalidaEstandar.consola("¿Que desea hacer?\n");
        SalidaEstandar.consola("a) Agregar termino.\n");
        SalidaEstandar.consola("b) Consultar un termino.\n");
        SalidaEstandar.consola("c) Consultar un subtermino.\n");
        SalidaEstandar.consola("d) Listar todos los terminos/subterminos y sus paginas.\n");
        SalidaEstandar.consola("e) Listar solo los terminos de un rango de letras iniciales.\n");
        SalidaEstandar.consola("f) Listar solo los terminos que tengan paginas dentro de un rango especifico.\n");
        SalidaEstandar.consola("q) Salir.\n");
    }

    /**
     * Recibe la entrada del usuario para elegir una de las opciones.
     */
    public void seleccionarOpcion() {
        String respuesta = EntradaConsola.consolaCadena();
        switch (respuesta) {
            case "a":
                indice.agregarTermino();
                break;
            case "b":
                indice.mostrarTermino();
                break;
            case "c":
                indice.mostrarSubTermino();
                break;
            case "d":
                indice.mostrarTerminos();
                break;
            case "e":
                indice.mostrarTerminosInicial();
                break;
            case "f":
                indice.mostrarTerminosRango();
                break;
            case "q":
                System.exit(0);
        }
    }

}
