package registros.computadoras;

/**
 * Esta clase guarda los datos de un usuario.
 */
public class Usuario {
  protected static int id=0;
  protected int numUsuario;
  protected String nombre;
  protected int edad;

  /**
   * Crea una nueva instancia de Usuario.
   * @param nombre Es el nombre del usuario.
   * @param edad Es la edad del usuario.
   */
  public Usuario(String nombre, int edad) {
    numUsuario = id;
    id++;
    this.nombre = nombre;
    this.edad = edad;
  }

  /**
   * Obtiene el numero del usuario.
   * @return Regresa el numero del usuario.
   */
  public int getNumUsuario() {
      return numUsuario;
  }

  @Override
  public String toString() {
    return numUsuario + " " + nombre;
  }
}
