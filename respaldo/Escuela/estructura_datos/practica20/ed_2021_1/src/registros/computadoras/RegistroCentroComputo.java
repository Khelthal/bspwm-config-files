package registros.computadoras;

import edlineal.ListaLigada;
import entradasalida.SalidaEstandar;
import utilerias.fecha.Fecha;

/**
 * Esta clase se encarga de guardar registros de un centro de computo.
 */
public class RegistroCentroComputo {
  protected ListaLigada computadoras;
  protected ListaLigada centrosComputo;
  protected ListaLigada aplicaciones;
  protected ListaLigada usuarios;

  /**
   * Inicializa una nueva instancia de la clase.
   */
  public RegistroCentroComputo() {
    computadoras = new ListaLigada();
    centrosComputo = new ListaLigada();
    aplicaciones = new ListaLigada();
    usuarios = new ListaLigada();
  }

  /**
   * Lista todas las computadoras registradas.
   */
  public void listarComputadoras() {
    computadoras.imprimir();
    SalidaEstandar.consola("\n");
  }

  /**
   * Lista los centros de computo registrados.
   */
  public void listarCentrosComputo() {
    centrosComputo.imprimir();
    SalidaEstandar.consola("\n");
  }

  /**
   * Lista todas las aplicaciones registradas.
   */
  public void listarAplicaciones() {
    aplicaciones.imprimir();
    SalidaEstandar.consola("\n");
  }

  /**
   * Lista todos los usuarios registrados.
   */
  public void listarUsuarios() {
    usuarios.imprimir();
    SalidaEstandar.consola("\n");
  }

  /**
   * Imprime todas las computdoras registradas con sus datos.
   */
  public void imprimirComputadoras() {
    Computadora computadora;
    computadoras.inicializarIterador();
    while (computadoras.hayMas()) {
      computadora = (Computadora) computadoras.obtenerSigiuente();
      computadora.imprimir();
      SalidaEstandar.consola("\n");
    }
  }

  /**
   * Imprime las computadoras con la cantidad de ram especificada.
   * 
   * @param ram Es la ram necesaria.
   */
  public void imprimirComputadorasRamEspecifica(int ram) {
    Computadora computadora;
    computadoras.inicializarIterador();
    while (computadoras.hayMas()) {
      computadora = (Computadora) computadoras.obtenerSigiuente();
      if (computadora.getRam() == ram) {
        computadora.imprimir();
        SalidaEstandar.consola("\n");
      }
    }
  }

  /**
   * Imprime las caracteristicas de una computadora especifica.
   * 
   * @param computadora Es la computadora a imprimir.
   */
  public void imprimirComputadoraEspecifica(Computadora computadora) {
    if (computadora == null) {
      SalidaEstandar.consola("No existe una computadora con ese numero\n");
      return;
    }
    computadora.imprimir();
  }

  /**
   * Busca una computadora especifica.
   * 
   * @param numComputadora Es el numero de la computadora a buscar.
   * @return Regresa la computadora encontrada, null si no la encontro.
   */
  public Computadora buscarComputadora(int numComputadora) {
    Computadora computadora = null;
    computadoras.inicializarIterador();
    while (computadoras.hayMas()) {
      computadora = (Computadora) computadoras.obtenerSigiuente();
      if (computadora.getNumComputadora() == numComputadora) {
        break;
      }
    }
    if (computadora == null || computadora.getNumComputadora() != numComputadora) {
      return null;
    }
    return computadora;
  }

  /**
   * Busca las computadoras que contengan una aplicacion especifica.
   * @param nombreAplicacion Es el nombre de la aplicacion.
   */
  public void buscarComputadoraConAplicacion(String nombreAplicacion) {
    Computadora computadora = null;
    computadoras.inicializarIterador();
    while (computadoras.hayMas()) {
      computadora = (Computadora) computadoras.obtenerSigiuente();
      if (computadora.contieneAplicacion(nombreAplicacion)) {
        computadora.imprimir();
        SalidaEstandar.consola("\n");
      }
    }
  }

  /**
   * Busca las computadoras que contengan y puedan correr una aplicacion
   * especifica.
   * 
   * @param nombreAplicacion Es el nombre de la aplicacion.
   */
  public void buscarComputadoraCorreAplicacion(String nombreAplicacion) {
    Computadora computadora = null;
    computadoras.inicializarIterador();
    while (computadoras.hayMas()) {
      computadora = (Computadora) computadoras.obtenerSigiuente();
      if (computadora.correAplicacion(nombreAplicacion)) {
        computadora.imprimir();
        SalidaEstandar.consola("\n");
      }
    }
  }

  /**
   * Busca computadoras que tengan aplicaciones que necesiten una cierta cantidad
   * de ram.
   * 
   * @param ramMin Es la ram que se requiere.
   */
  public void buscarComputadoraConAplicacionRamMinima(int ramMin) {
    Computadora computadora = null;
    computadoras.inicializarIterador();
    while (computadoras.hayMas()) {
      computadora = (Computadora) computadoras.obtenerSigiuente();
      if (computadora.contieneAplicacionRamMinima(ramMin)) {
        computadora.imprimir();
        SalidaEstandar.consola("\n");
      }
    }
  }

  /**
   * Imprime los usuarios de una computadora.
   * 
   * @param numComputadora Es la computadora de la cual se desea conocer sus
   *                       usuarios.
   */
  public void imprimirUsuariosComputadora(int numComputadora) {
    Computadora computadora = buscarComputadora(numComputadora);
    if (computadora == null) {
      return;
    }
    ListaLigada usuariosComputadora = computadora.obtenerUsuarios();
    Usuario u = null;
    usuariosComputadora.inicializarIterador();
    while (usuariosComputadora.hayMas()) {
      u = (Usuario) usuariosComputadora.obtenerSigiuente();
      SalidaEstandar.consola(u + "\n");
    }
  }

  /**
   * Imprime los horarios en los que un usuario utilizo una computadora
   * especifica.
   * 
   * @param numComputadora Es el numero de la computadora.
   * @param numUsuario     Es el numero del usuario.
   */
  public void imprimirUsosUsuarioComputadora(int numComputadora, int numUsuario) {
    Computadora computadora = buscarComputadora(numComputadora);
    if (computadora == null) {
      return;
    }
    SalidaEstandar.consola("La computadora pertenece a el centro de computo: " + computadora.getCc() + "\n");
    ListaLigada usosUsuario = computadora.obtenerUsosUsuario(numUsuario);
    UsoComputadora u = null;
    SalidaEstandar.consola("Los horarios en los que el usuario utilizo la computadora son los siguientes:\n");
    usosUsuario.inicializarIterador();
    while (usosUsuario.hayMas()) {
      u = (UsoComputadora) usosUsuario.obtenerSigiuente();
      SalidaEstandar.consola(u + "\n");
    }
  }

  /**
   * Imprime las aplicaciones que utilizo un usuario especifico en una fecha
   * especifica.
   * 
   * @param numUsuario Es el numero del usuario.
   * @param fecha      Es la fecha.
   */
  public void imprimirAplicacionesUsuarioFecha(int numUsuario, Fecha fecha) {
    Computadora computadora = null;
    computadoras.inicializarIterador();
    SalidaEstandar.consola("Las aplicaciones que el usuario abrio en la fecha dada son las siguientes:\n");
    while (computadoras.hayMas()) {
      computadora = (Computadora) computadoras.obtenerSigiuente();
      ListaLigada usosUsuario = computadora.obtenerUsosUsuario(numUsuario);
      UsoComputadora u = null;
      usosUsuario.inicializarIterador();
      while (usosUsuario.hayMas()) {
        u = (UsoComputadora) usosUsuario.obtenerSigiuente();
        if (u.fechaDentroDelLimite(fecha)) {
          u.imprimirAplicaciones();
          SalidaEstandar.consola("\n");
        }
      }
    }
  }

  /**
   * Imprime la aplicacion mas utilizada por los usuarios.
   */
  public void imprimirAplicacionMasUtilizada() {
    Aplicacion aplicacion = null;
    Aplicacion aplicacionMaxima = null;
    int maxCount = 0;
    int newMax = 0;
    Usuario user = null;
    Computadora computadora = null;
    UsoComputadora usoActual = null;
    aplicaciones.inicializarIterador();
    while (aplicaciones.hayMas()) {
      aplicacion = (Aplicacion) aplicaciones.obtenerSigiuente();
      newMax = 0;
      usuarios.inicializarIterador();
      while (usuarios.hayMas()) {
        user = (Usuario) usuarios.obtenerSigiuente();

        computadoras.inicializarIterador();
        while (computadoras.hayMas()) {
          computadora = (Computadora) computadoras.obtenerSigiuente();
          ListaLigada usosUsuario = computadora.obtenerUsosUsuario(user.getNumUsuario());
          usosUsuario.inicializarIterador();
          if (usosUsuario.hayMas()) {
            usoActual = (UsoComputadora) usosUsuario.obtenerSigiuente();
            if (usoActual.contieneAplicacion(aplicacion+"")) {
              newMax++;
            }
          }
        }
      }
      if (newMax > maxCount) {
        maxCount = newMax;
        aplicacionMaxima = aplicacion;
      }
    }
    SalidaEstandar.consola("La aplicacion mas usada es: " + aplicacionMaxima + "\n");
  }

  /**
   * Imprime la aplicazion menos utilizada por los usuarios.
   */
  public void imprimirAplicacionMenosUtilizada() {
    Aplicacion aplicacion = null;
    Aplicacion aplicacionMinima = null;
    int minCount = -1;
    int newMin = 0;
    Usuario user = null;
    Computadora computadora = null;
    UsoComputadora usoActual = null;
    aplicaciones.inicializarIterador();
    while (aplicaciones.hayMas()) {
      aplicacion = (Aplicacion) aplicaciones.obtenerSigiuente();
      newMin = 0;
      usuarios.inicializarIterador();
      while (usuarios.hayMas()) {
        user = (Usuario) usuarios.obtenerSigiuente();

        computadoras.inicializarIterador();
        while (computadoras.hayMas()) {
          computadora = (Computadora) computadoras.obtenerSigiuente();
          ListaLigada usosUsuario = computadora.obtenerUsosUsuario(user.getNumUsuario());
          usosUsuario.inicializarIterador();
          if (usosUsuario.hayMas()) {
            usoActual = (UsoComputadora) usosUsuario.obtenerSigiuente();
            if (usoActual.contieneAplicacion(aplicacion+"")) {
              newMin++;
            }
          }
        }
      }
      if (minCount == -1) {
        minCount = newMin;
      }
      else if (newMin  < minCount) {
        minCount = newMin;
        aplicacionMinima = aplicacion;
      }
    }
    SalidaEstandar.consola("La aplicacion menos usada es: " + aplicacionMinima + "\n");
  }

  /**
   * Imprime los usuarios que no utilizaron computadoras.
   */
  public void imprimirUsuariosInactivos() {
    Usuario user = null;
    Computadora computadora = null;
    usuarios.inicializarIterador();
    fuera: while (usuarios.hayMas()) {
      user = (Usuario) usuarios.obtenerSigiuente();

      computadoras.inicializarIterador();
      while (computadoras.hayMas()) {
        computadora = (Computadora) computadoras.obtenerSigiuente();
        ListaLigada usosUsuario = computadora.obtenerUsosUsuario(user.getNumUsuario());
        usosUsuario.inicializarIterador();
        if (usosUsuario.hayMas()) {
          continue fuera;
        }
      }
      SalidaEstandar.consola(user + "\n");
    }
  }

  /**
   * Imprime los usuarios que utilizaron computadoras mas de 15 veces.
   */
  public void imprimirUsuariosDemasiadoActivos() {
    Usuario user = null;
    Computadora computadora = null;
    int countUsos;
    usuarios.inicializarIterador();
    while (usuarios.hayMas()) {
      countUsos = 0;
      user = (Usuario) usuarios.obtenerSigiuente();

      computadoras.inicializarIterador();
      while (computadoras.hayMas()) {
        computadora = (Computadora) computadoras.obtenerSigiuente();
        ListaLigada usosUsuario = computadora.obtenerUsosUsuario(user.getNumUsuario());
        usosUsuario.inicializarIterador();
        if (usosUsuario.hayMas()) {
          usosUsuario.obtenerSigiuente();
          countUsos++;
        }
      }
      if (countUsos > 15) {
        SalidaEstandar.consola(user + "\n");
      }
    }
  }

  /**
   * Busca una aplicacion.
   * 
   * @param nombreAplicacion Es el nombre de la aplicacion a buscar.
   * @return Regresa la aplicacion encontrada.
   */
  public Aplicacion buscarAplicacion(String nombreAplicacion) {
    return (Aplicacion) aplicaciones.buscar(nombreAplicacion);
  }

  /**
   * Busca un centro de computo.
   * 
   * @param numCc Es el numero del centro de computo.
   * @return Regresa el centro de computo encontrado.
   */
  public CentroComputo buscarCentroComputo(int numCc) {
    CentroComputo centroComputo = null;
    centrosComputo.inicializarIterador();
    while (centrosComputo.hayMas()) {
      centroComputo = (CentroComputo) centrosComputo.obtenerSigiuente();
      if (centroComputo.getNumCc() == numCc) {
        break;
      }
    }
    if (centroComputo == null || centroComputo.getNumCc() != numCc) {
      return null;
    }
    return centroComputo;
  }

  /**
   * Agrega una computadora al registro.
   * 
   * @param computadora Es la computadora a agregar.
   * @return Regresa <b>true</b> si pudo agregar la computadora, <b>false</b> en
   *         caso contrario.
   */
  public boolean agregarComputadora(Computadora computadora) {
    if (computadoras.buscar(computadora) != null) {
      return false;
    }
    return computadoras.agregar(computadora) != -1;
  }

  /**
   * Elimina una computadora del registro.
   * 
   * @param numComputadora Es el numero de la computadora a eliminar.
   * @return Regresa <b>true</b> si pudo eliminar la computadora.
   */
  public boolean eliminarComputadora(int numComputadora) {
    Computadora computadora = buscarComputadora(numComputadora);
    if (computadora == null) {
      return false;
    }
    return computadoras.eliminar(computadora) != null;
  }

  /**
   * Agrega una aplicacion a una computadora.
   * 
   * @param numComputadora   Es la computadora.
   * @param nombreAplicacion Es la aplicacion.
   * @return Regresa <b>true</b> si logro agregar la aplicacion.
   */
  public boolean agregarAplicacionComputadora(int numComputadora, String nombreAplicacion) {
    Computadora computadora = buscarComputadora(numComputadora);
    Aplicacion aplicacion = buscarAplicacion(nombreAplicacion);
    if (computadora == null || aplicacion == null) {
      return false;
    }

    return computadora.agregarAplicacion(aplicacion);
  }

  /**
   * Elimina una aplicacion de una computadora.
   * 
   * @param numComputadora   Es el numero de la computadora.
   * @param nombreAplicacion Es el nombre de la aplicacion.
   * @return Regresa true si pudo eliminar la aplicacion.
   */
  public boolean eliminarAplicacionComputadora(int numComputadora, String nombreAplicacion) {
    Computadora computadora = buscarComputadora(numComputadora);
    if (computadora == null) {
      return false;
    }

    return computadora.eliminarAplicacion(nombreAplicacion);
  }

  /**
   * Agrega un centro de computo al registro.
   * 
   * @param centroComputo Es el centro de computo.
   * @return Regresa true si pudo agregar el centro de computo.
   */
  public boolean agregarCentroComputo(CentroComputo centroComputo) {
    if (centrosComputo.buscar(centroComputo) != null) {
      return false;
    }
    return centrosComputo.agregar(centroComputo) != -1;
  }

  /**
   * Agrega una aplicacion al registro.
   * 
   * @param aplicacion Es la aplicacion agregada.
   * @return Regresa true si pudo agregar la aplicacion.
   */
  public boolean agregarAplicacion(Aplicacion aplicacion) {
    if (aplicaciones.buscar(aplicacion) != null) {
      return false;
    }
    return aplicaciones.agregar(aplicacion) != -1;
  }

  /**
   * Agrega un usuario al registro.
   * 
   * @param usuario Es el usuario agregado.
   * @return Regresa true si pudo agregar al usuario.
   */
  public boolean agregarUsuario(Usuario usuario) {
    if (usuarios.buscar(usuario) != null) {
      return false;
    }
    return usuarios.agregar(usuario) != -1;
  }
}
