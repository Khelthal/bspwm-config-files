package registros.diccionarios;

/**
 * Esta clase es una Palabra con <b>nombre</b>, <b>descripcion</b> y <b>tipo</b>.
 */
public class Palabra {
    protected String nombre;
    protected String descripcion;
    protected TipoPalabra tipo;

    /**
     * Crea una palabra con los valores indicados.
     * @param nombre Es el nombre de la Palabra.
     * @param descripcion Es la definicion de la Palabra.
     * @param tipo Es el tipo de Palabra.
     */
    public Palabra(String nombre, String descripcion, TipoPalabra tipo) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.tipo = tipo;
    }

    /**
     * Obtiene el tipo de la Palabra.
     * @return Regresa el tipo de la Palabra.
     */
    public TipoPalabra getTipo() {
        return tipo;
    }

    /**
     * Obtiene los datos de la Palabra en formato de String.
     * @return Regresa la cadena con los datos de la Palabra.
     */
    public String obtenerDatos() {
        return nombre + ": (" + tipo + ") " + descripcion;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
