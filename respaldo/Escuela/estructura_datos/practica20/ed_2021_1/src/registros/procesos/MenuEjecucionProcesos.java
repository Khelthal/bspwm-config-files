package registros.procesos;

import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;

/**
 * Esta clase enumerada se encarga de imprimir el menu para las distintas acciones con procesos.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class MenuEjecucionProcesos {
    protected EjecucionProcesos ejecucionProcesos = new EjecucionProcesos();

    /**
     * Inicia el menu
     */
    public void iniciar() {
        while (true) {
            imprimirMenu();
            SalidaEstandar.consola("\n");
            seleccionarOpcion();
            SalidaEstandar.consola("Presiona enter para continuar\n");
            EntradaConsola.consolaCadena();
        }
    }

    /**
     * Imprime las opciones del menu.
     */
    public void imprimirMenu() {
        SalidaEstandar.consola("¿Que desea hacer?\n");
        SalidaEstandar.consola("a) Agregar proceso a la cola.\n");
        SalidaEstandar.consola("b) Ejecutar procesos.\n");
        SalidaEstandar.consola("q) Salir.\n");
    }

    /**
     * Recibe la entrada del usuario para elegir una de las opciones.
     */
    public void seleccionarOpcion() {
        String respuesta = EntradaConsola.consolaCadena();
        switch (respuesta) {
            case "a":
                ejecucionProcesos.agregarProceso();
                break;
            case "b":
                ejecucionProcesos.ejecutarProcesos();
                break;
            case "q":
                System.exit(0);
        }
    }

}

