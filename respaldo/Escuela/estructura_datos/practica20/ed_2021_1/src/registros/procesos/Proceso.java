package registros.procesos;

/**
 * Esta clase se encarga de guardar datos de un proceso.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class Proceso {
    String nombre;
    TipoProceso tipo;
    String comando;
    String ruta;
    String propietario;

    /**
     * Crea una nueva instancia con los valores pasado como argumento.
     * @param nombre Es el nombre del proceso.
     * @param tipo Es el tipo del proceso.
     * @param comando Es el comando del proceso.
     * @param ruta Es la ruta del proceso.
     * @param propietario Es el propietario del proceso.
     */
    public Proceso(String nombre, TipoProceso tipo, String comando, String ruta, String propietario) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.comando = comando;
        this.ruta = ruta;
        this.propietario = propietario;
    }

    /**
     * Obtiene la prioridad de un proceso basandose en el tipo de proceso.
     * @return Regresa la prioridad del proceso.
     */
    public int getPrioridad() {
        switch (tipo) {
            case SISTEMA_OPERATIVO:
                return 4;
            case DESCARGA:
                return 3;
            case COPIADO:
                return 2;
            case IMPRESION:
                return 1;
            default:
                return 0;
        }
    }

    @Override
    public String toString() {
        return nombre + " " + tipo + " " + comando + " " + ruta + " " + propietario;
    }
}
