package registros.procesos;

/**
 * Esta clase enumerada se encarga de instanciar e iniciar la clase MenuEjecucionProcesos.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class PruebaEjecucionProcesos {
    public static void main(String[] args) {
        MenuEjecucionProcesos menuEjecucionProcesos = new MenuEjecucionProcesos();
        menuEjecucionProcesos.iniciar();
    }
}
