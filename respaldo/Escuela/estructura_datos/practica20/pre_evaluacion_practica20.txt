Práctica No. 20
Nombre del alumno: Elías Emiliano Beltrán González

	Pre-Evaluación para prácticas de Laboratorio de Estructuras de Datos						
							                                   PRE-EVALUACIÓN
                                       							   DEL ALUMNO
1	CUMPLE CON LA FUNCIONALIDAD SOLICITADA.	SI					
2	DISPONE DE CÓDIGO AUTODOCUMENTADO.	SI
3	DISPONE DE CÓDIGO DOCUMENTADO A NIVEL DE CLASE Y MÉTODO. SI					
4	DISPONE DE INDENTACIÓN CORRECTA. SI	
5	CUMPLE LA POO. SI					
6	DISPONE DE UNA FORMA FÁCIL DE UTILIZAR EL PROGRAMA PARA EL USUARIO.	SI
7	DISPONE DE UN REPORTE CON FORMATO IDC.	SI					
8	LA INFORMACIÓN DEL REPORTE ESTÁ LIBRE DE ERRORES DE ORTOGRAFÍA. SI					
9	SE ENTREGA EN TIEMPO Y FORMA LA PRÁCTICA.	SI
10	LA PRÁCTICA ESTÁ TOTALMENTE REALIZADA (ESPECIFIQUE EL PORCENTAJE COMPLETADO). 100%			
11	INCLUYE LA DOCUMENTACIÓN GENERADA CON JAVADOC.	SI					

Observaciones:
Dentro del paquete edlineal:
  Se creó la clase ListaLigadaHash

  Se movió la clase NodoDoble al paquete catalogo

  Dentro del paquete catalogo:
    Se creó la clase NodoHash
