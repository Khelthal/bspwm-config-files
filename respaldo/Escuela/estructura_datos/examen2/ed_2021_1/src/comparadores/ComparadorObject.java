package comparadores;

import edlineal.TipoOrden;

/**
 * Esta clase compara objetos para su ordenamiento.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class ComparadorObject {

    /**
     * Compara dos objetos como string.
     * @param o1 Es uno de los objetos a ser comparado.
     * @param o2 Es uno de los objetos a ser comparado.
     * @return Regresa un numero que indica si un objeto es menor, mayor o igual al otro.
     */
    public static int compare(Object o1, Object o2) {
        String s1 = o1.toString();
        String s2 = o2.toString();

        return s1.compareToIgnoreCase(s2);
    }
}
