package entradasalida;

/**
 * Esta clase se encarga de manejar la informacion que se muestra en pantalla.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class SalidaEstandar {
    /**
     * Muestra en pantalla la cadena recibida.
     * @param cadena Es la cadena a mostrar en pantalla.
     */
    public static void consola(String cadena){
        System.out.print(cadena);
    }

}
