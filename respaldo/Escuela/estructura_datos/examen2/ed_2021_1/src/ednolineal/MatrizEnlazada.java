package ednolineal;

import comparadores.ComparadorObject;
import edlineal.Arreglo;
import edlineal.catalogo.NodoDoble;
import entradasalida.SalidaEstandar;

/**
 * MatrizEnlazada es una matriz que usa NodoDoble.
 */
public class MatrizEnlazada {
  NodoDoble primero;

  /**
   * Determina si la matriz esta vacia.
   * @return Regresa true si esta vacia, false en caso contrario.
   */
  public boolean vacia() {
    return primero == null;
  }

  /**
   * Inserta un elemento al final de las cabezas.
   * @param info Es el elemento a insertar.
   * @return Regresa true si pudo insertar el elemento.
   */
  public boolean insertarFinalCabezas(Object info) {
    NodoDoble nuevoNodo = new NodoDoble(info);
    if (nuevoNodo == null) {
      return false;
    }
    if (vacia()) {
      primero = nuevoNodo;
      return true;
    } else {
      NodoDoble ultimo = obtenerUltimoCabezas(primero);
      ultimo.setLigaDer(nuevoNodo);
      return true;
    }
  }

  /**
   * Elimina un elemento de las cabezas.
   * @param info Es el elemento a eliminar.
   * @return Regresa el elemento eliminado.
   */
  public Object eliminarCabezas(Object info) {
    Object elementoBorrado = null;
    if (vacia() == false) { // no está vacía, hay algo
      NodoDoble anterior = primero;
      NodoDoble encontrado = primero;
      while (encontrado != null && !encontrado.getInfo().toString().equalsIgnoreCase(info.toString())) {
        anterior = encontrado;
        encontrado = encontrado.getLigaDer();
      }
      if (encontrado == null) {
        return null;
      } else {
        elementoBorrado = encontrado.getInfo();
        if (primero == encontrado) {
          primero = primero.getLigaDer();
        } else {
          NodoDoble siguiente = encontrado.getLigaDer();
          anterior.setLigaDer(siguiente);// 2
        }
        return elementoBorrado;
      }
    } else { // está vacía la lista, a)
      return null;
    }
  }

  private void imprimirCabezas(NodoDoble recorrer) {
    if (recorrer == null) {
      SalidaEstandar.consola("null");
    } else {
      SalidaEstandar.consola(recorrer.getInfo() + " -> ");
      imprimirCabezas(recorrer.getLigaDer());
    }
  }

  /**
   * Imprime la MatrizEnlazada.
   * */
  public void imprimir() {
    if (vacia()) {
      return;
    }
    imprimirCabezas(primero);
    SalidaEstandar.consola("\n");
    NodoDoble raiz = primero;
    while (raiz != null) {
      raiz = raiz.getLigaDer();
      SalidaEstandar.consola("^    ");
    }
    SalidaEstandar.consola("\n");
    raiz = primero;
    while (raiz != null) {
      raiz = raiz.getLigaDer();
      SalidaEstandar.consola("V    ");
    }
    SalidaEstandar.consola("\n");
    int prof = 0;
    while (true) {
      int validos = 0;
      raiz = primero;
      while (raiz != null) {
        Object s = obtenerInfoProfundidad(prof, raiz.getLigaIzq());
        raiz = raiz.getLigaDer();
        if (s != null) {
          validos++;
        }
        SalidaEstandar.consola(s + "    ");
      }
      SalidaEstandar.consola("\n");
      raiz = primero;
      while (raiz != null) {
        raiz = raiz.getLigaDer();
        SalidaEstandar.consola("^    ");
      }
      SalidaEstandar.consola("\n");
      raiz = primero;
      while (raiz != null) {
        raiz = raiz.getLigaDer();
        SalidaEstandar.consola("V    ");
      }
      SalidaEstandar.consola("\n");
      if (validos == 0) {
        break;
      }
      prof++;
    }
  }

  private Object obtenerInfoProfundidad(int prof, NodoDoble recorrer) {
    if (prof == 0 || recorrer == null) {
      return recorrer;
    } else {
      return obtenerInfoProfundidad(prof-1, recorrer.getLigaIzq());
    }
  }

  /**
   * Inserta un objeto al final de un grupo.
   * @param info Es el objeto a insertar.
   * @param grupo Es el grupo donde se desea insertar.
   * @return Regresa true si lo pudo insertar.
   */
  public boolean insertarEnGrupo(Object info, int grupo) {
    if (vacia()) {
      return false;
    } else {
      NodoDoble nodoGrupo = buscarNodoGrupo(primero, grupo);
      if (nodoGrupo == null) {
        return false;
      } else {
        NodoDoble nuevoNodo = new NodoDoble(info);
        if (nuevoNodo == null) {
          return false;
        } else {
          NodoDoble ultimo = obtenerUltimoGrupo(nodoGrupo);
          ultimo.setLigaIzq(nuevoNodo);
          nuevoNodo.setLigaDer(ultimo);
          return true;
        }
      }
    }
  }

  /**
   * Imprime un grupo especifico
   * @param grupo Grupo a imprimir.
   */
  public void imprimirGrupo(int grupo) {
    if (vacia()) {
      return;
    }
    NodoDoble nodoGrupo = buscarNodoGrupo(primero, grupo);
    while (nodoGrupo != null) {
      SalidaEstandar.consola(nodoGrupo + "\n");
      nodoGrupo = nodoGrupo.getLigaIzq();
      SalidaEstandar.consola("^\n");
      SalidaEstandar.consola("\n");
      SalidaEstandar.consola("V\n");
    }
  }

  /**
   * Busca un elemento en la cabeza de la matriz.
   * @param info Es el elemento a buscar.
   * @return Regresa el objeto buscado.
   */
  public Object buscarCabeza(Object info) {
    NodoDoble recorrer = primero;

    while (recorrer != null && !recorrer.getInfo().toString().equalsIgnoreCase(info.toString())) {
      recorrer = recorrer.getLigaDer();
    }
    if (recorrer == null) {
      return null;
    } else {
      return recorrer.getInfo();
    }
  }

  /**
   * Busca un elemento en toda la matriz.
   * @param info Es el elemento a buscar.
   * @return Regresa un arreglo con la posicion de la fila y la cabeza.
   */
  public Arreglo buscar(Object info) {
    NodoDoble recorrer = primero;
    Arreglo arreglo = new Arreglo(2);
    int cabeza = 0;

    while (recorrer != null) {
      buscarProfundidad(0, recorrer, arreglo, info);
      if (!arreglo.vacia()) {
        arreglo.agregar(cabeza);
        break;
      }
      recorrer = recorrer.getLigaDer();
      cabeza++;
    }
    if (arreglo.vacia()) {
      return null;
    } else {
      return arreglo;
    }
  }

  private void buscarProfundidad(int prof, NodoDoble recorrer, Arreglo a, Object info) {
    if (recorrer == null || ComparadorObject.compare(recorrer.getInfo(), info) == 0) {
      if (recorrer != null) {
        a.agregar(prof);
      }
    } else {
      buscarProfundidad(prof+1, recorrer.getLigaIzq(), a, info);
    }
  }

  private NodoDoble buscarNodoGrupo(NodoDoble recorrer, int grupo) {
    if (grupo == 0 || recorrer == null) {
      return recorrer;
    } else {
      return buscarNodoGrupo(recorrer.getLigaDer(), grupo-1);
    }
  }

  private NodoDoble obtenerUltimoCabezas(NodoDoble recorrer) {
    if (vacia()) {
      return null;
    }
    if (recorrer.getLigaDer() == null) {
      return recorrer;
    } else {
      return obtenerUltimoCabezas(recorrer.getLigaDer());
    }
  }

  private NodoDoble obtenerUltimoGrupo(NodoDoble recorrer) {
    if (recorrer.getLigaIzq() == null) {
      return recorrer;
    } else {
      return obtenerUltimoGrupo(recorrer.getLigaIzq());
    }
  }
}
