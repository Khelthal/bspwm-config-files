package ednolineal.enumerados;

/**
 * Esta clase enumerada indica si una matriz debe recorrerse por renglones o por columnas o por profundidad.
 * */
public enum TipoMatriz3D {
  RENGLON,
  COLUMNA,
  PROFUNDIDAD
}
