package edlineal;

import comparadores.ComparadorObject;
import edlineal.catalogo.Nodo;
import edlineal.enumerados.TipoMatriz;
import ednolineal.Matriz2D;

/**
 * Esta clase es una lista enlazada que mantiene los elementos ordenados.
 */
public class ListaLigadaOrdenada extends ListaLigada {
  protected TipoOrden orden;

  public ListaLigadaOrdenada(TipoOrden orden) {
    super();
    this.orden = orden;
  }

  @Override
  public Object buscar(Object info) {
    int posicionB = 0;
    if (vacia()) {
      return -1;
    }
    Nodo recorrer = primero;

    if (orden == TipoOrden.CRECIENTE) {
      while (recorrer != null && ComparadorObject.compare(recorrer.getInfo(), info) < 0) {
        posicionB++;
        recorrer = recorrer.getLigaDer();
      }
    } else {
      while (recorrer != null && ComparadorObject.compare(recorrer.getInfo(), info) > 0) {
        posicionB++;
        recorrer = recorrer.getLigaDer();
      }
    }
    if (recorrer == null || ComparadorObject.compare(recorrer.getInfo(), info) != 0) {
      return (posicionB + 1) * -1;
    } else {
      return posicionB + 1;
    }
  }

  @Override
  public int agregar(Object info) {
    int posicionE = (int) buscar(info);
    if (posicionE > 0) {
      return -1;
    }
    posicionE *= -1;
    posicionE--;
    if (insertar(posicionE, info)) {
      return 1;
    } else {
      return -1;
    }
  }

  @Override
  public boolean insertar(int indice, Object info) {
    int posicionE = (int) buscar(info);
    if (posicionE > 0) {
      return false;
    }
    posicionE *= -1;
    posicionE--;
    if (posicionE == indice) {
      return super.insertar(indice, info);
    } else {
      return false;
    }
  }

  @Override
  public Object eliminar(Object info) {
    Object elementoBorrado = null;
    if (vacia() == false) { // no está vacía, hay algo
      Nodo anterior = primero;
      Nodo encontrado = primero;
      if (orden == TipoOrden.CRECIENTE) {
        while (encontrado != null && ComparadorObject.compare(encontrado.getInfo(), info) < 0) {
          anterior = encontrado;
          encontrado = encontrado.getLigaDer();
        }
      } else {
        while (encontrado != null && ComparadorObject.compare(encontrado.getInfo(), info) > 0) {
          anterior = encontrado;
          encontrado = encontrado.getLigaDer();
        }
      }
      if (encontrado == null || encontrado.getInfo() != info) { // no existe, f)
        return null;
      } else { // si existe,
        elementoBorrado = encontrado.getInfo(); // 1
        if (primero == ultimo) { // único, b)
          primero = null; // 2
          ultimo = null;
        } else if (primero == encontrado) { // c(, el primero
          primero = primero.getLigaDer(); // 2
        } else if (ultimo == encontrado) { // d), el último
          anterior.setLigaDer(null);// 2
          ultimo = anterior;// 3
        } else { // cualquier otro caso, es decir, en medio, e)
          Nodo siguiente = encontrado.getLigaDer();
          anterior.setLigaDer(siguiente);// 2
        }
        return elementoBorrado;
      }
    } else { // está vacía la lista, a)
      return null;
    }
  }

  @Override
  public int agregarInicio(Object info) {
    Nodo nuevoNodo = new Nodo(info);
    if (vacia()) {
      primero = nuevoNodo;
      ultimo = nuevoNodo;
      return 1;
    }

    if (orden == TipoOrden.CRECIENTE) {
      if (ComparadorObject.compare(info, primero.getInfo()) >= 0) {
        return -1;
      }
    } else {
      if (ComparadorObject.compare(info, primero.getInfo()) <= 0) {
        return -1;
      }
    }
    nuevoNodo.setLigaDer(primero);
    primero = nuevoNodo;
    return 1;
  }

  @Override
  public boolean cambiar(int indice, Object info) {
    int posicionE = (int) buscar(info);
    if (posicionE > 0) {
      return false;
    }
    if (eliminar(indice) == null) {
      return false;
    }

    return agregar(info) == 1;
  }

  @Override
  public boolean cambiar(Object infoVieja, Object infoNueva, int numOcurrencias) {
    if (numOcurrencias <= 0) {
      return false;
    }
    int posicionE = (int) buscar(infoNueva);
    if (posicionE > 0) {
      return false;
    }
    if (eliminar(infoVieja) == null) {
      return false;
    }

    return agregar(infoNueva) == 1;
  }

  @Override
  public Object clonar() {
    ListaLigadaOrdenada copia = new ListaLigadaOrdenada(orden);
    Nodo recorrer = primero;
    while (recorrer != null) {
      copia.agregar(recorrer.getInfo());
      recorrer = recorrer.getLigaDer();
    }

    return copia;
  }

  @Override
  public Object subLista(int indiceInicial, int indiceFinal) {
    if (vacia() || indiceInicial < 0) {
      return null;
    }

    Nodo recorrer = primero;
    int actual = 0;
    ListaLigadaOrdenada subLista = new ListaLigadaOrdenada(orden);

    while (recorrer != null) {
      if (actual >= indiceInicial && actual <= indiceFinal) {
        subLista.agregar(recorrer.getInfo());
      }
      actual++;
      recorrer = recorrer.getLigaDer();
    }

    if (subLista.vacia()) {
      return null;
    }
    return subLista;
  }

  @Override
  public void invertir() {
    if (vacia()) {
      return;
    }
    ColaLista colaLista = new ColaLista();
    while (!vacia()) {
      colaLista.poner(eliminar());
    }

    if (orden == TipoOrden.CRECIENTE) {
      orden = TipoOrden.DECRECIENTE;
    } else {
      orden = TipoOrden.CRECIENTE;
    }

    while (!colaLista.vacio()) {
      agregar(colaLista.quitar());
    }
  }

  @Override
  public Arreglo buscarValores(Object info) {
    Arreglo indices = new Arreglo(1);
    if (vacia()) {
      return null;
    }

    int posicionE = (int) buscar(info);
    if (posicionE < 0) {
      return null;
    }

    posicionE--;
    indices.agregar(posicionE);

    return indices;
  }

  @Override
  public int contar(Object info) {
    int posicionE = (int) buscar(info);
    if (posicionE > 0) {
      return 1;
    } else {
      return 0;
    }
  }

  @Override
  public void rellenar(Object info, int cantidad) {
    if (cantidad <= 0) {
      return;
    }
    agregar(info);
  }

  @Override
  public void rellenar(Object info) {
    vaciar();
    if (info instanceof Integer) {
      int num = (Integer) info;
      if (num > 0) {
        for (int i = 0; i < num; i++) {
          agregar(i + 1);
        }
      } else {
        for (int i = 0; i > num; i--) {
          agregar(i - 1);
        }
      }
    } else {
      if (info instanceof Character) {
        char letra = (Character) info;
        for (char i = 'A'; i <= letra; i++) {
          agregar(i);
        }
      } else {
        agregar(info);
      }
    }
  }

  @Override
  public Object redimensionar(int maximo) {
    Nodo recorrer = primero;
    int tamanio = 0;

    while (recorrer != null) {
      tamanio++;
      recorrer = recorrer.getLigaDer();
    }

    if (tamanio > maximo) {
      ListaLigada eliminados = new ListaLigada();
      while (tamanio > maximo) {
        eliminados.agregar(eliminar());
        tamanio--;
      }
      eliminados.invertir();
      return eliminados;
    }
    return null;
  }

  /**
   * Agrega un elemento al final de la lista si se mantiene el orden al agregarlo.
   * 
   * @param info Es el elemento a agregar.
   * @return Regresa 1 si pudo agregar el elemento, -1 en caso contrario.
   */
  public int agregarFinal(Object info) {
    Nodo nuevoNodo = new Nodo(info);
    if (nuevoNodo == null) {
      return -1;
    }
    if (vacia()) {
      primero = nuevoNodo;
      ultimo = nuevoNodo;
      return 1;
    }

    if (orden == TipoOrden.CRECIENTE) {
      if (ComparadorObject.compare(info, ultimo.getInfo()) <= 0) {
        return -1;
      }
    } else {
      if (ComparadorObject.compare(info, ultimo.getInfo()) >= 0) {
        return -1;
      }
    }
    ultimo.setLigaDer(nuevoNodo);
    ultimo = nuevoNodo;
    return 1;

  }

  /**
   * Elimina de la lista los elementos contenidos en la Matriz2D pasada como
   * argumento.
   * 
   * @param matriz Es la matriz que contiene los elementos a eliminar.
   * @return Regresa <b>true</b> si se pudo eliminar al menos un elemento, <b>false</b> en caso contrario.
   */
  public boolean eliminarMatriz2D(Matriz2D matriz) {
    boolean cambio = false;
    for (int columna = 0; columna < matriz.columnas(); columna++) {
      for (int renglon = 0; renglon < matriz.renglones(); renglon++) {
        if (eliminar(matriz.obtenerInfo(renglon, columna)) == null) {
          cambio = true;
        }
      }
    }
    return cambio;
  }
}
