package edlineal;

import comparadores.ComparadorObject;
import edlineal.catalogo.Nodo;
import edlineal.enumerados.TipoMatriz;
import ednolineal.Matriz2D;
import entradasalida.SalidaEstandar;

/**
 * Esta clase almacena elementos de forma dinamica.
 */
public class ListaLigada implements Lista {
  protected Nodo primero; // es obligatoria
  protected Nodo ultimo; // es opcional
  protected Nodo iterador;

  /**
   * Crea una nueva instancia de ListaLigada.
   */
  public ListaLigada() {
    primero = null;
    ultimo = null;
  }

  /**
   * Obtiene el elemento almacenado en el primer nodo de la lista.
   * 
   * @return Regresa el elemento almacenado en el primer nodo de la lista.
   */
  public Object getPrimero() {
    if (primero == null) {
      return null;
    }
    return primero.getInfo();
  }

  /**
   * Obtiene el elemento almacenado en el ultimo nodo de la lista.
   * 
   * @return Regresa el elemento almacenado en el ultimo nodo de la lista.
   */
  public Object getUltimo() {
    if (ultimo == null) {
      return null;
    }
    return ultimo.getInfo();
  }

  @Override
  public boolean vacia() {
    if (primero == null) {
      return true;
    } else {
      return false;
    }
  }

  @Override
  public int agregar(Object info) {
    Nodo nuevoNodo = new Nodo(info);

    if (nuevoNodo != null) { // que si puedo reservar memoria la MVJ
      if (vacia() == true) { // vcuando no hay nada
        primero = nuevoNodo;
        ultimo = nuevoNodo;
      } else { // cuando ya hay elementos
        ultimo.setLigaDer(nuevoNodo);
        ultimo = nuevoNodo;
      }
      return 1;
    } else { // que no hay más espacio o hay error
      return -1;
    }
  }

  public int agregarInicio(Object info) {
    Nodo nuevoNodo = new Nodo(info);

    if (nuevoNodo != null) {// si hay espacio
      if (vacia() == true) { // no hay nada
        primero = nuevoNodo;
        ultimo = nuevoNodo;
      } else { // hay varios
        nuevoNodo.setLigaDer(primero);
        primero = nuevoNodo;
      }
      return 1;
    } else { // no hay espacio
      return -1;
    }
  }

  @Override
  public void imprimir() {
    Nodo temp = primero;

    while (temp != null) {
      SalidaEstandar.consola(temp.getInfo() + " -> ");
      temp = temp.getLigaDer();
    }
    SalidaEstandar.consola("null");
  }

  /**
   * Imprime el contenido de la lista en orden natural de forma recursiva.
   */
  public void imprimirRR() {
    imprimirRR(primero);
  }

  /**
   * Imprime el contenido de la lista en orden natural de forma recursiva.
   * 
   * @param recorrer Es el nodo actual.
   */
  private void imprimirRR(Nodo recorrer) {
    if (recorrer == null) {
      SalidaEstandar.consola("null");
    } else {
      SalidaEstandar.consola(recorrer.getInfo() + " -> ");
      imprimirRR(recorrer.getLigaDer());
    }
  }

  public Object eliminarInicio() {
    Object elementoBorrado = null;

    if (vacia() == false) { // no está vacía
      elementoBorrado = primero.getInfo();// 1
      if (primero == ultimo) { // único, b)
        // elementoBorrado=primero.getInfo();//1
        primero = null;// 2
        ultimo = null;
      } else { // varios c)
        // elementoBorrado=primero.getInfo();//1
        primero = primero.getLigaDer(); // 2
      }
      return elementoBorrado;
    } else { // está vacía a)
      return null;
    }
  }

  @Override
  public void imprimirOI() {
    PilaLista pilaLista = new PilaLista();
    Nodo recorrer = primero;
    while (recorrer != null) {
      pilaLista.poner(recorrer);
      recorrer = recorrer.getLigaDer();
    }
    while (!pilaLista.vacio()) {
      SalidaEstandar.consola(pilaLista.quitar() + " -> ");
    }
    SalidaEstandar.consola("null");
  }

  /**
   * Imprime el contenido de la lista en orden inverso de forma recursiva.
   */
  public void imprimirOIRR() {
    PilaLista pilaLista = new PilaLista();
    aPila(primero, pilaLista);
    imprimirOIRR(0, pilaLista);
  }

  /**
   * Almacena los datos de la lista en una pila.
   * 
   * @param recorrer  Es el nodo actual.
   * @param pilaLista Es la pila donde se almacenan los elementos de la lista.
   */
  private void aPila(Nodo recorrer, PilaLista pilaLista) {
    if (recorrer != null) {
      pilaLista.poner(recorrer.getInfo());
      aPila(recorrer.getLigaDer(), pilaLista);
    }
  }

  /**
   * Imprime la lista en forma inversa, con un proceso recursivo.
   * 
   * @param pos       Es la posicion actual.
   * @param pilaLista Es la pilaLista que contiene los elementos de la lista.
   */
  private void imprimirOIRR(int pos, PilaLista pilaLista) {
    if (pilaLista.vacio()) {
      SalidaEstandar.consola("null");
    } else {
      SalidaEstandar.consola(pilaLista.quitar() + " -> ");
      imprimirOIRR(pos + 1, pilaLista);
    }
  }

  @Override
  public Object buscar(Object info) {
    Nodo recorrer = primero;

    while (recorrer != null && !recorrer.getInfo().toString().equalsIgnoreCase(info.toString())) {
      recorrer = recorrer.getLigaDer();
    }
    if (recorrer == null) {
      return null;
    } else {
      return recorrer.getInfo();
    }
  }

  /**
   * Busca el objeto indicado en la lista con un metodo recursivo.
   * 
   * @param info Es el objeto a buscar.
   * @return Regresa el objeto buscado si se encuentra en la lista, null en caso
   *         contrario.
   */
  public Object buscarRR(Object info) {
    return buscarRR(primero, info);
  }

  /**
   * Busca el objeto indicado en la lista con un metodo recursivo.
   * 
   * @param recorrer Es el nodo actual.
   * @param info     Es el objeto a buscar.
   * @return Regresa el objeto buscado si se encuentra en la lista, null en caso
   *         contrario.
   */
  private Object buscarRR(Nodo recorrer, Object info) {
    if (recorrer != null && ComparadorObject.compare(recorrer.getInfo(), info) != 0) {
      return buscarRR(recorrer.getLigaDer(), info);
    } else {
      if (recorrer == null) {
        return null;
      } else {
        return recorrer.getInfo();
      }
    }
  }

  @Override
  public Object eliminar(Object info) {
    Object elementoBorrado = null;
    if (vacia() == false) { // no está vacía, hay algo
      Nodo anterior = primero;
      Nodo encontrado = primero;
      while (encontrado != null && !encontrado.getInfo().toString().equalsIgnoreCase(info.toString())) {
        anterior = encontrado;
        encontrado = encontrado.getLigaDer();
      }
      if (encontrado == null) { // no existe, f)
        return null;
      } else { // si existe,
        elementoBorrado = encontrado.getInfo(); // 1
        if (primero == ultimo) { // único, b)
          primero = null; // 2
          ultimo = null;
        } else if (primero == encontrado) { // c(, el primero
          primero = primero.getLigaDer(); // 2
        } else if (ultimo == encontrado) { // d), el último
          anterior.setLigaDer(null);// 2
          ultimo = anterior;// 3
        } else { // cualquier otro caso, es decir, en medio, e)
          Nodo siguiente = encontrado.getLigaDer();
          anterior.setLigaDer(siguiente);// 2
        }
        return elementoBorrado;
      }
    } else { // está vacía la lista, a)
      return null;
    }
  }

  /**
   * Elimina un objeto de la lista usando un metodo recursivo.
   * 
   * @param info Es el objeto a eliminar.
   * @return Regresa el objeto eliminado si se encontraba en la lista, null en
   *         caso contrario.
   */
  public Object eliminarRR(Object info) {
    Object elementoBorrado = null;
    if (vacia() == false) { // no está vacía, hay algo
      Nodo encontrado = primero;
      Nodo anterior = buscarAnteriorRR(primero, primero, info);
      if (ComparadorObject.compare(anterior.getInfo(), info) != 0 && primero != ultimo) {
        encontrado = anterior.getLigaDer();
      }
      if (encontrado == null) { // no existe, f)
        return null;
      } else { // si existe,
        elementoBorrado = encontrado.getInfo(); // 1
        if (primero == ultimo) { // único, b)
          primero = null; // 2
          ultimo = null;
        } else if (primero == encontrado) { // c(, el primero
          primero = primero.getLigaDer(); // 2
        } else if (ultimo == encontrado) { // d), el último
          anterior.setLigaDer(null);// 2
          ultimo = anterior;// 3
        } else { // cualquier otro caso, es decir, en medio, e)
          Nodo siguiente = encontrado.getLigaDer();
          anterior.setLigaDer(siguiente);// 2
        }
        return elementoBorrado;
      }
    } else { // está vacía la lista, a)
      return null;
    }
  }

  /**
   * Encuentra el nodo anterior al nodo con la info buscada.
   * 
   * @param anterior Es el nodo anterior.
   * @param recorrer Es el nodo actual
   * @param info     Es la info buscada.
   * @return Regresa el nodo anterior.
   */
  private Nodo buscarAnteriorRR(Nodo anterior, Nodo recorrer, Object info) {
    if (recorrer != null && ComparadorObject.compare(recorrer.getInfo(), info) == 0) {
      anterior = recorrer;
      return buscarAnteriorRR(anterior, recorrer.getLigaDer(), info);
    } else {
      return anterior;
    }
  }

  @Override
  public Object eliminar() {
    Object elementoBorrado = null;
    if (vacia() == false) { // hay algo
      elementoBorrado = ultimo.getInfo(); // 1
      if (primero == ultimo) { // único b)
        // elementoBorrado=ultimo.getInfo(); //1
        primero = null;// 2
        ultimo = null;
      } else { // hay varios c)
        // elementoBorrado=ultimo.getInfo(); //1
        Nodo penultimo = primero;
        while (penultimo.getLigaDer() != ultimo) {
          penultimo = penultimo.getLigaDer();
        }
        ultimo = penultimo;// 2
        ultimo.setLigaDer(null);// 3
      }
      return elementoBorrado;
    } else { // vacía a)
      return null;
    }
  }

  /**
   * Elimina el ultimo elemento de la lista con un metodo recursivo.
   * 
   * @return Regresa el elemento borrado, si la lista estaba vacia regresa
   *         <b>null</b>.
   */
  public Object eliminarRR() {
    Object elementoBorrado = null;
    if (vacia() == false) { // hay algo
      elementoBorrado = ultimo.getInfo(); // 1
      if (primero == ultimo) { // único b)
        // elementoBorrado=ultimo.getInfo(); //1
        primero = null;// 2
        ultimo = null;
      } else { // hay varios c)
        // elementoBorrado=ultimo.getInfo(); //1
        Nodo penultimo = primero;
        penultimo = buscarPenultimoRR(primero);
        ultimo = penultimo;// 2
        ultimo.setLigaDer(null);// 3
      }
      return elementoBorrado;
    } else { // vacía a)
      return null;
    }
  }

  /**
   * Busca el penultimo nodo de la lista.
   * 
   * @param penultimo Es el nodo a iterar.
   * @return Regresa el nodo encontrado.
   */
  private Nodo buscarPenultimoRR(Nodo penultimo) {
    if (penultimo.getLigaDer() != ultimo) {
      return buscarPenultimoRR(penultimo.getLigaDer());
    } else {
      return penultimo;
    }
  }

  @Override
  public boolean esIgual(Object lista2) {
    ListaLigada lista = (ListaLigada) lista2;
    if (vacia()) {
      return lista.vacia();
    } else {
      if (lista.vacia()) {
        return false;
      }
    }
    Nodo recorrer = primero;
    int indice = 0;

    while (recorrer != null) {
      if (ComparadorObject.compare(recorrer.getInfo(), lista.obtener(indice)) != 0) {
        return false;
      }
      indice++;
      recorrer = recorrer.getLigaDer();
    }

    if (lista.obtener(indice) != null) {
      return false;
    }

    return true;
  }

  /**
   * Compara la lista con otra lista de forma recursiva.
   * 
   * @param lista2 Es la lista con la que se va a comparar.
   * @return Regresa <b>true</b> si las listas son iguales, <b>false</b> en caso
   *         contrario.
   */
  public boolean esIgualRR(Object lista2) {
    if (!(lista2 instanceof ListaLigada)) {
      return false;
    }
    ListaLigada lista = (ListaLigada) lista2;
    if (vacia()) {
      return lista.vacia();
    } else {
      if (lista.vacia()) {
        return false;
      }
    }
    return esIgualRR(0, primero, lista);
  }

  /**
   * Compara los nodos de la lista con los de otra lista para determinar si son
   * iguales.
   * 
   * @param pos      Es la posicion actual.
   * @param recorrer Es el nodo actual.
   * @param lista    Es la lista a comparar.
   * @return Regresa <b>true</b> si las listas son iguales, <b>false</b> en caso
   *         contrario.
   */
  private boolean esIgualRR(int pos, Nodo recorrer, ListaLigada lista) {
    if (recorrer != null) {
      boolean iguales = ComparadorObject.compare(recorrer.getInfo(), lista.obtener(pos)) == 0;
      if (iguales) {
        return esIgualRR(pos + 1, recorrer.getLigaDer(), lista);
      } else {
        return false;
      }
    } else {
      return (lista.obtener(pos) == null);
    }
  }

  @Override
  public boolean cambiar(Object infoVieja, Object infoNueva, int numOcurrencias) {
    boolean cambio = false;
    Nodo recorrer = primero;
    while (recorrer != null) {
      if (numOcurrencias <= 0) {
        break;
      }
      if (ComparadorObject.compare(recorrer.getInfo(), infoVieja) == 0) {
        recorrer.setInfo(infoNueva);
        cambio = true;
        numOcurrencias--;
      }
      recorrer = recorrer.getLigaDer();
    }
    return cambio;
  }

  /**
   * Reemplaza en la lista n objetos iguales a un objeto indicado con un nuevo
   * objeto utilizando un metodo recursivo.
   * 
   * @param infoVieja      Es el objeto a reemplazar.
   * @param infoNueva      Es el objeto que va a reemplazar al anterior.
   * @param numOcurrencias Es el numero de veces que se reemplazara el objeto.
   * @return Regresa <b>true</b> si se cambio al menos un valor, <b>false</b> en
   *         caso contrario.
   */
  public boolean cambiarRR(Object infoVieja, Object infoNueva, int numOcurrencias) {
    return cambiarRR(primero, infoVieja, infoNueva, numOcurrencias) != numOcurrencias;
  }

  /**
   * Cambia la infoVieja por infoNueva en la cantidad de nodos especificada.
   * 
   * @param recorrer       Es el nodo actual.
   * @param infoVieja      Es la info vieja.
   * @param infoNueva      Es la info nueva.
   * @param numOcurrencias Es el numero de veces que debe reemplazar infoVieja;
   * @return Regresa el numero de ocurrencias final.
   */
  private int cambiarRR(Nodo recorrer, Object infoVieja, Object infoNueva, int numOcurrencias) {
    if (numOcurrencias == 0 || recorrer == null) {
      return numOcurrencias;
    } else {
      if (ComparadorObject.compare(recorrer.getInfo(), infoVieja) == 0) {
        recorrer.setInfo(infoNueva);
        numOcurrencias--;
      }
      return cambiarRR(recorrer.getLigaDer(), infoVieja, infoNueva, numOcurrencias);
    }
  }

  @Override
  public Arreglo buscarValores(Object info) {
    ListaLigada indices = new ListaLigada();
    if (vacia()) {
      return null;
    }
    Nodo recorrer = primero;
    int actual = 0;

    while (recorrer != null) {
      if (ComparadorObject.compare(recorrer.getInfo(), info) == 0) {
        indices.agregar(actual);
      }
      actual++;
      recorrer = recorrer.getLigaDer();
    }

    return indices.aArreglo();
  }

  /**
   * Busca las posiciones donde esta ubicado un objeto en la lista con un metodo
   * recursivo.
   * 
   * @param info Es el objeto a buscar.
   * @return Regresa un objeto de tipo Arreglo con las posiciones si se encontro
   *         al menos una vez el objeto, <b>null</b> en caso contrario.
   */
  public Arreglo buscarValoresRR(Object info) {
    ListaLigada indices = new ListaLigada();
    if (vacia()) {
      return null;
    }
    buscarValoresRR(0, primero, info, indices);
    return indices.aArreglo();
  }

  /**
   * Busca las posiciones donde esta ubicado un objeto en la lista mediante la
   * recursion.
   * 
   * @param pos      Es la posicion actual.
   * @param recorrer Es el nodo actual.
   * @param info     Es el objeto a buscar.
   * @param indices  Es la lista donde se guardan los indices encontrados.
   */
  private void buscarValoresRR(int pos, Nodo recorrer, Object info, ListaLigada indices) {
    if (recorrer != null) {
      if (ComparadorObject.compare(recorrer.getInfo(), info) == 0) {
        indices.agregar(pos);
      }
      buscarValoresRR(pos + 1, recorrer.getLigaDer(), info, indices);
    }
  }

  @Override
  public void vaciar() {
    primero = null;
    ultimo = null;
  }

  @Override
  public boolean agregarLista(Object lista2) {
    Arreglo arreglo = (Arreglo) lista2;
    boolean logrado = true;

    for (int i = 0; i < arreglo.numElementos(); i++) {
      if (agregar(arreglo.obtener(i)) == -1) {
        logrado = false;
      }
    }

    return logrado;
  }

  /**
   * Inserta los elementos de otra lista al final de la lista con un metodo
   * recursivo.
   * 
   * @param lista2 Es la lista a insertar.
   * @return Regresa <b>true</b> si lista2 es una lista valida, <b>false</b> en
   *         caso contrario.
   */
  public boolean agregarListaRR(Object lista2) {
    Arreglo arreglo = (Arreglo) lista2;
    return agregarListaRR(0, arreglo);
  }

  /**
   * Inserta los elementos de otra lista al final de la lista mediante la
   * recursion.
   * 
   * @param pos     Es la posicion actual.
   * @param arreglo Es el arreglo del cual se agregaran valores.
   * @return Regresa <b>true</b> si lista2 es una lista valida, <b>false</b> en
   *         caso contrario.
   */
  private boolean agregarListaRR(int pos, Arreglo arreglo) {
    if (pos < arreglo.numElementos()) {
      boolean logrado = agregar(arreglo.obtener(pos)) != -1;
      if (!logrado) {
        return false;
      } else {
        return agregarListaRR(pos + 1, arreglo);
      }
    } else {
      return true;
    }
  }

  @Override
  public void invertir() {
    if (vacia()) {
      return;
    }
    ColaLista colaLista = new ColaLista();
    while (!vacia()) {
      colaLista.poner(eliminar());
    }

    while (!colaLista.vacio()) {
      agregar(colaLista.quitar());
    }
  }

  /**
   * Reordena los elementos de la lista en orden inverso al original.
   */
  public void invertirRR() {
    if (vacia()) {
      return;
    }
    PilaLista pilaLista = new PilaLista();
    aPila(primero, pilaLista);
    vaciar();
    invertirRR(pilaLista);
  }

  /**
   * Aniade los elementos de una pila para invertir la lista.
   * 
   * @param pilaLista Es la pila que contiene los elementos de la lista original.
   */
  private void invertirRR(PilaLista pilaLista) {
    if (!pilaLista.vacio()) {
      agregar(pilaLista.quitar());
      invertirRR(pilaLista);
    }
  }

  @Override
  public int contar(Object info) {
    int contador = 0;
    Nodo recorrer = primero;
    while (recorrer != null) {
      if (ComparadorObject.compare(recorrer.getInfo(), info) == 0) {
        contador++;
      }
      recorrer = recorrer.getLigaDer();
    }
    return contador;
  }

  /**
   * Cuenta el numero de veces que aparece en la lista el objeto indicado con un
   * metodo recursivo.
   * 
   * @param info Es el objeto buscado.
   * @return Regresa el numero de veces que aparece en la lista el objeto
   *         indicado.
   */
  public int contarRR(Object info) {
    return contarRR(primero, info);
  }

  /**
   * Cuenta el numero de veces que aparece en la lista el objeto indicado.
   * 
   * @param info Es el objeto buscado.
   * @return Regresa el numero de veces que aparece en la lista el objeto
   *         indicado.
   */
  private int contarRR(Nodo recorrer, Object info) {
    if (recorrer != null) {
      if (ComparadorObject.compare(recorrer.getInfo(), info) == 0) {
        return 1 + contarRR(recorrer.getLigaDer(), info);
      } else {
        return 0 + contarRR(recorrer.getLigaDer(), info);
      }
    } else {
      return 0;
    }
  }

  @Override
  public boolean eliminarLista(Object lista2) {
    boolean cambiado = false;
    Arreglo lista = (Arreglo) lista2;

    for (int i = 0; i < lista.numElementos(); i++) {
      if (eliminar(lista.obtener(i)) != null) {
        cambiado = true;
      }
    }

    return cambiado;
  }

  /**
   * Elimina de la lista todos los elementos en comun con la lista argumento con
   * un metodo recursivo.
   * 
   * @param lista2 Es la lista de la cual se borraran los elementos.
   * @return Regresa <b>true</b> si lista2 es una lista valida.
   */
  public boolean eliminarListaRR(Object lista2) {
    if (!(lista2 instanceof Arreglo)) {
      return false;
    }
    Arreglo lista = (Arreglo) lista2;
    return eliminarListaRR(0, lista);
  }

  /**
   * Elimina de la lista todos los elementos en comun con la lista argumento.
   * 
   * @param pos   Es la posicion actual.
   * @param lista Es la lista de la cual se borraran los elementos.
   * @return Regresa <b>true</b> si lista2 es una lista valida.
   */
  private boolean eliminarListaRR(int pos, Arreglo lista) {
    if (pos < lista.numElementos()) {
      if (eliminarRR(lista.obtener(pos)) != null) {
        return (true | eliminarListaRR(pos + 1, lista));
      }
      return (false | eliminarListaRR(pos + 1, lista));
    } else {
      return false;
    }
  }

  @Override
  public void rellenar(Object info, int cantidad) {
    for (int i = 0; i < cantidad; i++) {
      agregar(info);
    }
  }

  /**
   * Inserta el objeto indicado el numero de veces indicado.
   * 
   * @param info     Es el objeto a ser insertado.
   * @param cantidad Es el numero de veces que <b>info</b> sera insertado.
   */
  public void rellenarRR(Object info, int cantidad) {
    if (cantidad > 0) {
      agregar(info);
      rellenarRR(info, cantidad - 1);
    }
  }

  @Override
  public void rellenar(Object info) {
    Nodo recorrer = primero;
    while (recorrer != null) {
      if (recorrer.getInfo() == null) {
        recorrer.setInfo(info);
      }
      recorrer = recorrer.getLigaDer();
    }
  }

  /**
   * Cambia el contenido de los nodos con info null por la nueva info.
   * 
   * @param info Es la nueva info.
   */
  public void rellenarRR(Object info) {
    rellenarRR(primero, info);
  }

  /**
   * Cambia el contenido de los nodos con info null por la nueva info.
   * 
   * @param recorrer Es el nodo actual.
   * @param info     Es la nueva info.
   */
  private void rellenarRR(Nodo recorrer, Object info) {
    if (recorrer != null) {
      if (recorrer.getInfo() == null) {
        recorrer.setInfo(info);
      }
      rellenarRR(recorrer.getLigaDer(), info);
    }
  }

  @Override
  public Object clonar() {
    ListaLigada copia = new ListaLigada();
    Nodo recorrer = primero;
    while (recorrer != null) {
      copia.agregar(recorrer.getInfo());
      recorrer = recorrer.getLigaDer();
    }

    return copia;
  }

  /**
   * Crea una copia de la lista con un metodo recursivo.
   * 
   * @return Regresa una copia de la lista.
   */
  public Object clonarRR() {
    ListaLigada copia = new ListaLigada();
    clonarRR(primero, copia);
    return copia;
  }

  /**
   * Almacena el contenido de la lista en una nueva lista.
   * 
   * @param recorrer Es el nodo actual.
   * @param copia    Es la lista donde se almacenan los elementos.
   */
  private void clonarRR(Nodo recorrer, ListaLigada copia) {
    if (recorrer != null) {
      copia.agregar(recorrer.getInfo());
      clonarRR(recorrer.getLigaDer(), copia);
    }
  }

  @Override
  public Object subLista(int indiceInicial, int indiceFinal) {
    if (vacia() || indiceInicial < 0) {
      return null;
    }

    Nodo recorrer = primero;
    int actual = 0;
    ListaLigada subLista = new ListaLigada();

    while (recorrer != null) {
      if (actual >= indiceInicial && actual <= indiceFinal) {
        subLista.agregar(recorrer.getInfo());
      }
      actual++;
      recorrer = recorrer.getLigaDer();
    }

    if (subLista.vacia()) {
      return null;
    }
    return subLista;
  }

  /**
   * Crea una sublista del rango indicado con un metodo recursivo.
   * 
   * @param indiceInicial Es el punto donde inicia la sublista.
   * @param indiceFinal   Es el punto donde termina la sublista.
   * @return Regresa una sublista si el rango es valido, <b>null</b> en caso
   *         contrario.
   */
  public Object subListaRR(int indiceInicial, int indiceFinal) {
    if (vacia() || indiceInicial < 0) {
      return null;
    }
    ListaLigada subLista = new ListaLigada();
    subListaRR(primero, 0, indiceInicial, indiceFinal, subLista);
    if (subLista.vacia()) {
      return null;
    }
    return subLista;
  }

  /**
   * Crea una sublista del rango indicado mediante la recursion.
   * 
   * @param recorrer      Es el nodo actual.
   * @param pos           Es la posicion actual.
   * @param indiceInicial Es el punto donde inicia la sublista.
   * @param indiceFinal   Es el punto donde termina la sublista.
   * @param subLista      Es la lista donde se almacenan los datos del rango.
   */
  private void subListaRR(Nodo recorrer, int pos, int indiceInicial, int indiceFinal, ListaLigada subLista) {
    if (recorrer != null) {
      if (pos >= indiceInicial && pos <= indiceFinal) {
        subLista.agregar(recorrer.getInfo());
      }
      subListaRR(recorrer.getLigaDer(), pos + 1, indiceInicial, indiceFinal, subLista);
    }
  }

  @Override
  public boolean insertar(int indice, Object info) {
    if (indice < 0) {
      return false;
    }

    Nodo encontrado = primero;
    Nodo anterior = primero;
    int actual = -1;

    while (encontrado != null && actual != indice) {
      actual++;
      if (actual != indice) {
        anterior = encontrado;
        encontrado = encontrado.getLigaDer();
      }
    }

    Nodo nuevoNodo = new Nodo(info);
    if (nuevoNodo == null) {
      return false;
    }

    if (actual != indice) {
      if (actual == -1) {
        primero = nuevoNodo;
        ultimo = nuevoNodo;
        return true;
      } else if (indice == actual + 1) {
        ultimo.setLigaDer(nuevoNodo);
        ultimo = nuevoNodo;
      }
      return false;
    } else {
      if (primero == ultimo) {
        nuevoNodo.setLigaDer(primero);
        ultimo = primero;
        primero = nuevoNodo;
      } else if (primero == encontrado) {
        nuevoNodo.setLigaDer(primero);
        primero = nuevoNodo;
      } else if (ultimo == encontrado) {
        anterior.setLigaDer(nuevoNodo);
        nuevoNodo.setLigaDer(ultimo);
      } else {
        anterior.setLigaDer(nuevoNodo);
        nuevoNodo.setLigaDer(encontrado);
      }
      return true;
    }
  }


    /**
     * Inserta en la posicion indice el objeto indicado mediante un metodo recursivo.
     * @param indice Es el indice donde se insertara el objeto.
     * @param info Es el objeto a insertar.
     * @return Regresa <b>true</b> si se pudo insertar, <b>false</b> en caso contrario.
     */
  public boolean insertarRR(int indice, Object info) {
    if (indice < 0) {
      return false;
    }

    if (vacia() && indice != 0) {
      return false;
    }
    if (indice == 0 || vacia()) {
      return agregarInicio(info) != -1;
    }

    Nodo encontrado = primero;
    Nodo anterior = primero;

    anterior = buscarInsercion(primero, primero, 0, indice);
    if (anterior == null) {
      return false;
    }
    encontrado = anterior.getLigaDer();
    Nodo nuevoNodo = new Nodo(info);
    if (nuevoNodo == null) {
      return false;
    }

    if (encontrado == null) {
      if (anterior != null) {
        ultimo.setLigaDer(nuevoNodo);
        ultimo = nuevoNodo;
      }
      return false;
    } else {
      if (primero == ultimo) {
        nuevoNodo.setLigaDer(primero);
        ultimo = primero;
        primero = nuevoNodo;
      } else if (primero == encontrado) {
        nuevoNodo.setLigaDer(primero);
        primero = nuevoNodo;
      } else if (ultimo == encontrado) {
        anterior.setLigaDer(nuevoNodo);
        nuevoNodo.setLigaDer(ultimo);
      } else {
        anterior.setLigaDer(nuevoNodo);
        nuevoNodo.setLigaDer(encontrado);
      }
      return true;
    }
  }

  /**
   * Obtiene el nodo anterior al nodo con la posicion buscada.
   * @param anterior Es el nodo anterior.
   * @param recorrer Es el nodo actual.
   * @param pos Es la posicion actual.
   * @param indice Es el indice deseado.
   * @return Regresa el nodo anterior a la posicion deseada.
   */
  private Nodo buscarInsercion(Nodo anterior, Nodo recorrer, int pos, int indice) {
    if (recorrer == null || pos == indice) {
      if(pos == indice) {
        return anterior;
      } else {
        return null;
      }
    } else {
      anterior = recorrer;
      return buscarInsercion(anterior, recorrer.getLigaDer(), pos+1, indice);
    }
  }

  public void inicializarIterador() {
    iterador = primero;
  }

  public boolean hayMas() {
    if (iterador != null) {
      return true;
    } else {
      return false;
    }
  }

  public Object obtenerSigiuente() {
    if (hayMas() == true) {
      Object info = iterador.getInfo();
      iterador = iterador.getLigaDer();
      return info;
    } else {
      return null;
    }
  }

  /**
   * Guarda los elementos de la ListaLigada en un arreglo.
   * 
   * @return Regresa un arreglo con los elementos de la lista ligada.
   */
  public Arreglo aArreglo() {
    Arreglo elementos;
    int contador = 0;
    Nodo recorrer;
    recorrer = primero;

    while (recorrer != null) {
      contador++;
      recorrer = recorrer.getLigaDer();
    }
    elementos = new Arreglo(contador);
    recorrer = primero;

    while (recorrer != null) {
      elementos.agregar(recorrer.getInfo());
      recorrer = recorrer.getLigaDer();
    }

    return elementos;
  }

  /**
   * Guarda los elementos de la ListaLigada en un arreglo mediante metodos recursivos.
   * 
   * @return Regresa un arreglo con los elementos de la lista ligada.
   */
  public Arreglo aArregloRR() {
    int count = contarNodosRR(primero);
    Arreglo elementos = new Arreglo(count);
    aArregloRR(primero, elementos);
    return elementos;
  }

  /**
   * Cuenta el numero total de nodos de la lista.
   * @param recorrer Es el nodo actual.
   * @return Regresa la cantidad de nodos en la lista.
   */
  private int contarNodosRR(Nodo recorrer) {
    if (recorrer != null) {
      return 1 + contarNodosRR(recorrer.getLigaDer());
    } else {
      return 0;
    }
  }

  /**
   * Guarda los elementos de la ListaLigada en un arreglo.
   * @param recorrer Es el nodo actual.
   * @param almacen Es el arreglo donde se guardaran los datos de la lista.
   */
  private void aArregloRR(Nodo recorrer, Arreglo almacen) {
    if (recorrer != null) {
      almacen.agregar(recorrer.getInfo());
      aArregloRR(recorrer.getLigaDer(), almacen);
    }
  }

  /**
   * Guarda los elementos de la ListaLigada en un arreglo, excepto aquellos que
   * esten contenidos en arregloADescartar.
   * 
   * @param arregloADescartar Es un arreglo con elementos que no deben ser
   *                          incluidos en el arreglo generado.
   * @return Regresa un arreglo con los elementos de la lista ligada, excepto
   *         aquellos que esten contenidos en arregloADescartar.
   */
  public Arreglo aArreglo(Arreglo arregloADescartar) {
    Arreglo elementos;
    int contador = 0;
    Nodo recorrer;
    recorrer = primero;

    while (recorrer != null) {
      if (arregloADescartar.buscar(recorrer.getInfo()) == null) {
        contador++;
      }
      recorrer = recorrer.getLigaDer();
    }
    elementos = new Arreglo(contador);
    recorrer = primero;

    while (recorrer != null) {
      if (arregloADescartar.buscar(recorrer.getInfo()) == null) {
        elementos.agregar(recorrer.getInfo());
      }
      recorrer = recorrer.getLigaDer();
    }

    return elementos;
  }

  /**
   * Guarda los elementos de la ListaLigada en un arreglo, excepto aquellos que
   * esten contenidos en arregloADescartar utilizando metodos recursivos.
   * 
   * @param arregloADescartar Es un arreglo con elementos que no deben ser
   *                          incluidos en el arreglo generado.
   * @return Regresa un arreglo con los elementos de la lista ligada, excepto
   *         aquellos que esten contenidos en arregloADescartar.
   */
  public Arreglo aArregloRR(Arreglo arregloADescartar) {
    Arreglo elementos = new Arreglo(contarNodosRR(primero));
    aArregloRR(primero, elementos, arregloADescartar);
    return elementos;
  }

  /**
   * Guarda los elementos de la ListaLigada en un arreglo.
   * @param recorrer Es el nodo actual.
   * @param almacen Es el arreglo donde se guardaran los datos de la lista.
   * @param arregloADescartar Contiene elementos que no deben aniadirse al nuevo arreglo.
   */
  private void aArregloRR(Nodo recorrer, Arreglo almacen, Arreglo arregloADescartar) {
    if (recorrer != null) {
      if (arregloADescartar.buscarRR(recorrer.getInfo()) == null) {
        almacen.agregar(recorrer.getInfo());
      }
      aArregloRR(recorrer.getLigaDer(), almacen, arregloADescartar);
    }
  }

  /**
   * Guarda los elementos de la ListaLigada en una Matriz2D.
   * 
   * @param renglones Es el numero de renglones de la Matriz2D.
   * @param columnas  Es el numero de columnas de la Matriz2D.
   * @return Regresa una Matriz2D con los elementos de la ListaLigada.
   */
  public Matriz2D aMatriz2D(int renglones, int columnas) {
    Matriz2D matriz2d = new Matriz2D(renglones, columnas);
    Nodo recorrer = primero;
    for (int renglon = 0; renglon < matriz2d.renglones(); renglon++) {
      for (int columna = 0; columna < matriz2d.columnas(); columna++) {
        if (recorrer == null) {
          break;
        }
        matriz2d.cambiarInfo(renglon, columna, recorrer.getInfo());
        recorrer = recorrer.getLigaDer();
      }
    }

    return matriz2d;
  }

  /**
   * Guarda los elementos de la ListaLigada en una Matriz2D utilizando metodos recursivos.
   * 
   * @param renglones Es el numero de renglones de la Matriz2D.
   * @param columnas  Es el numero de columnas de la Matriz2D.
   * @return Regresa una Matriz2D con los elementos de la ListaLigada.
   */
  public Matriz2D aMatriz2DRR(int renglones, int columnas) {
    Matriz2D matriz2d = new Matriz2D(renglones, columnas);
    aMatriz2DRR(primero, 0, 0, matriz2d);
    return matriz2d;
  }

  /**
   * Guarda los elementos de la ListaLigada en una Matriz2D utilizando la recursion.
   * @param recorrer Es el nodo actual.
   * @param reng Es el renglon actual.
   * @param col Es la columna actual.
   * @param matriz2d Es la matriz a rellenar.
   */
  private void aMatriz2DRR(Nodo recorrer, int reng, int col, Matriz2D matriz2d) {
    if (recorrer != null) {
      if (reng != matriz2d.renglones()-1 || col != matriz2d.columnas()-1) {
        matriz2d.cambiarInfo(reng, col, recorrer.getInfo());
        if (col != matriz2d.columnas()-1) {
          aMatriz2DRR(recorrer.getLigaDer(), reng, col+1, matriz2d);
        } else {
          aMatriz2DRR(recorrer.getLigaDer(), reng+1, 0, matriz2d);
        }
      } else {
        matriz2d.cambiarInfo(reng, col, recorrer.getInfo());
      }
    }
  }

  /**
   * Agrega los elementos de una Matriz2D al final de la ListaLigada.
   * 
   * @param matriz         Es la matriz de la cual se obtendran los elementos.
   * @param enumTipoMatriz Indica si los elementos se agregaran columna por
   *                       columna o renglon por renglon.
   * @return Regresa <b>true</b> si se pudieron agregar todos los elementos de la
   *         lista ligada, <b>false</b> en caso contrario.
   */
  public boolean agregarMatriz2D(Matriz2D matriz, TipoMatriz enumTipoMatriz) {
    boolean logrado = true;
    if (enumTipoMatriz == TipoMatriz.COLUMNA) {
      for (int columna = 0; columna < matriz.columnas(); columna++) {
        for (int renglon = 0; renglon < matriz.renglones(); renglon++) {
          if (agregar(matriz.obtenerInfo(renglon, columna)) == -1) {
            logrado = false;
          }
        }
      }
    } else {
      for (int renglon = 0; renglon < matriz.renglones(); renglon++) {
        for (int columna = 0; columna < matriz.columnas(); columna++) {
          if (agregar(matriz.obtenerInfo(renglon, columna)) == -1) {
            logrado = false;
          }
        }
      }
    }
    return logrado;
  }

  /**
   * Agrega los elementos de una Matriz2D al final de la ListaLigada.
   * 
   * @param matriz         Es la matriz de la cual se obtendran los elementos.
   * @param enumTipoMatriz Indica si los elementos se agregaran columna por
   *                       columna o renglon por renglon.
   * @return Regresa <b>true</b> si se pudieron agregar todos los elementos de la
   *         lista ligada, <b>false</b> en caso contrario.
   */
  public boolean agregarMatriz2DRR(Matriz2D matriz, TipoMatriz enumTipoMatriz) {
    if (enumTipoMatriz == TipoMatriz.COLUMNA) {
      return agregarMatriz2DColumnaRR(0, 0, matriz);
    } else {
      return agregarMatriz2DRenglonRR(0, 0, matriz);
    }
  }

  /**
   * Agrega los elementos de la matriz renglon por renglon.
   * @param reng Es el renglon actual.
   * @param col Es la columna actual.
   * @param matriz2d Es la matriz a agregar.
   * @return Regresa <b>true</b> si se pudieron agregar todos los elementos, <b>false</b> en caso contrario.
   */
  private boolean agregarMatriz2DRenglonRR(int reng, int col, Matriz2D matriz2d) {
    if (reng != matriz2d.renglones()-1 || col != matriz2d.columnas()-1) {
      boolean agregado = agregar(matriz2d.obtenerInfo(reng, col)) != -1;
      if (col != matriz2d.columnas()-1) {
        return agregado & agregarMatriz2DRenglonRR(reng, col+1, matriz2d);
      } else {
        return agregado & agregarMatriz2DRenglonRR(reng+1, 0, matriz2d);
      }
    } else {
      return agregar(matriz2d.obtenerInfo(reng, col)) != -1;
    }
  }

  /**
   * Agrega los elementos de la matriz columna por columna.
   * @param reng Es el renglon actual.
   * @param col Es la columna actual.
   * @param matriz2d Es la matriz a agregar.
   * @return Regresa <b>true</b> si se pudieron agregar todos los elementos, <b>false</b> en caso contrario.
   */
  private boolean agregarMatriz2DColumnaRR(int reng, int col, Matriz2D matriz2d) {
    if (reng != matriz2d.renglones()-1 || col != matriz2d.columnas()-1) {
      boolean agregado = agregar(matriz2d.obtenerInfo(reng, col)) != -1;
      if (reng != matriz2d.renglones()-1) {
        return agregado & agregarMatriz2DColumnaRR(reng+1, col, matriz2d);
      } else {
        return agregado & agregarMatriz2DColumnaRR(0, col+1, matriz2d);
      }
    } else {
      return agregar(matriz2d.obtenerInfo(reng, col)) != -1;
    }
  }

  /**
   * Cambia el elemento de la posicion especificada por un nuevo elemento.
   * 
   * @param indice Es la posicion donde se cambiara el elemento.
   * @param info   Es el nuevo elemento.
   * @return Regresa <b>true</b> si se pudo hacer el cambio, <b>false</b> en caso
   *         contrario.
   */
  public boolean cambiar(int indice, Object info) {
    if (indice < 0) {
      return false;
    }

    Nodo recorrer = primero;
    int actual = -1;

    while (recorrer != null && actual != indice) {
      actual++;
      if (actual != indice) {
        recorrer = recorrer.getLigaDer();
      }
    }

    if (actual != indice) {
      return false;
    } else {
      recorrer.setInfo(info);
      return true;
    }
  }

  /**
   * Cambia el elemento de la posicion especificada por un nuevo elemento usando metodos recursivos.
   * 
   * @param indice Es la posicion donde se cambiara el elemento.
   * @param info   Es el nuevo elemento.
   * @return Regresa <b>true</b> si se pudo hacer el cambio, <b>false</b> en caso
   *         contrario.
   */
  public boolean cambiarRR(int indice, Object info) {
    if (indice < 0) {
      return false;
    }
    return cambiarRR(primero, indice, info);
  }

  /**
   * Cambia el elemento de la posicion especificada por un nuevo elemento.
   * @param recorrer Es el nodo actual.
   * @param indice Es la posicion actual.
   * @return Regresa <b>true</b> si se pudo hacer el cambio, <b>false</b> en caso
   *         contrario.
   */
  private boolean cambiarRR(Nodo recorrer, int indice, Object info) {
    if (recorrer == null || indice == 0) {
      if (indice == 0 && recorrer != null) {
        recorrer.setInfo(info);
        return true;
      } else {
        return false;
      }
    } else {
      return cambiarRR(recorrer.getLigaDer(), indice-1, info);
    }
  }

  /**
   * Obtiene el elemento de la posicion especificada.
   * 
   * @param indice Es la posicion de la cual se obtendra el elemento.
   * @return Regresa el elemento si pudo encontrarlo, null en caso contrario.
   */
  public Object obtener(int indice) {
    if (indice < 0) {
      return null;
    }

    Nodo recorrer = primero;
    int actual = -1;

    while (recorrer != null && actual != indice) {
      actual++;
      if (actual != indice) {
        recorrer = recorrer.getLigaDer();
      }
    }

    if (actual != indice) {
      return null;
    } else {
      return recorrer.getInfo();
    }
  }

  /**
   * Obtiene el elemento de la posicion especificada usando metodo recursivo.
   * 
   * @param indice Es la posicion de la cual se obtendra el elemento.
   * @return Regresa el elemento si pudo encontrarlo, null en caso contrario.
   */
  public Object obtenerRR(int indice) {
    return obtenerRR(primero, indice);
  }

  /**
   * Obtiene el elemento de la posicion especificada mediante la recursividad.
   * 
   * @param recorrer Es el nodo actual.
   * @param indice Es la posicion de la cual se obtendra el elemento.
   * @return Regresa el elemento si pudo encontrarlo, null en caso contrario.
   */
  private Object obtenerRR(Nodo recorrer, int indice) {
    if (recorrer == null || indice == 0) {
      if (indice == 0 && recorrer != null) {
        return recorrer.getInfo();
      } else {
        return null;
      }
    } else {
      return obtenerRR(recorrer.getLigaDer(), indice-1);
    }
  }

  /**
   * Redimensiona el tamanio de la ListaLigada.
   * 
   * @param maximo Es el nuevo tamanio de la ListaLigada.
   * @return Regresa una ListaLigada con los elementos que fueron borrados, null
   *         en caso contrario.
   */
  public Object redimensionar(int maximo) {
    Nodo recorrer = primero;
    int tamanio = 0;

    while (recorrer != null) {
      tamanio++;
      recorrer = recorrer.getLigaDer();
    }

    if (tamanio > maximo) {
      ListaLigada eliminados = new ListaLigada();
      while (tamanio > maximo) {
        eliminados.agregar(eliminar());
        tamanio--;
      }
      eliminados.invertir();
      return eliminados;
    } else if (tamanio < maximo) {
      while (tamanio < maximo) {
        agregar(null);
        tamanio++;
      }
    }
    return null;
  }

  /**
   * Redimensiona el tamanio de la ListaLigada.
   * 
   * @param maximo Es el nuevo tamanio de la ListaLigada.
   * @return Regresa una ListaLigada con los elementos que fueron borrados, null
   *         en caso contrario.
   */
  public Object redimensionarRR(int maximo) {
    int count = contarNodosRR(primero);
    if (maximo < count) {
      ListaLigada eliminados = new ListaLigada();
      reducirRR(count, maximo, eliminados);
      eliminados.invertir();
      return eliminados;
    } else if (maximo > count) {
      rellenarRR(null, maximo - count);
    }
    return null;
  }

  /**
   * Disminuye el tamanio de la lista.
   * @param tam Es el tamanio actual.
   * @param maximo Es el nuevo tamanio.
   * @param borrados Es una ListaLigada con los elementos eliminados.
   */
  private void reducirRR(int tam, int maximo, ListaLigada borrados) {
    if (tam > maximo) {
      borrados.agregar(eliminarRR());
      reducirRR(tam-1, maximo, borrados);
    }
  }

  /**
   * Elimina el elemento en la posicion especificada.
   * 
   * @param indice Es la posicion del elemento a eliminar.
   * @return Regresa el elemento eliminado.
   */
  public Object eliminar(int indice) {
    if (indice < 0) {
      return null;
    }

    Nodo encontrado = primero;
    Nodo anterior = primero;
    int actual = -1;

    while (encontrado != null && actual != indice) {
      actual++;
      if (actual != indice) {
        anterior = encontrado;
        encontrado = encontrado.getLigaDer();
      }
    }

    if (actual != indice) {
      return null;
    } else {
      Object elementoBorrado = encontrado.getInfo(); // 1
      if (primero == ultimo) { // único, b)
        primero = null; // 2
        ultimo = null;
      } else if (primero == encontrado) { // c(, el primero
        primero = primero.getLigaDer(); // 2
      } else if (ultimo == encontrado) { // d), el último
        anterior.setLigaDer(null);// 2
        ultimo = anterior;// 3
      } else { // cualquier otro caso, es decir, en medio, e)
        Nodo siguiente = encontrado.getLigaDer();
        anterior.setLigaDer(siguiente);// 2
      }
      return elementoBorrado;
    }
  }

  /**
   * Elimina el elemento en la posicion especificada mediante metodos recursivos.
   * 
   * @param indice Es la posicion del elemento a eliminar.
   * @return Regresa el elemento eliminado.
   */
  public Object eliminarRR(int indice) {
    if (indice < 0 || vacia()) {
      return null;
    }

    if (indice == 0) {
      return eliminarInicio();
    }

    Nodo encontrado = primero;
    Nodo anterior = primero;

    anterior = buscarAnteriorIndiceRR(primero, primero, 0, indice);
    if (anterior == null) {
      return null;
    }
    encontrado = anterior.getLigaDer();

    if (encontrado == null) {
      return null;
    } else {
      Object elementoBorrado = encontrado.getInfo(); // 1
      if (primero == ultimo) { // único, b)
        primero = null; // 2
        ultimo = null;
      } else if (primero == encontrado) { // c(, el primero
        primero = primero.getLigaDer(); // 2
      } else if (ultimo == encontrado) { // d), el último
        anterior.setLigaDer(null);// 2
        ultimo = anterior;// 3
      } else { // cualquier otro caso, es decir, en medio, e)
        Nodo siguiente = encontrado.getLigaDer();
        anterior.setLigaDer(siguiente);// 2
      }
      return elementoBorrado;
    }
  }

  /**
   * Obtiene el nodo anterior al nodo con la posicion buscada.
   * @param anterior Es el nodo anterior.
   * @param recorrer Es el nodo actual.
   * @param pos Es la posicion actual.
   * @param indice Es el indice deseado.
   * @return Regresa el nodo anterior a la posicion deseada.
   */
  private Nodo buscarAnteriorIndiceRR(Nodo anterior, Nodo recorrer, int pos, int indice) {
    if (recorrer == null || pos == indice) {
      if(pos == indice) {
        return anterior;
      } else {
        return null;
      }
    } else {
      anterior = recorrer;
      return buscarAnteriorIndiceRR(anterior, recorrer.getLigaDer(), pos+1, indice);
    }
  }
}
