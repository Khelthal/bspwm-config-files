package edlineal;


/**
 * Esta clase es una cola que almacena los datos de acuerdo a su prioridad.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class ColaPrioridad implements Lote {
    protected Arreglo datos;
    protected Monticulo prioridades;

    /**
     * Crea una nueva instancia de ColaPrioridad con el tamanio y tipo de orden pasados como argumento.
     * @param MAXIMO Es el tamanio de la ColaPrioridad.
     * @param orden Es el tipo de orden de la ColaPrioridad.
     */
    public ColaPrioridad(int MAXIMO, TipoOrden orden) {
        datos = new Arreglo(MAXIMO);
        prioridades = new Monticulo(MAXIMO, orden);
    }

    @Override
    public boolean vacio() {
        return datos.vacia();
    }

    @Override
    public boolean lleno() {
        return datos.lleno();
    }

    @Override
    public boolean poner(Object info) {
        return poner(info, 0);
    }

    /**
     * Agrega un objeto a la cola dandole la prioridad indicada.
     * @param info Es el objeto a agregar.
     * @param prioridad Es la prioridad que tiene el objeto a ser agregado.
     * @return Regresa <b>true</b> si el objeto pudo ser agregado, <b>false</b> en caso contrario.
     */
    public boolean poner(Object info, int prioridad) {
        int indiceInicio = prioridades.obtenerDisponible();
        int indiceFin = prioridades.agregar(prioridad);

        if (indiceInicio == -1) {
            return false;
        }
        else if (indiceInicio == indiceFin) {
            datos.agregar(info);
        }
        else {
            indiceInicio = datos.agregar(info);
            int indicePadre;
            while (indiceInicio != indiceFin) {
                indicePadre = (indiceInicio+1)/2-1;
                Object infoHijo = datos.obtener(indiceInicio);
                Object infoPadre = datos.obtener(indicePadre);
                datos.cambiar(indicePadre, infoHijo);
                datos.cambiar(indiceInicio, infoPadre);
                indiceInicio = indicePadre;
            }
        }

        return true;
    }

    /**
     * Obtiene una lista con los indices de los elementos que deben intercambiarse al borrar un elemento.
     * @param indice Es el indice a partir del cual se obtienen las demas posiciones.
     * @return Regresa una pila con las posiciones que deben intercambiarse.
     */
    private PilaArreglo obtenerMovimientosBorrado(int indice) {
        PilaArreglo pila = new PilaArreglo(indice);
        while (indice != 0) {
            pila.poner(indice);
            indice = (indice+1)/2-1;
        }
        return pila;
    }

    @Override
    public Object quitar() {
        if (datos.vacia()) {
            return null;
        }
        Object objetoEliminado = datos.obtener(0);
        datos.cambiar(0, datos.obtener(datos.numElementos()-1));
        datos.eliminar();
        int indice = prioridades.eliminar();
        PilaArreglo movimientos = obtenerMovimientosBorrado(indice);
        indice = 0;
        while (!movimientos.vacio()) {
            int indiceNext = (Integer) movimientos.quitar();
            Object info = datos.obtener(indice);
            Object infoNext = datos.obtener(indiceNext);
            datos.cambiar(indice, infoNext);
            datos.cambiar(indiceNext, info);
            indice = indiceNext;
        }
        return objetoEliminado;
    }

    @Override
    public void imprimir() {
        datos.imprimirOI();
    }

    @Override
    public Object verLimite() {
        if (datos.vacia()) {
            return null;
        }
        return datos.obtener(0);
    }
}
