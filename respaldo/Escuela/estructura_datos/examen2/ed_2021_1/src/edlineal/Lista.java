package edlineal;

/**
 * Esta interface administra las operaciones sobre una lista de datos.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public interface Lista {

    /**
     * Determina si una lista esta vacia.
     * @return Regresa <b>true</b> si la lista esta vacia, <b>false</b> en caso contrario.
     */
    public boolean vacia();

    /**
     * Inserta al final elementos nuevos a la lista.
     * @param info Es el elemento que se va a agregar.
     * @return Regresa la posicion de memoria (indice) donde se agrego, -1 en caso contrario.
     */
    public int agregar(Object info);

    /**
     * Imprime el contenido de la lista en orden natural.
     */
    public void imprimir();

    /**
     * Imprime el contenido de la lista en orden inverso.
     */
    public void imprimirOI();

    /**
     * Busca el objeto indicado en la lista.
     * @param info Es el objeto a buscar.
     * @return Regresa el objeto buscado si se encuentra en la lista, null en caso contrario.
     */
    public Object buscar(Object info);

    /**
     * Elimina un objeto de la lista.
     * @param info Es el objeto a eliminar.
     * @return Regresa el objeto eliminado si se encontraba en la lista, null en caso contrario.
     */
    public Object eliminar(Object info);

    /**
     * Compara la lista con otra lista.
     * @param lista2 Es la lista con la que se va a comparar.
     * @return Regresa <b>true</b> si las listas son iguales, <b>false</b> en caso contrario.
     */
    public boolean esIgual(Object lista2);

    /**
     * Reemplaza en la lista n objetos iguales a un objeto indicado con un nuevo objeto.
     * @param infoVieja Es el objeto a reemplazar.
     * @param infoNueva Es el objeto que va a reemplazar al anterior.
     * @param numOcurrencias Es el numero de veces que se reemplazara el objeto.
     * @return Regresa <b>true</b> si se cambio al menos un valor, <b>false</b> en caso contrario.
     */
    public boolean cambiar(Object infoVieja, Object infoNueva, int numOcurrencias);

    /**
     * Busca las posiciones donde esta ubicado un objeto en la lista.
     * @param info Es el objeto a buscar.
     * @return Regresa un objeto de tipo Arreglo con las posiciones si se encontro al menos una vez el objeto, <b>null</b> en caso contrario.
     */
    public Arreglo buscarValores(Object info);

    /**
     * Elimina el ultimo elemento de la lista.
     * @return Regresa el elemento borrado, si la lista estaba vacia regresa <b>null</b>.
     */
    public Object eliminar();

    /**
     * Elimina todos los elementos de la lista.
     */
    public void vaciar();

    /**
     * Inserta los elementos de otra lista al final de la lista.
     * @param lista2 Es la lista a insertar.
     * @return Regresa <b>true</b> si lista2 es una lista valida, <b>false</b> en caso contrario.
     */
    public boolean agregarLista(Object lista2);

    /**
     * Reordena los elementos de la lista en orden inverso al original.
     */
    public void invertir();

    /**
     * Cuenta el numero de veces que aparece en la lista el objeto indicado.
     * @param info Es el objeto buscado.
     * @return Regresa el numero de veces que aparece en la lista el objeto indicado.
     */
    public int contar(Object info);

    /**
     * Elimina de la lista todos los elementos en comun con la lista argumento.
     * @param lista2 Es la lista de la cual se borraran los elementos.
     * @return Regresa <b>true</b> si lista2 es una lista valida.
     */
    public boolean eliminarLista(Object lista2);

    /**
     * Inserta el objeto indicado el numero de veces indicado.
     * @param info Es el objeto a ser insertado.
     * @param cantidad Es el numero de veces que <b>info</b> sera insertado.
     */
    public void rellenar(Object info, int cantidad);

    /**
     * Si el parametro pasado es un numero positivo, rellena la lista con numeros desde 1 hasta el numero (ascendente), si es negativo
     * rellena el arreglo con numeros desde -1 hasta el numero (descendente). Si el parametro es un char, rellena el arreglo con char
     * desde A hasta el char. Si es cualquier otro objeto, solo lo inserta una vez.
     * @param info Es el objeto para rellenar la lista.
     */
    public void rellenar(Object info);

    /**
     * Crea una copia de la lista.
     * @return Regresa una copia de la lista.
     */
    public Object clonar();

    /**
     * Crea una sublista del rango indicado.
     * @param indiceInicial Es el punto donde inicia la sublista.
     * @param indiceFinal Es el punto donde termina la sublista.
     * @return Regresa una sublista si el rango es valido, <b>null</b> en caso contrario.
     */
    public Object subLista(int indiceInicial, int indiceFinal);

    /**
     * Inserta en la posicion indice el objeto indicado.
     * @param indice Es el indice donde se insertara el objeto.
     * @param info Es el objeto a insertar.
     * @return Regresa <b>true</b> si se pudo insertar, <b>false</b> en caso contrario.
     */
    public boolean insertar(int indice, Object info);

}
