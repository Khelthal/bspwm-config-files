package imagen.lector;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Esta clase se encarga de leer archivos de imagen.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class LectorImagen {
    BufferedImage imagen;
    BufferedImage imagen2;
    int w;
    int h;

    /**
     * Crea una nueva instancia que lee una imagen con el nombre dado.
     * @param nombreImagen Es el nombre de la imagen.
     */
    public LectorImagen(String nombreImagen) {
        try {
            imagen = ImageIO.read(new File(nombreImagen));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        w = imagen.getWidth();
        h = imagen.getHeight();
        imagen2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
    }

    /**
     * Obtiene un pixel de la imagen en la coordenada dada.
     *
     * @param i Es la primera coordenada.
     * @param j Es la segunda coordenada.
     * @return Regresa el pixel ubicado en las coordenadas pasadas como argumento.
     */
    public int getPixel(int i, int j) {
        return imagen.getRGB(i, j);
    }

    /**
     * Inserta un pixel en la imagen en la coordenada dada.
     *
     * @param i     Es la primera coordenada.
     * @param j     Es la segunda coordenada.
     * @param color Es el nuevo pixel a insertar.
     */
    public void setRGB(int i, int j, int color) {
        imagen2.setRGB(i, j, color);
    }

    /**
     * Obtiene el ancho de la imagen.
     *
     * @return Regresa el ancho de la imagen.
     */
    public int getWidth() {
        return w;
    }

    /**
     * Obtiene la altura de la imagen.
     *
     * @return Regresa la altura de la imagen.
     */
    public int getHeight() {
        return h;
    }

    /**
     * Escribe la imagen2 en un archivo.
     */
    public void escribirImagen() {
        try {
            ImageIO.write(imagen2, "JPG", new File("src/espejo.jpg"));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Redimensiona la imagen.
     * @param width Es el nuevo ancho de la imagen.
     * @param height Es la nueva altura de la imagen.
     */
    public void redimensionarImagen(int width, int height) {
        w = width;
        h = height;
        imagen2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
    }
}
