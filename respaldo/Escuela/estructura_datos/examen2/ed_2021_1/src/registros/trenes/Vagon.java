package registros.trenes;

import edlineal.ListaLigada;

/**
 * Vagon representa a un vagon y sus datos.
 */
public class Vagon {
  private static int ID = 0;
  private int id;
  private ListaLigada productos;

  public Vagon() {
    id = ID++;
    productos = new ListaLigada();
  }

  /**
   * Agrega un producto al vagon.
   * @param p Es el producto a agregar.
   * @return Regresa true si pudo agregar el producto, false en caso contrario.
   */
  public boolean agregarProducto(Producto p) {
    return productos.agregar(p) != -1;
  }

  /**
   * Busca un producto en el vagon.
   * @param p Es el producto buscado.
   * @return Regresa true si contiene el producto, false en caso contrario.
   */
  public boolean buscarProducto(String p) {
    return productos.buscarRR(p) != null;
  }

  @Override
  public String toString() {
    return "Vagon " + id;
  }
}
