package registros.trenes;

import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de imprimir el menu de registro de ferrocarriles y hacer llamadas
 * a la clase RegistroFerrocarriles.
 */
public class MenuRegistroFerrocarriles {
  protected RegistroFerrocarriles registro = new RegistroFerrocarriles();

  /**
   * Inicia el menu
   */
  public void iniciar() {
    while (true) {
      imprimirMenu();
      SalidaEstandar.consola("\n");
      seleccionarOpcion();
      SalidaEstandar.consola("Presiona enter para continuar\n");
      EntradaConsola.consolaCadena();
    }
  }

  /**
   * Imprime las opciones del menu.
   */
  public void imprimirMenu() {
    SalidaEstandar.consola("¿Que desea hacer?\n");
    SalidaEstandar.consola("a) Agregar ferrocarril.\n");
    SalidaEstandar.consola("b) Observar si un ferrocarril lleva locomotoras.\n");
    SalidaEstandar.consola("c) Desconectar vagon de ferrocarril.\n");
    SalidaEstandar.consola("d) Agregar vagon a ferrocarril.\n");
    SalidaEstandar.consola("e) Buscar producto en ferrocarril.\n");
    SalidaEstandar.consola("f) Especificar direccion de ferrocarril.\n");
    SalidaEstandar.consola("g) Mostrar el numero de trenes con destino especifico.\n");
    SalidaEstandar.consola("h) Mostrar el tiempo que requiere un tren para llegar al destino.\n");
    SalidaEstandar.consola("q) Salir.\n");
  }

  /**
   * Recibe la entrada del usuario para elegir una de las opciones.
   */
  public void seleccionarOpcion() {
    String respuesta = EntradaConsola.consolaCadena();
    switch (respuesta) {
    case "a":
      registro.agregarFerrocarril();
      break;
    case "b":
      SalidaEstandar.consola(registro.llevaLocomotoras(registro.seleccionarFerrocarril()) + "\n");
      break;
    case "c":
      registro.desconectarVagones(registro.seleccionarFerrocarril());
      break;
    case "d":
      registro.agregarVagones(registro.seleccionarFerrocarril());
      break;
    case "e":
      if (registro.buscarEnFerrocarril(registro.seleccionarFerrocarril())) {
        SalidaEstandar.consola("Ese ferrocarril si contiene el producto\n");
      } else {
        SalidaEstandar.consola("Ese ferrocarril no contiene el producto\n");
      }
      break;
    case "f":
      registro.definirSentidoFerrocarril(registro.seleccionarFerrocarril());
      break;
    case "g":
      SalidaEstandar.consola("El numero de trenes con ese destino es: " + registro.contarFerrocarrilesDestino() + "\n");
      break;
    case "h":
      SalidaEstandar.consola("El tiempo para llegar a ese destino es: " + registro.calcularTiempo(registro.seleccionarFerrocarril()) + " horas\n");
      break;
    case "q":
      System.exit(0);
    }
  }

}
