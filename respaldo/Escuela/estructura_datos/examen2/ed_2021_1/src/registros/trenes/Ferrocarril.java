package registros.trenes;

import edlineal.ListaLigada;

/**
 * Ferrocarril representa a un ferrocarril y sus datos.
 */
public class Ferrocarril {
  private static int ID=0;
  private int id;
  private int velocidadMax;
  private int velocidadActual;
  private boolean haciaAdelante;
  private String destino;
  private boolean enTerminal;
  private ListaLigada vagones;

  /**
   * Crea una nueva instancia de ferrocarril.
   * @param velocidadMax Es la velocidad maxima.
   * @param velocidadActual Es la velocidad actual.
   * @param haciaAdelante Es la dirrecion.
   * @param destino Es el destino.
   * @param enTerminal Indica si el tren se encuentra en terminal.
   */
  public Ferrocarril(int velocidadMax, int velocidadActual, boolean haciaAdelante, String destino, boolean enTerminal) {
    id = ID++;
    this.velocidadMax = velocidadMax;
    this.velocidadActual = velocidadActual;
    this.haciaAdelante = haciaAdelante;
    this.destino = destino;
    this.enTerminal = enTerminal;
    vagones = new ListaLigada();
  }

  public int getVelocidadMax() {
      return velocidadMax;
  }

  public int getVelocidadActual() {
      return velocidadActual;
  }

  public boolean isHaciaAdelante() {
      return haciaAdelante;
  }
  
  public void setHaciaAdelante(boolean haciaAdelante) {
      this.haciaAdelante = haciaAdelante;
  }

  public boolean isEnTerminal() {
      return enTerminal;
  }

  public String getDestino() {
      return destino;
  }

  /**
   * Agrega un vagon al ferrocarril.
   * @param v Es el vagon a agregar.
   * @return Regresa true si pudo agregar el vagon, false en caso contrario.
   */
  public boolean agregarVagon(Vagon v) {
    return vagones.agregar(v) != -1;
  }

  /**
   * Determina si el ferrocarril lleva locomotoras.
   * @return Regresa true si tiene al menos una locomotora, false en caso contrario.
   */
  public boolean llevaLocomotoras() {
    return !vagones.vacia();
  }

  /**
   * Busca si el ferrocarril contiene un producto especifico.
   * @param p Es el producto.
   * @return Regresa true si contiene el producto, false en caso contrario.
   */
  public boolean buscarProducto(String p) {
    vagones.inicializarIterador();
    return buscarProductoRR(p);
  }

  private boolean buscarProductoRR(String p) {
    if (vagones.hayMas()) {
      Vagon v = (Vagon) vagones.obtenerSigiuente();
      if (v.buscarProducto(p)) {
        return true;
      } else {
        return buscarProducto(p);
      }
    }
    return false;
  }
  
  /**
   * Imprime los vagones del ferrocarril.
   * */
  public void imprimirVagones() {
    vagones.imprimirRR();
  }

  /**
   * Elimina un vagon de la locomotora.
   * @param v Es el vagon a eliminar.
   * @return Regresa el vagon eliminado.
   */
  public Vagon eliminarVagon(String v) {
    return (Vagon) vagones.eliminarRR(v);
  }

  @Override
  public String toString() {
    return "Ferrocarril " + id;
  }
}
