package registros.trenes;

/**
 * Producto representa a un producto y sus datos.
 */
public class Producto {
  private String nombre;
  private String descripcion;
  private int cantidad;

  public Producto(String nombre, String descripcion, int cantidad) {
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.cantidad =cantidad;
  }

  public String getNombre() {
      return nombre;
  }

  public String getDescripcion() {
      return descripcion;
  }

  public int getCantidad() {
      return cantidad;
  }

  public void setCantidad(int cantidad) {
      this.cantidad = cantidad;
  }

  @Override
  public String toString() {
    return nombre;
  }
}
