package registros.trenes;

import edlineal.ListaLigada;
import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;

/**
 * RegistroFerrocarriles
 */
public class RegistroFerrocarriles {
  private ListaLigada ferrocarriles = new ListaLigada();

  /**
   * Agrega un ferrocarril a la lista de ferrocarriles.
   * */
  public void agregarFerrocarril() {
    SalidaEstandar.consola("Ingrese la velocidad maxima del ferrocarril (kilometros/hora) \n:");
    int velocidadMax = EntradaConsola.consolaInt();
    SalidaEstandar.consola("Ingrese la velocidad actual del ferrocarril (kilometros/hora) \n:");
    int velocidadActual = EntradaConsola.consolaInt();
    SalidaEstandar.consola("Ingrese si el ferrocarril va hacia adelante (1) o hacia atras (0) \n:");
    boolean haciaAdelante = EntradaConsola.consolaInt() != 0;
    SalidaEstandar.consola("Ingrese el destino del ferrocarril \n:");
    String destino = EntradaConsola.consolaCadena();
    SalidaEstandar.consola("Ingrese si el ferrocarril se encuentra en una terminal (1) o no (0) \n:");
    boolean enTerminal = EntradaConsola.consolaInt() != 0;

    Ferrocarril f = new Ferrocarril(velocidadMax, velocidadActual, haciaAdelante, destino, enTerminal);
    agregarVagones(f);
    ferrocarriles.agregar(f);
  }
  
  /**
   * Agrega un vagon a un ferrocarril.
   * @param f Es el ferrocarril al que se le agregara el vagon.
   */
  public void agregarVagones(Ferrocarril f) {
    if (f == null) {
      return;
    }
    if (!f.isEnTerminal()) {
      SalidaEstandar.consola("Ese ferrocarril no se encuentra en una terminal\n");
      return;
    }
    SalidaEstandar.consola("¿Desea agregar un vagon al ferrocarril s/n?\n:");
    String respuesta = EntradaConsola.consolaCadena();
    if (respuesta.equalsIgnoreCase("s")) {
      Vagon v = new Vagon();
      agregarProductos(v);
      if (f.agregarVagon(v)) {
        SalidaEstandar.consola("Vagon agregado correctamente\n");
        agregarVagones(f);
      } else {
        SalidaEstandar.consola("No se pudo agregar el vagon");
      }
    }    
  }

  /**
   * Permite agregar productos a un vagon.
   * @param v Es el vagon al que se le agregaran productos.
   */
  public void agregarProductos(Vagon v) {
    SalidaEstandar.consola("¿Desea agregar un producto al vagon s/n?\n:");
    String respuesta = EntradaConsola.consolaCadena();
    if (respuesta.equalsIgnoreCase("s")) {
      SalidaEstandar.consola("Ingrese el nombre del producto\n:");
      String nombre = EntradaConsola.consolaCadena();
      SalidaEstandar.consola("Ingrese la descripcion del producto\n:");
      String descripcion = EntradaConsola.consolaCadena();
      SalidaEstandar.consola("Ingrese la cantidad del producto\n:");
      int cantidad = EntradaConsola.consolaInt();
      Producto p = new Producto(nombre, descripcion, cantidad);
      if (v.agregarProducto(p)) {
        SalidaEstandar.consola("Producto agregado correctamente\n");
        agregarProductos(v);
      } else {
        SalidaEstandar.consola("No se pudo agregar el producto\n");
      }
    }    
  }

  /**
   * Determina si una locomotora contiene almenos un vagon.
   * @param f Es la locomotora a analizar.
   * @return Regresa <b>true</b> si la locomotora tiene al menos un vagon, <b>false</b> en caso contrario.
   */
  public boolean llevaLocomotoras(Ferrocarril f) {
    if (f == null) {
      return false;
    }
    return f.llevaLocomotoras();
  }

  public void desconectarVagones(Ferrocarril f) {
    if (f == null) {
      return;
    }
    if (!f.isEnTerminal()) {
      SalidaEstandar.consola("Ese ferrocarril no se encuentra en una terminal\n");
      return;
    }
    if (f.llevaLocomotoras()) {
      f.imprimirVagones();
      SalidaEstandar.consola("\n");
      SalidaEstandar.consola("Ingrese el nombre completo del vagon que desea desconectar \n:");
      f.eliminarVagon(EntradaConsola.consolaCadena());
    }
  }

  /**
   * Busca si un ferrocarril especifico contiene un producto especifico.
   * @param f Es el ferrocarril.
   * @return Regresa true si contiene el producto, false en caso contrario.
   */
  public boolean buscarEnFerrocarril(Ferrocarril f) {
    if (f == null) {
      return false;
    }
    SalidaEstandar.consola("Ingrese el nombre del producto buscado \n:");
    String producto = EntradaConsola.consolaCadena();

    return f.buscarProducto(producto);
  }

  /**
   * Define el sentido de un ferrocarril.
   * @param f Es el ferrocarril.
   */
  public void definirSentidoFerrocarril(Ferrocarril f) {
    SalidaEstandar.consola("Ingrese si el ferrocarril va hacia adelante (1) o hacia atras (0) \n:");
    boolean haciaAdelante = EntradaConsola.consolaInt() != 0;
    f.setHaciaAdelante(haciaAdelante);
  }

  /**
   * Cuenta el numero de ferrocarriles que tienen el destino especificado.
   * @return Regres la cantidad contada.
   */
  public int contarFerrocarrilesDestino() {
    SalidaEstandar.consola("Ingrese el destino que desea consultar \n:");
    String destino = EntradaConsola.consolaCadena();
    ferrocarriles.inicializarIterador();
    return contarFerrocarrilesDestinoRR(0, destino);
  }

  private int contarFerrocarrilesDestinoRR(int count, String destino) {
    if (ferrocarriles.hayMas()) {
      Ferrocarril f = (Ferrocarril) ferrocarriles.obtenerSigiuente();
      SalidaEstandar.consola(f.getDestino() + " : " + destino);
      if (f.getDestino().equalsIgnoreCase(destino)) {
        return contarFerrocarrilesDestinoRR(count+1, destino);
      } else {
        return contarFerrocarrilesDestinoRR(count, destino);
      }
    }
    return count;
  }

  /**
   * Calcula el tiempo que tomara al tren llegar a su destino.
   * @param f Es el ferrocarril a analizar.
   * @return Regresa el tiempo que le tomara al ferrocarril llegar.
   */
  public int calcularTiempo(Ferrocarril f) {
    if (f == null) {
      return 0;
    }
    SalidaEstandar.consola("Ingrese la distancia (kilometros) a la que se encuentra el destino del tren \n:");
    int distancia = EntradaConsola.consolaInt();
    return distancia / f.getVelocidadActual();
  }

  /**
   * Permite seleccionar un ferrocarril de los ferrocarriles registrados.
   * @return Regresa el ferrocarril encontrado.
   */
  public Ferrocarril seleccionarFerrocarril() {
    ferrocarriles.imprimirRR();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Ingrese el nombre junto a su ID del ferrocarril deseado \n:");
    return (Ferrocarril) ferrocarriles.buscarRR(EntradaConsola.consolaCadena());
  }

  
}
