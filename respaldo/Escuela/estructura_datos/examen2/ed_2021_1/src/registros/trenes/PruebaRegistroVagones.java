package registros.trenes;

/**
 * PruebaRegistroVagones se encarga de hacer pruebas con RegistroVagones.
 */
public class PruebaRegistroVagones {

  public static void main(String[] args) {
    MenuRegistroFerrocarriles menu = new MenuRegistroFerrocarriles();
    menu.iniciar();
  }
}
