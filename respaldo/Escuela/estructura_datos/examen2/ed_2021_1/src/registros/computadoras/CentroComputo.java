package registros.computadoras;

/**
 * Esta clase guarda informacion de un centro de computo.
 */
public class CentroComputo {
  protected static int id;
  protected int numCc;
  protected String nombre;
  protected int numAula;

  /**
   * Crea una nueva instancai de CentroComputo
   * @param nombre Es el nombre del centro.
   * @param numAula Es el numero de aula al que pertenece.
   */
  public CentroComputo(String nombre, int numAula) {
    numCc = id;
    id++;
    this.nombre = nombre;
    this.numAula = numAula;
  }

  /**
   * Obtiene el numero del centro de computo.
   * @return Regresa el numero del centro de computo.
   */
  public int getNumCc() {
      return numCc;
  }

  @Override
  public String toString() {
    return numCc + " " + nombre;
  }
}
