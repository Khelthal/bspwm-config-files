package registros.pintores;

/**
 * Esta clase enumerada contiene todos los posibles valores para las actividades de los pintores.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public enum ActividadPintor {
    PINTAR,
    EXPONER,
    CONVIVIR,
    FIRMAR_AUTOGRAFOS,
    OBSERVAR
}
