package registros.indices;

import comparadores.ComparadorObject;
import edlineal.ArregloOrdenado;
import edlineal.TipoOrden;
import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de manejar los indices.
 */
public class IndiceTerminos {
    protected ArregloOrdenado terminos = new ArregloOrdenado(10, TipoOrden.CRECIENTE);

    /**
     * Agrega un nuevo termino a el arreglo.
     */
    public void agregarTermino() {
        if (terminos.lleno()) {
            terminos.redimensionar(terminos.longitud()+10);
        }
        terminos.agregar(crearTermino());
    }

    /**
     * Crea una nueva instancia de Termino.
     * @return Regresa la instancia de Termino creada.
     */
    public Termino crearTermino() {
        SalidaEstandar.consola("Inserte el nombre del termino.\n:");
        String nombre = EntradaConsola.consolaCadena();
        String paginas;
        Termino termino;
        do {
            SalidaEstandar.consola("Inserte las paginas del termino separadas por comas, ejemplo: '1,3,5-10,15'.\n:");
            paginas = EntradaConsola.consolaCadena();
            termino = new Termino(nombre, paginas);
        } while (!termino.paginasCorrectas());

        SalidaEstandar.consola("¿Desea agregar un subtermino a " + termino + " (s/n)?.\n:");
        String respuesta = EntradaConsola.consolaCadena();
        while (respuesta.equalsIgnoreCase("s")) {
            termino.agregar(crearTermino());
            SalidaEstandar.consola("¿Desea agregar un subtermino a " + termino + " (s/n)?.\n:");
            respuesta = EntradaConsola.consolaCadena();
        }
        return termino;
    }

    /**
     * Busca una instancia de Termino entre los terminos con el nombre indicado por el usuario.
     * @return Regresa la instancia si fue encontrada, null en caso contrario.
     */
    public Termino buscarTermino() {
        SalidaEstandar.consola("Inserte el nombre del termino a buscar.\n:");
        String nombre = EntradaConsola.consolaCadena();
        return buscarTermino(nombre, terminos, false);
    }

    /**
     * Busca una instancia de Termino entre los subterminos con el nombre indicado por el usuario.
     * @return Regresa la instancia si fue encontrada, null en caso contrario.
     */
    public Termino buscarSubTermino() {
        SalidaEstandar.consola("Inserte el nombre del subtermino a buscar.\n:");
        String nombre = EntradaConsola.consolaCadena();
        for (int i = 0; i < terminos.numElementos(); i++) {
            Termino termino = (Termino) terminos.obtener(i);
            Termino encontrado = buscarTermino(nombre, termino.getSubTerminos(), true);
            if (encontrado != null) {
                return encontrado;
            }
        }
        return null;
    }

    /**
     * Busca una instancia de Termino con el nombre buscado entre todas las instancias de Termino agregadas.
     * @param buscado Es el nombre del Termino buscado.
     * @param arregloTerminos Es el arreglo que contiene los terminos.
     * @param recursivo Indica si la busqueda debe hacerse de forma recursiva.
     * @return Regresa la instancia de Termino si fue encotrada, null en caso contrario.
     */
    public Termino buscarTermino(String buscado, ArregloOrdenado arregloTerminos, boolean recursivo) {
        if (arregloTerminos.vacia()) {
            return null;
        }

        int indice = (Integer) arregloTerminos.buscar(buscado);
        if (indice > 0) {
            indice--;
            return (Termino) arregloTerminos.obtener(indice);
        }

        if (!recursivo) {
            return null;
        }

        for (int i = 0; i < arregloTerminos.numElementos(); i++) {
            Termino termino = (Termino) arregloTerminos.obtener(i);
            Termino encontrado = buscarTermino(buscado, termino.getSubTerminos(), true);
            if (encontrado != null) {
                return termino;
            }
        }
        return null;
    }

    /**
     * Muestra la informacion de una instancia de Termino encontrada entre los sub terminos.
     */
    public void mostrarSubTermino() {
        Termino termino = buscarSubTermino();
        if (termino == null) {
            SalidaEstandar.consola("El termino buscado no se encuentra en el indice.\n");
            return;
        }
        SalidaEstandar.consola( termino.obtenerDatos() + "\n");
        mostrarTerminos(termino.getSubTerminos(), 1);
    }

    /**
     * Muestra la informacion de una instancia de Termino encontrada entre los terminos.
     */
    public void mostrarTermino() {
        Termino termino = buscarTermino();
        if (termino == null) {
            SalidaEstandar.consola("El termino buscado no se encuentra en el indice.\n");
            return;
        }
        SalidaEstandar.consola( termino.obtenerDatos() + "\n");
        mostrarTerminos(termino.getSubTerminos(), 1);
    }

    /**
     * Muestra todos los terminos agregados.
     */
    public void mostrarTerminos() {
        mostrarTerminos(terminos, 0);
    }

    /**
     * Muestra todos los terminos agregados en arregloTerminos.
     * @param arregloTerminos Es el arreglo donde se buscaran los terminos.
     * @param tabulaciones Es el numero de tabulaciones que se agregaran para imprimir el termino.
     */
    public void mostrarTerminos(ArregloOrdenado arregloTerminos, int tabulaciones) {
        if (!arregloTerminos.vacia()) {
            for (int i = 0; i < arregloTerminos.numElementos(); i++) {
                Termino termino = (Termino) arregloTerminos.obtener(i);
                String salida = "";
                for (int v = 0; v < tabulaciones; v++) {
                    salida += "\t";
                }
                salida += termino.obtenerDatos();
                SalidaEstandar.consola(salida + "\n");
                mostrarTerminos(termino.getSubTerminos(), tabulaciones+1);
            }
        }
    }

    /**
     * Muestra todos los terminos cuya inicial se encuentre en un rango de letras.
     */
    public void mostrarTerminosInicial() {
        SalidaEstandar.consola("Inserte la letra donde inicia el rango.\n:");
        String inicio = EntradaConsola.consolaCadena();
        if (inicio.length() != 1) {
            SalidaEstandar.consola(inicio + " no es una letra valida.\n");
            return;
        }
        SalidaEstandar.consola("Inserte la letra donde termina el rango.\n:");
        String fin = EntradaConsola.consolaCadena();
        if (fin.length() != 1) {
            SalidaEstandar.consola(fin + " no es una letra valida.\n");
            return;
        }

        if (ComparadorObject.compare(inicio, fin) > 0) {
            return;
        }

        ArregloOrdenado encontrados = new ArregloOrdenado(terminos.numElementos(), TipoOrden.CRECIENTE);

        for (int i = 0; i < terminos.numElementos(); i++) {
            Termino termino = (Termino) terminos.obtener(i);
            String nombre = termino.toString();
            char inicial = nombre.charAt(0);
            if (ComparadorObject.compare(inicio, inicial) <= 0 && ComparadorObject.compare(fin, inicial) >= 0) {
                encontrados.agregar(termino);
            }
        }

        mostrarTerminos(encontrados, 0);
    }

    /**
     * Muestra todos los terminos que tengan al menos una pagina con un numero dentro del rango dado.
     */
    public void mostrarTerminosRango() {
        SalidaEstandar.consola("Inserte la pagina inicial del rango.\n:");
        int inicio = EntradaConsola.consolaInt();
        SalidaEstandar.consola("Inserte la pagina final del rango.\n:");
        int fin = EntradaConsola.consolaInt();
        ArregloOrdenado encontrados = new ArregloOrdenado(terminos.numElementos(), TipoOrden.CRECIENTE);
        for (int i = 0; i < terminos.numElementos(); i++) {
            Termino termino = (Termino) terminos.obtener(i);
            ArregloOrdenado paginas = termino.paginasToArregloOrdenado();

            for (int v = 0; v < paginas.numElementos(); v++) {
                int pagina = (Integer) paginas.obtener(v);
                if (pagina >= inicio && pagina <= fin) {
                    encontrados.agregar(termino);
                    break;
                }
            }
        }
        mostrarTerminos(encontrados, 0);
    }
}
