package interfaces;

import edlineal.Arreglo;
import edlineal.ListaLigada;
import edlineal.enumerados.TipoMatriz;
import ednolineal.Matriz2D;
import entradasalida.SalidaEstandar;

/**
 * PruebaListaLigada
 */
public class PruebaListaLigada {
  public static void main(String[] args) {
    ListaLigada ligada = new ListaLigada();
    ligada.agregar("A");
    ligada.agregar("B");
    ligada.agregar("C");
    ligada.agregar("D");
    ligada.imprimirRR();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Invirtiendo lista\n");
    ligada.invertirRR();
    ligada.imprimirRR();
    SalidaEstandar.consola("\n");
    Arreglo ignorar = new Arreglo(2);
    ignorar.agregar("A");
    ignorar.agregar("D");
    SalidaEstandar.consola("Convirtiendo a arreglo ignorando los elementos A y D\n");
    Arreglo arreglo = ligada.aArregloRR(ignorar);
    arreglo.imprimirOIRR();
    SalidaEstandar.consola("Conviritiendo a Matriz2D de 2 renglones y 3 columnas\n");
    Matriz2D matriz2d = ligada.aMatriz2DRR(2, 3);
    matriz2d.imprimirR();
    SalidaEstandar.consola("Agregando 5 veces el elemento Z\n");
    ligada.rellenarRR("Z", 5);
    ligada.imprimirRR();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Cambiando 3 veces el elemento Z por el elemento Y\n");
    ligada.cambiarRR("Z", "Y", 3);
    ligada.imprimirRR();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Cambiando el elemento de la posicion 0 por P\n");
    ligada.cambiarRR(0, "P");
    ligada.imprimirRR();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Obteniendo el elemento de la posicion 3\n");
    SalidaEstandar.consola(ligada.obtenerRR(3)+"\n");
    ListaLigada copia = (ListaLigada) ligada.clonarRR();
    SalidaEstandar.consola("Creando copia de la lista y comprobando si son iguales\n");
    SalidaEstandar.consola(ligada.esIgualRR(copia)+"\n");
    SalidaEstandar.consola("Redimensionando la copia a tamanio 4\n");
    ListaLigada residuo = (ListaLigada) copia.redimensionarRR(4);
    copia.imprimirRR();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Mostrando elementos eliminados al redimensionar\n");
    residuo.imprimirRR();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Redimensionando la copia a tamanio 9\n");
    copia.redimensionarRR(9);
    copia.imprimirRR();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Rellenando la copia con el elemento J\n");
    copia.rellenarRR("J");
    copia.imprimirRR();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Eliminando en la copia el elemento de la posicion 2\n");
    copia.eliminarRR(2);
    copia.imprimirRR();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Insertando elemento W en la posicion 3\n");
    copia.insertarRR(3, "W");
    copia.imprimirRR();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Creando Matriz2D\n");
    Matriz2D agregame = new Matriz2D(2, 2);
    agregame.cambiarInfo(0, 0, 0);
    agregame.cambiarInfo(0, 1, 1);
    agregame.cambiarInfo(1, 0, 2);
    agregame.cambiarInfo(1, 1, 3);
    agregame.imprimirR();
    SalidaEstandar.consola("Agregando Matriz2D por columnas a la copia\n");
    copia.agregarMatriz2DRR(agregame, TipoMatriz.COLUMNA);
    copia.imprimirRR();
    SalidaEstandar.consola("\n");
  }
}
