package interfaces;

import ednolineal.Matriz2D;
import ednolineal.Matriz2DNumerica;
import ednolineal.TipoLog;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de realizar pruebas con la clase Matriz2DNumerica.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class PruebaMatriz2DNumerica {
    public static void main(String[] args) {
        SalidaEstandar.consola("Creando matriz1 \n");
        Matriz2DNumerica x = new Matriz2DNumerica(5, 5, 2);
        x.imprimirR();
        SalidaEstandar.consola("Creando matriz2 \n");
        Matriz2DNumerica y = new Matriz2DNumerica(5, 5, 6);
        y.imprimirR();
        SalidaEstandar.consola("Sumando matrices \n");
        x.sumarMatriz(y);
        x.imprimirR();
        SalidaEstandar.consola("Doblando largo \n");
        x.doblarLargo();
        x.imprimirR();
        SalidaEstandar.consola("Doblando ancho \n");
        x.doblarAncho();
        x.imprimirR();
        SalidaEstandar.consola("Elevando matriz a la potencia 2 \n");
        x.potencia(2);
        x.imprimirR();

    }
}
