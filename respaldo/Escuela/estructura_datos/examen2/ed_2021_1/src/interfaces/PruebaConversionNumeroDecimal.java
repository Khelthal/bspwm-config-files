package interfaces;

import calculos.conversiones.ConversionNumeroDecimal;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de realizar pruebas con la clase ConversionNumeroDecimal.
 */
public class PruebaConversionNumeroDecimal {

  public static void main(String[] args) {
    int num = 65029;
    SalidaEstandar.consola("El numero en decimal es : " + num + "\n");
    String hexadecimal = ConversionNumeroDecimal.cambiarBaseNumero(65029, 16);
    SalidaEstandar.consola("El numero en hexadecimal es : " + hexadecimal + "\n");
    String binario = ConversionNumeroDecimal.cambiarBaseNumero(65029, 2);
    SalidaEstandar.consola("El numero en binario es : " + binario + "\n");
    SalidaEstandar.consola("El numero en binario convertido con el metodo aBinario es: " + ConversionNumeroDecimal.aBinario(num) + "\n");
  }
}
