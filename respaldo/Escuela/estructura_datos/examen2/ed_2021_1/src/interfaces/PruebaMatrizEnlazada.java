package interfaces;

import edlineal.Arreglo;
import ednolineal.MatrizEnlazada;
import entradasalida.SalidaEstandar;

/**
 * PruebaMatrizEnlazada
 */
public class PruebaMatrizEnlazada {

  public static void main(String[] args) {
    MatrizEnlazada matriz = new MatrizEnlazada();
    matriz.insertarFinalCabezas("A");
    matriz.insertarFinalCabezas("B");
    matriz.insertarFinalCabezas("C");
    matriz.insertarEnGrupo("d", 0);
    matriz.insertarEnGrupo("e", 1);
    matriz.insertarEnGrupo("f", 2);
    matriz.imprimir();
    Arreglo posiciones = matriz.buscar("f");
    SalidaEstandar.consola("Elemento encontrado en cabeza " + posiciones.obtener(1) + ", fila " + posiciones.obtener(0) + "\n");
  }
}
