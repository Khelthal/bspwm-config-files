package interfaces;

import edlineal.Arreglo;
import edlineal.ListaLigadaOrdenada;
import edlineal.TipoOrden;
import edlineal.enumerados.TipoMatriz;
import ednolineal.Matriz2D;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de realizar pruebas con la clase ListaLigadaOrdenada.
 */
public class PruebaListaLigadaOrdenada {

  public static void main(String[] args) {
    ListaLigadaOrdenada lista = new ListaLigadaOrdenada(TipoOrden.CRECIENTE);
    Arreglo datos = new Arreglo(7);
    datos.agregar("F");
    datos.agregar("X");
    datos.agregar("B");
    datos.agregar("D");
    datos.agregar("C");
    datos.agregar("Z");
    datos.agregar("B");

    lista.agregar("F");
    lista.agregar("X");
    lista.agregar("B");
    lista.agregar("D");
    lista.agregar("C");
    lista.agregarInicio("Z");
    lista.agregarFinal("B");

    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Vaciando lista\n");
    lista.vaciar();
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Arreglo creado\n");
    datos.imprimir();
    SalidaEstandar.consola("Rellenando lista con arreglo\n");
    lista.agregarLista(datos);
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Eliminando elemento Z\n");
    lista.eliminar("Z");
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Cambiando elemento D por elemento W\n");
    lista.cambiar("D", "W", 1);
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Invirtiendo\n");
    lista.invertir();
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Buscando la poscion del elemento B\n");
    lista.buscarValores("B").imprimir();
    SalidaEstandar.consola("Creando Matriz2D\n");
    Matriz2D datos2d = new Matriz2D(2, 2);
    datos2d.cambiarInfo(0, 0, "W");
    datos2d.cambiarInfo(0, 1, "B");
    datos2d.cambiarInfo(1, 0, "R");
    datos2d.cambiarInfo(1, 1, "K");
    datos2d.imprimirR();
    SalidaEstandar.consola("Agregando Matriz2D a la lista\n");
    lista.agregarMatriz2D(datos2d, TipoMatriz.COLUMNA);
    lista.imprimir();
    SalidaEstandar.consola("\n");
    SalidaEstandar.consola("Eliminando Matriz2D de la lista\n");
    lista.eliminarMatriz2D(datos2d);
    lista.imprimir();
    SalidaEstandar.consola("\n");
  }
}
