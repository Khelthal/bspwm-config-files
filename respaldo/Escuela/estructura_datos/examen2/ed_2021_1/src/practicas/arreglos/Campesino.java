package practicas.arreglos;

import edlineal.Arreglo;

/**
 * Esta clase maneja arreglos y distintas operaciones con arreglos de tipo Double.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class Campesino {
    protected String nombre;
    protected Arreglo terrenos;

    /**
     * Crea un campesino con el nombre indicado y con un tamanio de 7.
     * @param nombre Es el nombre del campesino.
     */
    public Campesino(String nombre) {
        this.nombre = nombre;
        this.terrenos = new Arreglo(7);
    }

    /**
     * Agrega un numero de toneladas al Arreglo terrenos del Campesino.
     * @param toneladas Es el numero de toneladas a agregar.
     * @return Regresa la posicion donde se agregaron las toneladas.
     */
    public int agregar(Double toneladas) {
        return terrenos.agregar(toneladas);
    }

    /**
     * Obtiene el numero de toneladas producidas en la posicion dada.
     * @param indice Es la posicion de la que se obtendran las toneladas.
     * @return Regresa el numero de toneladas producidas por el campesino.
     */
    public Double obtener(int indice) {
        return (Double) terrenos.obtener(indice);
    }

    /**
     * Obtiene el numero de terrenos del campesino.
     * @return Regresa el numero de terrenos del campesino.
     */
    public int numElementos() {
        return terrenos.numElementos();
    }

    /**
     * Calcula el promedio de la suma de la produccion de los terrenos del campesino.
     * @return Regresa el promedio de la suma de la produccion de los terrenos del campesino.
     */
    public Double calcularPromedio() {
        Double promedio = 0.0;
        for (int i = 0; i < terrenos.numElementos(); i++) {
            promedio += (Double) terrenos.obtener(i);
        }
        return promedio/terrenos.longitud();
    }

    /**
     * Busca el terreno con menor produccion.
     * @return Regresa el nombre del terreno con menor produccion.
     */
    public String obtenerTerrenoMinimo() {
        Double minimo = (Double) terrenos.obtener(0);
        int menor = 0;
        for (int i = 0; i < terrenos.numElementos(); i++) {
            Double valor = (Double) terrenos.obtener(i);
            if (valor < minimo) {
                minimo = valor;
                menor = i;
            }
        }
        return obtenerTerreno(menor);
    }

    /**
     * Obtiene el nombre del terreno en el indice indicado.
     * @param indice El indice del terreno a seleccionar.
     * @return Regresa el nombre del terreno.
     */
    public String obtenerTerreno(int indice) {
        switch (indice) {
            case 0:
                return "Zacatecas";
            case 1:
                return "Fresnillo";
            case 2:
                return "Calera";
            case 3:
                return "Jerez";
            case 4:
                return "Morelos";
            case 5:
                return "Trancoso";
            case 6:
                return "OjoCaliente";
        }
        return "";
    }

    /**
     * Obtiene el nombre del campesino.
     * @return Regresa el nombre del campesino.
     */
    public String toString() {
        return nombre;
    }
}
