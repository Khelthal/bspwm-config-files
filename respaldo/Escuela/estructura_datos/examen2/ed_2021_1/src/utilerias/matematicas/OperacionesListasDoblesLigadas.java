package utilerias.matematicas;

import edlineal.ListaDobleLigada;

/**
 * Esta clase se encarga de realizar operaciones basicas con ListaDobleLigada.
 */
public class OperacionesListasDoblesLigadas {

  /**
   * Calcula la suma de todos los elementos de la ListaDobleLigada.
   * @param lista Es la lista de la que se obtendra la sumatoria.
   * @return Regresa la sumatoria calculada.
   */
  public static double calcularSumatoriaLista(ListaDobleLigada lista) {
    double suma = 0.0;
    lista.inicializarIteradorIzq();

    while (lista.hayMasIzq()) {
      suma += (double) lista.obtenerSigiuenteIzq();
    }

    return suma;
  }

  /**
   * Crea una nueva ListaDobleLigada con los valores del producto de dos listas.
   * @param lista1 Es la primera lista a multiplicar.
   * @param lista2 Es la segunda lista a multiplicar.
   * @return Regresa 
   */
  public static ListaDobleLigada multiplicarListas(ListaDobleLigada lista1, ListaDobleLigada lista2) {
    double valor;
    ListaDobleLigada resultado = new ListaDobleLigada();

    lista1.inicializarIteradorIzq();
    lista2.inicializarIteradorIzq();

    while (lista1.hayMasIzq() && lista2.hayMasIzq()) {
      valor = (double) lista1.obtenerSigiuenteIzq() * (double) lista2.obtenerSigiuenteIzq();
      resultado.agregar(valor);
    }

    return resultado;
  }
}
