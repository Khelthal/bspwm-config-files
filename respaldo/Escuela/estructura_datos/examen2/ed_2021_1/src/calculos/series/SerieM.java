package calculos.series;

import edlineal.ListaLigada;

/**
 * Esta clase se encarga de calcular el resultado de la serie m - x**1/2! + x**3/3! - x**5/5! + x**7/7! … x**n/m! + n/m! + n.
 */
public class SerieM {

  /**
   * Recibe los argumentos para calcular la serie.
   * @param x Es el valor de x.
   * @param m Es el vamor de m (debe ser primo).
   * @return Regresa el resultado de la serie.
   */
  public static double calcularSerieM(int x, int m) {
    if (!SeriePrimos.esPrimo(m)) {
      return 0;
    }
    ListaLigada primos = SeriePrimos.obtenerSeriePrimos(m);
    return m + obtenerSerieM(primos, 0, x, m, false);
  }

  /**
   * Calcula el resultado de la serie.
   * @param primos Es una lista con los numeros primos desde 2 hasta m.
   * @param indice Es el indice.
   * @param x Es el valor de x.
   * @param m Es el valor de m.
   * @param sumando Indica si el elemento actual de la serie debe ser positivo o negativo.
   * @return Regresa el resultado calculado.
   */
  private static double obtenerSerieM(ListaLigada primos, int indice, int x, int m, boolean sumando) {
    Integer primo = (Integer) primos.eliminarInicio();
    if (primo == null) {
      return 0;
    }
    else {
      int n = 1 + indice*2;
      double fact = factorial(primo);
      double valor = potencia(x, n);
      valor /= fact;
      if (!sumando) {
        valor *= -1;
      }
      if (primo == m) {
        valor += n/fact;
        valor += n;
      }
      return valor + obtenerSerieM(primos, indice+1, x, m, !sumando);
    }
  }

  /**
   * Calcula el factorial de un numero.
   * @param num Es el numero del que se desea obtener factorial.
   * @return Regresa el factorial calculado.
   */
  public static long factorial(int num) {
    if (num > 1) {
      return num * factorial(num-1);
    } else {
      return 1;
    }
  }

  /**
   * Calcula el resultado de un numero elevado a una potencia positiva.
   * @param num Es el numero a elevar.
   * @param potencia Es el exponente.
   * @return Regresa el numero elevado a la potencia especificada.
   */
  public static long potencia(int num, int potencia) {
    if (potencia == 0) {
      return 1;
    } else {
      return num * potencia(num, potencia-1);
    }
  }
}
