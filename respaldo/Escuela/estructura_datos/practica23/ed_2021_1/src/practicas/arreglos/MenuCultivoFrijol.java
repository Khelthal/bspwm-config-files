package practicas.arreglos;

import edlineal.Arreglo;
import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;

/**
 * Este es un menu para realizar operaciones con la clase Campesino.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class MenuCultivoFrijol {
    private Arreglo campesinos = new Arreglo(5);

    public int agregar(Campesino campesino) {
        return campesinos.agregar(campesino);
    }

    /**
     * Inicia el menu
     */
    public void iniciar() {
        while (true) {
            imprimirMenu();
            SalidaEstandar.consola("\n");
            seleccionarOpcion();
            SalidaEstandar.consola("Presiona enter para continuar\n");
            EntradaConsola.consolaCadena();
        }
    }

    /**
     * Imprime las opciones del menu.
     */
    public void imprimirMenu() {
        SalidaEstandar.consola("¿Que desea hacer?\n");
        SalidaEstandar.consola("a) Calcular el promedio de toneladas producidas por un campesino indicado en todas sus tierras.\n");
        SalidaEstandar.consola("b) Mostrar el campesino que produjo mas toneladas en promedio en todas sus tierras.\n");
        SalidaEstandar.consola("c) Mostrar el lugar con mas toneladas producidas.\n");
        SalidaEstandar.consola("d) Calcular que lugar le conviene a un campesino indicado dejar de cultivar.\n");
        SalidaEstandar.consola("e) Mostrar la suma de toneladas producidas en Zacatecas, Jerez y Calera.\n");
        SalidaEstandar.consola("f) Mostrar el lugar mas conveniente para cultivar el siguiente anio.\n");
        SalidaEstandar.consola("g) Mostrar el campesino que menos ha cosechado este anio.\n");
        SalidaEstandar.consola("h) Mostrar cuantas toneladas producen Juan, Pedro y Miguel en Zacatecas, Jerez y Calera.\n");
        SalidaEstandar.consola("q) Salir.\n");
    }

    /**
     * Recibe la entrada del usuario para elegir una de las opciones.
     */
    public void seleccionarOpcion() {
        String respuesta = EntradaConsola.consolaCadena();
        switch (respuesta) {
            case "a":
                mostrarPromedio(seleccionarCampesino());
                break;
            case "b":
                mostrarCampesinoMaximo();
                break;
            case "c":
                mostrarTerrenoMaximo();
                break;
            case "d":
                mostrarTerrenoMalo(seleccionarCampesino());
                break;
            case "e":
                mostrarToneladasZCJ();
                break;
            case "f":
                mostrarTerrenoMaximo();
                break;
            case "g":
                mostrarCampesinoMinimo();
                break;
            case "h":
                mostrarToneladasJPMZCJ();
                break;
            case "q":
                System.exit(0);
        }
    }

    /**
     * Permite al usuario seleccionar un objeto del arreglo campesinos.
     * @return Regresa el objeto seleccionado.
     */
    public Object seleccionarCampesino() {
        for (int i = 0; i < campesinos.numElementos(); i++) {
            SalidaEstandar.consola(i+": ");
            SalidaEstandar.consola(campesinos.obtener(i)+"\n");
        }
        SalidaEstandar.consola("Ingrese el indice del campesino a consultar:");
        SalidaEstandar.consola("\n");
        int indice = EntradaConsola.consolaInt();
        if (indice < 0 || indice >= campesinos.numElementos()) {
            indice = 0;
        }
        return campesinos.obtener(indice);
    }

    /**
     * Imprime el promedio de produccion del Campesino seleccionado.
     * @param campesino Es el Campesino del cual se mostrara el promedio.
     */
    public void mostrarPromedio(Object campesino) {
        Campesino evaluado = (Campesino) campesino;
        SalidaEstandar.consola("El promedio de " + evaluado + " es: " + evaluado.calcularPromedio()+"\n");
    }

    /**
     * Imprime el Campesino con mayor produccion.
     */
    public void mostrarCampesinoMaximo() {
        Campesino mayor = (Campesino) campesinos.obtener(0) ;
        Double maximo = mayor.calcularPromedio();
        for (int i = 1; i < campesinos.numElementos(); i++) {
            Campesino temporal = (Campesino) campesinos.obtener(i);
            if (temporal.calcularPromedio() > maximo) {
                mayor = temporal;
                maximo = mayor.calcularPromedio();
            }
        }
        SalidaEstandar.consola("El campesino con mayor produccion es: " + mayor +"\n");
    }

    /**
     * Imprime el Campesino con menor produccion.
     */
    public void mostrarCampesinoMinimo() {
        Campesino menor = (Campesino) campesinos.obtener(0) ;
        Double minimo = menor.calcularPromedio();
        for (int i = 1; i < campesinos.numElementos(); i++) {
            Campesino temporal = (Campesino) campesinos.obtener(i);
            if (temporal.calcularPromedio() < minimo) {
                menor = temporal;
                minimo = menor.calcularPromedio();
            }
        }
        SalidaEstandar.consola("El campesino con menor produccion es: " + menor +"\n");
    }

    /**
     * Muestra el terreno con mayor produccion.
     */
    public void mostrarTerrenoMaximo() {
        Campesino campesino = (Campesino) campesinos.obtener(0);
        Double maximo = campesino.obtener(0);
        int mayor = 0;
        for (int i = 0; i < campesino.numElementos(); i++) {
            Double temp = obtenerSumaTerreno(i);
            if (temp > maximo) {
                maximo = temp;
                mayor = i;
            }
        }
        SalidaEstandar.consola("El terreno con mayor produccion es: " + campesino.obtenerTerreno(mayor)+"\n");
    }

    /**
     * Muestra el terreno del Campesino con menor produccion.
     * @param campesino Es el campesino del cual se buscara su peor terreno.
     */
    public void mostrarTerrenoMalo(Object campesino) {
        Campesino evaluado = (Campesino) campesino;
        SalidaEstandar.consola("A " + campesino + " le conviene dejar de cultivar en: " + evaluado.obtenerTerrenoMinimo()+"\n");
    }

    /**
     * Obtiene el total de produccion de un terreno.
     * @param indice Es el indice para indicar el terreno a examinar.
     * @return Regresa el total de produccion del terreno seleccionado.
     */
    public Double obtenerSumaTerreno(int indice) {
        Campesino campesino = (Campesino) campesinos.obtener(0);
        Double temp = 0.0;
        for (int j = 0; j < campesinos.numElementos(); j++) {
            Campesino evaluado = (Campesino) campesinos.obtener(j);
            temp += evaluado.obtener(indice);
        }
        return temp;
    }

    /**
     * Muestra la suma de la produccion en Zacatecas, Calera y Jerez.
     */
    public void mostrarToneladasZCJ() {
        Double suma = 0.0;
        suma += obtenerSumaTerreno(0);
        suma += obtenerSumaTerreno(2);
        suma += obtenerSumaTerreno(3);
        SalidaEstandar.consola("La suma de toneladas producidas por Zacatecas, Calera y Jerez es: "+suma+"\n");
    }

    /**
     * Muestra la suma de la produccion de Juan, Pedro y Miguel en Zacatecas, Calera y Jerez.
     */
    public void mostrarToneladasJPMZCJ() {
        Campesino juan = (Campesino) campesinos.obtener(0);
        Campesino pedro = (Campesino) campesinos.obtener(2);
        Campesino miguel = (Campesino) campesinos.obtener(3);

        Arreglo zonas = new Arreglo(3);
        zonas.agregar(0);
        zonas.agregar(2);
        zonas.agregar(3);

        Double total = 0.0;

        for (int i = 0; i < zonas.numElementos(); i++) {
            total += juan.obtener((int) zonas.obtener(i));
            total += pedro.obtener((int) zonas.obtener(i));
            total += miguel.obtener((int) zonas.obtener(i));
        }

        SalidaEstandar.consola("El total de toneladas producidas por Juan, Pedro y Miguel en Zacatecas, Jerez y Calera es: " + total + "\n");
    }
}