package edlineal;

import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de almacenar datos numericos de forma ordenada.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class Monticulo {
    protected ArregloNumerico datos;
    protected TipoOrden orden;

    /**
     * Crea un nuevo monticulo con el tamanio y el tipo de orden indicados.
     * @param maxima Es el tamanio del monticulo.
     * @param orden Es el tipo de orden del monticulo.
     */
    public Monticulo(int maxima, TipoOrden orden) {
        datos = new ArregloNumerico(maxima+1);
        this.orden = orden;
    }

    /**
     * Agrega un elemento al monticulo.
     * @param info Es el elemento a ser agregado.
     * @return Regresa el indice donde se agrego el elemento.
     */
    public int agregar(Object info) {
        int indice = obtenerDisponible();
        if (indice < 0) {
            return -1;
        }
        else {
            indice = datos.agregar(info);
            if (indice <= 0) {
                return indice;
            }
        }
        int indicePadre = (indice+1)/2-1;
        return ordenarInsercion(indicePadre, indice);
    }

    /**
     * Elimina el primer elemento del monticulo.
     * @return Regresa la posicion actual del ultimo elemento del monticulo despues de ser reordenado.
     */
    public int eliminar() {
        if (datos.vacia()) {
            return -1;
        }
        datos.cambiar(0, datos.obtener(datos.numElementos()-1));
        datos.eliminar();
        return ordenarEliminacion(0);
    }

    /**
     * Reordena los elementos despues de eliminar.
     * @param indice Es el indice donde reordenara los datos.
     * @return
     */
    private int ordenarEliminacion(int indice) {
        int indiceIzq = (indice+1)*2-1;
        int indiceDer = (indice+1)*2;

        if (indiceIzq >= datos.numElementos()) {
            return indice;
        }
        else if (indiceDer >= datos.numElementos()) {
            double info = (Double) datos.obtener(indice);
            double infoIzq = (Double) datos.obtener(indiceIzq);
            if (orden == TipoOrden.CRECIENTE) {
                if (info < infoIzq) {
                    datos.cambiar(indice, infoIzq);
                    datos.cambiar(indiceIzq, info);
                    return indiceIzq;
                }
                return indice;
            }
            else {
                if (info > infoIzq) {
                    datos.cambiar(indice, infoIzq);
                    datos.cambiar(indiceIzq, info);
                    return indiceIzq;
                }
                return indice;
            }
        }
        else {
            double info = (Double) datos.obtener(indice);
            double infoIzq = (Double) datos.obtener(indiceIzq);
            double infoDer = (Double) datos.obtener(indiceDer);
            double infoMax;
            int indiceMax;
            if (orden == TipoOrden.CRECIENTE) {
                if (infoIzq >= infoDer) {
                    infoMax = infoIzq;
                    indiceMax = indiceIzq;
                }
                else {
                    infoMax = infoDer;
                    indiceMax = indiceDer;
                }
                if (info < infoMax) {
                    datos.cambiar(indice, infoMax);
                    datos.cambiar(indiceMax, info);
                    return ordenarEliminacion(indiceMax);
                }
                return indice;
            }
            else {
                if (infoIzq <= infoDer) {
                    infoMax = infoIzq;
                    indiceMax = indiceIzq;
                }
                else {
                    infoMax = infoDer;
                    indiceMax = indiceDer;
                }
                if (info > infoMax) {
                    datos.cambiar(indice, infoMax);
                    datos.cambiar(indiceMax, info);
                    return ordenarEliminacion(indiceMax);
                }
                return indice;
            }
        }
    }

    /**
     * Obtiene la primera posicion disponible en el arreglo.
     * @return Regresa el indice de la primera posicion disponible.
     */
    public int obtenerDisponible() {
        if (datos.lleno()) {
            return -1;
        }
        int indice = datos.numElementos();
        return indice;
    }

    /**
     * Ordena el monticulo despues de insertar un elemento.
     * @param indicePadre Es el indice del padre del elemento insertado.
     * @param indice Es el indice del elemento insertado.
     * @return Regresa el indice donde quedo colocado el elemento insertado.
     */
    private int ordenarInsercion(int indicePadre, int indice) {
        double info = (Double) datos.obtener(indice);
        double infoPadre = (Double) datos.obtener(indicePadre);

        if (orden == TipoOrden.CRECIENTE) {
            if (infoPadre < info) {
                datos.cambiar(indicePadre, info);
                datos.cambiar(indice, infoPadre);
            }
            else {
                return indice;
            }
        }
        else {
            if (infoPadre > info) {
                datos.cambiar(indicePadre, info);
                datos.cambiar(indice, infoPadre);
            }
            else {
                return indice;
            }
        }
        indice = indicePadre;
        if (indice == 0) {
            return indice;
        }

        indicePadre = (indice+1)/2-1;
        info = (Double) datos.obtener(indice);
        infoPadre = (Double) datos.obtener(indicePadre);
        if (orden == TipoOrden.CRECIENTE) {
            if (infoPadre < info) {
                return ordenarInsercion(indicePadre, indice);
            }
            else {
                return indice;
            }
        }
        else {
            if (infoPadre > info) {
                return ordenarInsercion(indicePadre, indice);
            }
            else {
                return indice;
            }
        }
    }

    /**
     * Determina si el monticulo esta vacio.
     * @return Regresa <b>true</b> si el monticulo esta vacio, <b>false</b> en caso contrario.
     */
    public boolean vacio() {
        return datos.vacia();
    }

    /**
     * Imprime los datos del monticulo.
     */
    public void imprimir() {
        datos.imprimir();
    }

    /**
     * Imprime los datos del monticulo en orden inverso.
     */
    public void imprimirOI() {
        datos.imprimirOI();
    }

    /**
     * Obtiene el siguiente elemento del monticulo.
     * @return Regresa el siguiente elemento del monticulo.
     */
    public Object obtener() {
        if (datos.vacia()) {
            return null;
        }
        return datos.obtener(0);
    }
}
