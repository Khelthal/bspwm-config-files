package edlineal;
import entradasalida.SalidaEstandar;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Esta clase maneja arreglos de tipo numerico y distintas operaciones con arreglos numericos.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class ArregloNumerico extends Arreglo {
    public ArregloNumerico(int maxima) {
        super(maxima);
    }

    @Override
    public int agregar(Object info){
        if (info instanceof Number) {
            info = numberToDouble((Number) info);
            return super.agregar(info);
        }
        return -1;
    }

    @Override
    public Object buscar(Object info){
        if (info instanceof Double) {
            info = numberToDouble((Number) info);
            return super.buscar(info);
        }
        return null;
    }

    @Override
    public Object eliminar(Object info){
        if (info instanceof Number) {
            info = numberToDouble((Number) info);
            return super.eliminar(info);
        }
        return null;
    }

    @Override
    public Number obtener(int indice) {
        Number n = (Number) super.obtener(indice);
        if (n == null) {
            return n;
        }
        n = numberToDouble(n);
        return n;
    }

    @Override
    public boolean cambiar(Object infoVieja, Object infoNueva, int numOcurrencias) {
        if (infoVieja instanceof  Number && infoNueva instanceof Number) {
            infoVieja = numberToDouble((Number) infoVieja);
            infoNueva = numberToDouble((Number) infoNueva);
            return super.cambiar(infoVieja, infoNueva, numOcurrencias);
        }
        return false;
    }

    @Override
    public boolean cambiar(int indice, Object info) {
        if (info instanceof Number) {
            info = info = numberToDouble((Number) info);
            return super.cambiar(indice, info);
        }
        return false;
    }

    @Override
    public Arreglo buscarValores(Object info) {
        if (info instanceof Number) {
            info = numberToDouble((Number) info);
            return super.buscarValores(info);
        }
        return null;
    }

    @Override
    public Number eliminar(int indice){
        return (Number) super.eliminar(indice);
    }

    @Override
    public Number eliminar() {
        return eliminar(limite);
    }

    @Override
    public boolean agregarLista(Object lista2) {
        if (!(lista2 instanceof ArregloNumerico)) {
            return false;
        }
        return super.agregarLista(lista2);
    }

    @Override
    public int contar(Object info) {
        if (info instanceof Number) {
            info = numberToDouble((Number) info);
            return super.contar(info);
        }
        return 0;
    }

    @Override
    public boolean eliminarLista(Object lista2) {
        if (!(lista2 instanceof ArregloNumerico)) {
            return false;
        }
        return super.eliminarLista(lista2);
    }

    @Override
    public void rellenar(Object info, int cantidad) {
        if (info instanceof Number) {
            info = numberToDouble((Number) info);
            super.rellenar(info, cantidad);
        }
    }

    @Override
    public void rellenar(Object info) {
        if (info instanceof Number) {
            info = numberToDouble((Number) info);
            super.rellenar(info);
        }
    }

    @Override
    public boolean insertar(int indice, Object info) {
        if (info instanceof Number) {
            info = numberToDouble((Number) info);
            return super.insertar(indice, info);
        }
        return false;
    }

    @Override
    public boolean copiarArreglo(Arreglo arreglo2) {
        if (arreglo2 instanceof ArregloNumerico) {
            return super.copiarArreglo(arreglo2);
        }
        return false;
    }

    /**
     * Convierte enteros y flotantes a Double.
     * @param n Es el numero a ser convertido.
     * @return Regresa el numero convertido a Double.
     */
    public Number numberToDouble(Number n) {
        if (n instanceof Byte) {
            byte pivote = (byte) n;
            Double nuevo = (double) pivote;
            n = nuevo;
        }
        else if (n instanceof Integer) {
            int pivote = (int) n;
            Double nuevo = (double) pivote;
            n = nuevo;
        }
        else if (n instanceof Float) {
            float pivote = (float) n;
            Double nuevo = (double) pivote;
            n = nuevo;
        }
        return n;
    }

    /**
     * Multiplica todos los valores del arreglo por el escalar dado.
     * @param escalar Es el escalar por el que se multiplicara el arreglo.
     */
    public void porEscalar(Number escalar) {
        double num = (double) numberToDouble(escalar);
        for (int i = 0; i < numElementos(); i++) {
            cambiar(i, (double) obtener(i)*num);
        }
    }

    /**
     * Suma a todos los valores del arreglo el escalar dado.
     * @param escalar Es el escalar que se sumara al arreglo.
     */
    public void sumaEscalar(Number escalar) {
        double num = (double) numberToDouble(escalar);
        for (int i = 0; i < numElementos(); i++) {
            cambiar(i, (double)obtener(i)+num);
        }
    }

    /**
     * Suma los valores de arreglo2 al arreglo actual.
     * @param arreglo2 Es el arreglo del cual se tomaran los valores para sumar.
     */
    public void sumar(ArregloNumerico arreglo2) {
        if (numElementos() != arreglo2.numElementos()) {
            return;
        }
        for (int i = 0; i < arreglo2.numElementos(); i++) {
            cambiar(i, (double) obtener(i)+(double) arreglo2.obtener(i));
        }
    }

    /**
     * Multiplica los valores del arreglo actual por los valores de arreglo2.
     * @param arreglo2 Es el arreglo del cual se tomaran los valores para multiplicar.
     */
    public void multiplicar(ArregloNumerico arreglo2) {
        if (numElementos() != arreglo2.numElementos()) {
            return;
        }
        for (int i = 0; i < arreglo2.numElementos(); i++) {
            cambiar(i, (double) obtener(i)* (double) arreglo2.obtener(i));
        }
    }

    /**
     * Eleva todos los valores del arreglo a la potencia escalar.
     * @param escalar Es la potencia a la cual se elevaran los valores del arreglo.
     */
    public void potencia(Number escalar) {
        double num = (double) numberToDouble(escalar);
        for (int i = 0; i < numElementos(); i++) {
            cambiar(i, pow((double) obtener(i), num));
        }
    }

    /**
     * Calcula el producto punto entre el arreglo actual y arreglo2.
     * @param arreglo2 Es el arreglo con el cual se aplicara el producto punto.
     * @return Regresa el resultado del producto punto.
     */
    public double productoPunto(ArregloNumerico arreglo2) {
        double productoPunto = 0.0;
        for (int i = 0; i < numElementos(); i++) {
            double x1 = (double) obtener(i);
            double x2 = (double) arreglo2.obtener(i);
            productoPunto += x1*x2;
        }
        return productoPunto;
    }

    /**
     * Calcula la magnitud del arreglo.
     * @return Regresa la magnitud del arreglo.
     */
    public double magnitud() {
        Double magnitud = 0.0;
        for (int i = 0; i < numElementos(); i++) {
            magnitud += pow((double) obtener(i),2);
        }
        return sqrt(magnitud);
    }

    /**
     * Calcula la norma euclidianta de el arreglo actual con arreglo2.
     * @param arreglo2 Es el arreglo con el cual se aplicara la norma euclidiana.
     * @return Regresa el resultado de la norma euclidiana.
     */
    public double normaEuclidiana(ArregloNumerico arreglo2) {
        if (numElementos() != arreglo2.numElementos()) {
            return -1.0;
        }
        Double normaEuclidiana = 0.0;
        for (int i = 0; i < numElementos(); i++) {
            double a = (Double) obtener(i);
            double b = (Double) arreglo2.obtener(i);
            normaEuclidiana += pow(b-a, 2);
        }
        return sqrt(normaEuclidiana);
    }

    /**
     * Calcula la suma de los elementos del arreglo mas la suma de todos los elementos de los
     * arreglos conteindos en arreglos.
     * @param arreglos Es el arreglo que contiene los arreglos que seran sumados.
     * @return Regresa la suma de los elementos de los arreglos.
     */
    public double sumarArreglos(Arreglo arreglos) {
        double suma = 0;
        for (int i = 0; i < numElementos(); i++) {
            suma += (double) obtener(i);
        }
        for (int i = 0; i < arreglos.numElementos(); i++) {
            ArregloNumerico arregloNumerico = (ArregloNumerico) arreglos.obtener(i);
            for (int v = 0; v < arregloNumerico.numElementos(); v++) {
                suma += (double) arregloNumerico.obtener(v);
            }
        }
        return suma;
    }

    /**
     * Calcula la suma de los elementos del arreglo mas la suma de los elementos del
     * arreglo escalares
     * @param escalares Es el arreglo que se sumara.
     * @return Regres la suma de los elementos de los arreglos.
     */
    public double sumarEscalares(ArregloNumerico escalares) {
        double suma = 0.0;
        for (int i = 0; i < numElementos(); i++) {
            suma += (double) obtener(i);
        }

        for (int i = 0; i < escalares.numElementos(); i++) {
            suma += (double) escalares.obtener(i);
        }
        return suma;
    }

    /**
     * Regresa la suma de los elementos en las posiciones contenidas en el arreglo arregloIndices.
     * @param arregloIndices Es el arreglo que contiene las posiciones de los elementos a sumar.
     * @return Regresa la suma de los elementos en los indices indicados.
     */
    public double sumarIndices(ArregloNumerico arregloIndices) {
        double suma = 0.0;
        for (int i = 0; i < arregloIndices.numElementos(); i++) {
            double encontrado = (double) arregloIndices.obtener(i);
            int indice = (int) encontrado;
            Number n = obtener(indice);
            if (n == null) {
                continue;
            }
            suma += (double) n;
        }
        return suma;
    }

    /**
     * Cambia el contenido de datos por el contenido del arreglo.
     * @param arreglo Es el arreglo que contiene los nuevos datos.
     */
    public void recibirArreglo(double[] arreglo) {
        vaciar();
        redimensionar(arreglo.length);
        for (int i = 0; i < arreglo.length; i++) {
            agregar(arreglo[i]);
        }
    }
}
