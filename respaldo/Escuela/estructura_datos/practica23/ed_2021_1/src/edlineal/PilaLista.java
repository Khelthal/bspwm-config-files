package edlineal;

import edlineal.catalogo.Nodo;

/**
 * Esta clase maneja pilas y distintas operaciones con pilas.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class PilaLista implements Lote {
    protected ListaLigada pila;

    /**
     * Crea una nueva instancia de pila.
     */
    public PilaLista(){
        pila=new ListaLigada();
    }

    @Override
    public boolean vacio(){
        return pila.vacia();
    }

    @Override
    public boolean lleno(){
        Nodo nodo = new Nodo("x");
        return (nodo == null);
    }

    @Override
    public boolean poner(Object info){
        int retorno=pila.agregar(info);
        if(retorno==-1){
            return false;
        }else{
            return true;
        }
    }

    @Override
    public Object quitar(){
        return pila.eliminar();
    }

    @Override
    public void imprimir(){
        pila.imprimir();
    }

    @Override
    public Object verLimite(){
        if (pila.vacia()) {
            return null;
        }
        else {
            return pila.getUltimo();
        }
    }
}
