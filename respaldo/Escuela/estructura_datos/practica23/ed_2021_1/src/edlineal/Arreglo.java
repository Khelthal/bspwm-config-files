package edlineal;

import comparadores.ComparadorObject;
import ednolineal.Matriz2D;
import entradasalida.SalidaEstandar;

/**
 * Esta clase maneja arreglos y distintas operaciones con arreglos.
 * 
 * @author Clase de Estructura de Datos.
 * @version 2.0
 */
public class Arreglo implements VectorDatos {
  protected Object datos[];
  protected int MAXIMO;
  protected int limite;

  /**
   * Crea un nuevo arreglo con el tamanio maximo indicado.
   * 
   * @param maxima Es el tamanio maximo del arreglo.
   */
  public Arreglo(int maxima) {
    MAXIMO = maxima;
    datos = new Object[MAXIMO];
    limite = -1;
  }

  @Override
  public boolean vacia() {
    if (limite == -1) {
      return true;
    } else {
      return false;
    }
  }

  @Override
  public boolean lleno() {
    if (limite == (MAXIMO - 1)) {
      return true;
    } else {
      return false;
    }
  }

  @Override
  public int agregar(Object info) {
    if (lleno() == false) {
      limite = limite + 1;
      datos[limite] = info;
      return limite;
    } else {
      return -1;
    }
  }

  @Override
  public void imprimir() {
    for (int posicion = limite; posicion >= 0; posicion--) {
      SalidaEstandar.consola(datos[posicion] + "\n");
    }
  }

  /**
   * Imprime el contenido de la lista en orden natural de forma recursiva.
   */
  public void imprimirRR() {
    imprimirRR(limite);
  }

  /**
   * Imprime el contenido de la lista en orden natural de forma recursiva.
   * 
   * @param pos Es la posicion a imprimir actual.
   */
  private void imprimirRR(int pos) {
    if (pos >= 0) {
      SalidaEstandar.consola(datos[pos] + "\n");
      imprimirRR(pos - 1);
    }
  }

  @Override
  public void imprimirOI() {
    for (int posicion = 0; posicion <= limite; posicion++) {
      SalidaEstandar.consola(datos[posicion] + "\n");
    }
  }

  /**
   * Imprime el contenido de la lista en orden inverso de forma recursiva.
   */
  public void imprimirOIRR() {
    imprimirOIRR(0);
  }

  /**
   * Imprime el contenido de la lista en orden inverso de forma recursiva.
   * 
   * @param pos Es la posicion a imprimir actual.
   */
  private void imprimirOIRR(int pos) {
    if (pos <= limite) {
      SalidaEstandar.consola(datos[pos] + "\n");
      imprimirOIRR(pos + 1);
    }
  }

  @Override
  public Object buscar(Object info) {
    int localidad = 0;
    while (localidad <= limite && !info.toString().equalsIgnoreCase(datos[localidad].toString())) {
      localidad++;
    }
    if (localidad > limite) {
      return null;
    } else {
      return localidad;
    }
  }

  /**
   * Busca el objeto indicado en la lista de forma recursiva.
   * 
   * @param info Es el objeto a buscar.
   * @return Regresa el objeto buscado si se encuentra en la lista, null en caso
   *         contrario.
   */
  public Object buscarRR(Object info) {
    return buscarRR(0, info);
  }

  /**
   * Busca el objeto indicado en la lista de forma recursiva.
   * 
   * @param Es   el indice actual.
   * @param info Es el objeto a buscar.
   * @return Regresa el objeto buscado si se encuentra en la lista, null en caso
   *         contrario.
   */
  private Object buscarRR(int pos, Object info) {
    if (pos <= limite) {
      if (ComparadorObject.compare(datos[pos], info) == 0) {
        return pos;
      } else {
        return buscarRR(pos + 1, info);
      }
    } else {
      return null;
    }
  }

  @Override
  public Object eliminar(Object info) {
    Integer posicion = (Integer) buscar(info);
    if (posicion != null) { // si lo encontro
      Object elementoBorrado = datos[posicion];
      limite = limite - 1;
      for (int mov = posicion; mov <= limite; mov++) {
        datos[mov] = datos[mov + 1];
      }
      return elementoBorrado;
    } else { // no lo encontroo
      return null;
    }
  }

  /**
   * Elimina un objeto de la lista de forma recursiva.
   * 
   * @param info Es el objeto a eliminar.
   * @return Regresa el objeto eliminado si se encontraba en la lista, null en
   *         caso contrario.
   */
  public Object eliminarRR(Object info) {
    Integer posicion = (Integer) buscarRR(info);
    if (posicion == null) {
      return null;
    }
    Object eliminado = datos[posicion];
    limite--;
    desplazarRR((int) posicion);
    return eliminado;
  }

  /**
   * Elimina un objeto de la lista de forma recursiva.
   * 
   * @param pos Es la posicion desde donde se empezaran a recorrer los elementos
   *            siguientes.
   */
  private void desplazarRR(int pos) {
    if (pos <= limite) {
      datos[pos] = datos[pos + 1];
      desplazarRR(pos + 1);
    }
  }

  @Override
  public int longitud() {
    return MAXIMO;
  }

  @Override
  public int numElementos() {
    return limite + 1;
  }

  @Override
  public boolean esIgual(Object lista2) {
    if (!(lista2 instanceof Arreglo)) {
      return false;
    }

    Arreglo lista = (Arreglo) lista2;
    if (MAXIMO != lista.longitud() || limite != lista.numElementos() - 1) {
      return false;
    }

    for (int i = 0; i <= limite; i++) {
      if (!(obtener(i).toString().equalsIgnoreCase(lista.obtener(i).toString()))) {
        return false;
      }
    }
    return true;
  }

  /**
   * Compara la lista con otra lista de forma recursiva.
   * 
   * @param lista2 Es la lista con la que se va a comparar.
   * @return Regresa <b>true</b> si las listas son iguales, <b>false</b> en caso
   *         contrario.
   */
  public boolean esIgualRR(Object lista2) {
    if (!(lista2 instanceof Arreglo)) {
      return false;
    }

    Arreglo lista = (Arreglo) lista2;
    if (MAXIMO != lista.longitud() || limite != lista.numElementos() - 1) {
      return false;
    }

    return esIgualRR(0, lista);
  }

  /**
   * Compara la lista con otra lista de forma recursiva.
   * 
   * @param pos    Es la posicion a comparar actual.
   * @param lista2 Es la lista con la que se va a comparar.
   * @return Regresa <b>true</b> si las listas son iguales, <b>false</b> en caso
   *         contrario.
   */
  private boolean esIgualRR(int pos, Arreglo lista2) {
    if (pos <= limite) {
      if (ComparadorObject.compare(datos[pos], lista2.obtener(pos)) != 0) {
        return false;
      } else {
        return esIgualRR(pos + 1, lista2);
      }
    } else {
      return true;
    }
  }

  @Override
  public Object obtener(int indice) {
    if (indice > limite || indice < 0) {
      return null;
    }

    return datos[indice];
  }

  @Override
  public boolean cambiar(Object infoVieja, Object infoNueva, int numOcurrencias) {
    boolean cambio = false;
    for (int i = 0; i <= limite; i++) {
      if (numOcurrencias == 0) {
        break;
      }
      if (obtener(i).toString().equalsIgnoreCase(infoVieja.toString())) {
        cambio = cambiar(i, infoNueva);
        numOcurrencias--;
      }
    }
    return cambio;
  }

  /**
   * Reemplaza en la lista n objetos iguales a un objeto indicado con un nuevo
   * objeto de forma recursiva.
   * 
   * @param infoVieja      Es el objeto a reemplazar.
   * @param infoNueva      Es el objeto que va a reemplazar al anterior.
   * @param numOcurrencias Es el numero de veces que se reemplazara el objeto.
   * @return Regresa <b>true</b> si se cambio al menos un valor, <b>false</b> en
   *         caso contrario.
   */
  public boolean cambiarRR(Object infoVieja, Object infoNueva, int numOcurrencias) {
    return cambiarRR(0, infoVieja, infoNueva, numOcurrencias) != numOcurrencias;
  }

  /**
   * Reemplaza en la lista n objetos iguales a un objeto indicado con un nuevo
   * objeto de forma recursiva.
   * 
   * @param pos            Es la posicion actual.
   * @param infoVieja      Es el objeto a reemplazar.
   * @param infoNueva      Es el objeto que va a reemplazar al anterior.
   * @param numOcurrencias Es el numero de veces que se reemplazara el objeto.
   * @return Regresa el numero de ocurrencias final.
   */
  private int cambiarRR(int pos, Object infoVieja, Object infoNueva, int numOcurrencias) {
    if (pos <= limite && numOcurrencias > 0) {
      if (ComparadorObject.compare(datos[pos], infoVieja) == 0) {
        datos[pos] = infoNueva;
        numOcurrencias--;
      }
      return cambiarRR(pos + 1, infoVieja, infoNueva, numOcurrencias);
    } else {
      return numOcurrencias;
    }
  }

  @Override
  public boolean cambiar(int indice, Object info) {
    if (indice > limite || indice < 0) {
      return false;
    }
    datos[indice] = info;
    return true;
  }

  @Override
  public Arreglo buscarValores(Object info) {
    Arreglo lista = new Arreglo(contar(info));
    if (lista.longitud() == 0) {
      return null;
    }
    for (int i = 0; i <= limite; i++) {
      if (obtener(i).toString().equalsIgnoreCase(info.toString())) {
        lista.agregar(i);
      }
    }

    return lista;
  }

  /**
   * Busca las posiciones donde esta ubicado un objeto en la lista de forma
   * recursiva.
   * 
   * @param info Es el objeto a buscar.
   * @return Regresa un objeto de tipo Arreglo con las posiciones si se encontro
   *         al menos una vez el objeto, <b>null</b> en caso contrario.
   */
  public Arreglo buscarValoresRR(Object info) {
    Arreglo lista = new Arreglo(contarRR(info));
    if (lista.longitud() == 0) {
      return null;
    }
    buscarValoresRR(0, info, lista);
    return lista;
  }

  /**
   * Busca las posiciones donde esta ubicado un objeto en la lista de forma
   * recursiva.
   * 
   * @param pos     Es la posicion a revisar.
   * @param info    Es el objeto a buscar.
   * @param almacen Es un arreglo donde se almacenan las posiciones encontradas.
   */
  private void buscarValoresRR(int pos, Object info, Arreglo almacen) {
    if (pos <= limite) {
      if (ComparadorObject.compare(datos[pos], info) == 0) {
        almacen.agregar(pos);
      }
      buscarValoresRR(pos + 1, info, almacen);
    }
  }

  @Override
  public Object eliminar(int indice) {
    if (indice > limite || indice < 0) {
      return null;
    }
    Object elementoBorrado = obtener(indice);
    for (int mov = indice; mov < limite; mov++) {
      datos[mov] = datos[mov + 1];
    }
    limite = limite - 1;
    return elementoBorrado;
  }

  /**
   * Elimina el Objeto en la posicion indicada.
   * 
   * @param indice Es la posicion donde se borrara el Objeto.
   * @return Regresa el objeto eliminado si pudo borrar alguno, <b>null</b> en
   *         caso contrario.
   */
  public Object eliminarRR(int indice) {
    if (indice > limite || indice < 0) {
      return null;
    }
    Object elementoBorrado = obtener(indice);
    limite--;
    desplazarRR(indice);
    return elementoBorrado;
  }

  @Override
  public Object eliminar() {
    return eliminar(limite);
  }

  /**
   * Elimina el ultimo elemento de la lista por merodo recursivo.
   * 
   * @return Regresa el elemento borrado, si la lista estaba vacia regresa
   *         <b>null</b>.
   */
  public Object eliminarRR() {
    return eliminarRR(limite);
  }

  @Override
  public void vaciar() {
    limite = -1;
  }

  @Override
  public boolean agregarLista(Object lista2) {
    if (!(lista2 instanceof Arreglo)) {
      return false;
    }
    Arreglo lista = (Arreglo) lista2;

    if (MAXIMO - (limite + 1) < lista.longitud()) {
      return false;
    }

    for (int i = 0; i < lista.numElementos(); i++) {
      agregar(lista.obtener(i));
    }

    return true;
  }

  /**
   * Inserta los elementos de otra lista al final de la lista de forma recursiva.
   * 
   * @param lista2 Es la lista a insertar.
   * @return Regresa <b>true</b> si lista2 es una lista valida, <b>false</b> en
   *         caso contrario.
   */
  public boolean agregarListaRR(Object lista2) {
    if (!(lista2 instanceof Arreglo)) {
      return false;
    }
    Arreglo lista = (Arreglo) lista2;

    if (MAXIMO - (limite + 1) < lista.longitud()) {
      return false;
    }
    agregarListaRR(0, lista);
    return true;
  }

  /**
   * Inserta los elementos de otra lista al final de la lista de forma recursiva.
   * 
   * @param pos    Es la posicion actual.
   * @param lista2 Es la lista a insertar.
   */
  private void agregarListaRR(int pos, Arreglo lista2) {
    if (pos < lista2.numElementos()) {
      agregar(lista2.obtener(pos));
      agregarListaRR(pos + 1, lista2);
    }
  }

  @Override
  public void invertir() {
    Arreglo copia = (Arreglo) clonar();
    vaciar();
    for (int i = copia.numElementos() - 1; i >= 0; i--) {
      agregar(copia.obtener(i));
    }
  }

  /**
   * Reordena los elementos de la lista en orden inverso al original.
   */
  public void invertirRR() {
    Arreglo copia = (Arreglo) clonarRR();
    vaciar();
    invertirRR(copia.numElementos() - 1, copia);
  }

  /**
   * Reordena los elementos de la lista en orden inverso al original.
   * 
   * @param pos   Es la posicion actual.
   * @param copia Es el arreglo de donde se extraeran los datos.
   */
  private void invertirRR(int pos, Arreglo copia) {
    if (pos >= 0) {
      agregar(copia.obtener(pos));
    }
  }

  @Override
  public int contar(Object info) {
    int conteoElementos = 0;

    for (int i = 0; i <= limite; i++) {
      if (obtener(i).toString().equalsIgnoreCase(info.toString())) {
        conteoElementos++;
      }
    }

    return conteoElementos;
  }

  /**
   * Cuenta el numero de veces que aparece en la lista el objeto indicado de forma
   * recursiva.
   * 
   * @param info Es el objeto buscado.
   * @return Regresa el numero de veces que aparece en la lista el objeto
   *         indicado.
   */
  public int contarRR(Object info) {
    return contarRR(0, info);
  }

  /**
   * Cuenta el numero de veces que aparece en la lista el objeto indicado de forma
   * recursiva.
   * 
   * @param pos  Es la posicion actual a revisar.
   * @param info Es el objeto buscado.
   * @return Regresa el numero de veces que aparece en la lista el objeto
   *         indicado.
   */
  private int contarRR(int pos, Object info) {
    if (pos <= limite) {
      if (ComparadorObject.compare(datos[pos], info) == 0) {
        return 1 + contarRR(pos + 1, info);
      } else {
        return 0 + contarRR(pos + 1, info);
      }
    } else {
      return 0;
    }
  }

  @Override
  public boolean eliminarLista(Object lista2) {
    if (!(lista2 instanceof Arreglo)) {
      return false;
    }

    Arreglo lista = (Arreglo) lista2;

    for (int i = 0; i < lista.numElementos(); i++) {
      Arreglo posiciones = buscarValores(lista.obtener(i));
      if (posiciones == null) {
        continue;
      }
      int desplazamientos = 0;
      for (int j = 0; j < posiciones.numElementos(); j++) {
        int eliminar = (Integer) posiciones.obtener(j);
        eliminar -= desplazamientos;
        eliminar(eliminar);
        desplazamientos++;
      }
    }

    return true;
  }

  /**
   * Elimina de la lista todos los elementos en comun con la lista argumento.
   * @param lista2 Es la lista de la cual se borraran los elementos.
   * @return Regresa <b>true</b> si lista2 es una lista valida.
   */
  public boolean eliminarListaRR(Object lista2) {
    if (!(lista2 instanceof Arreglo)) {
      return false;
    }
    int tamanio = limite;
    Arreglo lista = (Arreglo) lista2;
    eliminarListaRR(0, lista);
    return limite != tamanio;
  }

  /**
   * Elimina de la lista todos los elementos en comun con la lista argumento.
   * @param pos Es la posicion actual.
   * @param lista2 Es la lista de la cual se borraran los elementos.
   */
  private void eliminarListaRR(int pos, Arreglo lista2) {
    if (pos < lista2.numElementos()) {
      eliminarRR(lista2.obtener(pos));
      eliminarListaRR(pos + 1, lista2);
    }
  }

  @Override
  public void rellenar(Object info, int cantidad) {
    for (int i = 0; i < cantidad; i++) {
      agregar(info);
    }
  }

  /**
   * Inserta el objeto indicado el numero de veces indicado de forma recursiva.
   * 
   * @param info     Es el objeto a ser insertado.
   * @param cantidad Es el numero de veces que <b>info</b> sera insertado.
   */
  public void rellenarRR(Object info, int cantidad) {
    if (cantidad > 0) {
      agregar(info);
      rellenarRR(info, cantidad - 1);
    }
  }

  @Override
  public void rellenar(Object info) {
    vaciar();
    if (info instanceof Integer) {
      int num = (Integer) info;
      if (num > 0) {
        for (int i = 0; i < num; i++) {
          agregar(i + 1);
        }
      } else {
        for (int i = 0; i > num; i--) {
          agregar(i - 1);
        }
      }
    } else {
      if (info instanceof Character) {
        char letra = (Character) info;
        for (char i = 'A'; i <= letra; i++) {
          agregar(i);
        }
      } else {
        agregar(info);
      }
    }
  }

  /**
   * Si el parametro pasado es un numero positivo, rellena la lista con numeros
   * desde 1 hasta el numero (ascendente), si es negativo rellena el arreglo con
   * numeros desde -1 hasta el numero (descendente). Si el parametro es un char,
   * rellena el arreglo con char desde A hasta el char. Si es cualquier otro
   * objeto, solo lo inserta una vez (proceso recursivo).
   * 
   * @param info Es el objeto para rellenar la lista.
   */
  public void rellenarRR(Object info) {
    vaciar();
    if (info instanceof Integer) {
      int num = (Integer) info;
      rellenarInt(0, num);
    } else {
      if (info instanceof Character) {
        char letra = (Character) info;
        rellenarChar(0, letra);
      } else {
        agregar(info);
      }
    }
  }

  /**
   * Agrega chars desde A hasta c.
   * 
   * @param pos Es la posicion actual.
   * @param c   Es el ultimo char a agregar.
   */
  private void rellenarChar(int pos, char c) {
    if ('A' + pos <= c) {
      agregar((char) ('A' + pos));
      rellenarChar(pos + 1, c);
    }
  }

  /**
   * Agrega enteros desde cero hasta i;
   * 
   * @param pos Es la posicion actual.
   * @param i   Es el ultimo numero del rango a agregar.
   */
  private void rellenarInt(int pos, int i) {
    int abs = (i >= 0) ? i : -i;
    if (pos < abs) {
      if (i >= 0) {
        agregar(pos + 1);
      } else {
        agregar(-(pos + 1));
      }
      rellenarInt(pos + 1, i);
    }
  }

  @Override
  public Object clonar() {
    Arreglo copia = new Arreglo(MAXIMO);
    for (int i = 0; i < numElementos(); i++) {
      copia.agregar(obtener(i));
    }
    return copia;
  }

  /**
   * Crea una copia de la lista de forma recursiva.
   * 
   * @return Regresa una copia de la lista.
   */
  public Object clonarRR() {
    Arreglo copia = new Arreglo(MAXIMO);
    clonarRR(0, copia);
    return copia;
  }

  /**
   * Crea una copia de la lista de forma recursiva.
   * 
   * @param pos   Es la posicion actual.
   * @param copia Es la copia donde se almacenaran los datos del arreglo.
   */
  private void clonarRR(int pos, Arreglo copia) {
    if (pos <= limite) {
      copia.agregar(datos[pos]);
      clonarRR(pos + 1, copia);
    }
  }

  @Override
  public Object subLista(int indiceInicial, int indiceFinal) {
    if (indiceInicial < 0 || indiceFinal > limite) {
      return null;
    }
    if (indiceInicial > indiceFinal) {
      return null;
    }
    Arreglo sublista = new Arreglo(indiceFinal - indiceInicial + 1);

    for (int i = indiceInicial; i <= indiceFinal; i++) {
      sublista.agregar(obtener(i));
    }

    return sublista;
  }

  /**
   * Crea una sublista del rango indicado de forma recursiva.
   * 
   * @param indiceInicial Es el punto donde inicia la sublista.
   * @param indiceFinal   Es el punto donde termina la sublista.
   * @return Regresa una sublista si el rango es valido, <b>null</b> en caso
   *         contrario.
   */
  public Object subListaRR(int indiceInicial, int indiceFinal) {
    if (indiceInicial < 0 || indiceFinal > limite) {
      return null;
    }
    if (indiceInicial > indiceFinal) {
      return null;
    }
    Arreglo sublista = new Arreglo(indiceFinal - indiceInicial + 1);
    subListaRR(indiceInicial, indiceFinal, sublista);
    return sublista;
  }

  /**
   * Crea una sublista del rango indicado de forma recursiva.
   * 
   * @param indiceInicial Es el punto donde inicia la sublista.
   * @param indiceFinal   Es el punto donde termina la sublista.
   * @param sublista      Es un arreglo donde se guarda el segmento de la lista.
   */
  private void subListaRR(int indiceInicial, int indiceFinal, Arreglo sublista) {
    if (indiceInicial <= indiceFinal) {
      sublista.agregar(datos[indiceInicial]);
      subListaRR(indiceInicial + 1, indiceFinal, sublista);
    }
  }

  @Override
  public void redimensionar(int maximo) {
    if (maximo < 0) {
      return;
    }
    Arreglo copia = (Arreglo) clonar();
    datos = new Object[maximo];
    limite = -1;
    MAXIMO = maximo;
    for (int i = 0; i < copia.numElementos(); i++) {
      agregar(copia.obtener(i));
    }
  }

  /**
   * Redimensiona el arreglo de forma recursiva.
   * 
   * @param maximo Es el nuevo tamanio del arreglo.
   */
  public void redimensionarRR(int maximo) {
    if (maximo < 0) {
      return;
    }
    Arreglo copia = (Arreglo) clonarRR();
    datos = new Object[maximo];
    limite = -1;
    MAXIMO = maximo;
    agregarListaRR(0, copia);
  }

  @Override
  public boolean insertar(int indice, Object info) {
    if (lleno()) {
      return false;
    }
    if (indice > limite || indice < 0) {
      return false;
    }

    limite++;
    for (int i = limite; i > indice; i--) {
      datos[i] = datos[i - 1];
    }

    datos[indice] = info;
    return true;
  }

  /**
   * Inserta en la posicion indice el objeto indicado de forma recursiva.
   * 
   * @param indice Es el indice donde se insertara el objeto.
   * @param info   Es el objeto a insertar.
   * @return Regresa <b>true</b> si se pudo insertar, <b>false</b> en caso
   *         contrario.
   */
  public boolean insertarRR(int indice, Object info) {
    if (lleno()) {
      return false;
    }
    if (indice > limite || indice < 0) {
      return false;
    }

    limite++;
    recorrerDerechaRR(indice, limite);
    datos[indice] = info;
    return true;
  }

  /**
   * Recorre las posiciones del arreglo hacia la derecha.
   * 
   * @param posIzq Es el indice a partir del cual se empieza a recorrer.
   * @param posDer Es el limite del arreglo.
   */
  private void recorrerDerechaRR(int posIzq, int posDer) {
    if (posDer > posIzq) {
      datos[posDer] = datos[posDer - 1];
      recorrerDerechaRR(posIzq, posDer - 1);
    }
  }

  @Override
  public boolean copiarArreglo(Arreglo arreglo2) {
    Arreglo lista = (Arreglo) arreglo2;
    if (lista.longitud() != longitud()) {
      return false;
    }

    redimensionar(lista.longitud());
    vaciar();
    agregarLista(arreglo2);
    return true;
  }

  /**
   * Copia el contenido del arreglo recibido al arreglo actual de forma recursiva.
   * 
   * @param arreglo2 Es el arreglo del cual se copiara el contenido.
   * @return Regresa la copia del arreglo.
   */
  public boolean copiarArregloRR(Arreglo arreglo2) {
    Arreglo lista = (Arreglo) arreglo2;
    if (lista.longitud() != longitud()) {
      return false;
    }
    redimensionarRR(lista.longitud());
    vaciar();
    agregarListaRR(arreglo2);
    return true;
  }

  /**
   * Crea una sublista con los elementos de los indices contenidos en
   * arregloIndices.
   * 
   * @param arregloIndices Es el arreglo con buffer2.vaciar();las posiciones de
   *                       los elementos de la sublista.
   * @return Regresa la sublista con los elementos de las posiciones indicadas.
   */
  public Arreglo subLista(ArregloNumerico arregloIndices) {
    Arreglo arreglo = new Arreglo(arregloIndices.numElementos());
    for (int i = 0; i < arregloIndices.numElementos(); i++) {
      Object o = obtener((int) arregloIndices.obtener(i));
      if (o == null) {
        continue;
      }
      arreglo.agregar(o);
    }
    return arreglo;
  }

  /**
   * Crea una sublista con los elementos de los indices contenidos en
   * arregloIndices de forma recursiva.
   * 
   * @param arregloIndices Es el arreglo con buffer2.vaciar();las posiciones de
   *                       los elementos de la sublista.
   * @return Regresa la sublista con los elementos de las posiciones indicadas.
   */
  public Arreglo subListaRR(ArregloNumerico arregloIndices) {
    Arreglo arreglo = new Arreglo(arregloIndices.numElementos());
    subListaRR(0, arregloIndices, arreglo);
    return arreglo;
  }

  /**
   * Crea una sublista con los elementos de los indices contenidos en
   * arregloIndices de forma recursiva.
   * 
   * @param pos     Es la posicion actual.
   * @param indices Es el arreglo con buffer2.vaciar();las posiciones de los
   *                elementos de la sublista.
   * @param almacen Es el arreglo donde se guardan los datos para la sublista.
   */
  private void subListaRR(int pos, ArregloNumerico indices, Arreglo almacen) {
    if (pos < indices.numElementos()) {
      Object o = obtener((int) indices.obtener(pos));
      if (o != null) {
        almacen.agregar(o);
      }
      subListaRR(pos + 1, indices, almacen);
    }
  }

  /**
   * Cambia el contenido de datos por el contenido del arreglo.
   * 
   * @param arreglo Es el arreglo que contiene los nuevos datos.
   */
  public void recibirArreglo(Object[] arreglo) {
    vaciar();
    redimensionar(arreglo.length);
    for (int i = 0; i < arreglo.length; i++) {
      agregar(arreglo[i]);
    }
  }

  /**
   * Cambia el contenido de datos por el contenido del arreglo de forma recursiva.
   * 
   * @param arreglo Es el arreglo que contiene los nuevos datos.
   */
  public void recibirArregloRR(Object[] arreglo) {
    vaciar();
    redimensionarRR(arreglo.length);
    recibirArregloRR(0, arreglo);
  }

  /**
   * Cambia el contenido de datos por el contenido del arreglo de forma recursiva.
   * 
   * @param pos     Es la posicion actual.
   * @param arreglo Es el arreglo que contiene los nuevos datos.
   */
  private void recibirArregloRR(int pos, Object[] arreglo) {
    if (pos < arreglo.length) {
      agregar(arreglo[pos]);
      recibirArregloRR(pos + 1, arreglo);
    }
  }

  /**
   * Obtiene una copia de datos.
   * 
   * @return Regresa una copia de datos.
   */
  public Object[] leerArreglo() {
    Object[] arreglo = new Object[numElementos()];
    for (int i = 0; i < numElementos(); i++) {
      arreglo[i] = datos[i];
    }
    return arreglo;
  }

  /**
   * Obtiene una copia de datos.
   * 
   * @return Regresa una copia de datos.
   */
  public Object[] leerArregloRR() {
    Object[] arreglo = new Object[numElementos()];
    leerArregloRR(0, arreglo);
    return arreglo;
  }

  /**
   * Obtiene una copia de datos.
   * 
   * @param pos     Es la posicion actual.
   * @param arreglo Es el arreglo donde se almacenaran los datos.
   */
  private void leerArregloRR(int pos, Object[] arreglo) {
    if (pos <= limite) {
      arreglo[pos] = datos[pos];
      leerArregloRR(pos + 1, arreglo);
    }
  }

  /**
   * Convierte el arreglo a ListaLigada.
   * 
   * @return Regresa una ListaLigada con los datos del arreglo.
   */
  public ListaLigada arregloAListaRR() {
    ListaLigada convertido = new ListaLigada();
    arregloAListaRR(0, convertido);
    return convertido;
  }

  /**
   * Convierte el arreglo a ListaLigada.
   * 
   * @param pos     Es la posicion actual.
   * @param almacen Es la lista donde se almacenaran los datos.
   */
  private void arregloAListaRR(int pos, ListaLigada almacen) {
    if (pos <= limite) {
      almacen.agregar(datos[pos]);
      arregloAListaRR(pos + 1, almacen);
    }
  }

  /**
   * Convierte el arreglo a una Matriz2D.
   * 
   * @param numReng Es el numero de renglones de la matriz.
   * @param numCol  Es el numero de columnas de la matriz.
   * @return Regresa la Matriz2D creada.
   */
  public Matriz2D reDim(int numReng, int numCol) {
    if (numReng * numCol <= limite) {
      return null;
    }
    Matriz2D matriz2d = new Matriz2D(numReng, numCol);
    reDim(0, 0, 0, matriz2d);
    return matriz2d;
  }

  /**
   * Almacena los datos del arreglo en una Matriz2D.
   * 
   * @param pos      Es la posicion actual en el arreglo.
   * @param reng     Es el renglon actual.
   * @param col      Es la columna actual.
   * @param matriz2d Es la matriz donde se almacenan los datos.
   */
  private void reDim(int pos, int reng, int col, Matriz2D matriz2d) {
    if (pos <= limite) {
      if (reng != matriz2d.renglones() - 1 || col != matriz2d.columnas() - 1) {
        matriz2d.cambiarInfo(reng, col, datos[pos]);
        if (col != matriz2d.columnas() - 1) {
          reDim(pos + 1, reng, col + 1, matriz2d);
        } else {
          reDim(pos + 1, reng + 1, 0, matriz2d);
        }
      } else {
        matriz2d.cambiarInfo(reng, col, datos[pos]);
      }
    }
  }

  @Override
  public String toString() {
    String elementos = "[";
    for (int i = 0; i < numElementos() - 1; i++) {
      elementos += datos[i];
      elementos += ", ";
    }

    if (numElementos() > 0) {
      elementos += datos[numElementos() - 1];
    }
    elementos += "]";
    return elementos;
  }

  /**
   * Convierte el arreglo a String de forma recursiva.
   * 
   * @return Regresa el String creado.
   */
  public String toStringRR() {
    String elementos = "[ ";
    elementos += toStringRR(0);
    elementos += "]";
    return elementos;
  }

  /**
   * Convierte el arreglo a String de forma recursiva.
   * 
   * @param pos Es la posicion actual.
   * @return Regresa el String creado.
   */
  private String toStringRR(int pos) {
    if (pos <= limite) {
      return datos[pos] + ", " + toStringRR(pos + 1);
    } else {
      return "";
    }
  }

}
