package edlineal.catalogo;

/**
 * Esta clase es un nodo que guarda una llave y un valor.
 */
public class NodoHash {
  protected Object clave;
  protected Object valor;
  protected NodoHash ligaDer;

  /**
   * Crea una nueva instancia de NodoHash.
   * @param clave Es la clave del nodo.
   * @param valor Es el valor del nodo.
   */
  public NodoHash(Object clave, Object valor) {
    this.clave = clave;
    this.valor = valor;
  }

  /**
   * Obtiene la clave del nodo.
   * @return Regresa la clave del nodo.
   */
  public Object getClave() {
    return clave;
  }

  /**
   * Obtiene el valor del nodo.
   * @return Regresa el valor del nodo.
   */
  public Object getValor() {
    return valor;
  }

  /**
   * Obtiene la liga derecha del nodo.
   * @return Regresa la liga derecha del nodo.
   */
  public NodoHash getLigaDer() {
    return ligaDer;
  }

  /**
   * Establece un nuevo valor al nodo.
   * @param valor Es el nuevo valor a establecer.
   */
  public void setValor(Object valor) {
    this.valor = valor;
  }

  /**
   * Establece la liga derecha del nodo.
   * @param ligaDer Es la nueva liga derecha del nodo.
   */
  public void setLigaDer(NodoHash ligaDer) {
    this.ligaDer = ligaDer;
  }

  @Override
  public String toString() {
    return valor.toString();
  }
}
