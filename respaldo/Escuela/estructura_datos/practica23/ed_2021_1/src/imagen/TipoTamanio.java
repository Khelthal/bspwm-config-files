package imagen;

/**
 * Son los tipos de tamanio para redimensionar una imagen.
 */
public enum TipoTamanio {
    TERCIO,
    MITAD,
    DOBLE,
    TRIPLE,
    CUADRUPLE
}
