package interfaces;

import edlineal.Arreglo;
import edlineal.enumerados.TipoMatriz;
import ednolineal.*;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de realizar pruebas con la clase Matriz2D.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class PruebaMatriz2D {
    public static void main(String[] args) {
        Matriz2D probando = new Matriz2D(3, 2);
        probando.cambiarInfo(0, 0, "1");
        probando.cambiarInfo(0, 1, "2");
        probando.cambiarInfo(1, 0, "3");
        probando.cambiarInfo(1, 1, "4");
        probando.cambiarInfo(2, 0, "5");
        probando.cambiarInfo(2, 1, "6");
        probando.imprimirR();
        SalidaEstandar.consola("Creando Matriz3D\n");

        Arreglo matrices = new Arreglo(4);
        matrices.agregar(new Matriz2D(3, 2, "a"));
        matrices.agregar(new Matriz2D(3, 2, "b"));
        matrices.agregar(new Matriz2D(3, 2, "c"));
        matrices.agregar(new Matriz2D(3, 2, "d"));

        Matriz3D matriz3D = probando.aMatriz3D(matrices);
        matriz3D.imprimirProfundidades();

        Matriz2D vacia = new Matriz2D(10, 10);

        SalidaEstandar.consola("Creando nueva matriz y comprobando si esta vacia: " + vacia.vacia() + "\n");
        SalidaEstandar.consola("Transformando la matriz vacia en vector renglon\n");
        vacia.vectorRenglon(10, 1);
        vacia.imprimirR();
        SalidaEstandar.consola("Eliminando 3 columnas del vector renglon\n");
        vacia.eliminarColumna(TipoColumna.DERECHA);
        vacia.eliminarColumna(TipoColumna.DERECHA);
        vacia.eliminarColumna(TipoColumna.DERECHA);
        vacia.imprimirR();
        SalidaEstandar.consola("Agregando nuevos renglones y dos columnas\n");
        vacia.agregarMatrizRenglon(new Matriz2D(1, 7, 2));
        vacia.agregarMatrizRenglon(new Matriz2D(1, 7, 3));
        vacia.agregarMatrizRenglon(new Matriz2D(1, 7, 4));
        vacia.agregarMatrizColumna(new Matriz2D(4, 2, 5));
        vacia.imprimirR();
        SalidaEstandar.consola("Obteniendo matriz transpuesta\n");
        vacia.aplicarTranspuesta();
        vacia.imprimirR();
        SalidaEstandar.consola("Imprimiendo matriz\n");
        probando.imprimirR();
        SalidaEstandar.consola("Conviritendo a Arreglo por columnas\n");
        (probando.reDim(TipoMatriz.COLUMNA)).imprimirOI();
        SalidaEstandar.consola("Conviritendo a Arreglo por renglones\n");
        (probando.reDim(TipoMatriz.RENGLON)).imprimirOI();
    }
}
