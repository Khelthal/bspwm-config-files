package interfaces;

import imagen.ImageEditor;
import imagen.TipoTamanio;

/**
 * Esta clase se encarga de hacer pruebas con la clase ImageEditor.
 */
public class PruebaImageEditor {
    public static void main(String[] args) {
        ImageEditor imageEditor = new ImageEditor();
        imageEditor.leerImagen("src/krilin.jpg");
        imageEditor.modificarBrillo(-50);
        imageEditor.generarMarco(4, 255<<8);
        imageEditor.invertirX();
        imageEditor.invertirY();
        imageEditor.escribirImagen();
    }
}
