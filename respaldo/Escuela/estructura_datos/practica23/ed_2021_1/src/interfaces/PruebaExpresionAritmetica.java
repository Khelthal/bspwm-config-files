package interfaces;

import entradasalida.SalidaEstandar;
import utilerias.matematicas.ExpresionAritmetica;

/**
 * Esta clase se encarga de realizar pruebas con la clase ExpresionAritmetica.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class PruebaExpresionAritmetica {
    public static void main(String[] args) {
        String original = "(x-2)*y/2^[var1- 9]";
        SalidaEstandar.consola("La expresion ingresada es: " + original + "\n");
        String prueba;
        prueba = ExpresionAritmetica.convertirExpresion(original);
        SalidaEstandar.consola("La nueva expresion es: "+prueba+"\n");
        SalidaEstandar.consola("Evaluando la expresion infija a prefija...\n");
        SalidaEstandar.consola("La forma prefija de la expresion es: "+ExpresionAritmetica.infijaAPrefija(prueba)+"\n");
        SalidaEstandar.consola("El resultado de la operacion es: "+ExpresionAritmetica.evaluarPrefija(ExpresionAritmetica.infijaAPrefija(prueba))+"\n");
        SalidaEstandar.consola("Evaluando la expresion infija a postfija...\n");
        SalidaEstandar.consola("La forma postfija de la expresion es: "+ExpresionAritmetica.infijaAPostfija(prueba)+"\n");
        SalidaEstandar.consola("El resultado de la operacion es: "+ExpresionAritmetica.evaluarPostfija(ExpresionAritmetica.infijaAPostfija(prueba))+"\n");
    }
}
