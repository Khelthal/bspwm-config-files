package interfaces;

import calculos.series.SeriePrimos;
import edlineal.ListaLigada;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de realizar pruebas con la clase SeriePrimos.
 */
public class PruebaSeriePrimos {

  public static void main(String[] args) {
    SalidaEstandar.consola("Obteniendo serie de numeros primos desde 2 hasta 50\n");
    ListaLigada lista = SeriePrimos.obtenerSeriePrimos(50);
    lista.imprimir();
    SalidaEstandar.consola("\n");
  }
}
