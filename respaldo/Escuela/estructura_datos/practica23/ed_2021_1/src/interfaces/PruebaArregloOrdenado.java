package interfaces;

import edlineal.Arreglo;
import edlineal.ArregloOrdenado;
import edlineal.TipoOrden;
import entradasalida.SalidaEstandar;

/**
 * Esta clase maneja pruebas con ArregloOrdenado.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class PruebaArregloOrdenado {
    public static void main(String[] args) {
        ArregloOrdenado a = new ArregloOrdenado(9, TipoOrden.CRECIENTE);
        a.agregar("d");
        a.agregar("e");
        a.agregar("f");
        a.agregar("g");
        a.agregar("i");

        Arreglo b = new Arreglo(12);
        b.agregar("a");
        b.agregar("b");
        b.agregar("g");

        a.imprimirOI();
        SalidaEstandar.consola("Invirtiendo el arreglo\n");
        a.invertir();
        a.imprimirOI();
        SalidaEstandar.consola("Probar metodo rellenar\n");
        ArregloOrdenado c = new ArregloOrdenado(6, TipoOrden.DECRECIENTE);
        c.rellenar('F');
        c.imprimirOI();
        SalidaEstandar.consola("Probar metodo esSubLista\n");
        ArregloOrdenado d = new ArregloOrdenado(7, TipoOrden.DECRECIENTE);
        d.agregar('E');
        d.agregar('D');
        d.agregar('C');
        d.agregar('A');
        d.agregar('B');
        d.agregar('F');
        d.agregar('G');

        SalidaEstandar.consola("Es una sublista: " + d.esSubLista(c) + "\n");
        SalidaEstandar.consola("Probar metodo cambiarLista\n");
        ArregloOrdenado e = new ArregloOrdenado(9, TipoOrden.CRECIENTE);
        e.rellenar(9);
        e.imprimirOI();
        ArregloOrdenado f = new ArregloOrdenado(3, TipoOrden.CRECIENTE);
        f.agregar(1);
        f.agregar(4);
        f.agregar(7);
        ArregloOrdenado g = new ArregloOrdenado(3, TipoOrden.CRECIENTE);
        g.agregar(50);
        g.agregar(40);
        g.agregar(80);
        e.cambiarLista(f, g);
        SalidaEstandar.consola("Cambiando la lista\n");
        e.imprimirOI();
        SalidaEstandar.consola("Probar metodo retenerLista\n");
        e.retenerLista(g);
        e.imprimirOI();
        SalidaEstandar.consola("Probar metodo arregloDesordenado\n");
        ArregloOrdenado h = new ArregloOrdenado(9, TipoOrden.DECRECIENTE);
        h.rellenar('I');
        Arreglo i = (Arreglo) h.arregloDesordenado();
        i.imprimirOI();
        SalidaEstandar.consola("Vaciando el arreglo\n");
        i.vaciar();
        i.imprimirOI();
        SalidaEstandar.consola("Probando metodo insertar\n");
        ArregloOrdenado j = new ArregloOrdenado(5, TipoOrden.DECRECIENTE);
        j.agregar("a");
        j.agregar("c");
        j.agregar("d");
        j.agregar("e");
        j.insertar(4, "b");
        j.imprimirOI();
        SalidaEstandar.consola("Probando el metodo copiarArreglo\n");
        Arreglo k = new Arreglo(5);
        k.agregar("b");
        k.agregar("a");
        k.agregar("d");
        k.agregar("c");
        k.agregar("f");
        k.imprimir();
        SalidaEstandar.consola("Copiando el arreglo k en j\n");
        j.copiarArreglo(k);
        j.imprimirOI();
    }
}
