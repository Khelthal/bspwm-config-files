package calculos.regresionlineal;

import edlineal.Arreglo;
import edlineal.ArregloNumerico;
import entradasalida.archivos.ArchivoTexto;

public class RegresionLineal {
    /**
     * Calcula los valores de p y b;
     * @return Regresa un arreglo con los valores de p y b;
     */
    public static ArregloNumerico calcular() {
        double a = 0;
        double p = 0;
        double b = 0;
        ArregloNumerico resultado = new ArregloNumerico(2);
        Arreglo x = ArchivoTexto.leerNumeros("src/calculos/x.txt", 498);
        Arreglo y = ArchivoTexto.leerNumeros("src/callculos/y.txt", 498);
        ArregloNumerico ypredicho = new ArregloNumerico(498);
        double costo;
        int repeticiones = 100000;

        for (int i = 0; i < repeticiones; i++) {
            ypredicho.vaciar();
            for (int j = 0; j < x.numElementos(); j++) {
                double xnum = (double) x.obtener(j);
                ypredicho.agregar(calcularHipotesis(p, b, xnum));
            }

            costo = calcularCosto(ypredicho, y);
        }

        resultado.agregar(p);
        resultado.agregar(b);

        return resultado;
    }

    /**
     * Calcula la hipotesis.
     * @param p Es el valor de p.
     * @param b Es el valor de b.
     * @param x Es el valor de x.
     * @return Regresa el resultado de la hipotesis.
     */
    public static double calcularHipotesis(double p, double b, double x) {
        return p + b * x;
    }

    /**
     * Calcula el valor del costo.
     * @param ho Es y predicho.
     * @param y Es y esperado.
     * @return Regresa el valor del costo.
     */
    public static double calcularCosto(Arreglo ho, Arreglo y) {
        double multiplicador = 1.0/(2.0*ho.numElementos());
        double costo = 0.0;

        for (int i = 0; i < ho.numElementos(); i++) {
            double honum = (double) ho.obtener(i);
            double ynum = (double) y.obtener(i);
            double incremento = honum - ynum;
            incremento = Math.pow(incremento, 2);
            costo += incremento;
        }
        return costo;
    }
}
