package calculos.correlacion;

import edlineal.ListaDobleLigada;
import edlineal.ListaLigada;
import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;
import entradasalida.archivos.ArchivoTexto;

/**
 * Esta clase se encarga de probar la correlacion de Pearson con ListaDobleLigada.
 */
public class PruebaCorrelacionPearsonLDL {

  public static void main(String[] args) {
    SalidaEstandar.consola("Ingrese la ruta del archivo con los valores de X\n:");
    String fileX = EntradaConsola.consolaCadena();
    SalidaEstandar.consola("Ingrese la ruta del archivo con los valores de Y\n:");
    String fileY = EntradaConsola.consolaCadena();
    ListaDobleLigada listaX = ArchivoTexto.leerNumerosListaDobleLigada(fileX);
    ListaDobleLigada listaY = ArchivoTexto.leerNumerosListaDobleLigada(fileY);
    double coeficientePearson = Correlacion.calcularCoeficientePearson(listaX, listaY);
    SalidaEstandar.consola("El resultado es: " + coeficientePearson + "\n");
    if (coeficientePearson == -1) {
      SalidaEstandar.consola("Correlacion negativa perfecta\n");
    }
    else if (coeficientePearson > -1 && coeficientePearson < 0) {
      SalidaEstandar.consola("Correlacion negativa\n");
    }
    else if (coeficientePearson == 0) {
      SalidaEstandar.consola("Ninguna correlacion\n");
    }
    else if (coeficientePearson > 0 && coeficientePearson < 1) {
      SalidaEstandar.consola("Correlacion positiva\n");
    }
    else if (coeficientePearson == 1) {
      SalidaEstandar.consola("Correlacion positiva perfecta\n");
    }

    ListaLigada listaLX = ArchivoTexto.leerNumerosListaLigada(fileX);
    ListaLigada listaLY = ArchivoTexto.leerNumerosListaLigada(fileY);
    coeficientePearson = Correlacion.calcularCoeficientePearsonMuestral(listaLX, listaLY);
    SalidaEstandar.consola(coeficientePearson + "\n");
  }
}
