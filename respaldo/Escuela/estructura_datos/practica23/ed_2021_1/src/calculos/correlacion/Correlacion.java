package calculos.correlacion;

import edlineal.ListaDobleLigada;
import edlineal.ListaLigada;
import entradasalida.SalidaEstandar;
import utilerias.matematicas.OperacionesListasDoblesLigadas;
import utilerias.matematicas.OperacionesListasLigadas;

import static java.lang.Math.sqrt;

import calculos.covarianza.Covarianza;

/**
 * Esta clase se encarga de calcular la correlacion.
 */
public class Correlacion {

  /**
   * Calcula el coeficiente de Pearson a partir de datos obtenidos en ListaDobleLigada.
   * @param listaX Es la lista con los datos de X.
   * @param listaY Es la lista con los datos de Y.
   * @return Regresa el coeficiente de Pearson calculado.
   */
  public static double calcularCoeficientePearson(ListaDobleLigada listaX, ListaDobleLigada listaY) {
    int n = 0;

    listaX.inicializarIteradorIzq();

    while (listaX.hayMasIzq()) {
      n++;
      listaX.obtenerSigiuenteIzq();
    }

    ListaDobleLigada listaXY = OperacionesListasDoblesLigadas.multiplicarListas(listaX, listaY);

    double a = OperacionesListasDoblesLigadas.calcularSumatoriaLista(listaXY);
    double xSum = OperacionesListasDoblesLigadas.calcularSumatoriaLista(listaX);
    double ySum = OperacionesListasDoblesLigadas.calcularSumatoriaLista(listaY);
    double b = xSum * ySum;

    double arriba = n * a - b;

    ListaDobleLigada listaXX = OperacionesListasDoblesLigadas.multiplicarListas(listaX,
        (ListaDobleLigada) listaX.clonar());
    ListaDobleLigada listaYY = OperacionesListasDoblesLigadas.multiplicarListas(listaY,
        (ListaDobleLigada) listaY.clonar());

    double xxSum = OperacionesListasDoblesLigadas.calcularSumatoriaLista(listaXX);
    double yySum = OperacionesListasDoblesLigadas.calcularSumatoriaLista(listaYY);

    double c = n * xxSum - xSum * xSum;
    c = sqrt(c);

    double d = n * yySum - ySum * ySum;
    d = sqrt(d);

    double abajo = c * d;

    return arriba / abajo;
  }

  /**
   * Calcula el coeficiente de Pearson muestral a partir de datos obtenidos en ListaLigada.
   * @param listaX Es la lista con los datos de X.
   * @param listaY Es la lista con los datos de Y.
   * @return Regresa el coeficiente de Pearson muestral calculado.
   */
  public static double calcularCoeficientePearsonMuestral(ListaLigada listaX, ListaLigada listaY) {
    double xProm = 0;
    double yProm = 0;
    int n = 0;

    listaX.inicializarIterador();
    while (listaX.hayMas()) {
      n++;
      xProm += (double) listaX.obtenerSigiuente();
    }

    listaY.inicializarIterador();
    while (listaY.hayMas()) {
      yProm += (double) listaY.obtenerSigiuente();
    }

    xProm /= n;
    yProm /= n;

    ListaLigada xResta = OperacionesListasLigadas.sumarConstanteALista(listaX, -xProm);
    ListaLigada xxResta = OperacionesListasLigadas.multiplicarListas(xResta, (ListaLigada) xResta.clonar());
    ListaLigada yResta = OperacionesListasLigadas.sumarConstanteALista(listaY, -yProm);
    ListaLigada yyResta = OperacionesListasLigadas.multiplicarListas(yResta, (ListaLigada) yResta.clonar());
    ListaLigada xyResta = OperacionesListasLigadas.multiplicarListas(xResta, yResta);

    double arriba = OperacionesListasLigadas.calcularSumatoriaLista(xyResta);
    double abajo = OperacionesListasLigadas.calcularSumatoriaLista(xxResta) * OperacionesListasLigadas.calcularSumatoriaLista(yyResta);
    abajo = sqrt(abajo);

    return arriba / abajo;
  }

  /**
   * Calcula el coeficiente de Pearson poblacional a partir de datos obtenidos en ListaLigada.
   * @param listaX Es la lista con los datos de X.
   * @param listaY Es la lista con los datos de Y.
   * @return Regresa el coeficiente de Pearson poblacional calculado.
   */
  public static double calcularCoeficientePearsonPoblacional(ListaLigada listaX, ListaLigada listaY) {
    double xProm = 0;
    double yProm = 0;
    int n = 0;

    listaX.inicializarIterador();
    while (listaX.hayMas()) {
      n++;
      xProm += (double) listaX.obtenerSigiuente();
    }

    listaY.inicializarIterador();
    while (listaY.hayMas()) {
      yProm += (double) listaY.obtenerSigiuente();
    }

    xProm /= n;
    yProm /= n;

    double covarianza = Covarianza.calcularCovarianza(listaX, listaY);
    ListaLigada xResta = OperacionesListasLigadas.sumarConstanteALista(listaX, -xProm);
    ListaLigada xxResta = OperacionesListasLigadas.multiplicarListas(xResta, (ListaLigada) xResta.clonar());
    ListaLigada yResta = OperacionesListasLigadas.sumarConstanteALista(listaY, -yProm);
    ListaLigada yyResta = OperacionesListasLigadas.multiplicarListas(yResta, (ListaLigada) yResta.clonar());

    double desviacionX = OperacionesListasLigadas.calcularSumatoriaLista(xxResta);
    desviacionX /= n;
    desviacionX = sqrt(desviacionX);

    double desviacionY = OperacionesListasLigadas.calcularSumatoriaLista(yyResta);
    desviacionY /= n;
    desviacionY = sqrt(desviacionY);

    return covarianza / (desviacionX*desviacionY);
  }
}
