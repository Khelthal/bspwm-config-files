package calculos.mcd;

/**
 * Esta clase se encarga de calcular el algoritmo de Euclides.
 */
public class Euclides {

  /**
   * Calcula el MCD de dos numeros utilizando el algoritmo de euclides.
   * @param a Es uno de los numeros.
   * @param b ES uno de los numeros.
   * @return Regresa el MCD de los 2 numeros.
   */
  public static int calcularMCD(int a, int b) {
    if (a == b) {
      return a;
    } else {
      if (a > b) {
        int temp = a;
        a = b;
        b = temp;
      }
      return calcularMCD(b-a, a);
    }
  }
}
