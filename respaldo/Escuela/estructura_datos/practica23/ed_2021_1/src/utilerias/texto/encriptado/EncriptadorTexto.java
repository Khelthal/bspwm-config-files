package utilerias.texto.encriptado;

import edlineal.Arreglo;
import edlineal.PilaArreglo;
import entradasalida.SalidaEstandar;
import entradasalida.archivos.ArchivoTexto;

/**
 * Esta clase se encarga de encriptar y desencriptar cadenas de texto.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class EncriptadorTexto {
    /**
     * Encripta el texto pasado como argumento.
     * @param texto Es el texto a encriptar.
     * @return Regresa la cadena encriptada.
     */
    public static String encriptarTexto(String texto) {
        String encriptado = "";
        Arreglo posiciones = ArchivoTexto.leerNumeros("src/coordenadas.txt", 6);
        PilaArreglo pila = new PilaArreglo(texto.length() + posiciones.numElementos());

        while (true) {
            boolean completo = true;
            for (int i = 0; i < posiciones.numElementos(); i+=2) {
                if (i + 2 >= posiciones.numElementos()) {
                    int pos1 = (int) (double) posiciones.obtener(i);
                    int extension = (int) (double) posiciones.obtener(i+1);
                    extension--;
                    if (pos1 + extension >= texto.length()) {
                        posiciones.eliminar(i);
                        posiciones.eliminar(i);
                    }
                    break;
                }
                int pos1 = (int) (double) posiciones.obtener(i);
                int extension = (int) (double) posiciones.obtener(i+1);
                extension--;
                int pos2 = (int) (double) posiciones.obtener(i+2);

                if (pos1 < 0 || extension < 0) {
                    posiciones.eliminar(i);
                    posiciones.eliminar(i);
                    completo = false;
                    break;
                }

                if (pos1 + extension >= texto.length()) {
                    posiciones.eliminar(i);
                    posiciones.eliminar(i);
                    completo = false;
                    break;
                }

                if (pos1 + extension >= pos2) {
                    posiciones.eliminar(i+2);
                    posiciones.eliminar(i+2);
                    completo = false;
                    break;
                }
            }
            if (completo) {
                break;
            }
        }

        int i = 0;
        while (i < texto.length()){
            if (!posiciones.vacia()) {
                if (i == (int)(double) posiciones.obtener(0)) {
                    pila.poner('>');
                    for (int j = 0; j < (int) (double) posiciones.obtener(1); j++) {
                        pila.poner(texto.charAt(i));
                        i++;
                    }
                    i--;
                    pila.poner('<');
                    posiciones.eliminar(0);
                    posiciones.eliminar(0);
                    while (!pila.vacio()) {
                        encriptado += pila.quitar();
                    }
                } else {
                    encriptado += texto.charAt(i);
                }
            } else {
                encriptado += texto.charAt(i);
            }
            i++;
        }

        return encriptado;
    }

    /**
     * Desencripta una cadena de texto.
     * @param encriptado Es la cadena a desencriptar.
     * @return Regresa la cadena desencriptada.
     */
    public static String desencriptarTexto(String encriptado) {
        String texto = "";
        String aux;
        PilaArreglo pila = new PilaArreglo(encriptado.length());

        int i = 0;

        while (i < encriptado.length()) {
            char c = encriptado.charAt(i);
            if (obtenerTipoAgrupacion(c) > 0) {
                i++;
                int cierre = obtenerTipoAgrupacion(c);
                int iguales = -1;
                while ((obtenerTipoAgrupacion(encriptado.charAt(i)) != cierre || c == encriptado.charAt(i)) || iguales != -1) {
                    if (obtenerTipoAgrupacion(encriptado.charAt(i)) == cierre) {
                        if (encriptado.charAt(i) == c) {
                            iguales++;
                        }
                        else {
                            iguales--;
                        }
                    }
                    pila.poner(encriptado.charAt(i));
                    i++;
                }
                aux = "";
                while (!pila.vacio()) {
                    aux += pila.quitar()+"";
                }
                aux = desencriptarTexto(aux);
                texto += aux;
            } else {
                texto += c;
            }
            i++;
        }

        return texto;
    }

    /**
     * Desencripta un archivo de texto.
     * @param archivo Es el nombre del archivo de texto.
     * @param tamanioArchivo Es el numero de lineas del archivo de texto.
     * @return Regresa una cadena con el texto del archivo desencriptado.
     */
    public static String desencriptarArchivo(String archivo, int tamanioArchivo) {
        Arreglo lineas = ArchivoTexto.leer(archivo, tamanioArchivo);
        String desencriptado = "";

        for (int i = 0; i < lineas.numElementos(); i++) {
            String linea = (String) lineas.obtener(i);
            desencriptado += desencriptarTexto(linea)+"\n";
        }
        return desencriptado;
    }

    /**
     * Obtiene el tipo de agrupador de un caracter.
     * @param c Es agrupador a analizar.
     * @return Regresa el valor del agrupador.
     */
    private static int obtenerTipoAgrupacion(char c) {
        switch (c) {
            default:
                return 0;
            case '<':
            case '>':
                return 1;
            case '[':
            case ']':
                return 2;
            case '{':
            case '}':
                return 3;
        }
    }
}
