package ednolineal;

/**
 * Contiene los tipos de renglon.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public enum TipoRenglon {
    SUPERIOR,
    INFERIOR
}
