package registros.procesos;

import edlineal.Arreglo;
import edlineal.ColaPrioridad;
import edlineal.TipoOrden;
import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;

/**
 * Esta clase enumerada se encarga de agregar y ejecutar procesos.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class EjecucionProcesos {
    Arreglo catalogoProcesos = new Arreglo(10);
    ColaPrioridad procesos = new ColaPrioridad(50, TipoOrden.CRECIENTE);

    /**
     * Crea una nueva instancia de EjecucionProcesos con un catalogo de procesos predefinido.
     */
    public EjecucionProcesos() {
        catalogoProcesos.agregar(new Proceso("brave", TipoProceso.DESCARGA, "/usr/lib/brave-bin/brave", "/usr/lib/brave-bin/", "elias"));
        catalogoProcesos.agregar(new Proceso("termite", TipoProceso.COPIADO, "/usr/bin/termite", "/usr/bin/", "elias"));
        catalogoProcesos.agregar(new Proceso("Xorg", TipoProceso.SISTEMA_OPERATIVO, "/usr/lib/Xorg", "/usr/lib", "root"));
        catalogoProcesos.agregar(new Proceso("NetworkManager", TipoProceso.DESCARGA, "/usr/bin/NetworkManager", "/usr/bin", "root"));
        catalogoProcesos.agregar(new Proceso("godot", TipoProceso.COPIADO, "/usr/bin/godot", "/usr/bin/", "elias"));
        catalogoProcesos.agregar(new Proceso("rtkit-daemon", TipoProceso.IMPRESION, "/usr/lib/rtkit-daemon", "/usr/lib", "rtkit"));
        catalogoProcesos.agregar(new Proceso("code", TipoProceso.COPIADO, "/usr/bin/code", "/usr/bin", "elias"));
        catalogoProcesos.agregar(new Proceso("lightdm", TipoProceso.SISTEMA_OPERATIVO, "/usr/lib/lightdm", "/usr/lib", "root"));
        catalogoProcesos.agregar(new Proceso("bash", TipoProceso.SISTEMA_OPERATIVO, "/bin/bash", "/bin", "elias"));
        catalogoProcesos.agregar(new Proceso("picom", TipoProceso.SISTEMA_OPERATIVO, "/usr/bin/picom", "/usr/bin", "elias"));
    }

    /**
     * Permite al usuario seleccionar el indice de un proceso del catalogo.
     * @return Regresa el indice seleccionado por el usuario.
     */
    public int seleccionarProceso() {
        for (int i = 0; i < catalogoProcesos.numElementos(); i++) {
            SalidaEstandar.consola(i + ": " + catalogoProcesos.obtener(i)+"\n");
        }
        SalidaEstandar.consola("Seleccione el indice del proceso a agregar a la cola\n:");
        int indice = EntradaConsola.consolaInt();
        if (indice < 0 || indice >= catalogoProcesos.numElementos()) {
            indice = 0;
        }
        return indice;
    }

    /**
     * Agrega un proceso a la cola de procesos.
     */
    public void agregarProceso() {
        Proceso proceso = (Proceso) catalogoProcesos.obtener(seleccionarProceso());
        if (!procesos.poner(proceso, proceso.getPrioridad())) {
            SalidaEstandar.consola("Memoria llena, no se puede agregar el proceso\n");
        }
    }

    /**
     * Ejecuta todos los procesos de la cola de procesos.
     */
    public void ejecutarProcesos() {
        while (!procesos.vacio()) {
            SalidaEstandar.consola("Ejecutando " + procesos.quitar() + "\n");
        }
    }
}
