package registros.indices;

import edlineal.ArregloOrdenado;
import edlineal.TipoOrden;

/**
 * Esta clase guarda nombre, paginas y sub terminos.
 */
public class Termino {
    protected String nombre;
    protected String paginas;
    protected ArregloOrdenado subTerminos;

    /**
     * Crea una instancia de la clase Termino.
     * @param nombre Es el nombre del Termino.
     * @param paginas Son las paginas que abarca el Termino.
     */
    public Termino(String nombre, String paginas) {
        this.nombre = nombre;
        this.paginas = paginas;
        this.subTerminos = new ArregloOrdenado(2, TipoOrden.CRECIENTE);
    }

    /**
     * Obtiene el atributo subTerminos.
     * @return Regresa el atributo subTerminos.
     */
    public ArregloOrdenado getSubTerminos() {
        return subTerminos;
    }

    /**
     * Agrega un Termino al arreglo de subTerminos.
     * @param termino Es el Termino a ser agregado.
     * @return Regresa la posicion donde el termino fue agregado, -1 si no se pudo agregar el elemento.
     */
    public int agregar(Object termino) {
        if (subTerminos.lleno()) {
            subTerminos.redimensionar(subTerminos.longitud()+2);
        }
        return subTerminos.agregar(termino);
    }

    /**
     * Calcula si el atributo paginas del Termino tiene un valor posible.
     * @return Regresa <b>true</b> si paginas tiene valores correctos, <b>false</b> en caso contrario.
     */
    public boolean paginasCorrectas() {
        try {
            String num = "";
            String numF = "";
            boolean rango = false;
            for (int i = 0; i < paginas.length(); i++) {
                if (paginas.charAt(i) != ',') {
                    if (paginas.charAt(i) == '-') {
                        rango = true;
                    } else {
                        if (rango) {
                            numF += paginas.charAt(i);
                        } else {
                            num += paginas.charAt(i);
                        }
                    }
                } else {
                    if (rango) {
                        Integer.parseInt(num);
                        Integer.parseInt(numF);
                        num = "";
                        numF = "";
                        rango = false;
                    } else {
                        Integer.parseInt(num);
                        num = "";
                    }
                }
            }
            if (rango) {
                Integer.parseInt(num);
                Integer.parseInt(numF);
                rango = false;
            } else {
                Integer.parseInt(num);
            }
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     * Crea un arreglo con las paginas del Termino.
     * @return Regresa el arreglo creado.
     */
    public ArregloOrdenado paginasToArregloOrdenado() {
        ArregloOrdenado nums = new ArregloOrdenado(10, TipoOrden.CRECIENTE);
        String num = "";
        String numF = "";
        boolean rango = false;
        for (int i = 0; i < paginas.length(); i++) {
            if (paginas.charAt(i) != ',') {
                if (paginas.charAt(i) == '-') {
                    rango = true;
                } else {
                    if (rango) {
                        numF += paginas.charAt(i);
                    } else {
                        num += paginas.charAt(i);
                    }
                }
            } else {
                if (rango) {
                    int ini = Integer.parseInt(num);
                    int fin = Integer.parseInt(numF);
                    for (int v = ini; v <= fin; v++) {
                        nums.agregar(v);
                    }
                    num = "";
                    numF = "";
                    rango = false;
                } else {
                    nums.agregar(Integer.parseInt(num));
                    num = "";
                }
            }
        }
        if (rango) {
            int ini = Integer.parseInt(num);
            int fin = Integer.parseInt(numF);
            for (int v = ini; v <= fin; v++) {
                nums.agregar(v);
            }
        } else {
            nums.agregar(Integer.parseInt(num));
        }
        return nums;
    }

    /**
     * Regresa el nombre y las paginas del Termino en formato de string.
     * @return Regresa una cadena con el nombre y las paginas del Termino.
     */
    public String obtenerDatos() {
        return nombre + ", " + paginas;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
