package registros.diccionarios;

import edlineal.ArregloOrdenado;
import edlineal.TipoOrden;
import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de realizar todas las operaciones del registro de palabras.
 */
public class RegistroPalabras {
    protected ArregloOrdenado palabrasRegistradas = new ArregloOrdenado(10, TipoOrden.CRECIENTE);

    /**
     * Agrega una palabra al registro.
     */
    public void agregarPalabra() {
        if (palabrasRegistradas.lleno()) {
            palabrasRegistradas.redimensionar(palabrasRegistradas.longitud()+10);
        }

        SalidaEstandar.consola("Ingrese el nombre de la palabra.\n:");
        String nombre = EntradaConsola.consolaCadena();
        SalidaEstandar.consola("Ingrese la descripcion de la palabra.\n:");
        String descripcion = EntradaConsola.consolaCadena();
        TipoPalabra tipoPalabra = seleccionarTipoPalabra();
        palabrasRegistradas.agregar(new Palabra(nombre, descripcion, tipoPalabra));
    }

    /**
     * Muestra los tipos de palabras existentes.
     */
    public void mostarTiposPalabra() {
        for (int i = 0; i < TipoPalabra.values().length; i++) {
            SalidaEstandar.consola( i + ": " + TipoPalabra.values()[i]+"\n");
        }
    }

    /**
     * Permite al usuario seleccionar un tipo de palabra.
     * @return Regresa el tipo de palabra seleccionado.
     */
    public TipoPalabra seleccionarTipoPalabra() {
        mostarTiposPalabra();
        SalidaEstandar.consola("Ingrese el indice del tipo de palabra a elegir.\n:");
        int indice = EntradaConsola.consolaInt();
        switch (indice) {
            default:
            case 0:
                return TipoPalabra.ADJETIVO;
            case 1:
                return TipoPalabra.ADVERBIO;
            case 2:
                return TipoPalabra.SUSTANTIVO;
            case 3:
                return TipoPalabra.DIMINUTIVO;
            case 4:
                return TipoPalabra.ACUSATIVO;
            case 5:
                return  TipoPalabra.INFINITIVO;
        }
    }

    /**
     * Muestra la descripcion de una palabra ingresada por el usuario.
     */
    public void mostrarPalabra() {
        SalidaEstandar.consola("Ingrese la palabra a consultar.\n:");
        String busqueda = EntradaConsola.consolaCadena();
        int indiceB = (Integer) palabrasRegistradas.buscar(busqueda);
        if (indiceB < 0) {
            SalidaEstandar.consola("La palabra no se encuentra en el registro.\n");
            return;
        }
        indiceB--;
        Palabra palabraEncontrada = (Palabra) palabrasRegistradas.obtener(indiceB);
        SalidaEstandar.consola(palabraEncontrada.obtenerDatos() + "\n");
    }

    /**
     * Muestra la lista de todas las palabras registradas con su definicion.
     */
    public void mostrarPalabrasDefinicion() {
        for (int i = 0; i < palabrasRegistradas.numElementos(); i++) {
            Palabra palabra = (Palabra) palabrasRegistradas.obtener(i);
            SalidaEstandar.consola(palabra.obtenerDatos() + "\n");
        }
    }

    /**
     * Muestra la lista de todas las palabras que inicien con alguna de las letras indicadas por el usuario.
     */
    public void mostrarPalabrasInicial() {
        SalidaEstandar.consola("Ingrese las letras a buscar.\n:");
        String letras = EntradaConsola.consolaCadena();
        ArregloOrdenado encontrados = new ArregloOrdenado(palabrasRegistradas.numElementos(), TipoOrden.CRECIENTE);
        for (int i = 0; i < letras.length(); i++) {
            char letra = letras.charAt(i);
            if (letra == ' ') {
                continue;
            }
            int indiceInicio = validarIndicePalabra((Integer) palabrasRegistradas.buscar(letra));
            int limite = (Integer) palabrasRegistradas.buscar(((char) (letra+1)));
            if (limite < 0) {
                limite *= -1;
            }
            limite--;

            for (int v = indiceInicio; v < limite; v++) {
                encontrados.agregar(palabrasRegistradas.obtener(v));
            }
        }
        encontrados.imprimirOI();
    }

    /**
     * Asegura que el indice dado por el usuario este dentro de un rango valido.
     * @param indice Es el indice a evaluar.
     * @return Regresa el indice con las modificaciones necesarias para validarlo.
     */
    public int validarIndicePalabra(int indice) {
        if (indice < 0) {
            indice *= -1;
        }
        indice--;
        return (indice <= palabrasRegistradas.numElementos())?indice:palabrasRegistradas.numElementos();
    }

    /**
     * Muestra una lista con todas las palabras del tipo indicado por el usuario.
     */
    public void mostrarPalabrasTipo() {
        TipoPalabra tipoPalabra = seleccionarTipoPalabra();
        ArregloOrdenado encontrados = new ArregloOrdenado(palabrasRegistradas.numElementos(), TipoOrden.CRECIENTE);
        for (int i = 0; i < palabrasRegistradas.numElementos(); i++) {
            Palabra palabra = (Palabra) palabrasRegistradas.obtener(i);
            if (palabra.getTipo() == tipoPalabra) {
                encontrados.agregar(palabra);
            }
        }
        encontrados.imprimirOI();
    }
}
