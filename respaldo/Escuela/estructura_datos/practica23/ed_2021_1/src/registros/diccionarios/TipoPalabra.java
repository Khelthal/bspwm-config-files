package registros.diccionarios;

/**
 * Esta clase enumerada contiene todos los tipos de palabra disponibles.
 */
public enum TipoPalabra {
    ADJETIVO,
    ADVERBIO,
    SUSTANTIVO,
    DIMINUTIVO,
    ACUSATIVO,
    INFINITIVO
}
