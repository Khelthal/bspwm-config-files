package registros.diccionarios;

import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de imprimir el menu del diccionario y de hacer llamadas a la clase RegistroPalabras.
 */
public class MenuRegistroPalabras {
    protected RegistroPalabras registro = new RegistroPalabras();

    /**
     * Inicia el menu
     */
    public void iniciar() {
        while (true) {
            imprimirMenu();
            SalidaEstandar.consola("\n");
            seleccionarOpcion();
            SalidaEstandar.consola("Presiona enter para continuar\n");
            EntradaConsola.consolaCadena();
        }
    }

    /**
     * Imprime las opciones del menu.
     */
    public void imprimirMenu() {
        SalidaEstandar.consola("¿Que desea hacer?\n");
        SalidaEstandar.consola("a) Agregar palabra al diccionario.\n");
        SalidaEstandar.consola("b) Consultar la definicion de una palabra especifica.\n");
        SalidaEstandar.consola("c) Mostrar las palabras registradas en el diccionario con su definicion.\n");
        SalidaEstandar.consola("d) Mostrar las palabras que inicien con una letra especifica.\n");
        SalidaEstandar.consola("e) Mostrar todas las palabras de un tipo especifico.\n");
        SalidaEstandar.consola("q) Salir.\n");
    }

    /**
     * Recibe la entrada del usuario para elegir una de las opciones.
     */
    public void seleccionarOpcion() {
        String respuesta = EntradaConsola.consolaCadena();
        switch (respuesta) {
            case "a":
                registro.agregarPalabra();
                break;
            case "b":
                registro.mostrarPalabra();
                break;
            case "c":
                registro.mostrarPalabrasDefinicion();
                break;
            case "d":
                registro.mostrarPalabrasInicial();
                break;
            case "e":
                registro.mostrarPalabrasTipo();
                break;
            case "q":
                System.exit(0);
        }
    }

}
