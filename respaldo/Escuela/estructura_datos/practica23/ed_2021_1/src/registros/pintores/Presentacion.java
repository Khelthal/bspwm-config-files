package registros.pintores;

/**
 * Esta clase se encarga de almacenar los datos de una presentacion.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class Presentacion {
    protected String nombre;
    protected String ciudad;
    protected int capacidadPersonas;

    /**
     * Crea una instancia de presentacion con el nombre, ciudad y capacidad de persona pasados como argumento.
     * @param nombre Es el nombre de la presentacion.
     * @param ciudad Es la ciudad de la presentacion.
     * @param capacidadPersonas Es la capacidad de personas de la presentacion.
     */
    public Presentacion(String nombre, String ciudad, int capacidadPersonas) {
        this.nombre = nombre;
        this.ciudad = ciudad;
        this.capacidadPersonas = capacidadPersonas;
    }

    @Override
    public String toString() {
        return nombre;
    }

    /**
     * Regresa los datos de la presentacion con formato String.
     * @return Regresa los datos de la presentacion con formato de string.
     */
    public String obtenerDatos() {
        return "Nombre: " + nombre + ", ciudad: " + ciudad + ", capacidad: " + capacidadPersonas + " personas";
    }
}
