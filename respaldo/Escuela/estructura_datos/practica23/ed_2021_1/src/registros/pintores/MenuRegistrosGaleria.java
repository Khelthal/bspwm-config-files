package registros.pintores;

import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;

import java.util.Locale;

/**
 * Esta clase se encarga de manipular a la clase RegistrosGaleria.
 * @author Clase de Estructura de Datos.
 * @version 1.0
 */
public class MenuRegistrosGaleria {
    protected RegistrosGaleria registro;

    public MenuRegistrosGaleria(RegistrosGaleria registro) {
        this.registro = registro;
    }

    /**
     * Inicia el menu
     */
    public void iniciar() {
        while (true) {
            imprimirMenu();
            SalidaEstandar.consola("\n");
            seleccionarOpcion();
            SalidaEstandar.consola("Presiona enter para continuar\n");
            EntradaConsola.consolaCadena();
        }
    }

    /**
     * Imprime las opciones del menu.
     */
    public void imprimirMenu() {
        SalidaEstandar.consola("¿Que desea hacer?\n");
        SalidaEstandar.consola("a) Mostrar la actividad mas popular del anio.\n");
        SalidaEstandar.consola("b) Mostrar al pintor mas trabajador.\n");
        SalidaEstandar.consola("c) Mostrar el nombre y edad del pintor que desarrollo una actividad especifica en un mes especifico.\n");
        SalidaEstandar.consola("d) Mostrar en que mes y en que presentacion pinto o expuso un pintor especifico.\n");
        SalidaEstandar.consola("e) Mostrar el mes en el que se pintaron menos obras de arte.\n");
        SalidaEstandar.consola("f) Mostrar los nombres y domicilios de los pintores que descansaron o convivieron todos los meses.\n");
        SalidaEstandar.consola("g) Mostrar el mes en el que todos los pintores expusieron.\n");
        SalidaEstandar.consola("h) Mostrar en que mes un pintor especifico dedico mas tiempo a inspirarse.\n");
        SalidaEstandar.consola("i) Mostrar en que evento/presentacion no convivio nadie.\n");
        SalidaEstandar.consola("q) Salir.\n");
    }

    /**
     * Recibe la entrada del usuario para elegir una de las opciones.
     */
    public void seleccionarOpcion() {
        String respuesta = EntradaConsola.consolaCadena();
        switch (respuesta) {
            case "a":
                registro.imprimirActividadMaxima();
                break;
            case "b":
                registro.imprimirPintorMaximo();
                break;
            case "c":
                registro.imprimirPintorActividadMes(obtenerNombreMes(), obtenerNombreActividad());
                break;
            case "d":
                String nombre = obtenerNombrePintor();
                SalidaEstandar.consola(nombre + " pinto y/o expuso en las siguientes presentaciones\n");
                registro.imprimirMesPresentacionPintorActividad(nombre, ActividadPintor.PINTAR);
                registro.imprimirMesPresentacionPintorActividad(nombre, ActividadPintor.EXPONER);
                break;
            case "e":
                registro.imprimirMesMenorActividad(ActividadPintor.PINTAR);
                break;
            case "f":
                registro.imprimirPintoresDescansados();
                break;
            case "g":
                registro.imprimirMesActividadTotal(ActividadPintor.EXPONER);
                break;
            case "h":
                registro.imprimirMaximaActividadMesPintor(obtenerNombrePintor(), ActividadPintor.OBSERVAR);
                break;
            case "i":
                registro.imprimirPresentacionSinActividad(ActividadPintor.CONVIVIR);
                break;
            case "q":
                System.exit(0);
        }
    }

    /**
     * Pide al usuario el nombre de un pintor.
     * @return Regresa el nombre ingresado por el usuario.
     */
    public String obtenerNombrePintor() {
        registro.imprimirPintores();
        SalidaEstandar.consola("Escriba el nombre del pintor a inspeccionar\n:");
        return EntradaConsola.consolaCadena();
    }

    /**
     * Pide al usuario el nombre de un mes.
     * @return Regresa el nombre del mes ingresado por el usuario.
     */
    public String obtenerNombreMes() {
        registro.imprimirMeses();
        SalidaEstandar.consola("Escriba el nombre del mes a inspeccionar\n:");
        return EntradaConsola.consolaCadena();
    }

    /**
     * Pide al usuario el nombre de una actividad de pintor.
     * @return Regresa la ActividadPintor con el nombre ingresado por el usuario.
     */
    public ActividadPintor obtenerNombreActividad() {
        for (int i = 0; i < ActividadPintor.values().length; i++) {
            SalidaEstandar.consola(ActividadPintor.values()[i]+"\n");
        }
        SalidaEstandar.consola("Escriba el nombre de la actividad a inspeccionar\n:");
        String actividad = EntradaConsola.consolaCadena();
        switch (actividad.toLowerCase(Locale.ROOT)) {
            case "exponer":
                return ActividadPintor.EXPONER;
            case "firmar_autografos":
                return ActividadPintor.FIRMAR_AUTOGRAFOS;
            case "pintar":
                return ActividadPintor.PINTAR;
            case "convivir":
                return ActividadPintor.CONVIVIR;
            case "observar":
                return ActividadPintor.OBSERVAR;
            default:
                return ActividadPintor.PINTAR;
        }
    }
}
