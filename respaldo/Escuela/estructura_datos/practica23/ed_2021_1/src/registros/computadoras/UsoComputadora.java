package registros.computadoras;

import edlineal.ListaLigada;
import utilerias.fecha.Fecha;

/**
 * Esta clase guarda datos de una sesion de uso de una computadora.
 */
public class UsoComputadora {

  protected Usuario usuario;
  protected Fecha fechaInicio;
  protected Fecha fechaFin;
  protected ListaLigada aplicacionesUsadas;

  /**
   * Crea una nueva instancia de UsoComputadora.
   * @param usuario Es el usuario que utilizo la computadora.
   * @param fechaInicio Es la fecha en la que inicio la sesion.
   * @param fechaFin Es la fecha en la que termino la sesion.
   */
  public UsoComputadora(Usuario usuario, Fecha fechaInicio, Fecha fechaFin) {
    this.usuario = usuario;
    this.fechaInicio = fechaInicio;
    this.fechaFin = fechaFin;
    aplicacionesUsadas = new ListaLigada();
  }

  /**
   * Obtiene al usuario que utilizo la computadora.
   * @return Regresa al usuario que utilizo la computadora.
   */
  public Usuario getUsuario() {
      return usuario;
  }

  /**
   * Agrega una aplicacion al registro.
   * @param aplicacion Es la aplicacion a agregar.
   * @return Regresa true si pudo agregar la aplicacion.
   */
  public boolean registarUsoAplicacion(Aplicacion aplicacion) {
    if (aplicacionesUsadas.buscar(aplicacion) != null) {
      return false;
    }
    return aplicacionesUsadas.agregar(aplicacion) != -1;
  }

  /**
   * Imprime las aplicaciones que se usaron durante la sesion.
   * */
  public void imprimirAplicaciones() {
    aplicacionesUsadas.imprimir();
  }

  /**
   * Determina si una fecha se encuentra entre 2 fechas.
   * @param fecha Es la fecha a evaluar.
   * @return Regresa true si la fecha se encuentra entre la fecha de inicio y la fecha de fin de la sesion.
   */
  public boolean fechaDentroDelLimite(Fecha fecha) {
    if (fecha.compareTo(fechaInicio) >= 0 && fecha.compareTo(fechaFin) <= 0) {
      return true;
    }
    return false;
  }

  /**
   * Determina si se utilizo una aplicacion especifica.
   * @param nombreAplicacion Es la aplicacion a buscar.
   * @return Regresa true si contiene la aplicacion.
   */
  public boolean contieneAplicacion(String nombreAplicacion) {
    return (aplicacionesUsadas.buscar(nombreAplicacion) != null);
  }

  @Override
  public String toString() {
      return "Inicio: " + fechaInicio + "  Fin: " + fechaFin;
  }
}
