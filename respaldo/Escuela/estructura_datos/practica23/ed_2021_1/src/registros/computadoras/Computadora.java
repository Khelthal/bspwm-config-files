package registros.computadoras;

import comparadores.ComparadorObject;
import edlineal.ListaLigada;
import entradasalida.SalidaEstandar;

/**
 * Esta clase guarda los datos de una computadora.
 */
public class Computadora {
  protected static int id=0;
  protected CentroComputo cc;
  protected int numComputadora;
  protected int ram;
  protected int discoDuro;
  protected String procesador;
  protected String marca;
  protected ListaLigada aplicaciones;
  protected ListaLigada usos;

  /**
   * Crea una nueva instancia de computadora.
   * @param cc Es el centro de computo al que pertenece.
   * @param ram Es la ram que tiene.
   * @param discoDuro Es la capacidad del disco duro.
   * @param procesador Es el procesador que tiene.
   * @param marca Es la marca de la computadora.
   */
  public Computadora(CentroComputo cc, int ram, int discoDuro, String procesador, String marca) {
    this.cc = cc;
    numComputadora = id;
    id++;
    this.ram = ram;
    this.discoDuro = discoDuro;
    this.procesador = procesador;
    this.marca = marca;
    aplicaciones = new ListaLigada();
    usos = new ListaLigada();
  }

  /**
   * Obtiene el numero de la computadora.
   * @return Regresa el numero de la computadora.
   */
  public int getNumComputadora() {
      return numComputadora;
  }

  /**
   * Obtiene la cantidad de ram.
   * @return Regresa la cantidad de ram.
   */
  public int getRam() {
      return ram;
  }

  /**
   * Obtiene el centro de computo.
   * @return Regresa el centro de computo.
   */
  public CentroComputo getCc() {
      return cc;
  }

  /**
   * Imprime las caracteristicas de la computadora.
   * */
  public void imprimir() {
    SalidaEstandar.consola("Computadora: " + numComputadora);
    SalidaEstandar.consola(", ram: " + ram + "GB");
    SalidaEstandar.consola(", disco duro: " + discoDuro + "GB");
    SalidaEstandar.consola(", procesador: " + procesador);
    SalidaEstandar.consola(", marca: " + marca + "\n");
    if (aplicaciones.vacia()) {
      SalidaEstandar.consola("No hay aplicaciones instaladas\n");
    } else {
      SalidaEstandar.consola("Aplicaciones instaladas: ");
      aplicaciones.imprimir();
      SalidaEstandar.consola("\n");
    }
  }

  /**
   * Determina si la computadora tiene una aplicacion especifica.
   * @param nombreAplicacion Es la aplicacion a buscar.
   * @return Regresa true si contiene la aplicacion.
   */
  public boolean contieneAplicacion(String nombreAplicacion) {
    return (aplicaciones.buscar(nombreAplicacion) != null);
  }

  /**
   * Agrega una aplicacion a la computadora.
   * @param aplicacion Es la aplicacion a agregar.
   * @return Regresa true si logro agregar la aplicacion.
   */
  public boolean agregarAplicacion(Aplicacion aplicacion) {
    if (aplicaciones.buscar(aplicacion) != null) {
      return false;
    }
    return aplicaciones.agregar(aplicacion) != -1;
  }

  /**
   * Elimina una aplicacion de la computadora.
   * @param nombreAplicacion Es la aplicacion a borrar.
   * @return Regresa true si logro eliminar la aplicacion.
   */
  public boolean eliminarAplicacion(String nombreAplicacion) {
    return aplicaciones.eliminar(nombreAplicacion) != null;
  }

  /**
   * Determina si la computadora puede correr una aplicacion instalada.
   * @param nombreAplicacion Es la aplicacion a comprobar.
   * @return Regresa true si puede correr la aplicacion.
   */
  public boolean correAplicacion(String nombreAplicacion) {
    if (contieneAplicacion(nombreAplicacion)) {
      Aplicacion aplicacion = (Aplicacion) aplicaciones.buscar(nombreAplicacion);
      return (ram >= aplicacion.getRamMin());
    }
    return false;
  }

  /**
   * Determina si la computadora tiene aplicaciones que requieran como minimo la ram indicada.
   * @param ramMin Es la ram minima.
   * @return Regresa true si la computadora contiene aplicaciones que requieran como minimo la ram indicada.
   */
  public boolean contieneAplicacionRamMinima(float ramMin) {
    Aplicacion aplicacion;
    aplicaciones.inicializarIterador();
    while (aplicaciones.hayMas()) {
      aplicacion = (Aplicacion) aplicaciones.obtenerSigiuente();
      if (aplicacion.getRamMin() >= ramMin) {
        return true;
      }
    }
    return false;
  }

  /**
   * Agrega un registro de uso a la computadora.
   * @param usoComputadora Es el uso registrado.
   * @return Regresa true si logro registrar el uso.
   */
  public boolean agregarRegistroUso(UsoComputadora usoComputadora) {
    if (usos.buscar(usoComputadora) != null) {
      return false;
    }
    return usos.agregar(usoComputadora) != -1;
  }

  /**
   * Obtiene los usuarios que han usado la computadora.
   * @return Regresa la lista de los usuarios que han usado la computadora.
   */
  public ListaLigada obtenerUsuarios() {
    ListaLigada usuarios = new ListaLigada();
    UsoComputadora uso = null;
    usos.inicializarIterador();
    while (usos.hayMas()) {
      uso = (UsoComputadora) usos.obtenerSigiuente();
      usuarios.agregar(uso.getUsuario());
    }
    return usuarios;
  }

  /**
   * Obtiene los usos de un usuario especifico en la computadora.
   * @param numUsuario Es el usuario del cual se buscan sus usos.
   * @return Regresa una ListaLigada con los usos del usuario.
   */
  public ListaLigada obtenerUsosUsuario(int numUsuario) {
    ListaLigada usosUsuario = new ListaLigada();
    UsoComputadora uso = null;
    usos.inicializarIterador();
    while (usos.hayMas()) {
      uso = (UsoComputadora) usos.obtenerSigiuente();
      if (uso.getUsuario().getNumUsuario() == numUsuario) {
        usosUsuario.agregar(uso);
      }
    }
    return usosUsuario;
  }

  public String toString() {
    return "Computadora " + numComputadora;
  }
}
