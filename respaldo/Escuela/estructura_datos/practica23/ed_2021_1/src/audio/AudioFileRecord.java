package audio;

import java.io.*;

import audio.wavfile.*;
import edlineal.ArregloNumerico;
import entradasalida.SalidaEstandar;

/**
 * Esta clase se encarga de manejar operaciones con el buffer de archivos de audio wav
 */
public class AudioFileRecord {
    private long numFrames;  //numero de tramas, número de muestras totales del archivo por canal
    private long sampleRate; //numero de muestras por segundo a la que se discretiza la señal
    private int numChannels; //número de canales
    private double[] buffer; //audio original
    private ArregloNumerico buffer2; //audio modificado
    private String archivo;   //nombre de archivo dado por el usuario
    private WavFile wavFileR; //apuntador de archivo leido
    private WavFile wavFileW;  //apuntador de archivo a escribir
    private String nomArchivo; //nombre archivo (uno.wav)
    private String nomRuta;    //ruta donde esta guardado el archivo (/home)
    private int validBits;     //bits usados para la discretización

    /**
     * Crea una nueva instancia que recibe el nombre del archivo de audio.
     * @param archivo Es el archivo de audio a cargar.
     */
    public AudioFileRecord(String archivo) {
        this.archivo = archivo;
        // Abre el archivo wav y asigna parámetros a las variables
        try {
            File file = new File(archivo);
            wavFileR = WavFile.openWavFile(file);
            nomArchivo = file.getName();
            nomRuta = file.getParent();
        } catch (Exception e) {

        }
        numChannels = wavFileR.getNumChannels();
        numFrames = wavFileR.getNumFrames();
        sampleRate = wavFileR.getSampleRate();
        validBits=wavFileR.getValidBits();
    }

    /**
     * Lee el archivo de audio.
     */
    public void leerAudio() {
        try {

            // Muestra datos del archivo
            wavFileR.display();

            // Crea buffer de origen y de temporal
            buffer = new double[(int) numFrames * numChannels];
            buffer2 = new ArregloNumerico(buffer.length);

            // Lee tramas totales
            wavFileR.readFrames(buffer, (int) numFrames);

            // Recorrer todas las tramas del archivo y guardarlas en el arreglo.
            buffer2.recibirArreglo(buffer);

            // Cierra archivo
            wavFileR.close();
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    /**
     * Aplica los cambios hechos en el buffer.
     */
    public void escribirAudio() {
        try {

            //Crear el apuntador al archivo para guardar datos del temporal
            File file = new File(nomRuta + "/2_" + nomArchivo);

            // Creamos un nuevo archivo de audio con los mismos datos que el original
            wavFileW = WavFile.newWavFile(file, numChannels, numFrames, validBits, sampleRate);

            // Escribimos los frames o muestras totales del temporal
            Object[] bufferTmp = buffer2.leerArreglo();
            double[] bufferDouble = new double[bufferTmp.length];
            for (int i = 0; i < bufferTmp.length; i++) {
                bufferDouble[i] = (double) bufferTmp[i];
            }
            wavFileW.writeFrames(bufferDouble, (int) numFrames);

            // Cerramos el archivo
            wavFileW.close();
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    /**
     * Incrementa el volumen del archivo de audio.
     * @param intensidad Es la cantidad a incrementar.
     */
    public void subirVolumen(int intensidad) {
        double incrementoAjustado = ((double)intensidad)/25.0;
        for (int i = 0; i < buffer2.numElementos(); i++) {
            double n = (double) buffer2.obtener(i);
            if (n > 0) {
                n += incrementoAjustado;
            }
            if (n < 0){
                n -= incrementoAjustado;
            }
            buffer2.cambiar(i, n);
        }
    }

    /**
     * Decrementa el volumen del archivo de audio.
     * @param intensidad Es la cantidad a decrementar.
     */
    public void bajarVolumen(int intensidad) {
        double decrementoAjustado = ((double)intensidad)/25.0;
        for (int i = 0; i < buffer2.numElementos(); i++) {
            double n = (double) buffer2.obtener(i);
            if (n > 0) {
                if (n < decrementoAjustado) {
                    n = 0;
                } else {
                    n -= decrementoAjustado;
                }
            }

            if (n < 0) {
                if (n > -decrementoAjustado) {
                    n = 0;
                } else {
                    n += decrementoAjustado;
                }
            }
            buffer2.cambiar(i, n);
        }
    }

    /**
     * Acelera la velocidad del archivo de audio.
     * @param multiplicador Es el multiplicador de que tanto aumentara la velocidad.
     */
    public void acelerar(int multiplicador) {
        int residuo = buffer2.numElementos() % multiplicador;
        ArregloNumerico acelerado;
        if (residuo == 0) {
            acelerado = new ArregloNumerico(buffer2.numElementos()/multiplicador);
        } else {
            acelerado = new ArregloNumerico((buffer2.numElementos()/multiplicador)+1);
        }
        for (int i = multiplicador-1; i < buffer2.numElementos(); i+=multiplicador) {
            double promedio = 0.0;
            for (int v = i-(multiplicador-1); v <= i; v++) {
                promedio += (double) buffer2.obtener(v);
            }
            promedio /= (double) multiplicador;
            acelerado.agregar(promedio);
        }


        if (residuo != 0) {
            double promedio = 0.0;
            for (int i = buffer2.numElementos()-residuo; i < buffer2.numElementos(); i++) {
                promedio += (double) buffer2.obtener(i);
            }
            promedio /= (double) residuo;
            acelerado.agregar(promedio);
        }

        buffer2.vaciar();
        buffer2.agregarLista(acelerado);
        numFrames = buffer2.numElementos()/numChannels;
    }

    /**
     * Disminuye la velocidad del archivo de audio.
     * @param multiplicador Es el multiplicador que indica que tanto disminuira la velocidad.
     */
    public void retrasar(int multiplicador) {
        ArregloNumerico retrasado = new ArregloNumerico(buffer2.numElementos()*multiplicador);
        for (int i = 0; i < buffer2.numElementos()-1; i++) {
            double count = 0.0;
            double incremento = (double) buffer2.obtener(i+1) - (double) buffer2.obtener(i);
            incremento /= multiplicador;
            for (int v = i*multiplicador; v < (i*multiplicador)+multiplicador; v++) {
                retrasado.agregar((double) buffer2.obtener(i) + incremento*count);
                count++;
            }
        }

        for (int i = 0; i < multiplicador; i++) {
            retrasado.agregar((double) buffer2.obtener(buffer2.numElementos()-1));
        }

        buffer2.vaciar();
        buffer2.redimensionar(retrasado.numElementos());
        buffer2.agregarLista(retrasado);
        numFrames = buffer2.numElementos()/numChannels;
    }

    /**
     * Elimina del archivo de audio los fragmentos con sonido bajo.
     */
    public void eliminarSilencio() {
        double minimo = 0.03;
        ArregloNumerico recorrido = new ArregloNumerico(buffer2.numElementos());

        for (int i = 0; i < buffer2.numElementos(); i++) {
            double n = (double) buffer2.obtener(i);
            if (!(n > -minimo && n < minimo)) {
                recorrido.agregar(n);
            }
        }
        buffer2.vaciar();
        buffer2.agregarLista(recorrido);
        numFrames = buffer2.numElementos()/numChannels;
    }

    /**
     * Invierte el eje X del archivo de audio.
     */
    public void invertirEjeX() {
        buffer2.invertir();
    }

    /**
     * Invierte el eje Y del archivo de audio.
     */
    public void invertirEjeY() {
        buffer2.porEscalar(-1);
    }
}
