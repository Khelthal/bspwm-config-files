package analisis.cadenas;

import entradasalida.EntradaConsola;
import entradasalida.SalidaEstandar;
import registros.diccionarios.RegistroPalabras;

/**
 * Esta clase se encarga de recibir cadenas y mostrar errores en las mismas.
 */
public class MenuCorrectorTexto {
    /**
     * Inicia el menu
     */
    public void iniciar() {
        while (true) {
            imprimirMenu();
            SalidaEstandar.consola("\n");
            seleccionarOpcion();
            SalidaEstandar.consola("Presiona enter para continuar\n");
            EntradaConsola.consolaCadena();
        }
    }

    /**
     * Imprime las opciones del menu.
     */
    public void imprimirMenu() {
        SalidaEstandar.consola("¿Que desea hacer?\n");
        SalidaEstandar.consola("a) Analizar una cadena de texto.\n");
        SalidaEstandar.consola("b) Analizar un archivo de texto.\n");
        SalidaEstandar.consola("q) Salir.\n");
    }

    /**
     * Recibe la entrada del usuario para elegir una de las opciones.
     */
    public void seleccionarOpcion() {
        String respuesta = EntradaConsola.consolaCadena();
        switch (respuesta) {
            case "a":
                SalidaEstandar.consola("Escriba la cadena de texto a analizar\n:");
                CorrectorTexto.buscarErroresCadena(EntradaConsola.consolaCadena(), true);
                break;
            case "b":
                SalidaEstandar.consola("Escriba el nombre del archivo a analizar\n:");
                String archivo = EntradaConsola.consolaCadena();
                SalidaEstandar.consola("Escriba el numero de lineas del archivo a analizar\n:");
                int lineas = EntradaConsola.consolaInt();
                CorrectorTexto.buscarErroresArchivo(archivo, lineas);
                break;
            case "q":
                System.exit(0);
        }
    }

}
