﻿using System;
using System.Threading;

namespace Hilos2
{
  class Program {
    public static int CalculoPesado(int incremento) {
      int result = 0;
      for (int i = 0; i < 1000000000; i++) {
        result += incremento;
      }

      return result;
    }

    public static void Main(String[] args) {
      int result = 0;
      Thread t = new Thread(() => result = CalculoPesado(-1));
      t.Start();
      for (int i = 0; i < 10; i++) {
        Console.WriteLine(result);
      }
      Console.WriteLine("Esperando a que termine la ejecucion del hilo paralelo");
      t.Join();
      Console.WriteLine("Resultado final: {0}", result);
    }
  }
}
