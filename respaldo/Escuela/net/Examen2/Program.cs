﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace TareasULAM
{
    class Program
    {

        const int NumIteraciones = 35;

        static void Main(string[] args)
        {
            //clase para medir tiempo de ejecución
            Stopwatch ts = Stopwatch.StartNew();

            //método que hacer el calculo secuencial
            ts.Stop();
            Stopwatch ttask = Stopwatch.StartNew();

            //método
            ttask.Stop();

            //método con 32 hilos
            Stopwatch tthread = Stopwatch.StartNew();
            LlamaThreadsULAM();
            tthread.Stop();

            Console
                .WriteLine("El tiempo de ejecución método secuencial es {0}ms!",
                ts.ElapsedMilliseconds);

            Console
                .WriteLine("El tiempo de ejecución método de tareas es {0}ms!",
                ttask.ElapsedMilliseconds);
            Console
                .WriteLine("El tiempo de ejecución método de hilos es {0}ms!",
                tthread.ElapsedMilliseconds);
        }

        static void LlamaSecuencial()
        {
            long sumatotal = 0;

            //llama 35 veces
            for (int i = 0; i < NumIteraciones; i++)
            {
                sumatotal += ULAM();
            }
            Console
                .WriteLine("La suma total de todas las series {0}", sumatotal);
        }

        static void LlamaTaksULAM()
        {
            long sumatotal = 0;
            Task<long>[] tareas = new Task<long>[NumIteraciones];
            for (int i = 0; i < NumIteraciones; i++)
            {
                tareas[i] = Task.Run(() => ULAM());
            }
            Task.WaitAll (tareas);
            foreach (var tarea in tareas)
            {
                sumatotal += tarea.Result;
            }

            Console
                .WriteLine("La suma total de todas las series {0}", sumatotal);
        }

        static void LlamaThreadsULAM()
        {
          long calculado = 0;
          long[] numeros = new long[35];

          for (int i = 0; i < 35; i++) {
            numeros[i] = 0;
          }
          Thread[] hilos = new Thread[35];

          for (int v = 0; v < hilos.Length; v++) {
            hilos[v] = new Thread(() => numeros[v] = ULAM());
            hilos[v].Start();
            Console.WriteLine("Hilo {0} iniciado", v);
          }

          for (int i = 0; i< hilos.Length; i++) {
            hilos[i].Join();
            calculado += numeros[i];
          }

          Console.WriteLine("La suma total de todas las series {0}", calculado);
        }

        //método de la serie TareasULAM
        static long ULAM()
        {
            long suma = 0;
            var numero = Int32.MaxValue >> 4;
            int[] serie = new int[1];
            if (numero > 0)
            {
                while (numero != 1)
                {
                    if (numero % 2 == 0)
                        numero /= 2;
                    else
                        numero = numero * 3 + 1;
                    serie[serie.Length - 1] = numero;
                    Array.Resize(ref serie, serie.Length + 1);
                }
            }
            for (int i = 0; i < 10000000; i++) {

            }
            for (int i = 0; i < serie.Length; i++)
            {
                suma += serie[i];
            }
            Console.WriteLine("Ya acabe :V");
            return suma;
        }
    }
}
