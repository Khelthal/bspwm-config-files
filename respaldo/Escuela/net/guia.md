Para compilacion

#Para compilar un archivo con Main
csc Archivo.cs

#Para compliar un archivo sin Main
csc /t:module Archivo.cs

#Para compilar un archivo con Main que hace referencia a un archivo sin main
csc /t:module ArchivoSinMain.cs
csc /addmodule:ArchivoSinMain.netmodule ArchivoConMain.cs

#Para crear un dll
csc /target:library /out:Archivo.dll Archivo.cs /keyfile:firArchivo.snk

#Para compilar un archivo con Main que requiere de un dll
csc /referece:ArchivoSinMain.dll Archivo.cs

#############################################################################

#Para añadir al ensamblado global
gacutil -i Archivo.dll

#Para eliminar un archivo del ensamblado global
gacutil -u Archivo.dll


#############################################################################

#Para crear un classlib
dotnet new classlib

#Para agregar una referencia a un proyecto
dotnet add referece *ruta del proyecto*

#############################################################################

Tipos de acceso

- Public
- Private
- Protected
- Internal
- Protected Internal

Tipos de datos primitivos C#

Integer:
- sbyte
- byte
- short
- ushort
- int
- uint
- long
- ulong

Real floating-point:
- float
- double

Real with decimal precision:
- decimal

Boolean:
- bool

Character:
- char

String:
- string

Object:
- object
