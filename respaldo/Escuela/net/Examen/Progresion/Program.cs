﻿using System;

namespace Progresion {
    class Program {
        static void Main(string[] args) {
            object[][] sucesiones = new object[1][];
            int last = 0;
            while (true) {
                sucesiones[last] = GenerarFila();
                ImprimirSucesion(sucesiones[last]);

                Console.WriteLine("¿Desea generar otra sucesion? (s/n)");
                if (Console.ReadLine() == "s") {
                    last++;
                    object[][] resize = new object[last+1][];
                    for (int i = 0; i < sucesiones.Length; i++) {
                        resize[i] = sucesiones[i];
                    }
                    sucesiones = resize;
                }
                else {
                    break;
                }
            }

            Console.WriteLine("\nSucesiones generadas");

            for (int i = 0; i < sucesiones.Length; i++) {
                ImprimirSucesion(sucesiones[i]);
                Console.WriteLine();
            }
        }

        static object[] GenerarFila() {
            object[] sucesion;
            Console.WriteLine("Ingrese el nombre del alumno");
            string nombre = Console.ReadLine();
            Console.WriteLine("Ingrese el numero inicial a1");
            double a1 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Ingrese el valor de la razon r");
            double razon = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Ingrese el numero de valores que desea obtener (n)");
            int n = Convert.ToInt32(Console.ReadLine());
            sucesion = new object[n+2];
            sucesion[0] = nombre;
            sucesion[1] = razon;

            for (int i = 2; i < sucesion.Length; i++) {
                sucesion[i] = a1*(Math.Pow(razon, i-2));
            }

            return sucesion;
        }

        public static void ImprimirSucesion(object[] sucesion) {
            Console.WriteLine("Alumno: {0}", sucesion[0]);
            Console.WriteLine("Razon: {0}", sucesion[1]);
            for (int i = 2; i < sucesion.Length; i++) {
                Console.Write("{0}{1}", sucesion[i], (i == sucesion.Length-1)?"\n":", ");
            }
        }
    }
}
