﻿using System;

namespace AlgebraMatricial
{
    class Program
    {
        static void Main(string[] args)
        {
            double[][] matrizA = {
                new double[] {5, 3, -4, -2},
                new double[] {8, -1, 0, -3}
            };

            double[][] matrizB = {
                new double [] {1, 4, 0},
                new double [] {-5, 3, 7},
                new double [] {0, -9, 5},
                new double [] {5, 1, 4}
            };

            double[][] resultado = MultiplicarMatrices(matrizA, matrizB);

            for (int i = 0; i < resultado.Length; i++) {
                for (int j = 0; j < resultado[0].Length; j++) {
                    Console.Write(resultado[i][j]+" ");
                }
                Console.WriteLine();
            }
        }

        public static double[][] MultiplicarMatrices(double[][] matrizA, double[][] matrizB) {
            if (matrizA[0].Length != matrizB.Length) {
                return null;
            }


            double[][] resultado = new double[matrizA.Length][];
            for (int i = 0; i < resultado.Length; i++) {
                resultado[i] = new double[matrizB[0].Length];
            }
            

            for (int renglonA = 0; renglonA < matrizA.Length; renglonA++) {
                for (int columnaB = 0; columnaB < matrizB[0].Length; columnaB++) {
                    double suma = 0.0;
                    for (int columnaA = 0; columnaA < matrizA[0].Length; columnaA++) {
                        suma += matrizA[renglonA][columnaA] * matrizB[columnaA][columnaB];
                    }
                    resultado[renglonA][columnaB] = suma;
                }
            }

            return resultado;
        }
    }
}
