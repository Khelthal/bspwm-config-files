using System;
using System.IO;
namespace Matrices {
    public class Utils {
        public static double[][] MatLoad(string fn, int nRows, char sep) {
            double[][] result = new double[nRows][];
            string line = "";
            string[] tokens = null;
            FileStream ifs = new FileStream(fn, FileMode.Open);
            StreamReader sr = new StreamReader(ifs);

            int i = 0;
            while ((line = sr.ReadLine()) != null) {
                if (line.StartsWith("//") == true) {
                    continue;
                }
                tokens = line.Split(sep);
                result[i] = new double[tokens.Length];

                for (int j = 0; j < tokens.Length; j++) {
                    result[i][j] = double.Parse(tokens[j]);
                }
                ++i;
            }
            sr.Close();
            ifs.Close();

            return result;
        }

        public static void MatShow(double[][] d) {
            for (int i = 0; i < d.Length; i++) {
                for (int j = 0; j < d[i].Length; j++) {
                    Console.Write("{0} ", d[i][j]);
                }
                Console.WriteLine();
            }
        }
    }

    class MySort {
        public static void BubbleSort(double[] arr) {
            double temp;
            for (int j = 0; j <= arr.Length - 2; j++) {
                for (int i = 0; i <= arr.Length - 2; i++) {
                    if (arr[i] > arr[i + 1]) {
                        temp= arr[i + 1];
                        arr[i + 1] = arr[i];
                        arr[i] = temp;
                    }
                }
            }
        }

        public static void DeleteDuplicated(double[][] arr) {
            double temp;
            int diferentes;
            for (int i = 0; i < arr.Length; i++) {
                temp = arr[i][0];
                diferentes = 1;
                for (int j = 1; j < arr[i].Length; j++) {
                    if (arr[i][j] == temp) {
                        arr[i][j] = -100.00;
                    } else {
                        temp = arr[i][j];
                        diferentes++;
                    }
                }

                if (diferentes != arr[i].Length) {
                    double[] aux = new double[diferentes];
                    int pos = 0;
                    for (int j = 0; j < arr[i].Length; j++) {
                        if (arr[i][j] != -100.00) {
                            aux[pos] = arr[i][j];
                            pos++;
                        }
                    }
                    arr[i] = aux;
                }
            }
        }
   } 

    class MatricesProgram {
        static void Main(string[] args) {
            Console.WriteLine("\nCargando la matriz con el archivo datos.tvs.txt \n");
            string fn = "datos.tvs.txt";
            double[][] d = Utils.MatLoad(fn, 4, '\t');

            for (int i = 0; i < d.Length; i++) {
                MySort.BubbleSort(d[i]);
            }

            MySort.DeleteDuplicated(d);

            for (int i = 0; i < d.Length; i++) {
                MySort.BubbleSort(d[i]);
            }

            Utils.MatShow(d);

            Console.WriteLine("\nFin de la aplicacion");
        }
    }
}
