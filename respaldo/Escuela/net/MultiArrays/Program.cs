﻿using System;
using System.Collections;

namespace MultiArrays {
    public class Program {
        static int[] ArreUni = new int[10];
        static int[,]ArrBidi = {
            {1,2,3},
            {4,5,6},
            {7,8,9}
        };

        static int [,,] ArreTridi = {
            {
                {1, 2},
                {3, 5},
                {5, 6}
            },
            {
                {10, 20},
                {30, 40},
                {50, 60}
            }
        };

        static string cadena = "Hola Mundo";

        static int [][] array_of_array = {
            new int[]{1,2,3},
            new int[]{10,20,30,40,50,60,70,80}
        };

        static ArrayList Lista = new ArrayList();

        public static void Main(string[] args) {
            for (int i = 0; i < ArreUni.Length; i++) {
                ArreUni[i] = i+1;
            }
            Lista.Add("Hola");
            Lista.Add(23);
            Lista.Add(3.5);

            Array.Resize<int>(ref ArreUni,15);
            Console.WriteLine("Arreglo Uni");
            for (int i = 0; i < ArreUni.Length; i++) {
                Console.WriteLine("Arreglo[{0}]={1}",i,ArreUni[i]);
            }

            Console.WriteLine("Arreglo Bidimensional");
            for (int i = 0; i < ArrBidi.GetLength(0); i++) {
                for (int j = 0; j < ArrBidi.GetLength(1); j++) {
                    Console.WriteLine("Matriz[{0},{1}]={2}",i, j, ArrBidi[i,j]);
                }
            }

            Console.WriteLine("Tridimensional");

            for (int i = 0; i < ArreTridi.GetLength(0); i++) {
                for (int j = 0; j < ArreTridi.GetLength(1); j++) {
                    for (int v = 0; v < ArreTridi.GetLength(2); v++) {
                        Console.WriteLine("Matriz[{0},{1},{2}]={3}",i, j, v, ArreTridi[i,j,v]);
                    }
                }
            }

            Console.WriteLine("Arreglo de arreglos");
            for (int i = 0; i < array_of_array.Length; i++) {
                for (int j = 0; j < array_of_array[i].Length; j++) {
                    Console.Write("{0}{1}",array_of_array[i][j], j==(array_of_array[i].Length-1)?"\n":", ");
                }
            }


            Console.WriteLine("Imprimir coleccion");
            foreach (Object elemento in Lista) {
                Console.WriteLine(elemento);
            }
        }
    }
}
