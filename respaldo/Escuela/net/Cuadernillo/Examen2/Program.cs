﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace TareasULAM
{
    class Program
    {
        const int NumIteraciones = 35;

        static void Main(string[] args)
        {
          /*
            //clase para medir tiempo de ejecución
            Stopwatch ts = Stopwatch.StartNew();

            //método que hacer el calculo secuencial
            LlamaSecuencial();
            ts.Stop();
            Stopwatch ttask = Stopwatch.StartNew();

            //método
            LlamaTaksULAM();
            ttask.Stop();

            Console
                .WriteLine("El tiempo de ejecución método secuencial es {0}ms!",
                ts.ElapsedMilliseconds);
            Console
                .WriteLine("El tiempo de ejecución método de tareas es {0}ms!",
                ttask.ElapsedMilliseconds);
                */
          Geometrica(3, 2, 5);
        }

        static void LlamaSecuencial()
        {
            long sumatotal = 0;

            //llama 35 veces
            for (int i = 0; i < NumIteraciones; i++)
            {
                Console.WriteLine("\n La serie[{0}]", i);
                sumatotal += ULAM();
            }
            Console
                .WriteLine("La suma total de todas las series {0}", sumatotal);
        }

        static void LlamaTaksULAM()
        {
            long sumatotal = 0;
            Task<long>[] tareas = new Task<long>[NumIteraciones];
            for (int i = 0; i < NumIteraciones; i++)
            {
                Console.WriteLine("\n La serie[{0}]", i);
                tareas[i] = Task.Run(() => ULAM());
            }
            Task.WaitAll (tareas);
            foreach (var tarea in tareas)
            {
                sumatotal += tarea.Result;
            }

            Console
                .WriteLine("La suma total de todas las series {0}", sumatotal);
        }

        //método de la serie TareasULAM
        static long ULAM()
        {
            long suma = 0;
            var numero = Int32.MaxValue >> 4;
            int[] serie = new int[1];
            if (numero > 0)
            {
                while (numero != 1)
                {
                    if (numero % 2 == 0)
                        numero /= 2;
                    else
                        numero = numero * 3 + 1;
                    serie[serie.Length - 1] = numero;
                    Array.Resize(ref serie, serie.Length + 1);
                }
            }
            for (int i = 0; i < serie.Length; i++)
            {
                Console.Write("{0} - ", serie[i]);
                suma += serie[i];
            }
            return suma;
        }

        static void Geometrica(int a1, int razon, int n)
        {
            object[] sucesion = new object[n];
            sucesion = new object[n+2];
            for (int i = 0; i < sucesion.Length; i++) {
                sucesion[i] = a1*(Math.Pow(razon, i-2));
            }

            Console.WriteLine("Serie generada");
            for (int i = 2; i < sucesion.Length; i++) {
                Console.Write("{0}{1}", sucesion[i], (i == sucesion.Length-1)?"\n":", ");
            }
        }
    }
}
