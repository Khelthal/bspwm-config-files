namespace OrdenacionSimultanea {
  class InsertionSort {
    public static void Sort(decimal[] arreglo) {
      for (int i = 1; i < arreglo.Length; i++) {
        for (int j = i; j >= 1; j--) {
          if (arreglo[j-1] > arreglo[j]) {
            decimal temp = arreglo[j];
            arreglo[j] = arreglo[j-1];
            arreglo[j-1] = temp;
          } else {
            break;
          }
        }
      }
    }
  }
}
