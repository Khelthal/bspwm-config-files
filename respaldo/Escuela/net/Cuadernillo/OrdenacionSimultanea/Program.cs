﻿using System;
using System.Threading;
using SourceClassLibrary;

namespace OrdenacionSimultanea
{
  class Program {

    public static void Main() {
      Generador t1 = new Generador(4,5,7,8);
      Generador t2 = new Generador(2,4,6,7);
      Generador main = new Generador(3,5,8,9);

      Thread hilo1 = new Thread(() => GenerarEnHilo(t1));
      Thread hilo2 = new Thread(() => GenerarEnHilo(t2));

      hilo1.Start();
      hilo2.Start();
      GenerarEnHilo(main);

      hilo1.Join();
      hilo2.Join();
    }

    public static void GenerarEnHilo(Generador gm) {
      gm.Generar(2000);
      InsertionSort.Sort(gm.ArreSerie);
      gm.Mostrar();
    }
  }
}
