using System;
namespace SentenciaIF
{
  class Program
   {
    static void Main(string[] args)
      {
        //declaración y asignación de variables 
        int primero= 2;
        int segundo= 0;
        //usar la sentencia if para evaluar una condición y obtener la salida 
        Console.WriteLine("Una sentencia if sencilla");
        if (primero == 2)
         {
           Console.WriteLine("La sentencia If se evaluó como verdadera");
         }
        else {
           Console.WriteLine("La sentencia If se evaluó como falsa");
        }
        Console.WriteLine("Esta línea de salida es independiente de la sentencia If");
        Console.WriteLine();
      //El siguiente if evalua dos condiciones y las ejecuta si ambas son verdaderas 

        Console.WriteLine("Una sentencia If suando el operador &&.");
        if (primero == 2 && segundo == 0)
           {
            Console.WriteLine("La sentencia If con operador && se evaluó como verdadera");
           }
        else {
            Console.WriteLine("La sentencia If con operador && se evaluó como falsa");
        }
 	Console.WriteLine("Esta línea de salida es independiente de la sentencia If \n");
 	
 	//se crea un if anidado
	Console.WriteLine("Sentencias anidadas de if.");
         if (primero == 2)
          {
            if (segundo == 0)
               {
                Console.WriteLine("Tanto las condiciones externas como las internas son verdaderas.");
               }
            else {
                Console.WriteLine("Tanto las condiciones externas como las internas son falsas.");
            }
            Console.WriteLine("La sentencia if externa es verdadera, la sentencia if interna puede ser verdadera.");
          }
         else {
            Console.WriteLine("La sentencia if externa es verdadera, la sentencia if interna puede ser falsa.");
         }
        Console.WriteLine("Esta línea de salida es independiente de la sentencia If");
        Console.WriteLine();
    }
 }
}
