﻿using System;
using SourceClassLibrary;

namespace PruebasEstadisticas {
    public class Program {
        static void Main(string[] args)
        {
            Generador gm = new Generador(4,5,7,8);
            gm.Generar();
            PruebaPromedio(gm.ArreSerie);
            PruebaFrecuencias(gm.ArreSerie, 5);
        }
        
        static void PruebaPromedio(decimal[] Npsedo){
            double xmedia = 0.0;
            double suma = 0.0;
            double zo;
            double n;
            for (n = 0; n < Npsedo.Length; n++){
                suma = suma + decimal.ToDouble(Npsedo[Convert.ToUInt32(n)]);
            }    
            xmedia = suma / n;
            zo = Math.Abs((xmedia-1.0/2.0) * Math.Sqrt(n) / Math.Sqrt(1.0/12.0));
            if (zo < 1.96){
                Console.WriteLine("Zo {0} si pasa las pruebas de promedios", zo);
            }else{
                Console.WriteLine("Zo {0} no pasa las pruebas de promedios", zo);
                    
            }
        }

        static void PruebaFrecuencias(decimal[] npsedo, int n) {
            double sumatoria = 0.0;
            decimal FE = (decimal) npsedo.Length / n;
            for (int i = 0; i < npsedo.Length; i++) {
                double a = Convert.ToDouble((npsedo[i]-FE));
                a = Math.Pow(a, 2);
                a = a / (double) FE;
                sumatoria += a;
            }

            if (sumatoria < 9.49) {
                Console.WriteLine("Xo {0} si pasa las pruebas de frecuencias", sumatoria);
            } else {
                Console.WriteLine("Xo {0} no pasa las pruebas de frecuencias", sumatoria);
            }
        }
    }
}
