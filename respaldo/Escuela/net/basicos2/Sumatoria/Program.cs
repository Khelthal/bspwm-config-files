﻿using System;

namespace Sumatoria {
    public class Program {
        public static void Main(string[] args) {
            long sumatoria = 0, limite;

            limite = Convert.ToInt64(Console.ReadLine());

            for (int i = 0; i <= limite; i += 5) {
                sumatoria += i;
            }

            Console.WriteLine(sumatoria);            
        }
    }
}
