﻿using System;

namespace Potencias {
    class Program {
        static void Main(string[] args) {
            int n;
            long suma = 0;
            Console.WriteLine("Ingrese el valor de n");
            n = Convert.ToInt32(Console.ReadLine());
            for (int i = 1; i <= n; i++) {
                suma += i*i;
            }

            Console.WriteLine("La suma de potencias sucesivas desde 1 hasta {0} es: {1}",n,suma);
        }
    }
}
