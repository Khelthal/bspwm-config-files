﻿using System;

namespace Naturales {
    public class Program {
        public static void Main(string[] args) {
            int n, contador=0;
            string cadena;
            Console.WriteLine("Ingrese el ultimo numero que desea mostrar");
            cadena = Console.ReadLine();
            n = Convert.ToInt32(cadena);
            contador = 1;
            for (int i = 1; i <= n; i++) {
                for (int j = 0; j < i; j++) {
                    string numero = Convert.ToString(contador);
                    string completo = "";
                    for (int v = 0; v < cadena.Length - numero.Length; v++) {
                        completo += "0";
                    }
                    completo += numero;
                    Console.Write(completo);
                    contador++;
                    if (contador > n) {
                        break;
                    }
                }
                Console.WriteLine();

                if (contador > n) {
                    break;
                }
            }
        }
    }
}
