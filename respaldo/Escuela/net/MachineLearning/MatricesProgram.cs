using System;
using System.IO;
namespace Matrices {
    public class Utils {
        public static double[] VecCreate(int n, double val = 0.0) {
            double[] result = new double[n];
            for (int i = 0; i < n; ++i) {
                result[i] = val;
            }
            return result;
        }

        public static void VecShow(double[] vec, int dec, int wid) {
            for (int i = 0; i < vec.Length; ++i) {
                Console.Write(vec[i].ToString("F"+dec).PadLeft(wid));

                Console.WriteLine();
            }
        }

        public static double[][] MatCreate(int rows, int cols) {
            double[][] result = new double[rows][];
            for (int i = 0; i < rows; i++) {
                result[i] = new double[cols];
            }
            return result;
        }
        
        public static void MatShow(double[][] mat, int dec, int wid) {
            int nRows = mat.Length;
            int nCols = mat[0].Length;
            for (int i = 0; i < nRows; i++) {
                for (int j = 0; j < nCols; j++) {
                    double x = mat[i][j];
                    Console.Write(x.ToString("F" + dec).PadLeft(wid));
                }
            }
            Console.WriteLine();
        }

        public static double[][] MatLoad(string fn, int nRows, int[] cols, char sep) {
            int nCols = cols.Length;
            double[][] result = MatCreate(nRows, nCols);
            string line = "";
            string[] tokens = null;
            FileStream ifs = new FileStream(fn, FileMode.Open);
            StreamReader sr = new StreamReader(ifs);

            int i = 0;
            while ((line = sr.ReadLine()) != null) {
                if (line.StartsWith("//") == true) {
                    continue;
                }
                tokens = line.Split(sep);

                for (int j = 0; j < nCols; j++) {
                    int k = cols[j];
                    result[i][j] = double.Parse(tokens[k]);
                }
                ++i;
            }
            sr.Close();
            ifs.Close();

            return result;
        }
    }

    class MatricesProgram {
        static void Main(string[] args) {
            Console.WriteLine("\nIniciando Vector y Matrices\n");

            Console.WriteLine("Creando y mostrando un vector con 4 celdas con el contenido de 3.5 \n");
            double[] v = Utils.VecCreate(4, 3.5);
            Utils.VecShow(v, 3, 6);

            Console.WriteLine("\nCreando y visualizando matriz de 4x3\n");
            double[][] m = Utils.MatCreate(3, 4);
            Utils.MatShow(m, 2, 6);

            Console.WriteLine("\nCargando la matriz de 4x3 con el archivo data_ml.tsv \n");
            string fn = "data_ml.tsv";
            double[][] d = Utils.MatLoad(fn, 4, new int[] {0, 1, 2}, '\t');
            Utils.MatShow(d, 1, 6);

            Console.WriteLine("\nFin de la aplicacion");
            Console.ReadLine();
        }
    }
}
