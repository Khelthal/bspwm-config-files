﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Threading.Tasks;

namespace TareasULAM
{
    class Program
    {
        const int numIteraciones=35;

        static void Main(string[] args)
        {
            Stopwatch ts=Stopwatch.StartNew();
            ts.Stop();
            Stopwatch ttask= Stopwatch.StartNew();
            LlamaTaskULAM();
            ttask.Stop();

            Console.WriteLine("El tiempo de ejecución método secuencial es {0}",ts.ElapsedMilliseconds);

            Console.WriteLine("El tiempo de ejecución método de tarea es {0}",ttask.ElapsedMilliseconds);



        }
        static void LlamaSecuencial(){
            long sumatoria=0;
            for (int i = 0; i < numIteraciones; i++)
            {
                Console.WriteLine("\n La serie[{0}]", i);
                sumatoria+=ULAM();
            }
            Console.WriteLine("La suma total de todas las series {0}", sumatoria);
        }

        static void LlamaTaskULAM(){
            long sumatorial = 0;
            Task<long>[] tareas = new Task<long>[numIteraciones];
            for (int i = 0; i < numIteraciones; i++)
            {
                Console.WriteLine("\n La serie[{0}]", i);
                tareas[i]=Task.Run(()=>ULAM());
            }
            Task.WaitAll(tareas);
            foreach (var tarea in tareas)
            {
                sumatorial+=tarea.Result;
            }
            Console.WriteLine("La suma total de todas las series {0}", sumatorial);
        }

        static long ULAM(){
            long suma=0;
            var numero = Int32.MaxValue >> 4;
            int[] serie = new int[1];
            if (numero>0){
                while (numero!=1)
                {
                    if (numero%2==0)
                    {
                        numero/=2;

                    }else
                    {
                        numero=numero*3+1;
                        serie[serie.Length-1]=numero;
                        Array.Resize(ref serie, serie.Length+1);

                    }
                }
                for (int i = 0; i < serie.Length; i++)
                {
                    Console.Write("{0} - ",serie[i]);
                    suma+=serie[i];
                }

            }
            return suma;
        }
    }
}
