﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace ArchivoTareas
{
    class Program
    {
        static void Main(string[] args)
        {
            int contadormain=0;
            Task<int> task=;

            for (int i = 0; i < 100000; i++)
            {
                contadormain+=i;
                task.Wait();
                var resultado=task.Result;
                Console.WriteLine("Contado main {0}", contadormain);
                Console.WriteLine("Contador = {0}",resultado);
            }
            
        }
        static async Task<int> LecturaArchivoAsync(){
            Console.WriteLine("Inicia metodo asincrono");
            String archivo=@"/home/elias/Documentos/Escuela/estructura_datos/practica21/ed_2021_1/src/calculos/x.txt";
            int contador=0;
            using (StringReader r=new StringReader (archivo)){
                string cad = await r.ReadToEndAsync();
                contador+=cad.Length;
                Console.WriteLine("{0},{1}",cad,cad.Length);
                for (int i = 0; i < 100000; i++)
                {
                    int codigo=cad.GetHashCode();
                    if(codigo==0){
                        contador--;
                    }
                }
            }
            Console.WriteLine("Termina metodo asincrono");
            return contador;
        }
    }
}
