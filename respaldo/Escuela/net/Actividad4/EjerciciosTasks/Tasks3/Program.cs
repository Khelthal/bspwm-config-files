﻿using System;
using System.Threading.Tasks;

namespace Tasks3
{
  class Program {
    public static void Main(string[] args) {
      Task<int> tarea = Task.Run(() => dameUnNumero());
      int i = tarea.Result;
      Console.WriteLine(i);
    }

    public static int dameUnNumero() {
      int x = 0;
      for (int i = 0; i< 100000000; i++) {
        x++;
      }

      return x;
    }
  }
}
