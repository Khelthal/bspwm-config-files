﻿using System;
using System.IO;

namespace Delegados2
{
  class Program
  {
    public delegate void ImprimeSaludo(string men);
    static FileStream Archivo;
    static StreamWriter Buffer;

    public static void ImprimePantalla(string s) {
      Console.WriteLine("Este es el mensaje {0}", s);
    }

    public static void ImprimeArchivo(string cad) {
      Archivo = new FileStream("/home/elias/Documentos/Escuela/net/MensajeSalida.txt", FileMode.Append, FileAccess.Write);
      Buffer = new StreamWriter(Archivo);
      Buffer.WriteLine(cad);
      Buffer.Flush();
      Buffer.Close();
      Archivo.Close();
    }

    public static void EnviarMensaje(ImprimeSaludo ip) {
      ip("Hola A todos");
    }

    static void Main(string[] args)
    {
      ImprimeSaludo imp1 = new ImprimeSaludo(ImprimePantalla);
      ImprimeSaludo imp2 = new ImprimeSaludo(ImprimeArchivo);
      EnviarMensaje(imp1);
      EnviarMensaje(imp2);
    }
  }
}
