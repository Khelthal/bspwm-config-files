﻿using System;

namespace CalculosGeometrico
{
    public class aRectangulo{
        private double altura;
        private double baseR;
        public aRectangulo():this(0.0,0.0){
        }
        public aRectangulo(double altura, double baseR){
            this.altura = altura;
            this.baseR = baseR;
        }

        public double AreaR(){
            return altura*baseR;
        }
        public void MuestraInfo(){
            Console.WriteLine("La altura es {0}", altura);
            Console.WriteLine("La base es {0}", altura);
            Console.WriteLine("para los valores es {0} y {1}, el area es {2}", altura, baseR, AreaR());
        }
    
    }
}
