using System;
namespace SourceClassLibrary{
  public class Generador{
   //declaraciones
    private int xo,a,c,xn,m;
    private decimal[] arreSerie=new decimal[1];
    //constructores
 public Generador():this(0,0,0,0){}
 public Generador(int xo,int a, int c, int m){
     this.xo=xo;
      this.a=a;
      this.c=c;
      this.m=m;
    }
    //propiedades
   public decimal[] ArreSerie{ get => arreSerie; set => arreSerie=value;}
   public int Xo {get=>xo; set=>xo=value;}
   public int A {get=>a; set=>a=value;}
   public int C {get=>c; set=>c=value;}
   public int Xn {get=>xn; set=>xn=value;}
   public int M {get=>m; set=>m=value;}
   //métodos
   public bool Validar (){
    int d=2^64;
    if ((xo>0) && (a>0) && (c>0)){
      if ((m>xo) && (m>a) && (m>c)){
          if (m<d)
             return true;
          else
            {
              Console.WriteLine("M no puede ser mayor que la base del sistema ");
              return false;            
             }
   
       }
      else{
             Console.WriteLine(" M no es mayor que xo,a,c");
              return false; 
         } 

      }
        else{
             Console.WriteLine("Todas las entradas de datos deben ser mayor que 0");
              return false;
           }
   }//fin validar 

   public void Generar(){
      int xnconstante=xo;
       if (Validar()){
             while (xn!=xnconstante){
                  xn=(a*xo+c)%m;
                  xo=xn;
                  //crecer el arreglo
                   arreSerie[arreSerie.Length-1] =decimal.Parse(xn.ToString())/m;
                   Array.Resize(ref arreSerie, arreSerie.Length +1);
                }
          
             }

      }//fin generar
    public void Mostrar(){
        Console.WriteLine("Generación de números pseudoaleatorios \n");
          for (int i=0; i<arreSerie.Length-1;i++){
              Console.WriteLine(arreSerie[i]);
            }
      }

   }//fin clase
}//fin namespace