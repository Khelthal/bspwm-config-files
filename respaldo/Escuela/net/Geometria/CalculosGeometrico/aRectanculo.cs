﻿using System;

namespace CalculosGeometrico {
    public class aRectangulo {
        private double altura;
        private double baseR;

        public aRectangulo():this(0.0, 0.0){}
        public aRectangulo(double altura, double b) {
            this.altura = altura;
            this.baseR = b;
        }

        public double AreaR() {
            return altura*baseR;
        }

        public void MuestraInfo() {
            Console.WriteLine(System.String.Format("{0}", altura));
            Console.WriteLine(System.String.Format("La base es {0}", baseR));
            Console.WriteLine(System.String.Format("Para los valores {0} y {1}, el área es {2}", altura, baseR, AreaR()));
        }
    }
}
