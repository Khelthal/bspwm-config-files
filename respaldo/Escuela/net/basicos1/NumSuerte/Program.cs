﻿using System;

namespace NumSuerte {
  class Program {
    public static void Main(string[] args) {
      Console.WriteLine("Escriba el dia de nacimiento");
      int dia = Convert.ToInt32(Console.ReadLine());
      Console.WriteLine("Escriba el mes de nacimiento");
      int mes = Convert.ToInt32(Console.ReadLine());
      Console.WriteLine("Escriba anio de nacimiento");
      int anio = Convert.ToInt32(Console.ReadLine());

      string suma = Convert.ToString(dia + mes + anio);
      int total = 0;

      for (int i = 0; i < suma.Length; i++) {
        total += (int) char.GetNumericValue(suma[i]);
      }

      Console.WriteLine(System.String.Format("El numero de la suerte es: {0}", total));
    }
  }
}
