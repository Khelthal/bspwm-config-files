﻿using System;

namespace Intervalo {
  class Programa {
    public static void Main(string[] args) {
      Console.WriteLine("Escriba el numero inferior del limite");
      int inf = Convert.ToInt32(Console.ReadLine());
      Console.WriteLine("Escriba el numero superior del limite");
      int sup = Convert.ToInt32(Console.ReadLine());
      Console.WriteLine("Escriba el numero");
      int num = Convert.ToInt32(Console.ReadLine());

      if (num >= inf && num <= sup) {
        Console.WriteLine(System.String.Format("El numero {0} esta en el intervalo [{1}, {2}]", num, inf, sup));
      } else {
        Console.WriteLine(System.String.Format("El numero {0} no esta en el intervalo [{1}, {2}]", num, inf, sup));
      }
    }
  }
}
