﻿using System;

namespace VelocidadLuz {
  public class Program {
    const long spd = 300000;
    public static void Main(string[] args) {
      Console.WriteLine("Escriba los segundos");
      int segundos = Convert.ToInt32(Console.ReadLine());
      Console.WriteLine(System.String.Format("La distancia recorrida en {0} segundos es: {1}", segundos, segundos*spd));
    }
  }
}
