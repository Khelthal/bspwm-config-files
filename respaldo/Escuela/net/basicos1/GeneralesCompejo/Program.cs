﻿using System;

namespace GeneralesCompejo {
 public class NumComplejo {
   private int real;
   private int imaginaria;
   private const string ima="i";

   public int Real{
     get {
       return real;
     }

     set {
       real = value;
     }
   }

   public int Imaginaria{
     get {
       return imaginaria;
     }

     set {
       imaginaria = value;
     }
   }

   public NumComplejo(int r, int i) {
     real = r;
     imaginaria = i;
   }

   public NumComplejo():this(0, 0) {}

   public override string ToString() {
     return System.String.Format("{0} + {1}{2}", real, ima);
   }

 }
}
