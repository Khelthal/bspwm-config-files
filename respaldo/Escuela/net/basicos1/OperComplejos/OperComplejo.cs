using System;
using GeneralesComplejo;
namespace OperacionesCom {
	public class PruebaComplejos {
		static NumComplejo n1 = new NumComplejo(8, -2);
		static NumComplejo n2 = new NumComplejo(-9, -12);
		static NumComplejo res;

		public static void Main(string[] args) {
			res = n1 / n2;
			Console.WriteLine(res.ToString());
			Console.ReadLine();
		}
	}
}