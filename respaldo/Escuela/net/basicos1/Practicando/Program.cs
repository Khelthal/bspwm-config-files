﻿using System;
using GeneralesComplejo;
namespace Practicando {
  public class Program {
    private static NumComplejo n1 = new NumComplejo(3, 8);
    private static NumComplejo n2 = new NumComplejo(1, 5);
    private static NumComplejo res;

    public static void Main(string[] args) {
      res = n1 * n2;
      Console.WriteLine(res);
    }
  }
}
