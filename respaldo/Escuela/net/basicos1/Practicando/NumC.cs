﻿using System;

namespace GeneralesComplejo {
 public class NumComplejo {
   private double real;
   private double imaginaria;
   private const string ima="i";

   public double Real{
     get {
       return real;
     }

     set {
       real = value;
     }
   }

   public double Imaginaria{
     get {
       return imaginaria;
     }

     set {
       imaginaria = value;
     }
   }

   public NumComplejo(double r, double i) {
     real = r;
     imaginaria = i;
   }

   public NumComplejo():this(0, 0) {}

   public override string ToString() {
     return System.String.Format("{0} + {1}{2}", real, imaginaria, ima);
   }

   public static NumComplejo operator +(NumComplejo n1, NumComplejo n2) {
   	NumComplejo nres = new NumComplejo();
   	nres.Real = n1.Real + n2.Real;
   	nres.Imaginaria = n1.Imaginaria + n2.Imaginaria;
   	return nres;
   }

   public static NumComplejo operator -(NumComplejo n1, NumComplejo n2) {
   	NumComplejo nres = new NumComplejo();
   	nres.Real = n1.Real - n2.Real;
   	nres.Imaginaria = n1.Imaginaria - n2.Imaginaria;
   	return nres;
   }

   public static NumComplejo operator *(NumComplejo n1, NumComplejo n2) {
   	NumComplejo nres = new NumComplejo();
   	nres.Real = n1.Real * n2.Real - n1.Imaginaria * n2.Imaginaria;
   	nres.Imaginaria = n1.Real * n2.Imaginaria + n2.Real * n1.Imaginaria;
   	return nres;
   }

   public static NumComplejo operator /(NumComplejo n1, NumComplejo n2) {
   	NumComplejo nres = new NumComplejo();
   	double divisor = n2.Real * n2.Real + n2.Imaginaria * n2.Imaginaria;
   	nres.Real = n1.Real * n2.Real + n1.Imaginaria * n2.Imaginaria;
   	nres.Real = nres.Real/divisor;
   	nres.Imaginaria = n2.Real * n1.Imaginaria - n1.Real * n2.Imaginaria;
   	nres.Imaginaria = nres.Imaginaria/divisor;
   	// -9 * -2 - 8 * -12
   	return nres;
   }
 }
}
