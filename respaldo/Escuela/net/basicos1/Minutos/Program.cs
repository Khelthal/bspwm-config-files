﻿using System;

namespace Minutos {
  class Program {
    public static void Main(string[] args) {
      Console.WriteLine("Escriba el numero de segundos");
      int segundos = Convert.ToInt32(Console.ReadLine());
      int sobrante = segundos%60;

      if (sobrante == 0) {
        Console.WriteLine(System.String.Format("Faltan 0 segundos para completar {0} minutos", segundos/60));
      } else {
        Console.WriteLine(System.String.Format("Faltan {0} segundos para completar {1} minutos", 60-sobrante, (segundos/60)+1));
      }
    }
  }
}
