Definiciones básicas
- Dato : Característica de un elemento dentro de un objeto de estudio
- Información : Conjunto de datos ordenados y/o procesados
- Campo : Lugar donde se almacena un dato
- Registro : Conjunto de campos relacionados con un elemento dentro de un objeto de estudio
- Tabla : Conjunto de registros relacionados con un elemento dentro de un objeto de estudio
- Base de Datos : Conjunto de tablas relacionadas entre si dentro de un objeto de estudio con el proposito de almacenar de forma ordenada y estructurada los datos
                  concernientes al objeto de estudio
- Sistema Manejador de Base de Datos : Es un conjunto de rutinas o subprogramas en el cual cada una de ellas tiene una tarea específica a realizar y que nos permite
                                       definir, construir y manipular una base de datos
- Esquema : Es la estructura que conforma la base de datos
- Instancia : Son los datos que se encuentran en la base de datos en un momento dado

Objetivos de los sistemas de base de datos
- No redundancia : Evita que se almacenen datos repetidos, excepto los que se relacionan
- Consistencia : Garantiza que un dato que se tenga que repetir conserve su valor
- Integridad : Los valores que estan permitidos para un campo
- Seguridad : Evita que usuarios no autorizados accedan a los datos
- Concurrencia : Permite que varios usuarios puedan acceder simultáneamente a los mismos datos
- Disponibilidad : Garantiza que los datos estén siempre disponibles para los usuarios
- Transacciones : Verifica que un conjunto de operaciones agrupadas se ejecuten correctamente

Niveles de base de datos
- Nivel físico : Almacenamiento físico
- Nivel conceptual (Aquí trabajamos nosotros)
- Nivel de Visión: Describe una parte de la base de datos, depende del usuario y su relación que tenga con el sistema de base de datos


Modelo: Es una representación de la realidad que contiene las características generales de algo que se va a realizar

Modelo de datos
Existen 3 tipos:
- Modelos Lógicos Basados en Objetos (Describir en los niveles conceptual y de visión)
- Modelos Lógicos Basados en Registros (Describir en los niveles conceptual y físico)
- Modelos Físicos de Datos

Modelo Entidad Relación
Tipos de entidades:
- Tangibles
- Intangibles

Modelo Relacional
- Tuplas (renglones y columnas)
- Llave primaria
- Instancia
- Esquema

Independencia de los datos
- Independencia física de los datos
- Independencia lógica de los datos

Lenguage de la Definición de los Datos (DDL)
- Se utiliza para especificar el esquema de una Base de Datos
- Da como resultado un conjunto de datos que son almacenados en el Diccionario de Datos

Diccionario de Datos : Es un conjunto de tablas que contiene metadatos. Se consulta antes de leer o modificar datos de la Base de Datos

Lenguage de la Manipulación de los Datos (DML) : Inserción, Modificación, Eliminación y Recuperación
- Se utiliza para manipular los datos de una Base de Datos
- Tiene como finalidad mostrar al usuario final los datos de una forma clara y sencilla
- El Lenguaje de Consulta se encarga de procesar la recuperación de datos.

Sistema Manejador de Base de Datos
- Es la porción más importante del software de un sistema de base de datos.
Funciones principales:
- Crear y organizar la Base de Datos
- Manejar losd atos de acuerdo a las peticiones de los usuarios
- Registrar el uso de las bases de datos
- Interacción con el manejador de archivos (A través de las sentencias en DML)
- Respaldo y recuperación
- Control de concurrencia
- Seguridad e integridad

Administrador de la Base de Datos
- Define el esquema
- Modifica el esquema y la organización física
- Concede autorización para el acceso a los datos
- Brinda mantenimiento rutinario

Usuarios de la Base de Datos : Toda persona que tiene algún tipo de contacto con el sistema de base de datos
Se clasifican en:
- Usuarios normales (Interactúan a través de un programa de aplicación)
- Programadores de Aplicación : Tienen un amplio dominio de DML, capaces de generar módulos o utilerías para manipular los datos
- Usuarios sofisticados : Interactúan con el Sistepa sin programas escritos, lo hacen a través de instrucciones en DML
- Usuarios especializados : Escriben aplicaciones de base de datos especializadas que no se refieren precisamente al manejo de los datos tradicional

Estructura General del Sistema
- Interface entre el sistema de base de datos y el sistema operativo
- Gestor de archivos
- Manejador de base de datos : Interfaz entre los datos y los programas de aplicación
- Procesador de consultas : Traduce las proposiciones en lenguajes de consulta a instrucciones de bajo nivel
- Compilador de DDL : Convierte las proposiciones DDL en un conjunto de tablas que contienen metadatos, las cuales se almacenan en un diccionario de datos
- Archivo de datos : En él se encuentran almacenados físicamente los datos
- Diccionario de datos : Contiene los datos referentes a la estructura de la base de datos
- Indices : Permiten un rápido acceso a registros que contienen valores específicos
