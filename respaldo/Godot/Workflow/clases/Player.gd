extends KinematicBody2D
class_name Player

var velocity = Vector2()
var spd = 4
onready var anim = $AnimatedSprite

func _physics_process(delta):
	velocity.x = ceil(Input.get_action_strength("RIGHT")-Input.get_action_strength("LEFT"))*spd
	anim.flip_h = (sign(velocity.x)==-1) if velocity.x != 0 else anim.flip_h
	
	if place_meeting(0, 1):
		velocity.y = 0.75
		if Input.is_action_just_pressed("JUMP"):
			velocity.y = -6
	else:
		if place_meeting(sign(velocity.x), 0):
			velocity.y = min(velocity.y+0.25, 2)
			if Input.is_action_just_pressed("JUMP"):
				velocity.y = -6
		else:
			velocity.y = min(velocity.y+0.25, 6)
	
	if Input.is_action_just_released("JUMP") and velocity.y < 0:
		velocity.y = 0
	
	for i in range(abs(velocity.x)):
		if !place_meeting(sign(velocity.x), 0):
			position.x += sign(velocity.x)
			if !place_meeting(0, 1) and place_meeting(0, 2):
				position.y += 1
		elif !place_meeting(sign(velocity.x), -1):
			position.x += sign(velocity.x)
			position.y -= 1
		else:
			break
	
	for i in range(abs(velocity.y)):
		if !place_meeting(0, sign(velocity.y)):
			position.y += sign(velocity.y)
		else:
			velocity.y = 0.75
			break
	
	if !place_meeting(0, 1):
		if velocity.y < 0:
			anim.set_animation("saltando")
		else:
			anim.set_animation("cayendo")
	else:
		if velocity.x == 0:
			anim.set_animation("parado")
		else:
			anim.set_animation("corriendo")

func place_meeting(x : int, y : int):
	return move_and_collide(Vector2(x, y), true, true, true)
