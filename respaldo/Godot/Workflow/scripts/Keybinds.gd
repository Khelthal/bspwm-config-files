extends MarginContainer

var can_change_key = false
var action_string
enum ACTIONS {DASH, JUMP, LEFT, RIGHT}

func _ready():
	$GridContainer/HBoxCont_DASH/Button.connect("pressed", self, "b_change_key_DASH")
	$GridContainer/HBoxCont_JUMP/Button.connect("pressed", self, "b_change_key_JUMP")
	$GridContainer/HBoxCont_LEFT/Button.connect("pressed", self, "b_change_key_LEFT")
	$GridContainer/HBoxCont_RIGHT/Button.connect("pressed", self, "b_change_key_RIGHT")
	_set_keys()

func _input(event):
	if can_change_key:
		if event is InputEventKey:
			_change_key(event)
		if event is InputEventJoypadButton:
			_change_key(event)
  
func _set_keys():
	for j in ACTIONS:
		get_node("GridContainer/HBoxCont_" + str(j) + "/Button").set_pressed(false)
		if !InputMap.get_action_list(j).empty():
			get_node("GridContainer/HBoxCont_" + str(j) + "/Button").set_text(InputMap.get_action_list(j)[0].as_text())
		else:
			get_node("GridContainer/HBoxCont_" + str(j) + "/Button").set_text("No Button!")

func _change_key(new_key : InputEvent):
	can_change_key = false
	var eventos : Array
	eventos = InputMap.get_action_list(action_string)
	InputMap.action_erase_events(action_string)
	for i in eventos:
		if i.get_class() == new_key.get_class():
			continue
		InputMap.action_add_event(action_string, i)
	
	for i in ACTIONS:
		if InputMap.action_has_event(i, new_key):
			InputMap.action_erase_event(i, new_key)
	
	InputMap.action_add_event(action_string, new_key)
	
	_set_keys()

func _mark_button(string : String):
	can_change_key = true
	action_string = string
	
	for j in ACTIONS:
		if j != string:
			get_node("GridContainer/HBoxCont_" + str(j) + "/Button").set_pressed(false)

func b_change_key_DASH():
	_mark_button("DASH")

func b_change_key_JUMP():
	_mark_button("JUMP")

func b_change_key_LEFT():
	_mark_button("LEFT")

func b_change_key_RIGHT():
	_mark_button("RIGHT")
