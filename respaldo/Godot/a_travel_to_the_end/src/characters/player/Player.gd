extends KinematicBody2D
class_name Player

var velocity : Vector2 = Vector2()
var spd : int = 3
var jspd : int = 6
var rebote : int = 0
onready var anim : AnimatedSprite = $AnimatedSprite

func _physics_process(_delta):
	var pared = false
	if rebote:
		rebote -= 1
	else:
		velocity.x = ceil(Input.get_action_strength("RIGHT")-Input.get_action_strength("LEFT"))*spd
	anim.set_flip_h((sign(velocity.x)==-1) if velocity.x != 0 else anim.flip_h)
	
	if place_meeting(0, 1):
		velocity.y = 0.75
		if Input.is_action_just_pressed("JUMP"):
			velocity.y = -jspd
	else:
		if place_meeting(int(sign(velocity.x)), 0):
			velocity.y = min(velocity.y+0.25, 2)
			if velocity.y > 0:
				pared = true
			if Input.is_action_just_pressed("JUMP"):
				velocity.y = -jspd
				velocity.x = -3 * sign(velocity.x)
				rebote = 4
		else:
			velocity.y = min(velocity.y+0.25, 6)
	
	if Input.is_action_just_released("JUMP") and velocity.y < 0:
		rebote = 0
		velocity.y = 0
	
	for _i in range(abs(velocity.x)):
		if !place_meeting(int(sign(velocity.x)), 0):
			position.x += sign(velocity.x)
			if !place_meeting(0, 1) and place_meeting(0, 2):
				position.y += 1
		elif !place_meeting(int(sign(velocity.x)), -1):
			position.x += sign(velocity.x)
			position.y -= 1
		else:
			break
	
	for _i in range(abs(velocity.y)):
		if !place_meeting(0, int(sign(velocity.y))):
			position.y += sign(velocity.y)
		else:
			velocity.y = 0.75
			break
	
	if !place_meeting(0, 1):
		if pared:
			anim.change_animation("slide")
		else:
			anim.change_animation("jump")
	else:
		if velocity.x == 0:
			anim.change_animation("stand")
		else:
			anim.change_animation("run")

func place_meeting(x : int, y : int):
	return move_and_collide(Vector2(x, y), true, true, true)
