extends AnimatedSprite

func _ready():
	for i in get_children():
		i.self_modulate = self_modulate

func _on_AnimatedSprite_frame_changed() -> void:
	for i in get_children():
		cambiar_frame(frame, i)

func cambiar_frame(frame : int, anim : AnimatedSprite) -> void:
	anim.set_frame(frame)
	for i in anim.get_children():
		cambiar_frame(frame, i)

func change_animation(new_anim : String, anim : AnimatedSprite = self) -> void:
	anim.set_animation(new_anim)
	for i in anim.get_children():
		change_animation(new_anim, i)

func set_flip_h(new_flip : bool, anim : AnimatedSprite = self) -> void:
	anim.flip_h = new_flip
	for i in anim.get_children():
		set_flip_h(new_flip, i)
