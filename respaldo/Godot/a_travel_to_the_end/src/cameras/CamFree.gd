extends Camera2D
class_name CamFree

onready var p : KinematicBody2D = get_tree().root.get_child(0).get_node("Player")
onready var mapa : Array = get_tree().root.get_child(0).get_node("CameraHandler").mapa

func _init(pos : Vector2 = Vector2(0, 0)):
	position = pos

func _ready():
	current = true

func _physics_process(_delta):
	position = p.position
	var posx : int = int(clamp(floor(p.position.x/464), 0, mapa.size()-1))
	var posy : int = int(clamp(floor(p.position.y/256), 0, mapa[0].size()-1))
	
	for i in mapa[posx][posy]:
		match i:
			"U":
				if position.y < posy*256 + 128:
					position.y = posy*256 + 128
			"D":
				if position.y > posy*256 + 128:
					position.y = posy*256 + 128
			"L":
				if position.x < posx*464 + 232:
					position.x = posx*464 + 232
			"R":
				if position.x > posx*464 + 232:
					position.x = posx*464 + 232
