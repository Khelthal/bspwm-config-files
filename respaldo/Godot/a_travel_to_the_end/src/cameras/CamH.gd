extends Camera2D
class_name CamH

onready var p : KinematicBody2D = get_tree().root.get_child(0).get_node("Player")
var centro : int
var sentido : String
var activo : bool = false
var destino : int
var dentro : bool = true

func _init(pos : int, dir : String):
	centro = pos
	sentido = dir

func _ready():
	var posy : int = int(floor(p.position.y/256))
	position.y = posy*256 + 128
	position.x = centro
	position.x += 232 if p.position.x > centro else -232
	current = true

func _physics_process(_delta):
	if activo:
		if destino == 1:
			if position.x + 14 < centro + 232:
				position.x += 14
			else:
				position.x = centro + 232
		else:
			if position.x - 14 > centro - 232:
				position.x -= 14
			else:
				position.x = centro - 232
	
		if abs(centro - position.x) == 232 and !dentro:
			if p.position.x > centro:
				get_parent().add_child(cambiar_camara(sentido.split("_to_")[1]))
				queue_free()
			if p.position.x < centro:
				get_parent().add_child(cambiar_camara(sentido.split("_to_")[0]))
				queue_free()

func activar() -> void:
	activo = true
	destino = 1 if p.position.x > centro else -1

func cambiar_camara(new_camara : String) -> Camera2D:
	match new_camara:
		"libre":
			return CamFree.new(position)
		"tile":
			return CamTile.new(position)
	return null

func get_class() -> String:
	return "CameraAdjustment"
