extends Camera2D
class_name CamV

onready var p : KinematicBody2D = get_tree().root.get_child(0).get_node("Player")
var centro : int
var sentido : String
var activo : bool = false
var destino : int
var dentro : bool = true

func _init(pos : int, dir : String):
	centro = pos
	sentido = dir

func _ready():
	var posx : int = int(floor(p.position.x/464))
	position.x = posx*464 + 232
	position.y = centro
	position.y += -128 if p.position.y < centro else 128
	current = true

func _physics_process(_delta):
	if activo:
		if destino == 1:
			if position.y + 14 < centro + 128:
				position.y += 14
			else:
				position.y = centro + 128
		else:
			if position.y - 14 > centro - 128:
				position.y -= 14
			else:
				position.y = centro - 128
	
		if abs(centro - position.y) == 128 and !dentro:
			if p.position.y > centro:
				get_parent().add_child(cambiar_camara(sentido.split("_to_")[1]))
				queue_free()
			if p.position.y < centro:
				get_parent().add_child(cambiar_camara(sentido.split("_to_")[0]))
				queue_free()

func activar() -> void:
	activo = true
	destino = 1 if p.position.y > centro else -1

func cambiar_camara(new_camara : String) -> Camera2D:
	match new_camara:
		"libre":
			return CamFree.new(position)
		"tile":
			return CamTile.new(position)
	return null

func get_class() -> String:
	return "CameraAdjustment"
