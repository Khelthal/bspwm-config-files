extends Camera2D
class_name CamTile

onready var p : KinematicBody2D = get_tree().root.get_child(0).get_node("Player")
onready var mapa : Array = get_tree().root.get_child(0).get_node("CameraHandler").mapa
var mode : bool
var desplazando : bool
var spd = 8

func _init(pos : Vector2 = Vector2(0, 0)):
	position = pos

func _ready():
	current = true

func _physics_process(_delta):
	var pos : Vector2 = p.position
	var posx : int = int(clamp(floor(p.position.x/464), 0, mapa.size()-1))
	var posy : int = int(clamp(floor(p.position.y/256), 0, mapa[0].size()-1))
	var ntext : String = mapa[posx][posy]
	
	if ntext != "":
		var new_mode = verify_mode(ntext)
		if new_mode != "":
			if new_mode == "H" or new_mode == "V":
				change_mode(new_mode == "H")
	
	if desplazando:
		if mode:
			if position.y != posy*256+128:
				for _i in range(spd):
					position.y += int(sign(posy*256+128-position.y))
			elif position.x != pos.x:
				for _i in range(spd):
					position.x += int(sign(pos.x-position.x))
					if comprobar_llegada(ntext):
						desplazando = false
						break
		else:
			if position.x != posx*464+232:
				for _i in range(spd):
					position.x += int(sign(posx*464+232-position.x))
			elif position.y != pos.y:
				for _i in range(spd):
					position.y += int(sign(pos.y-position.y))
					if comprobar_llegada(ntext):
						desplazando = false
						break
	else:
		position.x = int(pos.x) if mode else int(posx*464+232)
		position.y = int(pos.y) if !mode else int(posy*256+128)
	
	if !desplazando:
		for letra in ntext:
			match letra:
				"U":
					if position.y < posy*256 + 128:
						position.y = posy*256 + 128
				"D":
					if position.y > posy*256 + 128:
						position.y = posy*256 + 128
				"L":
					if position.x < posx*464 + 232:
						position.x = posx*464 + 232
				"R":
					if position.x > posx*464 + 232:
						position.x = posx*464 + 232

func verify_mode(select : String) -> String:
	if select[0] == "H" or select[0] == "V":
		return select[0]
	
	if select[0].is_valid_integer():
		var pos : Vector2 = p.position
		var posx = int(pos.x/464) if pos.x >= 0 else int(pos.x/464)-1
		var posy = int(pos.y/256) if pos.y >= 0 else int(pos.y/256)-1
		var x1 = select.substr(0, 2).to_int() 
		var x2 = select.substr(2, 2).to_int()
		var y1 = select.substr(4, 2).to_int()
		var y2 = select.substr(6, 2).to_int()
		var dir = select.substr(8, 1)
		
		if pos.x >= posx*464 + 16*x1 and pos.x <= posx*464 + 16*x2:
			if pos.y >= posy*256 + 16*y1 and pos.y <= posy*256 + 16*y2:
				return dir
		return "V" if dir == "H" else "H"
	return ""

func change_mode(new_mode : bool) -> void:
	if new_mode != mode:
		mode = new_mode
		desplazando = true

func comprobar_llegada(select : String) -> bool:
	var pos : Vector2 = p.position
	var posx = int(pos.x/464) if pos.x >= 0 else int(pos.x/464)-1
	var posy = int(pos.y/256) if pos.y >= 0 else int(pos.y/256)-1
	if mode:
		if position.x == pos.x:
			return true
		for letra in select:
			match letra:
				"R":
					if position.x > posx*464 + 232:
						return true
				"L":
					if position.x < posx*464 + 232:
						return true
	else:
		if position.y == pos.y:
			return true

		for letra in select:
			match letra:
				"U":
					if position.y < posy*256 + 128:
						return true
				"D":
					if position.y > posy*256 + 128:
						return true
	return false
