extends Area2D

signal cambio_camara

export(String, "", "libre_to_tile", "tile_to_libre", "libre_to_libre", "tile_to_tile") var sentido

func _ready():
	connect("body_entered", self, "_on_Area2D_body_entered")
	connect("body_exited", get_tree().root.get_child(0).get_node("CameraHandler"), "_on_transition_cam_v_body_exited")
	connect("cambio_camara", get_tree().root.get_child(0).get_node("CameraHandler"), "_on_transition_cam_v_body_entered")

func _on_Area2D_body_entered(body):
	if body is Player:
		emit_signal("cambio_camara", position.y, sentido)
