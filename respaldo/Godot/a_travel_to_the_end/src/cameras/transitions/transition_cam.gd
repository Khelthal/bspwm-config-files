extends Area2D

signal change_camera

export(String, "", "libre_to_tile", "tile_to_libre") var sentido

func _ready():
	connect("body_exited", self, "_on_Area2D_body_exited")
	connect("change_camera", get_tree().root.get_child(0).get_node("CameraHandler"), "_on_transition_cam_change_camera")

func _on_Area2D_body_exited(body):
	if body is Player:
		var select = 0 if position.x > body.position.x else 1
		emit_signal("change_camera", sentido.split("_to_")[select])
