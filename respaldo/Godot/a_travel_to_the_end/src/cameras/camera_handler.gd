extends Node

var size : Vector2
var mapa : Array

func _init():
	size = Vector2(4, 3)
	for _i in size.x:
		var columna : Array = []
		for _j in size.y:
			columna.append("")
		mapa.append(columna)
	
	mapa[0][0] = "L"
	mapa[1][0] = "R"
	mapa[2][0] = "00140016VLU"
	mapa[3][0] = "HDR"
	mapa[0][1] = "LD"
	mapa[1][1] = "D"
	mapa[2][1] = "00120006VDR"
	mapa[3][1] = "RUDL"
	mapa[1][2] = "LRU"

func _on_transition_cam_h_body_entered(pos, sentido):
	if get_child(0).get_class() != "CameraAdjustment":
		get_child(0).queue_free()
		add_child(CamH.new(pos, sentido))
	else:
		get_child(0).dentro = true

func _on_transition_cam_h_body_exited(body):
	if body is Player:
		if get_child(0) is CamH:
			get_child(0).activar()
			get_child(0).dentro = false

func _on_transition_cam_v_body_entered(pos, sentido):
	if get_child(0).get_class() != "CameraAdjustment":
		get_child(0).queue_free()
		add_child(CamV.new(pos, sentido))
	else:
		get_child(0).dentro = true

func _on_transition_cam_v_body_exited(body):
	if body is Player:
		if get_child(0) is CamV:
			get_child(0).activar()
			get_child(0).dentro = false

func _on_transition_cam_change_camera(sentido : String):
	var pos : Vector2 = get_child(0).position
	get_child(0).queue_free()
	add_child(change_camera(sentido, pos))

func change_camera(new_camera : String, pos : Vector2):
	match new_camera:
		"libre":
			return CamFree.new(pos)
		"tile":
			return CamTile.new(pos)
	return null
