type
  ArregloEnteros = array[10, int]

var arreglo = [1, 5, 4, 2, 8, 10, 25, 11, 0, 14]

for i in countup(0, 9):
  for j in countdown(i, 1):
    if (arreglo[j-1] > arreglo[j]):
      swap(arreglo[j-1], arreglo[j])
    else:
      break

for i in countup(0, 9):
  echo arreglo[i]
