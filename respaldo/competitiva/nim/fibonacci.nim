type
  ArregloFibonacci = array[40, int]

var memoria : ArregloFibonacci

proc fibonacci(n: int): int =
  if n < 2:
    result = n
  else:
    let izq : int = fibonacci(n-1)
    let der : int = fibonacci(n-2)
    result = izq + der
