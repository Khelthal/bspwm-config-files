#include <iostream>
using namespace std;

int main() {
  int n;
  string end;
  cin >> n;

  while(n--) {
    end = (n>0)? " " : "!\n";
    cout << "Ho" << end;
  }
}
