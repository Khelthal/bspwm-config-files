#include <bits/stdc++.h>
#include <cstdio>
#include <set>
using namespace std;

int main() {
  set<string> s1;
  string line;

  while(cin >> line) {
    s1.insert(line);
  }
  printf("%i\n", (int) s1.size());
}
