class Nodo {
  private:
    int data_ = 0;

  public:
    Nodo(int data) {
      data_ = data;
    }

    int data() const {
      return data_;
    }
};

int main() {
  Nodo * n = new Nodo(4);
  Nodo b {5};
}
