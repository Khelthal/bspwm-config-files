#include <algorithm>
#include <bits/stdc++.h>
#include <cstdio>
#include <tuple>
using namespace std;

int numPalabras;
string palabras[10001];
int valores[10001];

bool subString(string mayor, string menor) {
  int tamMayor, tamMenor;
  bool correcto;
  tamMayor = mayor.length() - 1;
  tamMenor = menor.length() - 1;
  if (tamMayor == tamMenor)
    return false;

  for (int i = 0; i <= tamMayor; i++) {
    if (tamMayor - i < tamMenor) {
      break;
    }

    if (mayor[i] == menor[0]) {
      correcto = true;
      for (int j = 0; j <= tamMenor; j++) {
        if (mayor[i + j] != menor[j]) {
          correcto = false;
          break;
        }
      }
      if (correcto) {
        return true;
      }
    }
  }

  return false;
}

void swap(string *a, string *b) {
  string t = *a;
  *a = *b;
  *b = t;
}

int partition(string arr[], int low, int high) {
  int pivot = arr[high].length();
  int i = (low - 1);

  for (int j = low; j <= high - 1; j++) {
    if (arr[j].length() <= pivot) {
      i++;
      swap(&arr[i], &arr[j]);
    }
  }
  swap(&arr[i + 1], &arr[high]);
  return (i + 1);
}

void quickSort(string arr[], int low, int high) {
  if (low < high) {
    int pi = partition(arr, low, high);
    quickSort(arr, low, pi - 1);
    quickSort(arr, pi + 1, high);
  }
}

int obtenerRastro(int indice) {
  int maximo = 1;
  bool found = false;
  for (int i = indice - 1; i >= 0; i--) {
    if (maximo > i + 1)
      break;
    if (valores[i] <= maximo) {
      continue;
    }
    if (subString(palabras[indice], palabras[i])) {
      maximo = max(maximo, valores[i]);
      found = true;
    }
  }
  if (found)
    valores[indice] = maximo + 1;

  return valores[indice];
}

int analizarStrings() {
  int maximo = 1;
  for (int i = 1; i < numPalabras; i++) {
    maximo = max(maximo, obtenerRastro(i));
  }
  return maximo;
}

int main() {
  cin >> numPalabras;
  while (numPalabras != 0) {
    for (int i = 0; i < numPalabras; i++) {
      cin >> palabras[i];
      valores[i] = 1;
    }
    quickSort(palabras, 0, numPalabras - 1);
    printf("%i\n", analizarStrings());
    cin >> numPalabras;
  }
}
