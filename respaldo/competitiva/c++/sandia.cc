#include <bits/stdc++.h>
using namespace std;

int main() {
  int w;
  cin >> w;
  if (w <= 3) {
    cout << "NO" << endl;
  } else {
    if (w%2 == 0) {
      cout << "SI" << endl;
    } else {
      cout << "NO" << endl;
    }
  }
}
