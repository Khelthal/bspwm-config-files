#include <bits/stdc++.h>
#include <iostream>
using namespace std;

int main() {
  int cases, year;
  cin >> cases;
  
  while(cases--) {
    cin >> year;
    year = 2015 - year;

    if (year > 0) {
      cout << year << " D.C." << endl;
    } else {
      year--;
      year = -year;
      cout << year << " A.C." << endl;
    }
  }
}
