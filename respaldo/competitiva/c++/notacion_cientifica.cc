#include <bits/stdc++.h>
#include <string>
using namespace std;

string round(string numero, int decimales) {
  if (numero.length() < decimales + 2) {
    for (int i = 0; i <= decimales + 3 - numero.length(); i++) {
      numero += '0';
    }
  } else if (numero.length() > decimales + 2) {
    for (int i = numero.length() - 1; i > decimales + 1; i--) {
      if (numero[i] == '5') {
        if (numero[i - 1] % 2 == 1) {
          numero[i - 1]++;
        }
      } else if (numero[i] > '5') {
        numero[i - 1]++;
      }
      if (numero[i - 1] > '9') {
        numero[i - 1] = '0';
        for (int j = i - 2; j > 1; j--) {
          numero[j]++;
          if (numero[j] <= '9')
            break;
          else {
            numero[j] = '0';
            if (j == 2) {
              numero[0]++;
            }
          }
        }
        if (numero[0] > '9') {
          numero[0] = '0';
          numero = "1" + numero;
        }
      }
    }
  }
  return numero.substr(0, decimales+2);
}

string notacion(string numero, int exponencial = 0) {
  string corregido;
  char signo;
  signo = (numero[0] == '-') ? '-' : '+';
  if (numero[0] == '-' || numero[0] == '+') {
    numero = numero.substr(1, numero.length());
  }

  int i = 0;

  if (numero[0] == '0') {
    while ((numero[i] < '1' || numero[i] > '9') && i < numero.length()) {
      if (numero[i] == '0') {
        exponencial--;
      }
      i++;
    }
    if (i == numero.length()) {
      exponencial = 0;
      corregido = numero;
      if (corregido == "0") {
        corregido += ".0";
      }
    } else {
      corregido = numero[i];
      corregido += '.';
      corregido += numero.substr(i + 1, numero.length());
    }
  } else {
    while (i < numero.length() - 1 && numero[i + 1] != '.') {
      exponencial++;
      i++;
    }
    corregido = numero[0];
    corregido += '.';
    for (int i = 1; i < numero.length(); i++) {
      if (numero[i] != '.') {
        corregido += numero[i];
      }
    }
  }
  if (corregido[corregido.length()-1] == '.')
    corregido += "0";
  string expsing = (exponencial >= 0)? "+" : "-";
  string expstring = to_string(exponencial);
  if (expsing[0] == '-') {
    expstring = expstring.substr(1, expstring.length());
  }
  if (expstring.length() == 1) {
    expstring = "0" + expstring;
  }
  return signo + round(corregido, 4) + "E" + expsing + expstring;
}

int main() {
  string num;
  cin >> num;

  cout << notacion(num) << endl;
}
