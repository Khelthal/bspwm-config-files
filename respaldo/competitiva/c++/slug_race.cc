#include <algorithm>
#include <iostream>
using namespace std;

int Level(int x) {
  if (x < 10) {
    return 1;
  }
  else if (x < 20) {
    return 2;
  }
  else {
    return 3;
  }
}

int main() {
  int slugs, slug, maxlvl;

  while (cin >> slugs) {
    maxlvl = 0;
    while (slugs--) {
      cin >> slug;
      maxlvl = max(maxlvl, Level(slug));
    }
    cout << maxlvl << endl;
  }
}
