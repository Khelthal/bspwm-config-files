#include <bits/stdc++.h>
using namespace std;

int main() {
  int n, m, p;
  cin >> n >> m;
  int arreglo[m];
  for (int i = 0; i < m; i++) {
    arreglo[i] = 0;
  }
  while (n--) {
    cin >> p;
    arreglo[p-1]++;
  }

  for (int i = 0; i < m; i++) {
    cout << i+1 << ": " << arreglo[i] << endl;
  }
}
