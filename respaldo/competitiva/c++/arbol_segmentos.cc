#include <algorithm>
#include <bits/stdc++.h>
using namespace std;

int sign(int num) {
  if (num == 0) {
    return 0;
  }
  if (num > 0) {
    return 1;
  } else {
    return -1;
  }
}

void print_sign(int num) {
  if (num == 0) {
    cout << "0";
    return;
  }
  if (num > 0) {
    cout << "+";
  } else {
    cout << "-";
  }
}

class Arbol {
private:
  int arbol[262145];
  int size = 0;

  int h_izq(int nodo) { return (nodo << 1); }

  int h_der(int nodo) { return (nodo << 1) + 1; }

  int padre(int num) { return (num >> 1); }

  void build(int arreglo[], int izq, int der, int nodo) {
    if (izq == der) {
      arbol[nodo] = sign(arreglo[izq - 1]);
    } else {
      int hijo_izq = h_izq(nodo);
      int hijo_der = h_der(nodo);
      int mitad = (izq + der) >> 1;
      build(arreglo, izq, mitad, hijo_izq);
      build(arreglo, mitad + 1, der, hijo_der);
      arbol[nodo] = sign(arbol[hijo_izq] * arbol[hijo_der]);
    }
  }

  void update(int izq, int der, int pos, int val, int nodo) {
    if (pos < izq || pos > der) {
      return;
    }
    if (izq == pos && der == pos) {
      arbol[nodo] = sign(val);
    }
    else {
      int hijo_izq = h_izq(nodo);
      int hijo_der = h_der(nodo);
      int mitad = (izq + der) >> 1;
      if (pos <= mitad) {
        update(izq, mitad, pos, val, hijo_izq);
      } else {
        update(mitad + 1, der, pos, val, hijo_der);
      }
      arbol[nodo] = sign(arbol[hijo_izq] * arbol[hijo_der]);
    }
  }

  int search(int izq, int der, int mizq, int mder, int nodo) {
    if ( (!(mizq >= izq and mizq <= der)) || (!(mder >= izq && mder <= der)) ) {
      return 1;
    }
    else if (mizq == izq and mder == der) {
      return arbol[nodo];
    }
    else {
      int hijo_izq = h_izq(nodo);
      int hijo_der = h_der(nodo);
      int mitad = (izq + der) >> 1;
      return search(izq, mitad, mizq, min(mitad, mder), hijo_izq) * search(mitad +1, der, max(mitad+1, mizq), mder, hijo_der);
    }
  }

public:
  void build(int arreglo[], int tam) {
    size = tam;
    build(arreglo, 1, size, 1);
  }

  void update(int pos, int val) {
    update(1, size, pos, val, 1);
  }

  int search(int mizq, int mder) {
    return search(1, size, mizq, mder, 1);
  }
};

int main() {
  int tam, instrucciones, arreglo[100000], a, b;
  char orden;
  Arbol arbol;

  while (cin >> tam >> instrucciones) {
    for (int i = 0; i < tam; i++)
      cin >> arreglo[i];

    arbol.build(arreglo, tam);
    for (int i = 0; i < instrucciones; i++) {
      cin >> orden >> a >> b;
      if (orden == 'C') {
        arbol.update(a, b);
      } else {
        print_sign(arbol.search(a, b));
      }
    }
    cout << endl;
  }
}
