#include <bits/stdc++.h>
using namespace std;

int main() {
  int n, matriz[101][101];
  string end;

  while (cin >> n) {
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        matriz[i][j] = 0;
      }
    }

    for (int i = 0; i < n; i++) {
      matriz[i][i] = 2;
      matriz[i][n-1-i] = 3;
    }

    for (int i = n/3; i < n - n/3; i++) {
      for (int j = n/3; j < n - n/3; j++) {
        matriz[i][j] = 1;
      }
    }

    matriz[n/2][n/2] = 4;

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        end = (j == n-1)? "\n" : "";
        cout << matriz[i][j] << end;
      }
    }
    cout << endl;
  }
}
