#include <cmath>
#include <iomanip>
#include <iostream>
using namespace std;
float calcularCargos(float);
int main() {
  float horas1, horas2, horas3, cargo1 = 0, cargo2 = 0, cargo3 = 0, total = 0;
  cout << "-----------ESTACIONAMIENTO-------------" << endl;
  cout << "Ingrese la cantidad de horas del primer auto: ";
  cin >> horas1;
  cargo1 = calcularCargos(horas1);
  cout << "Ingrese la cantidad de horas del segundo auto: ";
  cin >> horas2;
  cargo2 = calcularCargos(horas2);
  cout << "Ingrese la cantidad de horas del tercer auto: ";
  cin >> horas3;
  cargo3 = calcularCargos(horas3);
  total = cargo1 + cargo2 + cargo3;
  cout << endl << endl;
  cout << "Auto" << setw(15) << "Horas" << setw(15) << "Cargo" << endl;
  cout << "1" << setw(17) << horas1 << setw(15) << cargo1 << endl;
  cout << "2" << setw(17) << horas2 << setw(15) << cargo2 << endl;
  cout << "3" << setw(17) << horas3 << setw(15) << cargo3 << endl;
  cout << "Total" << setw(13) << horas1 + horas2 + horas3 << setw(15) << total
       << endl;
  return 0;
}
float calcularCargos(float hor) {
  float cargo = 0.0, residu = 0.0;
  if (hor <= 3) {
    cargo = 2.00;
  } else {
    if (hor > 3 && hor < 24) {
      residu = hor - 3;
      cargo = 2.00 + (ceil(residu) * 0.5);
    }
    if (hor >= 24) {
      cargo = 10.00;
    }
  }
  return cargo;
}
