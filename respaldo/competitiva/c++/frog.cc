#include <bits/stdc++.h>
#include <complex>
using namespace std;

int main() {
  int jump_h, jumps;
  bool victory = true;
  cin >> jump_h >> jumps;
  int arreglo[jumps];

  for (int i = 0; i < jumps; i++) {
    cin >> arreglo[i];
  }

  for (int i = 1; i < jumps; i++) {
    if ((int) abs(arreglo[i] - arreglo[i-1]) > jump_h) {
      victory = false;
      break;
    }
  }

  if (victory) {
    cout << "YOU WIN" << endl;
  } else {
    cout << "GAME OVER" << endl;
  }
}
