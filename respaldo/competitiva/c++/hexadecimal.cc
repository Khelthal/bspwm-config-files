#include <bits/stdc++.h>
#include <string>
using namespace std;

int main() {
  int pila[10], pos=0, x, entero, residuo;
  string hexa;
  cin >> x;

  while (x/16 != 0) {
    pila[pos] = x%16;
    pos++;
    x = x/16;
  }
  pila[pos] = x%16;
  pos++;

  for (int i = pos-1; i >= 0; i--) {
    if (pila[i] < 10) {
      hexa += to_string(pila[i]);
    } else {
      hexa += ('7' + pila[i]);
    }
  }
  cout << hexa << endl;
}
