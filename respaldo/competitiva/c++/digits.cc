#include <bits/stdc++.h>
#include <cmath>
#include <string>
using namespace std;

int main() {
  unsigned long long n, m;
  int count;

  cin >> count;
  for (int i = 0; i < count; i++) {
    cin >> n >> m;

    n = (unsigned long long) (pow(n, m));
    string x = (to_string(n));
    cout << x.length() << endl;
  }
}
