#include <bits/stdc++.h>
#include <cmath>
#include <math.h>
#include <string>
using namespace std;

int main() {
  int n, T, t, matriz[15][15];
  string max, end;

  cin >> n;
  while (n != 0) {
    for (int i = 0; i < n; i++) {
      matriz[i][0] = 1 * pow(2, i);
      for (int j = 1; j < n; j++) {
        matriz[i][j] = matriz[i][j - 1] * 2;
      }
    }

    max = to_string(matriz[n - 1][n - 1]);
    T = max.length();

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        end = (j == n - 1) ? "\n" : " ";
        max = to_string(matriz[i][j]);
        while (max.length() < T) {
          max = " " + max;
        } 
        cout << max << end;
      }
    }
    cout << endl;
    cin >> n;
  }
}
