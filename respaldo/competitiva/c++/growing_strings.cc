#include <algorithm>
#include <bits/stdc++.h>
#include <fstream>
#include <tuple>
using namespace std;

ifstream ein("input.txt");
ofstream eout("output.txt");

int numPalabras;
string palabras[10001];
int valores[10001];

bool subString(string mayor, string menor) {
  int tamMayor, tamMenor;
  bool correcto;
  tamMayor = mayor.length() - 1;
  tamMenor = menor.length() - 1;

  for (int i = 0; i <= tamMayor; i++) {
    if (tamMayor - i < tamMenor) {
      break;
    }

    if (mayor[i] == menor[0]) {
      correcto = true;
      for (int j = 0; j <= tamMenor; j++) {
        if (mayor[i + j] != menor[j]) {
          correcto = false;
          break;
        }
      }
      if (correcto) {
        return true;
      }
    }
  }

  return false;
}

void sort() {
  for (int i = 1; i < numPalabras; i++) {
    for (int j = i; j > 0; j--) {
      if (palabras[j].length() < palabras[j - 1].length()) {
        swap(palabras[j], palabras[j - 1]);
      } else {
        break;
      }
    }
  }
}

int analisisRecursivo(int acumulado, int posicion) {
  int obtenido, acumuladoInicial;
  acumuladoInicial = acumulado;
  for (int i = posicion; i < numPalabras; i++) {
    if (subString(palabras[i], palabras[posicion - 1])) {
      obtenido = analisisRecursivo(acumuladoInicial + 1, i + 1);
      if (obtenido > acumulado) {
        acumulado = obtenido;
      }
    }
  }

  return acumulado;
}

int analizarStrings() {
  int maximo;
  bool repetido;
  for (int i = 0; i < numPalabras; i++) {
    repetido = false;
    if (i == 0) {
      maximo = analisisRecursivo(1, i + 1);
    } else {
      maximo = max(maximo, analisisRecursivo(1, i + 1));
    }
  }
  return maximo;
}

int main() {
  ein >> numPalabras;
  while (numPalabras != 0) {
    for (int i = 0; i < numPalabras; i++) {
      ein >> palabras[i];
    }
    sort();
    cout << analizarStrings() << endl;
    ein >> numPalabras;
  }
}
