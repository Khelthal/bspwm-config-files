#ifndef lista_enlazada_h
#define lista_enlazada_h
#include "nodo.h"

template <class T> class ListaEnlazada {
private:
  T data_;
  Node<T> *primero_;
  Node<T> *ultimo_;

public:
  ListaEnlazada<T>();

  bool Vacia();

  void Imprimir();

  void Agregar(T data);

  T * Eliminar();
};
#include "lista_enlazada.cc"

#endif
