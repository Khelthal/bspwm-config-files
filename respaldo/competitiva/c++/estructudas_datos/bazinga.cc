#include <bits/stdc++.h>
#include <map>
using namespace std;

void imprimirVictoria(int n) {
  cout << "Caso #" << n << ": Bazinga!" << endl;
}

void imprimirEmpate(int n) {
  cout << "Caso #" << n << ": De novo!" << endl;
}

void imprimirDerrota(int n) {
  cout << "Caso #" << n << ": Raj trapaceou!" << endl;
}

int main() {
  string sheldon, raj;
  int n;


  cin >> n;

  for (int i = 1; i <= n; i++) {
    cin >> sheldon >> raj;

    if (sheldon == raj) {
      imprimirEmpate(i);
      continue;
    }

    if (sheldon == "pedra") {
      if(raj == "lagarto" || raj == "tesoura")
        imprimirVictoria(i);
      if(raj == "papel" || raj == "Spock")
        imprimirDerrota(i);
    }
    else if (sheldon == "papel") {
      if(raj == "pedra" || raj == "Spock")
        imprimirVictoria(i);
      if(raj == "lagarto" || raj == "tesoura")
        imprimirDerrota(i);
    }
    else if (sheldon == "tesoura") {
      if(raj == "papel" || raj == "lagarto")
        imprimirVictoria(i);
      if(raj == "Spock" || raj == "pedra")
        imprimirDerrota(i);
    }
    else if (sheldon == "lagarto") {
      if(raj == "Spock" || raj == "papel")
        imprimirVictoria(i);
      if(raj == "tesoura" || raj == "pedra")
        imprimirDerrota(i);
    }
    else if (sheldon == "Spock") {
      if(raj == "pedra" || raj == "tesoura")
        imprimirVictoria(i);
      if(raj == "lagarto" || raj == "papel")
        imprimirDerrota(i);
    }
  }
}
