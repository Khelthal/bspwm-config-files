#include <bits/stdc++.h>
using namespace std;

int main() {
  int a, b, c;

  cin >> a >> b >> c;

  if (b < a && c >= b) {
    cout << ":)" << endl;
  }
  else if (b > a && c <= b) {
    cout << ":(" << endl;
  }
  else if (b > a && c > b) {
    if (c - b < b - a) {
      cout << ":(" << endl;
    }
    else {
      cout << ":)" << endl;
    }
  }
  else if (b < a && c < b) {
    if (c - b > b-a) {
      cout << ":)" << endl;
    }
    else {
      cout << ":(" << endl;
    }
  }
  else if (a == b) {
    if (c > b) {
      cout << ":)" << endl;
    } 
    else {
      cout << ":(" << endl;
    }
  }
}
