#include "nodo.h"

template<class T> Node<T>::Node(T data) {
  data_ = data;
}

template<class T> T Node<T>::data() {
  return data_;
}

template<class T> void Node<T>::set_data(T data) {
  data_ = data;
}

template<class T> Node<T> * Node<T>::next() {
  return next_;
}

template<class T> void Node<T>::set_next(Node<T> *next) {
  next_ = next;
}
