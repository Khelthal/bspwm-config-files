#include <bits/stdc++.h>
using namespace std;

const string message = "LIFE IS NOT A PROBLEM TO BE SOLVED";

int main() {
  int n;

  cin >> n;

  for (int i = 0; i < n; i++) {
    cout << message[i];
  }
  cout << endl;
}
