#include <bits/stdc++.h>
using namespace std;

int main() {
  int n, newtons;
  string nombre;

  cin >> n;

  while (n--) {
    cin >> nombre >> newtons;

    if (nombre == "Thor") {
      cout << "Y" << endl;
    } else {
      cout << "N" << endl;
    }
  }
}
