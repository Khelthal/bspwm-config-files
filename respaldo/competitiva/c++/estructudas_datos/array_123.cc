#include <bits/stdc++.h>
using namespace std;

int main() {
  short matriz[70][70];
  short n;
  string end;

  while (cin >> n) {
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        matriz[i][j] = 3;
      }
    }

    for (int i = 0; i < n; i++) {
      matriz[i][i] = 1;
    }

    for (int i = 0; i < n; i++) {
      matriz[i][n-1-i] = 2;
    }

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        end = (j == n-1)? "\n" : "";
        cout << matriz[i][j] << end;
      }
    }
  }
}
