#include "lista_enlazada.h"
#include "nodo.h"
#include <bits/stdc++.h>
#include <iostream>

using namespace std;

template<class T> ListaEnlazada<T>::ListaEnlazada() {
  primero_ = NULL;
  ultimo_ = NULL;
}

template<class T> bool ListaEnlazada<T>::Vacia() {
  return primero_ == NULL;
}

template<class T> void ListaEnlazada<T>::Imprimir() {
  Node<T> *n = NULL;
  n = primero_;
  while (n != NULL) {
    cout << n->data() << " -> ";
    n = n->next();
  }
  cout << "null" << endl;
}

template<class T> void ListaEnlazada<T>::Agregar(T data) {
  Node<T> *nuevo_nodo = NULL;
  nuevo_nodo = new Node<T>(data);
  if (Vacia()) {
    primero_ = nuevo_nodo;
    ultimo_ = nuevo_nodo;
  } else {
    ultimo_->set_next(nuevo_nodo);
    ultimo_ = nuevo_nodo;
  }
}

template<class T> T * ListaEnlazada<T>::Eliminar() {
  if (Vacia()) {
    return NULL;
  }

  Node<T> *anterior;
  Node<T> *encontrado;
  encontrado = primero_;
  anterior = primero_;

  while (encontrado->next() != NULL) {
    anterior = encontrado;
    encontrado = encontrado->next();
  }

  T recuperado = primero_->data();
  T * eliminar = &recuperado;
  if (primero_ == ultimo_) {
    primero_ = NULL;
    ultimo_ = NULL;
  } else {
    anterior->set_next(NULL);
    ultimo_ = anterior;
  }
  return eliminar;
}
