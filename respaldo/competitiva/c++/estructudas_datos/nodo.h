#ifndef nodo_h
#define nodo_h

template <class T> class Node {
private:
  T data_;
  Node<T> *next_;

public:
  Node<T>(T data);

  T data();

  void set_data(T data);

  Node<T> * next();

  void set_next(Node<T> *next);
};
#include "nodo.cc"
#endif
