#include <bits/stdc++.h>
using namespace std;

int main() {
  double compra = 1318.50;


  while (cin >> compra) {
    if (compra < 500) {
      cout << compra << endl;
    }
    else if (compra <= 1000) {
      cout << compra-(compra/100.00 * 5.00) << endl;
    }
    else if (compra <= 7000) {
      cout << compra-(compra/100.00 * 11.00) << endl;
    }
    else if (compra <= 15000) {
      cout << compra-(compra/100.00 * 18.00) << endl;
    }
    else {
      cout << compra-(compra/100.00 * 25.00) << endl;
    }
  }
}
