#include <bits/stdc++.h>
using namespace std;

int main() {
  int matriz[100][100], n;
  bool hilera[100];

  cin >> n;

  while (n != 0) {
    for (int i = 0; i < n; i++) {
      matriz[0][i] = i + 1;
      hilera[i] = true;
    }

    for (int i = 1; i < n; i++) {
      for (int j = 0; j < n; j++) {
        if (matriz[i - 1][j] == 1) {
          hilera[j] = false;
        }

        if (hilera[j]) {
          matriz[i][j] = matriz[i - 1][j] - 1;
        } else {
          matriz[i][j] = matriz[i - 1][j] + 1;
        }
      }
    }

    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        string linea = (j == n - 1) ? "\n" : " ";
        if (j == 0) {
          if (matriz[i][j] < 10) {
            cout << "  ";
          } else if (matriz[i][j] < 100) {
            cout << " ";
          }
        }
        if (linea != "\n") {
          if (matriz[i][j + 1] < 10) {
            linea += "  ";
          } else if (matriz[i][j+1] < 100) {
            linea += " ";
          }
        }
        cout << matriz[i][j] << linea;
      }
    }
    cout << endl;
    cin >> n;
  }
}
