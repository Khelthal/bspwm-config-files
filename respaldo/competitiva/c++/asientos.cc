#include <bits/stdc++.h>
using namespace std;

int main() {
  int r, s, boletos;
  cin >> r >> s >> boletos;
  if (boletos <= r*s) {
    cout << boletos << " " << 0;
  } else {
    cout << r*s << " " << boletos-r*s;
  }
}
