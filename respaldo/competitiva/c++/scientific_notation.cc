#include <bits/stdc++.h>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
using namespace std;


string Round(string numero, short tam) {
  int rango = 2;
  for (int i = numero.length(); i > 1 + tam; i--) {
    if (numero[i] == '5') {
      int pos;
      pos = (int) numero[i-1];
      if (pos%2 != 0)
        numero[i-1] = numero[i-1]+1;
    } else if (numero[i] > '5') {
      numero[i-1] = numero[i-1]+1;
    }
    for (int j = i-1; i > 1; j--) {
      if (numero[j] == ':') {
        numero[j] = '0';
        if (j == 2) {
          numero[j-2] = numero[j-2]+1;
        } else {
          numero[j-1] = numero[j-1]+1;
        }
      } else {
        break;
      }
    }
  }
  if (numero[0] == ':') {
    numero[0] = '0';
    numero = '1' + numero;
    rango++;
  }
  string retorno = "";
  for (int i = 0; i < rango+tam; i++) {
    retorno += numero[i];
  }
  return retorno;
}

string Scientific(string numero, short tam, int exp) {
  string cadena = "";
  int i = 0;
  bool sumando = true, punto = false, positivo;
  if (numero[0] == '-') {
    positivo = numero[1] != '0';
  } else {
    positivo = numero[0] != '0';
  }
  
  while (!(numero[i] >= '1' && numero[i] <= '9')) {
    if (numero[i] == '0') {
      exp--;
    }
    i++;
  }
  if (!positivo)
    exp++;
  for (; i < numero.length(); i++) {
    if (numero[i] == '.') {
      sumando = false;
      continue;
    }
    if (sumando && positivo)
      exp++;
    cadena += numero[i];
    if (!punto) {
      punto = true;
      cadena += '.';
    }
  }
  int decimales = cadena.length() - 2;
  while (decimales < tam) {
    cadena += '0';
    decimales++;
  }
  string result = Round(cadena, tam);
  if (result.length() == 2+tam) {
    char signo = (numero[0] == '-')? '-' : '+';
    string exponencial;
    char signoexp = (exp < 0)? '-' : '+';
    if (exp < 0) {
      exp*=-1;
    }
    if (exp < 10) {
      exponencial = "0" + to_string(exp);
    } else {
      exponencial = to_string(exp);
    }
    exponencial = signoexp + exponencial;
    return signo + result + "E"+ exponencial;
  } else {
    if (numero[0] == '-') {
      result = "-" + result;
    }
    return Scientific(result, tam, exp-1);
  }
}



int main() {
  short digitos = 4;
  string numero;
  cin >> numero;

  numero = Scientific(numero, digitos, -1);
  cout << numero << endl;
}
