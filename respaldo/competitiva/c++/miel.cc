#include <bits/stdc++.h>
#include <cmath>
#include <iomanip>
using namespace std;

int main() {
  float V, D;
  float a, h;
  while (cin >> V >> D) {
    a = pow(D/2.0, 2) * 3.14;
    h = V/a;
    cout << fixed << setprecision(2) << "ALTURA = " << h << endl;
    cout << fixed << setprecision(2) << "AREA = " << a << endl;
  }
}
