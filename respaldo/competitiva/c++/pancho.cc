#include <bits/stdc++.h>
using namespace std;

int edificios[100000];

int main() {
  int casos, vistos=0, max=-1;
  cin >> casos;

  for (int i = 0; i < casos; i++) {
    cin >> edificios[i];
  }

  for (int i = casos-1; i >= 0; i--) {
    if (edificios[i] > max) {
      max = edificios[i];
      vistos++;
    }
  }
  cout << vistos << endl;
}
