#include <bits/stdc++.h>
using namespace std;

int main() {
  int cases, a, b;
  string na, nb, oa, ob, ganador;

  cin >> cases;

  while (cases--) {
    cin >> na >> oa >> nb >> ob >> a >> b;
    ganador = ((a+b) % 2 == 0) ? "PAR" : "IMPAR";
    if (oa == ganador) {
      cout << na << endl;
    } else {
      cout << nb << endl;
    }
  }
}
