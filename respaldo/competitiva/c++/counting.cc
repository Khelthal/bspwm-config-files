#include <algorithm>
#include <bits/stdc++.h>
using namespace std;

long sorted[100000];

int main() {
  long n ,a, mini=-1, maxi=-1;
  unsigned long long count;
  string end;
  cin >> n;
  for (long i = 0; i < n; i++) {
    cin >> a;
    if (mini == -1) {
      mini = a-1;
    } else {
      mini = min(mini, a-1);
    }
    maxi = max(maxi, a-1);
    sorted[a-1]++;
  }

  for (long i = mini; i <= maxi; i++) {
    for (long j = 0; j < sorted[i]; j++) {
      end = (i == maxi && j == sorted[i]-1)? "\n" : " ";
      cout << i+1 << end;
    }
  }

  for (long i = mini; i <= maxi; i++) {
    if (sorted[i] == 0)
      continue;
    count = 0;
    for (long j = i-1; j >= mini; j--) {
      count += sorted[j];
    }
    for (long j = 0; j < sorted[i]; j++) {
      end = (i == maxi && j == sorted[i]-1)? "\n" : " ";
      cout << count << end;
    }
  }
}
