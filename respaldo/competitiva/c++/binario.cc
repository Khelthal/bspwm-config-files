#include <bits/stdc++.h>
#include <cmath>
#include <math.h>
using namespace std;


int main() {
  unsigned int n, x, valor;
  bool encontrado;
  cin >> n;

  while (n--) {
    cin >> x;
    encontrado = false;
    for (int i = 0; i < 18; i++) {
      valor = pow(2, 17-i);
      if (valor <= x) {
        encontrado = true;
        cout << 1;
        x -= valor;
      } else {
        if (encontrado) {
          cout << 0;
        }
      }
    }
    cout << endl;
  }
}
