#include <array>
#include <bits/stdc++.h>
#include <cmath>
#include <iostream>
#include <math.h>
using namespace std;


int main() {
  int n;
  int a, b, c, d;
  bool bueno = false;

  cin >> a >> b >> c >> d;

  if (a+b > c && b + c > a && a + c > b) {
    bueno = true;
  }

  if (b + c > d && b + d > c && c + d > b) {
    bueno = true;
  }

  if (a + c > d && a + d > c && c + d > a) {
    bueno = true;
  }

  if (a + b > d && a + d > b && b + d > a) {
    bueno = true;
  }

  if (!bueno) {
    cout << "N" << endl;
  } else {
    cout << "S" << endl;
  }
}
