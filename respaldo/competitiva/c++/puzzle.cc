#include <bits/stdc++.h>
using namespace std;

int main() {
  string palabra, inversa;

  cin >> palabra;

  for (int i = palabra.length()-1; i >= 0; i--)
    inversa += palabra[i];

  cout << inversa << endl;
}
