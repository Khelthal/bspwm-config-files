#include <bits/stdc++.h>
using namespace std;

int main() {
  int n, m, izq, der, k;
  char mov;
  string end;

  cin >> n >> m >> k;
  izq = m;
  der = n - m;

  int pila_izq[n], pila_der[n];
  for (int i = 0; i < m; i++) {
    pila_izq[i] = i + 1;
  }
  for (int i = 0; i < der; i++) {
    pila_der[i] = n - i;
  }

  while (k--) {
    cin >> mov;
    if (mov == 'G') {
      cin >> mov;
      if (mov == 'I') {
        izq--;
        pila_der[der] = pila_izq[izq];
        der++;
      } else {
        der--;
        pila_izq[izq] = pila_der[der];
        izq++;
      }
    } else {
      cin >> mov;
      if (mov == 'I') {
        izq--;
      } else {
        der--;
      }
    }
  }
  cout << izq << endl;
  for (int i = 0; i < izq; i++) {
    end = (i == izq - 1) ? "\n" : " ";
    cout << pila_izq[i] << end;
  }
  cout << der << endl;
  for (int i = der - 1; i >= 0; i--) {
    end = (i == 0) ? "\n" : " ";
    cout << pila_der[i] << end;
  }
}
