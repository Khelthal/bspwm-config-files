#include <bits/stdc++.h>
#include <math.h>
using namespace std;

int main() {
  string line;
  int count=0, crows=3;

  while(crows != 0) {
    cin >> line;
    if (line == "caw") {
      crows--;
      cout << count << endl;
      count=0;
      cin >> line;
      continue;
    }

    for (int i = 0; i < 3; i++) {
      if (line[i] == '*') {
        count += pow(2, 2-i);
      }
    }
  }
}
