#include <bits/stdc++.h>
using namespace std;

int main() {
  int a, b, c;
  cin >> a >> b >> c;

  cout << "Frutos rojos" << endl;
  cout << "-----------------" << endl;
  cout << "Nombre  |Cantidad" << endl;
  cout << "--------+--------" << endl;
  cout << "Fresa   |" << a << endl;
  cout << "Mora    |" << b << endl;
  cout << "Cereza  |" << c << endl;
  cout << "Total   |" << a+b+c << endl;
  cout << "-----------------" << endl;
}
