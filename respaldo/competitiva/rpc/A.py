def calcularDistancia(x1, y1, x2, y2):
    distancia = (x2-x1)**2 + (y2-y1)**2
    distancia = distancia**(1/2)
    return distancia

datos = [int(x) for x in input().split()]
judges = datos[0]
tar = datos[1]
store = datos[2]

judgesList = []
tarList = []
storeList = []

for i in range(judges):
    judgesList.append([int(x) for x in input().split()])

for i in range(tar):
    tarList.append([int(x) for x in input().split()])

for i in range(store):
    storeList.append([int(x) for x in input().split()])

distancia = 0

for juez in judgesList:
    indiceMinimo = None
    distanciaMinima = None
    for i in range(len(tarList)):
        repositorio = tarList[i]
        if distanciaMinima == None:
            distanciaMinima = calcularDistancia(juez[0], juez[1], repositorio[0], repositorio[1])
            indiceMinimo = i
        elif (calcularDistancia(juez[0], juez[1], repositorio[0], repositorio[1]) < distanciaMinima):
            distanciaMinima = calcularDistancia(juez[0], juez[1], repositorio[0], repositorio[1])
            indiceMinimo = i
    if (indiceMinimo == None):
        break
    tarList.pop(indiceMinimo)
    distancia += distanciaMinima

for juez in judgesList:
    indiceMinimo = None
    distanciaMinima = None
    for i in range(len(storeList)):
        repositorio = storeList[i]
        if distanciaMinima == None:
            distanciaMinima = calcularDistancia(juez[0], juez[1], repositorio[0], repositorio[1])
            indiceMinimo = i
        elif (calcularDistancia(juez[0], juez[1], repositorio[0], repositorio[1]) < distanciaMinima):
            distanciaMinima = calcularDistancia(juez[0], juez[1], repositorio[0], repositorio[1])
            indiceMinimo = i
    if (indiceMinimo == None):
        break
    storeList.pop(indiceMinimo)
    distancia += distanciaMinima

print(distancia)
