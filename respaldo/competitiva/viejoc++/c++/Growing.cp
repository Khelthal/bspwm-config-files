#include <bits/stdc++.h>
using namespace std;

int main() {
  int x = 1;
  while (x != 0) {
    cin >> x;
    for (int i = 1; i <= x; i++) {
      string salto = i == x ? "\n" : " ";
      cout << i << salto;
    }
  }
}
