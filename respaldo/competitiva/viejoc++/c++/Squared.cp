#include <bits/stdc++.h>
#include <math.h>
using namespace std;

int main() {
  int n;
  cin >> n;

  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= 3; j++) {
      string salto = j == 3 ? "\n" : " ";
      cout << pow(i, j) << salto;
    }
  }
}
