#include <bits/stdc++.h>
using namespace std;

void swap(int *a, int *b) {
  int *tmp;
  tmp = a;
  a = b;
  b = tmp;
}

int main() {
  int a=10, b=20;
  swap(a, b);

  cout << a << endl;
  string x, y;
  x = "omg esto va a ser epico";
  y = x;
  y += " papus";
  cout << y << endl;
  cout << x << endl;
}
