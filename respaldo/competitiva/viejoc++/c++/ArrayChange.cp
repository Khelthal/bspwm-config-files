#include <bits/stdc++.h>
#include <math.h>
#include <tuple>
using namespace std;

int main() {
  int izq, der, a, b, arreglo[20];

  for (int i = 0; i < 20; i++) {
    cin >> arreglo[i];
  }

  for (int i = 0; i < 10; i++) {
    izq = i;
    der = 19-i;
    swap(arreglo[izq], arreglo[der]);
  }

  for (int i = 0; i < 20; i++) {
    cout << "N[" << i << "] = " << arreglo[i] << endl;
  }
}

