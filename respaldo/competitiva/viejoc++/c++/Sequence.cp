#include <bits/stdc++.h>
#include <iostream>
using namespace std;

int main() {
  int x, y;
  cin >> x >> y;
  for (int i = 1; i <= y; i++) {
    string salto = (i%x == 0 || i == y) ? "\n" : " ";
    cout << i << salto;
  }
}
