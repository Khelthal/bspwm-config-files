#include <bits/stdc++.h>
#include <ios>
#include <iostream>
using namespace std;

int main() {
  int n = 0;
  double sum = 0, total = 0;

  while (n >= 0) {
    cin >> n;
    if (n >= 0) {
      sum += n;
      total++;
    }
  }

  cout << fixed;
  cout << setprecision(2) << sum / total << endl;
}
