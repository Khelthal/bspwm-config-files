#include <bits/stdc++.h>
#include <iostream>
using namespace std;

int main() {
  int a, b, sum = 0;
  cin >> a;
  b = -1;

  while (b <= 0) {
    cin >> b;
    for (int i = 0; i < b; i++) {
      sum += a+i;
    }
  }
  cout << sum << endl;
}
