#include <bits/stdc++.h>
#include <iostream>
using namespace std;

int main() {
  int cases, x, y, i, sum;
  cin >> cases;

  for (int j = 0; j < cases; j++) {
    cin >> x;
    cin >> y;
    sum = 0;

    i = 0;
    while (y > 0) {
      if ((x+i)%2 != 0) {
        sum += x+i;
        y--;
      }
      i++;
    }
    cout << sum << endl;
  }
}
