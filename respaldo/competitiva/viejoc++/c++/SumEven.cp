#include <bits/stdc++.h>
using namespace std;

int main() {
  int a, sum;

  cin >> a;

  while (a != 0) {
    sum = 0;
    if (a%2 != 0) {
      a++;
    }

    for (int i = 0; i < 5; i++) {
      sum += a + (i*2);
    }

    cout << sum << endl;
    cin >> a;
  }
}
