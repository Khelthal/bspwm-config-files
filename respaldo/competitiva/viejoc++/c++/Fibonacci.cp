#include <bits/stdc++.h>
using namespace std;

int main() {
  unsigned long long fibonacci[61];
  fibonacci[0] = 0;
  fibonacci[1] = 1;

  for (int i = 2; i < 61; i++) {
    fibonacci[i] = fibonacci[i-1] + fibonacci[i-2];
  }

  int cases, n;
  cin >> cases;

  for (int i = 0; i < cases; i++) {
    cin >> n;
    cout << "Fib(" << n << ") = " << fibonacci[n] << endl;
  }
}
