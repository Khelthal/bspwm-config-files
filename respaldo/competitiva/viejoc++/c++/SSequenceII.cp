#include <bits/stdc++.h>
#include <iomanip>
#include <math.h>
using namespace std;

int main() {
  double s=0;

  for (int i = 0; i <= 19; i++) {
    double a = 1+2*i;
    double d = pow(2, i);
    s += a/d;
  }

  cout << fixed << setprecision(2) << s << endl;
}

