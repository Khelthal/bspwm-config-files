#include <bits/stdc++.h>
#include <iostream>
#include <tuple>

using namespace std;

int main() {
  int a, b, suma=0;
  cin >> a >> b;
  if (a > b) {
    swap(a, b);
  }

  for (int i = a; i <= b; i++) {
    if (i % 13 != 0) {
      suma += i;
    }
  }
  cout << suma << endl;
}
