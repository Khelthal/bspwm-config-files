#include <bits/stdc++.h>
#include <cmath>
#include <math.h>
using namespace std;

bool esPrimo(int n) {
  if (n == 1) {
    return false;
  }
  if (n == 2) {
    return true;
  }
  int limite = ceil(sqrt(n));

  for (int i = 2; i <= limite; i++) {
    if (n%i == 0) {
      return false;
    }
  }

  return true;
}

int main() {
  int cases, n;
  cin >> cases;

  for (int i = 0; i < cases; i++) {
    cin >> n;
    if (esPrimo(n)) {
      cout << n << " eh primo" << endl;
    }
    else {
      cout << n << " nao eh primo" << endl;
    }
  }
}

