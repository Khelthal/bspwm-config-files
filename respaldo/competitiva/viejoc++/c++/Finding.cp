#include <bits/stdc++.h>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <string>
using namespace std;

void obtenerPalabras(string palabras[], char matriz[][1000], int filas, int columnas) {
  string linea;
  for (int i = 0; i < filas+columnas-1; i++) {
    linea = "";
    if (i < filas) {
      for (int j = 0; j <= i; j++) {
        linea += (char) tolower(matriz[i-j][j]);
      }
    }
    else {
      for (int j = i-filas+1; j < columnas; j++) {
        linea += (char) tolower(matriz[i-j][j]);
      }
    }
    palabras[i] = linea;
  }
}

bool subString(string mayor, string menor) {
  int tamMayor, tamMenor;
  bool correcto;
  tamMayor = mayor.length()-1;
  tamMenor = menor.length()-1;

  for (int i = 0; i <= tamMayor; i++) {
    if (tamMayor-i < tamMenor) {
      break;
    }

    if (mayor[i] == menor[0]) {
      correcto = true;
      for (int j = 0; j <= tamMenor; j++) {
        if (mayor[i+j] != menor[j]) {
          correcto = false;
          break;
        }
      }
      if (correcto) {
        return true;
      }
    }
  }

  return false;
}

string invertirPalabra(string palabra) {
  string palabraInvertida="";
  for (int i = palabra.length()-1; i >= 0; i--) {
    palabraInvertida += palabra[i];
  }
  return palabraInvertida;
}

int buscarPalabra(string palabra, string palabras[], int numPalabras) {
  int mitad = ceil(numPalabras/2);

  if (palabra.length() > mitad+1) {
    return -2;
  }

  int izq, der;
  izq = palabra.length()-1;
  der = mitad + (mitad-izq);

  for (int i = izq; i <= der; i++) {
    if (subString(palabras[i], palabra) || subString(palabras[i], invertirPalabra(palabra))) {
      if (i == mitad) {
        return 0;
      }
      if (i > mitad) {
        return -1;
      }
      else {
        return 1;
      }
    }
  }

  return -2;
}


int main() {
  int numPalabras, filas, columnas, encontrado;
  cin >> numPalabras >> filas >> columnas;
  string linea;

  string palabras[numPalabras];

  for (int i = 0; i < numPalabras; i++) {
    cin >> palabras[i];
  }

  char matriz[1000][1000];

  for (int i = 0; i < filas; i++) {
    cin >> linea;
    for (int j = 0; j < columnas; j++) {
      matriz[i][j] = linea[j];
    }
  }

  string generados[filas+columnas-1];
  obtenerPalabras(generados, matriz, filas, columnas);
  for (int i = 0; i < numPalabras; i++) {
    encontrado = buscarPalabra(palabras[i], generados, filas+columnas-1);

    if (encontrado == 1) {
      cout << "2 Palavra \"" << palabras[i] <<"\" acima da diagonal secundaria" << endl;
    }
    else if (encontrado == 0) {
      cout << "1 Palavra \"" << palabras[i] <<"\" na diagonal secundaria" << endl;
    }
    else if (encontrado == -1) {
      cout << "3 Palavra \"" << palabras[i] <<"\" abaixo da diagonal secundaria" << endl;
    }
    else {
      cout << "4 Palavra \"" << palabras[i] <<"\" inexistente" << endl;
    }
  }
}
