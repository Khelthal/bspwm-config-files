#include <bits/stdc++.h>
using namespace std;

class Nodo {
  protected:
    int info;
    Nodo *next;

  public:
    int get_info() {
      return info;
    }
};
