#include <bits/stdc++.h>
using namespace std;


int main() {
  int n, no, m, stations[100], pos = 0;
  cin >> n;

  while (n != 0) {
    if (n == 13) {
      cout << 1 << endl;
    } else {
      no = n;
      m = 2;
      while (true) {
        for (int i = 0; i < 100; i++) {
          stations[i] = i + 1;
        }
        n = no;
        pos = 0;

        while (n != 1) {
          for (int i = pos+1; i < n; i++) {
            stations[i-1] = stations[i];
          }
          n--;
          pos--;
          pos = (pos+m)%n;
        }
        if (stations[0] == 13) {
          break;
        }
        m++;
      }
      cout << m << endl;
    }
    cin >> n;
  }
}
