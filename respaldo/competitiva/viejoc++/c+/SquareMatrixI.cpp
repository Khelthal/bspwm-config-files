#include <bits/stdc++.h>
using namespace std;

int main() {
  short matriz[100][100], n;
  cin >> n;
  while (n != 0) {
    for (short i = 0; i < n; i++) {
      for (short j = 0; j < n; j++) {
        matriz[i][j] = 0;
      }
    }

    for (short x = 0; x <= (short) ceil(n/2); x++) {
      for (short i = x; i < n-x; i++) {
        for (short j = x; j < n-x; j++) {
          matriz[i][j]++;
        }
      }
    }

    for (short i = 0; i < n; i++) {
      for (short j = 0; j < n; j++) {
        string linea = (j == n-1)? "\n" : "  ";
        if (j == 0) {
          cout << "  ";
        }
        if (linea != "\n") {
          if (matriz[i][j+1] < 10) {
            linea += " ";
          }
        }
        cout << matriz[i][j] << linea;
      }
    }
    cout << endl;

    cin >> n;
  }
}
