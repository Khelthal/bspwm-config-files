#include <bits/stdc++.h>
#include <iomanip>
#include <ios>
using namespace std;

int main() {
  int x, pares[5], impares[5], paresC = 0, imparesC = 0;

  for (int i = 0; i < 15; i++) {
    cin >> x;
    if (x % 2 == 0) {
      if (paresC == 5) {
        for (int j = 0; j < 5; j++) {
          cout << "par[" << j << "] = " << pares[j] << endl;
        }
        paresC = 0;
      }
      pares[paresC] = x;
      paresC++;
    } else {
      if (imparesC == 5) {
        for (int j = 0; j < 5; j++) {
          cout << "impar[" << j << "] = " << impares[j] << endl;
        }
        imparesC = 0;
      }
      impares[imparesC] = x;
      imparesC++;
    }
  }
  for (int j = 0; j < imparesC; j++) {
    cout << "impar[" << j << "] = " << impares[j] << endl;
  }

  for (int j = 0; j < paresC; j++) {
    cout << "par[" << j << "] = " << pares[j] << endl;
  }
}
