#include <bits/stdc++.h>
#include <iomanip>
using namespace std;

int main() {
  double suma=0, leido;
  char operacion;

  cin >> operacion;

  for (int i = 0; i < 144; i++) {
    cin >> leido;
    if ((int)i/12 <= 5) {
      if (i%12 > 11-(int)i/12) {
        suma += leido;
      }
    } else {
      if (i%12 > (int)i/12) {
        suma += leido;
      }
    }
  }

  if (operacion == 'S') {
    cout << fixed << setprecision(1) << suma << endl;
  } else {
    cout << fixed << setprecision(1) << suma / 30 << endl;
  }
}
