#include <algorithm>
#include <bits/stdc++.h>
#include <iomanip>
#include <ios>
using namespace std;

int main() {
  int n, indice=0, mini, leido;
  cin >> n;

  cin >> mini;

  for (int i = 1; i < n; i++) {
    cin >> leido;
    if (leido < mini) {
      mini = leido;
      indice = i;
    }
  }

  cout << "Menor valor: " << mini << endl;
  cout << "Posicao: " << indice << endl;
}

