escalas : list[int] = [1000, 500, 100, 50, 10, 5, 1]
equivalente : list[str] = ["M", "D", "C", "L", "X", "V", "I"]

num = int(input())
romano = ""
pos : int = 0
potencia_diez = True
for escala in escalas:
    if num >= escala:
        while num >= escala:
            romano += equivalente[pos]
            num -= escala
    if num != 0:
        incremento = 2 if potencia_diez else 1
        if num >= escala - escalas[pos+incremento]:
            romano += equivalente[pos+incremento] + equivalente[pos]
            num -= (escala - escalas[pos+incremento])
    pos += 1
    potencia_diez = not potencia_diez
print(romano)
