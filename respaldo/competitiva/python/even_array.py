n = int(input())

for i in range(n):
    size = int(input())
    arreglo = [int(x) for x in input().split()]
    pares = 0
    impares = 0
    incorrectos = 0
    for x in range(len(arreglo)):
        if x % 2 != arreglo[x] % 2:
            incorrectos += 1
        if arreglo[x] % 2 == 0:
            pares += 1
        else:
            impares += 1
    if impares != size//2:
        print(-1)
    else:
        print(incorrectos//2)
