caso = 0
try:
    while True:
        caso += 1
        num = int(input())
        total = (num+1)*(num)//2 + 1
        print("Caso {}: {} numero{}".format(caso, total, "s" if num > 0 else ""))
        if num == 0:
            print(0)
        else:
            print(0, end=" ")
            for i in range(num+1):
                for j in range(i):
                    total -= 1
                    print(i, end="\n" if total == 1 else " ")
        print()

except EOFError:
    pass
