m1 = set()
m2 = set()
d = {}

try:
    while True:
        nombres = input().split()
        m1.add(nombres[0])
        m2.add(nombres[1])
        if nombres[0] not in d:
            d[nombres[0]] = 1
        else:
            d[nombres[0]] += 1

        if nombres[1] not in d:
            d[nombres[1]] = 1
        else:
            d[nombres[1]] += 1
except EOFError:
    nombres = []
    for i in (m1-m2):
        nombres.append(i)

    nombres.sort()
    print("HALL OF MURDERERS")

    for i in nombres:
        print(i, d[i])
