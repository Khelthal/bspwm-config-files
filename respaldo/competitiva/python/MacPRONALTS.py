d = {"1001" : 1.50, "1002" : 2.50, "1003" : 3.50, "1004" : 4.50, "1005" : 5.50}

n = int(input())
total = 0.0

for i in range(n):
    datos = input().split()
    total += d[datos[0]] * int(datos[1])
print(format(total, '.2f'))
