def R(s):
    reve = ""
    for i in range(len(s)-1, -1, -1):
        reve += s[i]
    return reve

casos = int(input())

for _ in range(casos):
    valores = [int(x) for x in input().split()]
    n = valores[0]
    k = valores[1]
    s = input()
    left = s[0:k]
    right = s[n-k:n]
    if (k > (n-1)/2):
        print("NO")
    elif k == 0 or left == R(right):
        print("YES")
    else:
        print("NO")
