score = [0, 0]
juegos = 0
empates = 0
while True:
    puntos = [int(x) for x in input().split()]
    juegos += 1

    if puntos[0] > puntos[1]:
        score[0] += 1
    elif puntos[1] > puntos[0]:
        score[1] += 1
    else:
        empates += 1

    respuesta = 0

    while respuesta != 1 and respuesta != 2:
        respuesta = int(input("Novo grenal (1-sim 2-nao)\n"))

    if respuesta == 2:
        break

print("{} grenais".format(juegos))
print("Inter:{}".format(score[0]))
print("Gremio:{}".format(score[1]))
print("Empates:{}".format(empates))
if score[0] == score[1]:
    print("Não houve vencedor")
else:
    print("{} venceu mais".format("Inter" if score[0] > score[1] else "Gremio"))
