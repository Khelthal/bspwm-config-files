def sign(num):
    if num == 0:
        return 0
    if num > 0:
        return 1
    else:
        return -1

def print_sign(num):
    if num == 0:
        print(0, end="")
        return
    if num > 0:
        print("+", end="")
    else:
        print("-", end="")

class Arbol:
    def __init__(self):
        self.arbol = [0 for _ in range(2**18+1)]
        self.size = 0

    def __hijo_izq(self, num):
        return (num << 1)

    def __hijo_der(self, num):
        return (num << 1) + 1

    def __padre(self, num):
        return (num >> 1)

    def build(self, arreglo):
        self.size = len(arreglo)
        self._build(arreglo, 1, self.size, 1)

    def _build(self, arreglo, izq, der, nodo):
        if izq == der:
            self.arbol[nodo] = sign(arreglo[izq-1])
        else:
            hijo_izq = self.__hijo_izq(nodo)
            hijo_der = self.__hijo_der(nodo)
            mitad = (izq + der) >> 1
            self._build(arreglo, izq, mitad, hijo_izq)
            self._build(arreglo, mitad+1, der, hijo_der)
            self.arbol[nodo] = sign(self.arbol[hijo_izq] * self.arbol[hijo_der])

    def update(self, pos, val):
        self._update(1, self.size, pos, val, 1)

    def _update(self, izq, der, pos, val, nodo):
        if pos < izq or pos > der:
            return

        if izq == pos and der == pos:
            self.arbol[nodo] = sign(val)
        else:
            mitad = (izq + der) >> 1
            hijo_izq = self.__hijo_izq(nodo)
            hijo_der = self.__hijo_der(nodo)
            if pos <= mitad:
                self._update(izq, mitad, pos, val, hijo_izq)
            else:
                self._update(mitad+1, der, pos, val, hijo_der)
            self.arbol[nodo] = sign(self.arbol[hijo_izq] * self.arbol[hijo_der])

    def search(self, mizq, mder):
        return self._search(1, self.size, mizq, mder, 1)

    def _search(self, izq, der, mizq, mder, nodo):
        if (not (mizq >= izq and mizq <= der)) or (not (mder >= izq and mder <= der)):
            return 1
        elif (mizq == izq and mder == der):
            return self.arbol[nodo]
        else:
            mitad = (izq + der) >> 1
            hijo_izq = self.__hijo_izq(nodo)
            hijo_der = self.__hijo_der(nodo)
            return self._search(izq, mitad, mizq, min(mitad, mder), hijo_izq) * self._search(mitad+1, der, max(mitad+1, mizq), mder, hijo_der)

    def imprimir(self):
        for i in range(1, 8):
            print(self.arbol[i], end=("\n" if i == 7 else " "))

arbol = Arbol()

try:
    while True:
        ordenes = [int(x) for x in input().split()]
        instrucciones = ordenes[1]
        arreglo = [int(x) for x in input().split()]
        arbol.build(arreglo)
        for i in range(instrucciones):
            orden = [x for x in input().split()]
            if orden[0] == "C":
                arbol.update(int(orden[1]), int(orden[2]))
            else:
                print_sign(arbol.search(int(orden[1]), int(orden[2])))
        print()
except EOFError:
    pass
