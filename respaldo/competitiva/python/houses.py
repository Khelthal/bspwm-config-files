from math import sqrt

while True:
    datos = [int(x) for x in input().split()]
    if (len(datos) == 1):
        break
    size = (datos[0]*datos[1] / datos[2])*100
    print(int(sqrt(size)))
