try:
    while True:
        fecha = [int(x) for x in input().split(":")]
        fecha[0] += 1
        print("Atraso maximo: ", end="")
        if fecha[0] < 8:
            print(0)
        elif fecha[0] == 8:
            print(fecha[1])
        else:
            print((fecha[0]-8)*60 + fecha[1])
except EOFError:
    pass
