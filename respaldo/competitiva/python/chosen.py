n = int(input())
maximo = -1
num_maximo = ""

for i in range(n):
    datos = input().split()
    if maximo == -1:
        num_maximo = datos[0]
        maximo = float(datos[1])
    else:
        if float(datos[1]) > maximo:
            num_maximo = datos[0]
            maximo = float(datos[1])

if maximo < 8:
    print("Minimum note not reached")
else:
    print(num_maximo)
