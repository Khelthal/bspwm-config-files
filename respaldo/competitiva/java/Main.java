import java.util.Scanner;

public class Main {
  public static String cambiarBaseNumero(int num, int nuevaBase) {
    if (num/nuevaBase != 0) {
      return cambiarBaseNumero(num/nuevaBase, nuevaBase) + numeroAString(num%nuevaBase);
    } else {
      return "" + numeroAString(num%nuevaBase);
    }
  }

  private static char numeroAString(int num) {
    String r = "";
    if (num < 10) {
      r += num + "";
      return r.charAt(0);
    } else {
      char c = '7';
      c += num;
      return c;
    }
  }

  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    int num = Integer.parseInt(input.nextLine());
    System.out.println(cambiarBaseNumero(num, 16));
    input.close();
  }
}
