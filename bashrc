#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias tree='tree -C'
alias input='cat > ~/Documentos/competitiva/input.txt'
alias ranger='. ranger'
#PS1="\[\e[33m\]\u\[\e[m\]>\[\e[32m\]\h\[\e[m\]\[\e[37m\] -> \[\e[m\]\[\e[35m\]\W\[\e[m\]: "
export PS1="\[\e[36m\]\u\[\e[m\]>\[\e[33m\]\h\[\e[m\] -> \[\e[35m\]\W\[\e[m\]: "
path="/usr/lib/nim/compiler"
export PATH=/home/elias/.nimble/bin:/home/elias/.local/bin:$PATH
